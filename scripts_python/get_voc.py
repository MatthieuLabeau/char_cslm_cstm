from __future__ import division
import os
from collections import defaultdict
from nltk.util import ngrams

def filter_vocab(vocab, threshold):
    return dict((k, v) for k, v in vocab.iteritems() if v >= threshold), sum(v for k, v in vocab.iteritems() if v < threshold)

def create_files(path_to_data_file, path_to_char_file, path_to_word_file, path_to_map_file, threshold_w = 0, threshold_c = 100, n=5):
    #Count words/characters
    word_count=defaultdict(int)
    char_count=defaultdict(int)
    with open(path_to_data_file, 'r') as data_file:
        for line in data_file:
            for c in line.strip(' ').strip('\t').strip('\n'):
                char_count[c.lower()] += 1
            for w in line.strip().split():
                word_count[w.lower()] += 1
    w_f, unk_count = filter_vocab(word_count, threshold_w)
    c_f, _ = filter_vocab(char_count, threshold_c)

    max_len = len(max(w_f.keys(), key=len)) + (n-1)

    #Create char file and vocab 
    c_voc = defaultdict(int)
    c_voc['UNK'] = 0
    c_voc['bow'] = 1
    c_voc['eow'] = 2

    with open(path_to_char_file,'w') as char_file:
        for i, (c, k) in enumerate(sorted(c_f.items(), key=lambda x:x[1], reverse=True)):
            char_file.write( c + '\t' + str(k) + '\n')
            c_voc[c] = i+3

    #Create in parallel word file and word -> char n-grams map file
    with open(path_to_word_file,'w') as word_file:
        with open(path_to_map_file,'w') as map_file:
            map_file.write(str(n) + '\n')
            map_file.write(str(max_len) + '\n')
            for i, (w, k) in enumerate(sorted(w_f.items(), key=lambda x:x[1], reverse=True)):
                word_file.write( w +'\t' + str(k) + '\n')
                c_n_grams = ngrams([str(c_voc['bow'])]*(n-1) + [str(c_voc.get(c, c_voc['UNK'])) for c in w] + [str(c_voc['eow'])]*(n-1), n)
                map_file.write( str(i+3) + ' ' + str(len(c_n_grams)) + ' ' + ' '.join([' '.join(c_n_gram) for c_n_gram in c_n_grams]) + '\n')
            if unk_count > 0:
                word_file.write( 'unk' + '\t' + str(unk_count) + '\n')

# Exemple
#data_path = "/people/labeau/CSLM_CSTM/scripts_python/files/"
#filename = "data"

data_path = "/people/labeau/pynn/github/NonlexNN/data/"
filename = "train.wrd" 

#data_path = "/vol/work2/labeau/Char_CSLM_CSTM/data/tiger/"
#filename = "all.wrd"

data_filepath = os.path.join(data_path, filename)

local_path = "/vol/work2/labeau/Char_CSLM_CSTM/data/tiger/"
char_filepath = os.path.join(local_path, 'char.voc.out')
word_filepath = os.path.join(local_path, 'word.voc.out')
map_filepath = os.path.join(local_path, 'word_char.map.out')

create_files(data_filepath, char_filepath, word_filepath, map_filepath, 5, 100, 5)
