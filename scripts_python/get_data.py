from __future__ import division
import os
from collections import defaultdict
from nltk.util import ngrams
import math
import numpy
import operator

def get_vocabulary(path_to_voc_file):
    voc = defaultdict(int)
    voc_count = defaultdict(int)
    i = 3
    with open(path_to_voc_file, 'r') as voc_file:
        for line in voc_file:
            l = line.strip().split()
            voc[(l[0])] = i
            voc_count[(l[0])] = int(l[1])
            i += 1
        if 'unk' in voc:
            voc['unk'] = 0
    return voc, voc_count

def process_data_file(path_to_data_file, path_to_input_voc_file, path_to_output_voc_file, path_to_process_file, n = 4, remove_unk = False, sample_NCE = False, l = 25):
    voc_input, _ = get_vocabulary(path_to_input_voc_file)
    voc_output, voc_count = get_vocabulary(path_to_output_voc_file)
    if sample_NCE:
        warray = numpy.asarray(voc_count.keys())
        parray = numpy.asarray(voc_count.values())
        K = sum(parray)
        parray = parray/K
        voc_count = dict([ (v, math.log(k/K)) for v, k in voc_count.items() ])
    with open(path_to_process_file,'w') as p_file:
        with open(path_to_data_file, 'r') as data_file:
            p_file.write( str(n) + '\n')
            if sample_NCE:
                p_file.write( str(l) + '\n')
            for line in data_file:
                # Condition to handle unknowns words
                if ((0 not in [voc_input.get(w.lower(), 0) for w in line.strip().split()]) or (not remove_unk)):                  
                    n_grams = ngrams(line.strip().split(), n)
                    for n_gram in n_grams:
                        n_gram_ind = [str(voc_input.get(w.lower(), 0)) for w in n_gram[:-1]]
                        n_gram_ind.append(str(voc_output.get((n_gram[-1]).lower(), voc_output['unk'])))
                        if sample_NCE:
                            noise = numpy.random.choice(a=warray,size=l,replace=False,p=parray)
                            p_file.write(' '.join(n_gram_ind) + ' ' + ' '.join([str(voc_output[w]) for w in noise]) + '|' + str(voc_count.get((n_gram[-1]).lower())) + ' ' + ' '.join([str(voc_count[w]) for w in noise]) + '\n')
                        else:    
                            p_file.write(' '.join(n_gram_ind) + '\n')
                        
                        

data_path = "/people/labeau/pynn/github/NonlexNN/data/"
filename = "train.wrd"
devel_filename = "devel.wrd"
test_filename = "test.wrd"

local_path = "/vol/work2/labeau/Char_CSLM_CSTM/data/tiger/"

path_to_data_file = os.path.join(data_path, filename)
path_to_test_file = os.path.join(data_path, test_filename)
path_to_devel_file = os.path.join(data_path, devel_filename)

path_to_input_voc_file = os.path.join(local_path, 'word.voc.all')
path_to_output_voc_file = os.path.join(local_path, 'word.voc.all')

path_to_train_process_file = os.path.join(local_path, 'train_data.processed.all.CharNCE')
path_to_test_process_file = os.path.join(local_path, 'test_data.processed.all')
path_to_devel_process_file = os.path.join(local_path, 'devel_data.processed.all')

#process_data_file(path_to_data_file, path_to_input_voc_file, path_to_input_voc_file, path_to_train_process_file, 4, False, True)                
process_data_file(path_to_test_file, path_to_input_voc_file, path_to_output_voc_file, path_to_test_process_file, 4, False)
process_data_file(path_to_devel_file, path_to_input_voc_file, path_to_output_voc_file, path_to_devel_process_file, 4,  False)
