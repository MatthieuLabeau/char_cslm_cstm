#include "ioFile.H"

int main(int argc, char *argv[]) {
	if (argc != 2) {
		cout << "dataFileString" << endl;
		return 0;
	}
	// data file name
	char* dataFileString = argv[1];

	ioFile dataIof;
	// for writing
	dataIof.takeWriteFile(dataFileString);
	int ngramNumber = 2;
	// write ngramNumber to file
	dataIof.writeInt(ngramNumber);
	int n = 4;
	// write n to file
	dataIof.writeInt(n);
	int k = 3;
	// write k to file
	dataIof.writeInt(k);

	// context 1
	int context1[3] = { 100000, 100001, 100002 };

	// output words, the first is positive, all the rest is negative
	int outWords1[4] = { 20000, 20001, 20001, 20001 };

	// context 2
	int context2[3] = { 100003, 100004, 100005 };

	// output words, the first is positive, all the rest is negative
	int outWords2[4] = { 20000, 20001, 20001, 20001 };

	// probability for all words
	float p = 0.0001;

	// write the first context
	dataIof.writeIntArray(context1, n - 1);

	for (int i = 0; i < k + 1; i++) {
		// write word
		dataIof.writeInt(outWords1[i]);
		// then its probability
		dataIof.writeFloat(p);
	}

	// write the first context
	dataIof.writeIntArray(context2, n - 1);

	for (int i = 0; i < k + 1; i++) {
		// write word
		dataIof.writeInt(outWords2[i]);
		// then its probability
		dataIof.writeFloat(p);
	}

	// finish writing
	dataIof.freeWriteFile();
}
