#include "mainTensor.H"
#include "text.H"

Strip(read, Int, int)

int main(int argc, char *argv[]) {
	if (argc != 3) {
		cerr << "dataTrainFile vocSize" << endl;
		exit(1);
	}
	// inputs
	char* dataTrainFile = argv[1];
	int vocSize = atoi(argv[2]);
	string type = OVN_NCE;

	floatTensor countWord(vocSize, 1);
	ioFile origTrainFile;
	origTrainFile.takeReadFile(dataTrainFile);
	int ngramNumber;
	origTrainFile.readInt(ngramNumber);
	int N;
	origTrainFile.readInt(N);
	int k;
	origTrainFile.readInt(k);

	int nbBlock = ngramNumber / NCE_RESAMPLING_NGRAM_NUMBER;
	int remainingNgramNumber = ngramNumber % NCE_RESAMPLING_NGRAM_NUMBER;
	int nceOutputSize = k + 1;
	intTensor readTensor(NCE_RESAMPLING_NGRAM_NUMBER, N + nceOutputSize - 1);
	floatTensor logPropTensor(NCE_RESAMPLING_NGRAM_NUMBER, nceOutputSize);
	int idBlock, idNgram, idPos, traited = 0, nextShow = 1;
	float percent = 0;
	for (idBlock = 0; idBlock < nbBlock; idBlock++) {
		readIntTrainFile(origTrainFile, readTensor, logPropTensor, N, type);
		for (idNgram = 0; idNgram < readTensor.size[0]; idNgram++) {
			for (idPos = N; idPos < N + nceOutputSize - 1; idPos++) {
				countWord(readTensor(idNgram, idPos)) += 1;
			}
		}
# if PRINT_DEBUG
		traited += NCE_RESAMPLING_NGRAM_NUMBER;
		percent = (float)traited / ngramNumber;
		if (percent > nextShow * CONSTPRINT) {
			cout << percent << " ... " << flush;
		}
		while (percent > nextShow * CONSTPRINT) {
			nextShow += 1;
		}
# endif
	}
	if (remainingNgramNumber != 0) {
		readTensor.resize(remainingNgramNumber, N + nceOutputSize - 1);
		logPropTensor.resize(remainingNgramNumber, nceOutputSize);
		readIntTrainFile(origTrainFile, readTensor, logPropTensor, N, type);
		for (idNgram = 0; idNgram < readTensor.size[0]; idNgram++) {
			for (idPos = N; idPos < N + nceOutputSize - 1; idPos++) {
				countWord(readTensor(idNgram, idPos)) += 1;
			}
		}
	}
	float sumFreq = 0;
	int idWord;
	for (idWord = 0 ; idWord < vocSize ; idWord ++) {
		sumFreq += countWord(idWord);
	}
	cout << "countWordGen::main sumFreq : " << sumFreq << endl;
	countWord.scal(1 / sumFreq);
	cout << "countWordGen::main countWord after normalization : " << endl;
	countWord.write();

	// finish
	origTrainFile.freeReadFile();
}
