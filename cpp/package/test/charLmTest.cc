#include "mainModule.H"


vector<string> * tokenize(string * s, const string SEP){
  //cout << "line:" << *s<< endl;
  size_t pos = s->find(SEP);
  size_t org = 0;
  vector<string> * parse = new vector<string>();
  while (pos != string::npos){
    parse->push_back(s->substr(org,pos-org));
    //cout << "extract " << parse->size() << " " <<  parse->back() << endl;
    org = pos+SEP.length(); 
    pos = s->find(SEP,org);
  }
  parse->push_back(s->substr(org,s->size()-org));
  //cout << "extract " << parse->size() << " " <<  parse->back() << endl;
  return parse;
}


int
main(int argc, char *argv[])
{
  ifstream trainData;
  ifstream testData;
  char* train = argv[1];
  char* test = argv[2];
  char* hmf = argv[3];
  float lr = atof(argv[4]);
  int charLm = atoi(argv[5]);

  //Parameters
  const int blockSize = 500;
  //const int inputVocSize = 85298;
  const int outputVocSize = 12058;
  const int charVocSize = 62;
  const int wordDim = 128;
  const int charDim = 32;
  const int hlSize = 256;
  //const float lr = 0.1;
  const char* up_h = "1_1";
  const float weightDecay = 0.1;

  const int epochs = 10;
  const int trainingExamples = 600701;
  const int testingExamples = 139629;
 
  //Model Creation
  //Getting Data information
  trainData.open(train);
  if(! trainData){
    cerr << "ERROR - cannot open file Train" << train <<endl;
    exit(1);
  }
  string line;
  const string SSPACE = " ";

  int N;
  //First line
  std::getline(trainData, line);
  stringstream ss;
  ss << line;
  ss >> N;

  int n = N-1;
  int l = trainingExamples / blockSize;
  int l_test = testingExamples / blockSize;

  outils* otl = new outils();
  otl->sgenrand(time(NULL) + getpid());

  //Layer definitions                                                                                                                                                                                       
  Sequential* baseNetwork = new Sequential(1);
  if (charLm == 1) {
    baseNetwork->lkt = new CharEmbeddings(wordDim, n, charVocSize, charDim, blockSize, 1, otl);
     baseNetwork->lkt->getHashMaps(hmf);
  }
  else if (charLm == 2) {
    baseNetwork->lkt = new CharWordEmbeddings(outputVocSize, wordDim, n, charVocSize, charDim, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  }
  else {
    baseNetwork->lkt = new LookupTable(outputVocSize, wordDim, n, blockSize, 1, otl);
  }
  Module* module_L = new Linear(n * wordDim, hlSize, blockSize, otl);
  baseNetwork->add(module_L);
  Module* module_T = new Tanh(hlSize, blockSize);
  baseNetwork->add(module_T);
  ProbOutput* sl = new LinearSoftmax(hlSize, outputVocSize, blockSize, otl);
    
  //'Linking'                                                                                                                                                                                          
  floatTensor contextFeature = baseNetwork->output;
  floatTensor gradContextFeature;
  gradContextFeature.resize(contextFeature);
  floatTensor coefTensor; //?         

  //Training
  
  // Getting training data
  intTensor readTensor(N, trainingExamples);
  int* chars = new int[trainingExamples*N];
  
  for (int k = 0; k<trainingExamples; k++) {
    std::getline(trainData, line);
    vector<string> * ngrams = tokenize(&line, SSPACE);
    int index, length;
      
    // Get the numbers into a int array that will be put in an intTensor
    for (int i=0; i<ngrams->size(); i++){ 
      ss.clear();
      ss << (*ngrams)[i];
      ss >> chars[i+k*N];
    }
  }  
  readTensor.array2Tensor(chars, N, trainingExamples);
  
  //Testing                                                                                                                                      
  testData.open(test);
  if(! testData){
    cerr << "ERROR - cannot open file Test" << test <<endl;
    exit(1);
  }
  std::getline(testData, line);
  //Getting testing data                                                                                                                         
  intTensor readTensor_test(N, testingExamples);
  int* chars_test = new int[testingExamples*N];

  float* perp = new float[l_test];

  for (int k = 0; k<testingExamples; k++) {
    std::getline(testData, line);
    vector<string> * ngrams = tokenize(&line, SSPACE);
    int index, length;

    // Get the numbers into a int array that will be put in an intTensor                                                                                                                                     
    for (int i=0; i<ngrams->size(); i++){
      ss.clear();
      ss << (*ngrams)[i];
      ss >> chars_test[i+k*N];
    }
  }
  readTensor_test.array2Tensor(chars_test, N, testingExamples);
  
  intTensor context(n, blockSize);
  intTensor word(1, blockSize);
  
  //Training
  cout << "Training" << endl;
  for (int e=0; e<epochs; e++) {
    cout << "Epoch: " << e+1 << endl;
    for (int b=0; b<l; b++) {
      
      //cout << "Block: " << b*blockSize << ' ' << (b+1)*blockSize-1 << endl;
      //To loop
      //currentReadTensor.sub(readTensor, b*blockSize, (b+1)*blockSize-1, 0, N-1);
      // currentReadTensor.t();
      context.sub(readTensor, 0, N-2, b*blockSize, (b+1)*blockSize-1);
      word.sub(readTensor, N-1, N-1, b*blockSize, (b+1)*blockSize-1);
      word.t();
      //cout << context(0,0) << ' ' << context(0,1) << ' ' << context(0,2) << ' ' << context(0,3) << ' ' << context(0,4) << endl;
      //cout << context(1,0) << ' ' << context(1,1) << ' ' << context(1,2) << ' ' << context(1,3) << ' ' << context(1,4) << endl;
      //cout << context(2,0) << ' ' << context(2,1) << ' ' << context(2,2) << ' ' << context(2,3) << ' ' << context(2,4) << endl;
      //cout << word(0,0) << ' ' << word(0,1) << ' ' << word(0,2) << ' ' << word(0,3) << ' ' << word(0,4) << endl;
      
      //Foward
      baseNetwork->forward(context);
      sl->forward(contextFeature);
      
      //if ((b+1) % 50 == 0) {
	//cout << word.size[0] << ' ' << word.size[1] << endl;
	//cout << word(0) << ' ' << word(1) << ' ' << word(2) << ' ' << word(3) << ' ' << word(4) << endl;
      cout << "Block: " << b+1 << "  Perplexity : "  <<  exp(sl->computeF(word, coefTensor) / blockSize) << endl;
      //}

      //Backward
      gradContextFeature = sl->backward(word, coefTensor);
      baseNetwork->backward(gradContextFeature);
      
      //Update
      sl->updateParameters(lr);
      baseNetwork->updateParameters(lr, 1, up_h);
    }
    
    float acc = 0;
    for (int b=0; b<l_test; b++) {
      //To loop          
      context.sub(readTensor_test, 0, N-2, b*blockSize, (b+1)*blockSize-1);
      word.sub(readTensor_test, N-1, N-1, b*blockSize, (b+1)*blockSize-1);
      word.t();

      //Foward                                                                                                                                                                   
      baseNetwork->forward(context);
      sl->forward(contextFeature);
      
      perp[b] = sl->computeF(word, coefTensor) / blockSize;
      acc = acc + perp[b];
    }
    cout << "Test Perplexity : "  << exp(acc / l_test)   << endl;
  }
  return 0;
}
