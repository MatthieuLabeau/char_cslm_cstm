#include "mainModel.H"

template<typename modelType>
void copyModelNCE(modelType* destModel, modelType* origModel) {
	// copy model to oldModel
	destModel->baseNetwork->lkt->copy(origModel->baseNetwork->lkt);
	//oldModel->baseNetwork->lkt->bias.copy(model->baseNetwork->lkt->bias);

	for (int i = 0; i < origModel->baseNetwork->size; i++) {
		destModel->baseNetwork->modules[i]->copy(origModel->baseNetwork->modules[i]);
	}

	destModel->outputLayer->copy(origModel->outputLayer);
}

template<typename modelType>
void copyModelovn(modelType* destModel, modelType* origModel) {
	// copy model to oldModel
	destModel->baseNetwork->lkt->copy(origModel->baseNetwork->lkt);
	int i;
	for (i = 0; i < origModel->baseNetwork->size; i++) {
		destModel->baseNetwork->modules[i]->copy(origModel->baseNetwork->modules[i]);
	}

	for (i = 0; i < origModel->outputNetworkNumber; i++) {
		destModel->outputNetwork[i]->copy(origModel->outputNetwork[i]);
	}
}

int main(int argc, char *argv[]) {
	NeuralModel* rModel;
	NeuralModel* rModel2;
	char* modelName = "/vol/work2/dokhanh/discriminant.exp/debug/model/0";
	char* dataFile = "/vol/work2/dokhanh/discriminant.exp/debug/model/train.1";
#define modelType NgramWordTranslationModelNCE

	int blockSize = 128;
	READMODEL(rModel, blockSize, modelName);
	READMODEL(rModel2, blockSize, modelName);
	modelType* model =
			static_cast<modelType*>(rModel);
	modelType* model2 =
			static_cast<modelType*>(rModel2);
	int minEpoch = 1, maxEpoch = 10;

	float pre_f = model->computeF(dataFile);
	float f = 0;
	float sD = 0;
	float rD = 0;
	cout << "The objective function of 0 : " << pre_f << endl;

	copyModelNCE<modelType>(model2, model);

	cout << "Distance between the first two models : "
			<< model2->distance2(model) << endl;

	string learningRateType = "d";
	float learningRate = 0.00001;
	float learningRateDecay = 2;
	int update_lkt = 1;
	string update_hiddens = "1_1_1_1";
	int update_output_layer = 1;
	for (int i = minEpoch; i <= maxEpoch; i++) {
		model->train(dataFile, 0, i, learningRateType, learningRate,
				learningRateDecay, update_lkt, update_hiddens,
				update_output_layer);

		f = model->computeF(dataFile);
		cout << "The objective function of " << i << " : " << f << endl;

		sD = (pre_f - f) * learningRate;
		rD = model2->distance2(model);
		cout << "Distance between " << i << " and " << i - 1 << " : " << rD
				<< endl;
		cout << "Distance should be : " << sD << endl;
		cout << "Error : " << (sD - rD) / rD * 100 << endl;
		copyModelNCE(model2, model);
		pre_f = f;
	}

	delete rModel;
	delete model2;
}
