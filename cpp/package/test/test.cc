#include "mainModel.H"

int str_reverse (char* str, int lth) {
	char tmp;
	for (int i = 0 ; i < lth/2 ; i ++) {
		tmp = str[0];
		str[0] = str[lth - 1 - i];
		str[lth - 1 - i] = tmp;
	}
	return 0;
}

int
main(int argc, char *argv[])
{
	char* str = argv[1];
	int lth = atoi(argv[2]);
	str_reverse(str, lth);
	cout << "Reversed string : " << string(str) << endl;
	return 0;
}

