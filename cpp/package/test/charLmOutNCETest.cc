#include "mainModule.H"


vector<string> * tokenize(string * s, const string SEP){
  //cout << "line:" << *s<< endl;
  size_t pos = s->find(SEP);
  size_t org = 0;
  vector<string> * parse = new vector<string>();
  while (pos != string::npos){
    parse->push_back(s->substr(org,pos-org));
    //cout << "extract " << parse->size() << " " <<  parse->back() << endl;
    org = pos+SEP.length(); 
    pos = s->find(SEP,org);
  }
  parse->push_back(s->substr(org,s->size()-org));
  //cout << "extract " << parse->size() << " " <<  parse->back() << endl;
  return parse;
}


int
main(int argc, char *argv[])
{
  ifstream trainData;
  ifstream testData;
  char* train = argv[1];
  char* test = argv[2];
  char* hmf = argv[3];
  float lr = atof(argv[4]);
  int charLm = atoi(argv[5]);
  
  //Parameters
  const int blockSize = atoi(argv[7]);
  const int inputVocSize = 85298;
  const int outputVocSize = 12058;
  const int charVocSize = 62;
  const int wordDim = 256;
  const int charDim = 32;
  const int hlSize = 256;
  //const float lr = 0.1;
  const char* up_h = "1_1";
  float weightDecay = atof(argv[6]);

  const int epochs = 20;
  const int trainingExamples = 600701;
  const int testingExamples = 139629;
 
  //Model Creation
  //Getting Data information
  trainData.open(train);
  if(! trainData){
    cerr << "ERROR - cannot open file" << train <<endl;
    exit(1);
  }
  string line;
  const string SSPACE = " ";
  const string SVERTICAL = "|";

  int N;
  //First line
  std::getline(trainData, line);
  stringstream ss;
  ss << line;
  ss >> N;

  int K;
  //Second line
  std::getline(trainData, line);
  ss.clear();
  ss << line;
  ss >> K;

  int n = N-1;
  int l = trainingExamples / blockSize;
  int l_test = testingExamples / blockSize;

  outils* otl = new outils();
  otl->sgenrand(time(NULL) + getpid());

  //Layer definitions                                                                                                                                                                                       
  Embeddings* embed_C;
  if (charLm == 1) {
  embed_C = new CharEmbeddings(wordDim, n, charVocSize, charDim, blockSize, 1, otl);
  embed_C->getHashMaps(hmf);
    }
  else if (charLm == 2) {
    embed_C = new CharWordEmbeddings(outputVocSize, wordDim, n, charVocSize, charDim, blockSize, 1, otl);
    embed_C->getHashMaps(hmf);
  }
  else {
    embed_C = new LookupTable(inputVocSize, wordDim, n, blockSize, 1, otl);
    }
  Sequential* baseNetwork = new Sequential(1);
  baseNetwork->lkt = embed_C;
  Module* module_L = new Linear(n * wordDim, hlSize, blockSize, otl);
  baseNetwork->add(module_L);
  Module* module_T = new Tanh(hlSize, blockSize);
  baseNetwork->add(module_T);
  CharNCE* sl = new CharNCE( wordDim, charVocSize, charDim,
			     hlSize, inputVocSize, blockSize,
			     1, 1, otl, hmf);
  sl->changeNceOutputSize(K+1);
  //'Linking'                                                                                                                                                                                          
  floatTensor contextFeature = baseNetwork->output;
  floatTensor gradContextFeature;
  gradContextFeature.resize(contextFeature);
  CharEmbeddings* castedEmbed_C = dynamic_cast<CharEmbeddings*>(embed_C);
  CharWordEmbeddings* castedEmbed_C_W = dynamic_cast<CharWordEmbeddings*>(embed_C);
  
  if (castedEmbed_C || castedEmbed_C_W) {
    cout << sl->charEmbeddings->cLkt->weight.size[0] << ' ' << sl->charEmbeddings->cLkt->weight.size[1] << endl;
    cout << embed_C->cLkt->weight.size[0] <<' ' << embed_C->cLkt->weight.size[1]<< endl;
    sl->charEmbeddings->cLkt->weight.tieData(embed_C->cLkt->weight);
  }
  

  //Training
  
  // Getting training data
  intTensor readTensor(N+K, trainingExamples);
  int* chars = new int[trainingExamples*(N+K)];
  floatTensor logProbTensor(K+1, trainingExamples);
  float* logProbs = new float[trainingExamples*(K+1)];
  
  for (int k = 0; k<trainingExamples; k++) {
    std::getline(trainData, line);
    vector<string> * lineParts = tokenize(&line, SVERTICAL);
    std::string part1 ((*lineParts)[1]); 
    vector<string> * probs = tokenize(&part1, SSPACE);
    std::string part0 ((*lineParts)[0]);
    vector<string> * ngrams = tokenize(&part0, SSPACE);
    int index, length;
      
    // Get the numbers into a int array that will be put in an intTensor
    for (int i=0; i<ngrams->size(); i++){ 
      ss.clear();
      ss << (*ngrams)[i];
      ss >> chars[i+k*(N+K)];
    }

    for (int i=0; i<probs->size(); i++){
      ss.clear();
      ss << (*probs)[i];
      ss >> logProbs[i+k*(K+1)];
    } 
  } 
  readTensor.array2Tensor(chars, N+K, trainingExamples);
  logProbTensor.array2Tensor(logProbs, K+1, trainingExamples);  
  
  //Testing                                                                                                                                      
  testData.open(test);
  if(! testData){
    cerr << "ERROR - cannot open file" << test <<endl;
    exit(1);
  }
  std::getline(testData, line);
  //Getting testing data                                                                                                                         
  intTensor readTensor_test(N, testingExamples);
  int* chars_test = new int[testingExamples*N];

  for (int k = 0; k<testingExamples; k++) {
    std::getline(testData, line);
    vector<string> * ngrams = tokenize(&line, SSPACE);
    int index, length;

    // Get the numbers into a int array that will be put in an intTensor                                                                                                                                    
    for (int i=0; i<ngrams->size(); i++){
      ss.clear();
      ss << (*ngrams)[i];
      ss >> chars_test[i+k*N];
    }
  }
  readTensor_test.array2Tensor(chars_test, N, testingExamples);
  
  intTensor context(n, blockSize);
  intTensor word(K+1, blockSize);
  intTensor word_test(1, blockSize);
  floatTensor subLogProbTensor(K+1, blockSize);
  floatTensor output;

  //Training
  
  cout << "Training" << endl;
  
  for (int e=0; e<epochs; e++) {
    cout << "Epoch: " << e+1 << endl;
    //for (int b=0; b<3; b++) {
    for (int b=0; b< l; b++) {
      
      //To loop
      context.sub(readTensor, 0, N-2, b*blockSize, (b+1)*blockSize-1);
      word.sub(readTensor, N-1, N+K-1, b*blockSize, (b+1)*blockSize-1);    
      subLogProbTensor.sub(logProbTensor, 0, K, b*blockSize, (b+1)*blockSize-1);
      //cout << context(0,0) << ' ' << context(0,1) << ' ' << context(0,2) << ' ' << context(0,3) << ' ' << context(0,4) << endl;
      //cout << context(1,0) << ' ' << context(1,1) << ' ' << context(1,2) << ' ' << context(1,3) << ' ' << context(1,4) << endl;
      //cout << context(2,0) << ' ' << context(2,1) << ' ' << context(2,2) << ' ' << context(2,3) << ' ' << context(2,4) << endl;
      //cout << word(0,0) << ' ' << word(0,1) << ' ' << word(0,2) << ' ' << word(0,3) << ' ' << word(0,4) << endl;
      
      //Foward
      contextFeature = baseNetwork->forward(context);
      sl->nceForward(contextFeature, word);
      
      //if ((b+1) % 50 == 0) {
	//cout << word.size[0] << ' ' << word.size[1] << endl;
	//cout << word(0) << ' ' << word(1) << ' ' << word(2) << ' ' << word(3) << ' ' << word(4) << endl;
      cout << "Block: " << b+1 << "  Perplexity : "  <<  exp(sl->computeF(subLogProbTensor, word) / blockSize)  << endl;
      //}
      
      //Backward
      gradContextFeature = sl->backward(subLogProbTensor);
      sl->updateParameters(lr);
      baseNetwork->backward(gradContextFeature);
      baseNetwork->updateParameters(lr, 1, up_h);
    }
    
    
    float acc = 0;
    float acc_f = 0;
    //To do before testing
    sl->computeAllWeight();
    
    for (int b=0; b< l_test ; b++) {
    //for (int b=0; b<2; b++) {
      //To loop          
      context.sub(readTensor_test, 0, N-2, b*blockSize, (b+1)*blockSize-1);
      word_test.sub(readTensor_test, N-1, N-1, b*blockSize, (b+1)*blockSize-1);
      
      //Foward                                                                                                                                                                   
      baseNetwork->forward(context);
      
      acc += sl->computeNormalizedF(contextFeature, word_test) / blockSize;
      acc_f += sl->coefNorm();
    }
    cout << "Test Perplexity : "  << exp(acc / l_test)   << endl; 
    cout << "Mean norm : " << acc_f / l_test << endl;
    sl->changeCEBlockSize();
  }
  return 0;
}
