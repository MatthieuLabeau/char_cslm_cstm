SOURCES += \
    package/test/countWordGen.cc \
    package/test/forwardTrainTest.cc \
    package/test/readBinaryTrainingFile.cc \
    package/test/readFile.cc \
    package/test/test.cc \
    package/test/test_divers.cc \
    package/test/test_NgramModelNCE.cc \
    package/test/test_NgramModelNCE2.cc \
    package/test/test_writeAndRead_NgramModelNCE.cc \
    package/test/writeFile.cc
