#include "ioFile.H"
#include "Tensor.H"
#include "intTensor.H"
class outils;
#include "floatTensor.H"
#include "mainModule.H"
#include "text.H"

SPLIT_STRING
Strip(read, Int, int)

int main(int argc, char *argv[]) {
	if (argc != 5) {
		cout << "dataFileString inputVoc outputVoc type" << endl;
		return 0;
	}
	char* dataFileString = argv[1];
	char* inputVocName = argv[2];
	char* outputVocName = argv[3];
	string type = argv[4];
	ioFile dataIof;
	dataIof.takeReadFile(dataFileString);
	int ngramNumber;
	dataIof.readInt(ngramNumber);
	cout << "readBinaryTrainingFile::main ngramNumber : " << ngramNumber
			<< endl;
	int N;
	dataIof.readInt(N);
	cout << "readBinaryTrainingFile::main N : " << N << endl;
	int nceOutputSize = 1;
	if (type == OVN_NCE || type == WTOVN_NCE) {
		dataIof.readInt(nceOutputSize);
		nceOutputSize += 1;
		cout << "readBinaryTrainingFile::main nceOutputSize : " << nceOutputSize << endl;
	}
	intTensor readTensor(ngramNumber, N + nceOutputSize - 1);
	floatTensor coefTensor(ngramNumber, nceOutputSize);
	readIntTrainFile(dataIof, readTensor, coefTensor, N, type);

	ioFile dataInputVoc, dataOutputVoc;
	dataInputVoc.takeReadFile(inputVocName);
	dataInputVoc.format = TEXT;
	dataOutputVoc.takeReadFile(outputVocName);
	dataOutputVoc.format = TEXT;
	string inputVoc[1000000];
	string outputVoc[1000000];
	string inLine;
	string delim = " ";
	vector < string > v;
	int index;
	while (!dataInputVoc.getEOF()) {
		dataInputVoc.getLine(inLine);
		v = split_string(inLine, delim);
		if (v.size() == 2) {
			index = atoi((v.at(1)).c_str());
			inputVoc[index] = v.at(0);
		}
	}
	while (!dataOutputVoc.getEOF()) {
		dataOutputVoc.getLine(inLine);
		v = split_string(inLine, delim);
		if (v.size() == 2) {
			index = atoi((v.at(1)).c_str());
			outputVoc[index] = v.at(0);
		}
	}
	int i, j;
	// write readTensor with coef
	cout << "readBinaryTrainingFile::main file content : " << endl;
	for (i = 0 ; i < readTensor.size[0] ; i++) {
		for (j = 0 ; j < N - 1 ; j++) {
			cout << inputVoc[readTensor(i, j)] << " ";
		}
		cout << " --> ";
		for (j = N - 1 ; j < N + nceOutputSize - 1 ; j ++) {
			if (type == OVN_NCE || type == WTOVN_NCE || type == OVN_DISCRIM || type == WTOVN_DISCRIM) {
				cout << outputVoc[readTensor(i, j)] << " ||| " << coefTensor(i, j - N + 1) << " ";
			} else {
				cout << outputVoc[readTensor(i, j)] << " ";
			}
		}
		cout << endl;
	}
}
