#include "mainModel.H"

int main(int argc, char *argv[]) {
	outils* otl = new outils();
	int inputSize = 10;
	int outputSize = 20;
	int blockSize = 2;
	int n = 4;
	int projectionDimension = 10;
	string nonLinearType = "s";
	intTensor hiddenLayerSizeArray(1, 1);
	hiddenLayerSizeArray(0) = 10;
	string name = OVN_NCE;
	/*LinearNCE* outputLayer = new LinearNCE(inputSize, outputSize, blockSize,
	 otl);*/
	NgramModelNCE* model = new NgramModelNCE(name, inputSize, outputSize,
			blockSize, n, projectionDimension, nonLinearType,
			hiddenLayerSizeArray);
	ioFile writeIof;
	writeIof.takeWriteFile("/people/dokhanh/module");
	//outputLayer->write(&writeIof);
	model->write(&writeIof, 1);
	writeIof.freeWriteFile();

	/*LinearNCE* outputLayer1 = new LinearNCE(inputSize, outputSize, blockSize,
			otl);*/
	ioFile file;
	string name1 = file.recognition("/vol/work3/dokhanh/exp.NCE/sl2000.En.Joint_CSLM/model/1");
	NeuralModel* model1;
	READMODEL(model1, 0, "/vol/work3/dokhanh/exp.NCE/sl2000.En.Joint_CSLM/model/1");

	cout << "test_writeAndRead_NgramModelNCE::main outputLayer1->weight : "
			<< endl;
	static_cast<NgramModelNCE*>(model1)->outputLayer->weight.write();
	cout << "test_writeAndRead_NgramModelNCE::main outputLayer1->bias : "
			<< endl;
	static_cast<NgramModelNCE*>(model1)->outputLayer->bias.write();

	/*cout << "test_writeAndRead_NgramModelNCE distance between 2 models : "
			<< model1->distance2(model) << endl;*/

	//delete outputLayer;
	//delete outputLayer1;
	delete model;
	delete model1;
}
