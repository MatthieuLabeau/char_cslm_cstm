#include "ioFile.H"

int main(int argc, char *argv[]) {
	if (argc != 2) {
		cout << "dataFileString" << endl;
		return 0;
	}
	// data file name
	char* dataFileString = argv[1];

	ioFile dataIof;
	// for reading
	dataIof.takeReadFile(dataFileString);
	int ngramNumber;
	// read ngramNumber
	dataIof.readInt(ngramNumber);
	int n;
	// read n
	dataIof.readInt(n);
	int k;
	// read k
	dataIof.readInt(k);

	cout << "ngramNumber : " << ngramNumber << " n : " << n << " k : " << k << endl;

	int* context = new int[n - 1];
	int* outWord = new int[k + 1];
	float p;
	for (int idNgram = 0 ; idNgram < ngramNumber ; idNgram ++) {
		dataIof.readIntArray(context, n - 1);
		for (int i = 0 ; i < n - 1 ; i ++) {
			cout << context[i] << " ";
		}
		for (int j = 0 ; j < k + 1 ; j ++) {
			dataIof.readInt(outWord[j]);
			dataIof.readFloat(p);
			cout << outWord[j] << " with p : " << p << " ";
		}
		cout << endl;
	}

	// finish reading
	dataIof.freeReadFile();
}
