#include "mainModel.H"
#include "time.h"

Strip(read, Int, int)
Strip(write, Int, int)
Strip(read, Float, float)
Strip(write, Float, float)

void copyModelNCE(NgramModelNCE* oldModel, NgramModelNCE* model) {
	// copy model to oldModel
	oldModel->baseNetwork->lkt->weight.copy(model->baseNetwork->lkt->weight);
	//oldModel->baseNetwork->lkt->bias.copy(model->baseNetwork->lkt->bias);

	for (int i = 0; i < model->baseNetwork->size; i++) {
		Linear* validType =
				dynamic_cast<Linear*>(oldModel->baseNetwork->modules[i]);
		if (validType != NULL) {
			oldModel->baseNetwork->modules[i]->weight.copy(
					model->baseNetwork->modules[i]->weight);
			oldModel->baseNetwork->modules[i]->bias.copy(
					model->baseNetwork->modules[i]->bias);
		}
	}

	oldModel->outputLayer->weight.copy(model->outputLayer->weight);
	oldModel->outputLayer->bias.copy(model->outputLayer->bias);
}

int main(int argc, char *argv[]) {
	int n = 4;
	int ngramNumberTrain = 4;
	int ngramNumberTest = 3;
	int nceOutputSize = 3;
	int maxIteration = 5;
	string learningRateType = "d";
	float learningRate = 1;
	float learningRateDecay = 2;
	int update_lkt = 1;
	string update_hiddens = "1_1";
	int update_output_layer = 1;

	string name = "ovn_nce";
	int vocSize = 5;
	int blockSize = 2;
	int projectionDimension = 5;
	string nonLinearType = "s";
	intTensor hiddenLayerSizeArray(1, 1);
	hiddenLayerSizeArray = 5;

	float logProp = -1.609438;
	intTensor wordTensor;
	floatTensor logPropTensor(ngramNumberTrain, nceOutputSize);

	/*int wordData[30] = { 0, 1, 2, 3, 4, 1, 2, 3, 4, 0, 2, 3, 4, 0, 1, 3, 4, 0,
	 1, 2, 4, 0, 1, 2, 3, 0, 1, 2, 3, 4 };*/
	/*int wordData[6] = { 0, 1, 2, 3, 4, 0 };*/
	/*int wordData[12] = { 0, 1, 1, 2, 2, 3, 3, 4, 4, 0, 0, 1 };*/
	/*int wordData[6] = {1, 2, 3, 4, 0, 1};*/
	int wordData[24] = { 0, 1, 0, 1, 1, 2, 1, 2, 2, 3, 2, 3, 3, 4, 3, 4, 4, 0,
			1, 2, 0, 1, 2, 3 };
	wordTensor.array2Tensor(wordData, ngramNumberTrain, n - 1 + nceOutputSize);
	cout << "wordTensor : " << endl;
	wordTensor.write();
	intTensor ngramData(ngramNumberTest, n + nceOutputSize - 1);
	/*int ngramTensor[14] = { 0, 1, 1, 2, 2, 3, 3, 4, -1, -1, 0, 1, 1, 2 };*/
	int ngramTensor[21] = { 0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 0, -1, -1, -1, 0,
			1, 2, 1, 2, 3 };
	ngramData.array2Tensor(ngramTensor, ngramNumberTest, n + nceOutputSize - 1);
	logPropTensor = logProp;
	floatTensor ngramProp(ngramNumberTest, 1);
	// for test
	//cout << "test_NgramModelNCE::main here" << endl;
	NgramModelNCE* model = new NgramModelNCE(name, vocSize, vocSize, blockSize,
			n, projectionDimension, nonLinearType, hiddenLayerSizeArray);
	NgramModelNCE* oldModel = new NgramModelNCE(name, vocSize, vocSize,
			blockSize, n, projectionDimension, nonLinearType,
			hiddenLayerSizeArray);
	// for test
	//cout << "test_NgramModelNCE::main here 1" << endl;
	copyModelNCE(oldModel, model);
	// for test
	//cout << "test_NgramModelNCE::main here 2" << endl;

	cout << "distance between 2 models in the beginning : "
			<< oldModel->distance2(model) << endl;

	/*cout << "Objective function of 0 : "
	 << model->computeF(wordTensor, logPropTensor) << endl;*/
	// for test
	//cout << "test_NgramModelNCE::main here 2.0" << endl;
	cout << "Objective function of 0 : "
			<< model->computeF("/people/dokhanh/newTrain") << endl;
	// for test
	//cout << "test_NgramModelNCE::main here 2.1" << endl;

	cout << "Prop of 0 : " << endl;
	model->forwardProbability(ngramData, ngramProp);
	ngramProp.write();

	cout << "training" << endl;
	for (int iteration = 1; iteration <= maxIteration; iteration++) {
		/*model->trainTest(wordTensor, logPropTensor, iteration, learningRateType,
		 learningRate, learningRateDecay, update_lkt, update_hiddens,
		 update_output_layer);*/
		model->train("/people/dokhanh/newTrain", 0, iteration, learningRateType,
				learningRate, learningRateDecay, update_lkt, update_hiddens,
				update_output_layer);

		/*cout << "Objective function of " << iteration << " : "
		 << model->computeF(wordTensor, logPropTensor) << endl;*/
		cout << "Objective function of " << iteration << " : "
				<< model->computeF("/people/dokhanh/newTrain") << endl;

		cout << "Distance between " << iteration << " and " << iteration - 1
				<< " : " << oldModel->distance2(model) << endl;
		cout << "Prop of " << iteration << " : " << endl;
		model->forwardProbability(ngramData, ngramProp);
		ngramProp.write();
		copyModelNCE(oldModel, model);
	}

	delete model;
	delete oldModel;
}
