#include "mainModel.H"

int main(int argc, char *argv[]) {
	if (argc != 8) {
		cout
				<< "modelFileName blockSize incrUnk textFileName file_fprobs file_ngramNumber_for_each_hypo normalized"
				<< endl;
		return 0;
	} else {
		time_t start, end;
		char* modelFileName = argv[1];
		int blockSize = atoi(argv[2]);
		float incrUnk = pow(10, atof(argv[3]));
		char* textFileName = argv[4];
		char* outputFileName = argv[5];
		char* file_ngramNumber_name = argv[6];
		int normalized = atoi(argv[7]);
		ioFile iofC;
		if (!iofC.check(modelFileName, 1)) {
			return 1;
		}
		if (!iofC.check(textFileName, 1)) {
			return 1;
		}
		if (iofC.check(outputFileName, 0)) {
			cerr << "prob file exists" << endl;
			return 1;
		}
		NeuralModel* model;
		READMODEL(model, blockSize, modelFileName);

		model->incrUnk = incrUnk;
		time(&start);

		ioFile iof;
		iof.format = TEXT;
		iof.takeReadFile((char*) textFileName);
		ioFile iofO;
		iofO.format = TEXT;
		iofO.takeWriteFile(outputFileName);
		ioFile iof_ngramNumber;
		iof_ngramNumber.format = TEXT;
		iof_ngramNumber.takeWriteFile(file_ngramNumber_name);
		int readLineNumber = 0;
		int old_ngram_number = 0;
		int current_ngram_number = 0;

		char * maxNgramNumberEnv;
		int maxNgramNumber = BLOCK_NGRAM_NUMBER;
		maxNgramNumberEnv = getenv("BLOCK_NGRAM_NUMBER");
		if (maxNgramNumberEnv != NULL) {
			maxNgramNumber = atoi(maxNgramNumberEnv);
		}

		while (!iof.getEOF()) {
			old_ngram_number = model->dataSet->ngramNumber;
			model->dataSet->addLine(&iof);
			readLineNumber++;
			// write ngram number
			current_ngram_number = model->dataSet->ngramNumber
					- old_ngram_number;
			if (current_ngram_number != 0) {
				iof_ngramNumber.writeInt(current_ngram_number);
			}

#if PRINT_DEBUG
			if (readLineNumber % NLINEPRINT == 0 && readLineNumber != 0) {
				cout << readLineNumber << " ... " << flush;
			}
#endif
			if (model->dataSet->ngramNumber
					> maxNgramNumber - MAX_WORD_PER_SENTENCE) {
				// for test
				//cout << "text2Prob::main here ngramNumber: " << model->dataSet->ngramNumber << end;
				cout << "Compute " << model->dataSet->ngramNumber << " ngrams"
						<< endl;
				model->dataSet->createTensor();
				if (model->name == OVN_NCE) {
					static_cast<NgramModelNCE*>(model)->forwardUnnormalizedProbability(
							model->dataSet->dataTensor,
							model->dataSet->probTensor, normalized);
				} else if (model->name == WTOVN_NCE) {
					static_cast<NgramWordTranslationModelNCE*>(model)->forwardUnnormalizedProbability(
							model->dataSet->dataTensor,
							model->dataSet->probTensor);
				} else {
					cerr << "Computation of unnormalized probabilities on "
							<< model->name << " is impossible" << endl;
					exit(1);
				}
				for (int i = 0; i < model->dataSet->probTensor.length; i++) {
					iofO.writeFloat(model->dataSet->probTensor(i));
				}
				model->dataSet->reset();
			}
		}
		if (model->dataSet->ngramNumber != 0) {
			// for test
			//cout << "text2Prob::main here1 ngramNumber: " << model->dataSet->ngramNumber << endl;
			cout << "Compute with " << model->dataSet->ngramNumber << " ngrams"
					<< endl;
			model->dataSet->createTensor();
			if (model->name == OVN_NCE) {
				static_cast<NgramModelNCE*>(model)->forwardUnnormalizedProbability(
						model->dataSet->dataTensor, model->dataSet->probTensor,
						normalized);
			} else if (model->name == WTOVN_NCE) {
				static_cast<NgramWordTranslationModelNCE*>(model)->forwardUnnormalizedProbability(
						model->dataSet->dataTensor, model->dataSet->probTensor);
			} else {
				cerr << "Computation of unnormalized probabilities on "
						<< model->name << " is impossible" << endl;
				exit(1);
			}
			for (int i = 0; i < model->dataSet->probTensor.length; i++) {
				iofO.writeFloat(model->dataSet->probTensor(i));
			}
		}

#if PRINT_DEBUG
		cout << endl;
#endif
		time(&end);
		cout << "Finish after " << difftime(end, start) / 60 << " minutes"
				<< endl;
		delete model;
	}
	return 0;
}
