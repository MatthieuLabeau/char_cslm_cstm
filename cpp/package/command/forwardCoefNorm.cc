#include "mainModel.H"

using namespace std;

int main(int argc, char *argv[]) {
	if (argc != 5) {
		cout << "modelFileName blockSize textFileName outputFileName" << endl;
		return 0;
	} else {
		// always
		int addAuxil = 1;
		time_t start, end;
		char* modelFileName = argv[1];
		int blockSize = atoi(argv[2]);
		char* textFileName = argv[3];
		char* outputFileName = argv[4];
		ioFile iofC;
		if (!iofC.check(modelFileName, 1)) {
			return 1;
		}
		if (!iofC.check(textFileName, 1)) {
			return 1;
		}
		if (iofC.check(outputFileName, 0)) {
			cerr << "prob file exists" << endl;
			return 1;
		}

		NeuralModel* model;
		READMODEL(model, blockSize, modelFileName);
		if (model->name != OVN_NCE) {
			cerr << "Not a good model type" << endl;
			cerr << "Only ovn_nce" << endl;
			return (1);
		}
		model->incrUnk = 0;
		time(&start);

		ioFile iof;
		iof.format = TEXT;
		iof.takeReadFile((char*) textFileName);
		ioFile iofO;
		iofO.format = TEXT;
		iofO.takeWriteFile(outputFileName);
		int readLineNumber = 0;
		int current_ngram_number = 0;

		char * maxNgramNumberEnv;
		int maxNgramNumber = BLOCK_NGRAM_NUMBER;
		maxNgramNumberEnv = getenv("BLOCK_NGRAM_NUMBER");
		if (maxNgramNumberEnv != NULL) {
			maxNgramNumber = atoi(maxNgramNumberEnv);
		}

		while (!iof.getEOF()) {
			model->dataSet->addLine(&iof, addAuxil);
			readLineNumber++;
#if PRINT_DEBUG
			if (readLineNumber % NLINEPRINT == 0 && readLineNumber != 0) {
				cout << readLineNumber << " ... " << flush;
			}
#endif
			if (model->dataSet->ngramNumber
					> maxNgramNumber - MAX_WORD_PER_SENTENCE) {
				cout << "Compute " << model->dataSet->ngramNumber << " ngrams"
						<< endl;
				model->dataSet->createTensor();
				static_cast<NgramModelNCE*>(model)->forwardCoefNorm(
						model->dataSet->dataTensor, &iofO);
				model->dataSet->reset();
			}
		}
		if (model->dataSet->ngramNumber != 0) {
			// for test
			//cout << "text2Prob::main here1 ngramNumber: " << model->dataSet->ngramNumber << endl;
			cout << "Compute with " << model->dataSet->ngramNumber << " ngrams"
					<< endl;
			model->dataSet->createTensor();
			static_cast<NgramModelNCE*>(model)->forwardCoefNorm(
									model->dataSet->dataTensor, &iofO);
		}

#if PRINT_DEBUG
		cout << endl;
#endif
		time(&end);
		cout << "Finish after " << difftime(end, start) / 60 << " minutes"
				<< endl;
		delete model;
		iof.freeReadFile();
		iofO.freeWriteFile();
	}
	return 0;
}
