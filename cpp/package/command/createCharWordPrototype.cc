#include "mainModel.H"

NgramModel* readModel(int blockSize, char* sourceModelFileName) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramModel* sourceModel = new NgramModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize);
  return sourceModel;
}

NgramModelNCE* readModelNCE(int blockSize, char* sourceModelFileName) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramModelNCE* sourceModel = new NgramModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize);
  return sourceModel;
}

NgramCharModel* readCharModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModel* sourceModel = new NgramCharModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

NgramCharModelNCE* readCharModelNCE(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModelNCE* sourceModel = new NgramCharModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

int main(int argc, char *argv[]) {
  if (argc != 12) {
    cout << "type charModelFileName wordModelFileName charVocFileName inputVocFileName outputVocFileName charWordHMapFileName nonLinearType outputModelFileName" << endl;
    return 0;
  }
  time_t start, end;
  string name = argv[1];
  char* charModelFileName = argv[2];
  char* wordModelFileName = argv[3];
  char* charVocFileName = argv[4];
  char* contextVocFileName = argv[5];
  char* predictVocFileName = argv[6];
  char* charWordHMap = argv[7];
  string nonLinearType = argv[8];
  char* outputModelFileName = argv[9];
  char* codeWordFileName = argv[10];
  char* outputNetworkSizeFileName = argv[11];

  ioFile iofC;
  if (!iofC.check(charModelFileName, 1)) {
    return 1;
  }
  if (!iofC.check(wordModelFileName, 1)) {
    return 1;
  }
  if (!iofC.check(charVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(contextVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(predictVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(charWordHMap, 1)) {
    return 1;
  }
  if (iofC.check(outputModelFileName, 0)) {
    cerr << "Prototype exists" << endl;
    return 1;
  }
  ioFile iofChar;
  ioFile iofWord;
  string nameChar = iofChar.recognition(charModelFileName);
  string nameWord = iofWord.recognition(wordModelFileName);
  NeuralModel* charModel;
  NeuralModel* wordModel;
  int charDimensionSize;
  int charWordNumber;
  if (nameChar == CE) {
    charModel = readCharModel(1, charModelFileName, charWordHMap);
    NgramCharModel* charModelCast = static_cast<NgramCharModel*>(charModel);
    charDimensionSize = charModelCast->charDimensionSize;
    charWordNumber = charModelCast->charVoc->wordNumber;
  } else if (nameChar == CE_NCE) {
    charModel = readCharModelNCE(0, charModelFileName, charWordHMap);
    NgramCharModelNCE* charModelCast = static_cast<NgramCharModelNCE*>(charModel);
    charDimensionSize = charModelCast->charDimensionSize;
    charWordNumber = charModelCast->charVoc->wordNumber;
  } else {
    cerr << "The first model needs to be of type CE or CE_NCE" << endl;
    return 1;
  }
  if (nameWord == OVN ) {
    wordModel = readModel(0, wordModelFileName);
  } else if (nameWord == OVN_NCE) {
    wordModel =readModelNCE(0, wordModelFileName);
  } else {
    cerr << "The second model needs to be of type OVN or OVN_NCE" << endl;
    return 1;
  }

  //Check that info from the two models are corresponding
  if (charModel->dimensionSize != wordModel->dimensionSize) {
    cerr << "Error: the two models don't have the same word dimension" << endl;
    return 1;
  }

  intTensor hiddenLayerSizeArray;
  hiddenLayerSizeArray.copy(charModel->hiddenLayerSizeArray);
  hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1) = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1) * 2;

  ioFile mIof;
  //Create model here
  NeuralModel* model;
  int modelCharWordNumber = 0;
  if (name == CWE_NCE) {
    model = new NgramCharModelNCE(name, charWordHMap, charVocFileName, contextVocFileName, predictVocFileName,
				  charModel->mapIUnk, charModel->mapOUnk, charModel->BOS, charModel->blockSize, charModel->n,
				  charDimensionSize, charModel->dimensionSize * 2, nonLinearType, hiddenLayerSizeArray);
    NgramCharModelNCE* model_cast = static_cast<NgramCharModelNCE*>(model);
    modelCharWordNumber = model_cast->charVoc->wordNumber;
  } else if (name == CWE) {
    model = new NgramCharModel(name, charWordHMap, charVocFileName, contextVocFileName, predictVocFileName,
                               charModel->mapIUnk, charModel->mapOUnk, charModel->BOS, charModel->blockSize, charModel->n,
                               charDimensionSize, charModel->dimensionSize * 2, nonLinearType, hiddenLayerSizeArray,
                               codeWordFileName, outputNetworkSizeFileName);
    NgramCharModel* model_cast = static_cast<NgramCharModel*>(model);
    modelCharWordNumber = model_cast->charVoc->wordNumber;
  }


  //Add char weight and word weight to model
  if ( charWordNumber != modelCharWordNumber) {
    cerr << "Error: Wrong character vocabulary" << endl;
    return 1;
  }
  CharEmbeddings* clkt = static_cast<CharEmbeddings*>(charModel->baseNetwork->lkt);
  CharWordEmbeddings* cwlkt = static_cast<CharWordEmbeddings*>(model->baseNetwork->lkt);
  cwlkt->cLkt->weight.copy(clkt->cLkt->weight);
   
  if (wordModel->inputVoc->wordNumber != model->outputVoc->wordNumber) {
    cerr << "Error: Wrong output vocabulary" << endl;
    return 1;
  }
  cwlkt->Lkt->weight.copy(wordModel->baseNetwork->lkt->weight);

  mIof.takeWriteFile(outputModelFileName);
  model->write(&mIof, 1);
  mIof.freeWriteFile();
  delete model;
  return 0;
}
