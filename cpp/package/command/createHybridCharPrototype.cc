#include "mainModel.H"

NgramCharModel* readCharModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModel* sourceModel = new NgramCharModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

NgramCharModelNCE* readCharModelNCE(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModelNCE* sourceModel = new NgramCharModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

int main(int argc, char *argv[]) {
  if (argc != 11) {
    cout << "type charModelFileName charVocFileName inputVocFileName outputVocFileName charWordHMapFileName nonLinearType outputModelFileName" << endl;
    return 0;
  }
  time_t start, end;
  string name = argv[1];
  char* charModelFileName = argv[2];
  char* charVocFileName = argv[3];
  char* contextVocFileName = argv[4];
  char* predictVocFileName = argv[5];
  char* charWordHMap = argv[6];
  string nonLinearType = argv[7];
  char* outputModelFileName = argv[8];
  char* codeWordFileName = argv[9];
  char* outputNetworkSizeFileName = argv[10];
  ioFile iofC;
  if (!iofC.check(charModelFileName, 1)) {
    return 1;
  }
  if (!iofC.check(charVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(contextVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(predictVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(charWordHMap, 1)) {
    return 1;
  }
  if (iofC.check(outputModelFileName, 0)) {
    cerr << "Prototype exists" << endl;
    return 1;
  }
  ioFile iofChar;
  string nameChar = iofChar.recognition(charModelFileName);
  NeuralModel* charModel;
  int charDimensionSize;
  int charWordNumber;
  if (nameChar == CE) {
    charModel = readCharModel(0, charModelFileName, charWordHMap);
    NgramCharModel* charModelCast = static_cast<NgramCharModel*>(charModel);
    charDimensionSize = charModelCast->charDimensionSize;
    charWordNumber = charModelCast->charVoc->wordNumber;
  } else if (nameChar == CE_NCE) {
    charModel = readCharModelNCE(0, charModelFileName, charWordHMap);
    NgramCharModelNCE* charModelCast = static_cast<NgramCharModelNCE*>(charModel);
    charDimensionSize = charModelCast->charDimensionSize;
    charWordNumber = charModelCast->charVoc->wordNumber;
  } else {
    cerr << "The first model needs to be of type CE or CE_NCE" << endl;
    return 1;
  }

  ioFile mIof;
  //Create model here and get output weights
  NeuralModel* model;
  NgramCharModelNCE* charModelNCE = dynamic_cast<NgramCharModelNCE*>(charModel);
  NgramCharModel* charModelnoNCE = dynamic_cast<NgramCharModel*>(charModel);
  int modelCharWordNumber = 0;
  if (name == CE_H_NCE) {
    model = new NgramCharModelNCE(name, charWordHMap, charVocFileName, contextVocFileName, predictVocFileName,
				  charModel->mapIUnk, charModel->mapOUnk, charModel->BOS, charModel->blockSize, charModel->n,
				  charDimensionSize, charModel->dimensionSize, nonLinearType, charModel->hiddenLayerSizeArray);
    NgramCharModelNCE* model_cast = static_cast<NgramCharModelNCE*>(model);
    HybridNCE* out_h = static_cast<HybridNCE*>(model_cast->outputLayer);
    modelCharWordNumber = model_cast->charVoc->wordNumber;
    if (charModelNCE) {
      out_h->setFrequentEmbeddings(charModelNCE->outputLayer->weight, charModelNCE->outputLayer->bias);
    } else if (charModelnoNCE) {
      out_h->setFrequentEmbeddings(charModelnoNCE->outputNetwork[0]->weight, charModelnoNCE->outputNetwork[0]->bias);
    }
  } else if (name == CE_H) {
    model = new NgramCharModel(name, charWordHMap, charVocFileName, contextVocFileName, predictVocFileName,
			       charModel->mapIUnk, charModel->mapOUnk, charModel->BOS, charModel->blockSize, charModel->n,
			       charDimensionSize, charModel->dimensionSize, nonLinearType, charModel->hiddenLayerSizeArray,
			       codeWordFileName, outputNetworkSizeFileName);
    NgramCharModel* model_cast = static_cast<NgramCharModel*>(model);
    HybridSoftmax* out_h = static_cast<HybridSoftmax*>(model_cast->outputNetwork[0]);
    modelCharWordNumber = model_cast->charVoc->wordNumber;
    if (charModelNCE) {
      out_h->setFrequentEmbeddings(charModelNCE->outputLayer->weight, charModelNCE->outputLayer->bias);
    } else if (charModelnoNCE) {
      out_h->setFrequentEmbeddings(charModelnoNCE->outputNetwork[0]->weight, charModelnoNCE->outputNetwork[0]->bias);
    }
  }

  //Add char weight and word weight to model
  if (charWordNumber != modelCharWordNumber) {
    cerr << "Error: Wrong character vocabulary" << endl;
    return 1;
  }

  //Copy char and conv weight
  CharEmbeddings* clkt = static_cast<CharEmbeddings*>(charModel->baseNetwork->lkt);
  CharEmbeddings* in_clkt = static_cast<CharEmbeddings*>(model->baseNetwork->lkt);
  in_clkt->cLkt->weight.copy(clkt->cLkt->weight);
  in_clkt->convModule->weight.copy(clkt->convModule->weight);
  in_clkt->convModule->bias.copy(clkt->convModule->bias);

  //Copy hidden weight
  for (int i = 0; i < model->baseNetwork->size; i=i+2) {
    model->baseNetwork->modules[i]->weight.copy(charModel->baseNetwork->modules[i]->weight);
    model->baseNetwork->modules[i]->bias.copy(charModel->baseNetwork->modules[i]->bias);
  }

  mIof.takeWriteFile(outputModelFileName);
  model->write(&mIof, 1);
  mIof.freeWriteFile();
  delete model;
  return 0;
}
