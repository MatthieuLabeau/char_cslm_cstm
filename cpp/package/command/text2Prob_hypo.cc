#include "mainModel.H"

NeuralModel* readModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NeuralModel* sourceModel;
  if (name_sourceModel == OVN || name_sourceModel == OVN_AG || name_sourceModel == ROVN
      || name_sourceModel == CN || name_sourceModel == OVNB || name_sourceModel == MAXOVN
      || name_sourceModel == LBL) {
    sourceModel = new NgramModel();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize);
  } else if (name_sourceModel == OVN_NCE || name_sourceModel == OVN_NCE_AG) {
    sourceModel = new NgramModelNCE();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize);
  } else if (name_sourceModel == CE || name_sourceModel == CWE
             || name_sourceModel == CWE_L || name_sourceModel == CE_AG) {
    sourceModel = new NgramCharModel();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  } else if (name_sourceModel == CE_NCE || name_sourceModel == CWE_NCE || name_sourceModel == CWE_NCE_L
             || name_sourceModel == CE_CE_NCE || name_sourceModel == CWE_CE_NCE || name_sourceModel == LKT_CE_NCE
             || name_sourceModel == LKT_CWE_NCE || name_sourceModel == CE_CWE_NCE || name_sourceModel == CWE_CWE_NCE
             || name_sourceModel == CE_NCE_AG || name_sourceModel == CE_CE_NCE_AG || name_sourceModel == CE_H_NCE_AG
             || name_sourceModel == CE_CWE_NCE_AG || name_sourceModel == CWE_NCE_AG || name_sourceModel == LKT_CWE_NCE_AG
	     || name_sourceModel == CWE_CWE_NCE_AG) {
    sourceModel = new NgramCharModelNCE();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  }
  return sourceModel;
}

int main(int argc, char *argv[]) {
	if (argc != 9) {
		cout
				<< "modelFileName blockSize incrUnk textFileName file_fprobs file_ngramNumber_for_each_hypo normalize charWordHMap"
				<< endl;
		return 0;
	} else {
		time_t start, end;
		// for text2Prob_hypo, addAuxil = 1
		int addAuxil = 1;
		char* modelFileName = argv[1];
		int blockSize = atoi(argv[2]);
		float incrUnk = pow(10, atof(argv[3]));
		char* textFileName = argv[4];
		char* outputFileName = argv[5];
		char* file_ngramNumber_name = argv[6];
		int normalize = atoi(argv[7]);
		char* charWordHMap = argv[8];

		ioFile iofC;
		if (!iofC.check(modelFileName, 1)) {
			return 1;
		}
		if (!iofC.check(textFileName, 1)) {
			return 1;
		}
		if (iofC.check(outputFileName, 0)) {
			cerr << "prob file exists" << endl;
			return 1;
		}
		
		string name = iofC.recognition(modelFileName);
		NeuralModel* model;

		if (name == CE || name == CWE || name == CWE_L || name == CE_H || name == CE_AG) {
		  model = readModel(blockSize, modelFileName, charWordHMap);
		} else if (name == CE_NCE || name == CWE_NCE || name == CWE_NCE_L || name == CE_CE_NCE || name == CWE_CE_NCE
			   || name == LKT_CE_NCE || name == LKT_CWE_NCE || name == CE_CWE_NCE || name == CWE_CWE_NCE
			   || name == CE_H_NCE || name == CE_NCE_AG || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
			   || name == CE_CWE_NCE_AG || name == CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
		  model = readModel(blockSize, modelFileName, charWordHMap);
		} else if (name == OVN_NCE || name == OVN_NCE_AG) {
		  model = readModel(blockSize, modelFileName, charWordHMap);
		} else {
		  model = readModel(blockSize, modelFileName, charWordHMap);
		}

		model->incrUnk = incrUnk;
		time(&start);

		ioFile iof;
		iof.format = TEXT;
		iof.takeReadFile((char*) textFileName);
		ioFile iofO;
		iofO.format = TEXT;
		iofO.takeWriteFile(outputFileName);
		ioFile iof_ngramNumber;
		iof_ngramNumber.format = TEXT;
		iof_ngramNumber.takeWriteFile(file_ngramNumber_name);
		int readLineNumber = 0;
		int old_ngram_number = 0;
		int current_ngram_number = 0;

		char * maxNgramNumberEnv;
		int maxNgramNumber = BLOCK_NGRAM_NUMBER;
		maxNgramNumberEnv = getenv("BLOCK_NGRAM_NUMBER");
		if (maxNgramNumberEnv != NULL) {
			maxNgramNumber = atoi(maxNgramNumberEnv);
		}

		while (!iof.getEOF()) {
			old_ngram_number = model->dataSet->ngramNumber;
			model->dataSet->addLine(&iof, addAuxil);
			readLineNumber++;
			// write ngram number
			current_ngram_number = model->dataSet->ngramNumber
					- old_ngram_number;
			if (current_ngram_number != 0) {
				iof_ngramNumber.writeInt(current_ngram_number);
			}
			// for test
			//cout << "text2Prob::main readLineNumber: " << readLineNumber << endl;
			//model->dataSet->writeReBiNgram();
#if PRINT_DEBUG
			if (readLineNumber % NLINEPRINT == 0 && readLineNumber != 0) {
				cout << readLineNumber << " ... " << flush;
			}
#endif
			if (model->dataSet->ngramNumber
					> maxNgramNumber - MAX_WORD_PER_SENTENCE) {
				cout << "Compute " << model->dataSet->ngramNumber << " ngrams"
						<< endl;
				model->dataSet->createTensor();
//				forwardProbability(model, normalize);
				model->forwardProbability(model->dataSet->dataTensor,
						model->dataSet->probTensor, normalize);
				for (int i = 0; i < model->dataSet->probTensor.length; i++) {
					iofO.writeFloat(model->dataSet->probTensor(i));
				}
				model->dataSet->reset();
			}
		}
		if (model->dataSet->ngramNumber != 0) {
			cout << "Compute with " << model->dataSet->ngramNumber << " ngrams"
					<< endl;
			model->dataSet->createTensor();
//			forwardProbability(model, normalize);
			model->forwardProbability(model->dataSet->dataTensor,
					model->dataSet->probTensor, normalize);
			for (int i = 0; i < model->dataSet->probTensor.length; i++) {
				iofO.writeFloat(model->dataSet->probTensor(i));
			}
		}

#if PRINT_DEBUG
		cout << endl;
#endif
		time(&end);
		cout << "Finish after " << difftime(end, start) / 60 << " minutes"
				<< endl;
		delete model;
	}
	return 0;
}
