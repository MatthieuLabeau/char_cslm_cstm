#include "mainModel.H"

int main(int argc, char *argv[]) {
	if (argc != 3) {
		cout << "modelFileName outputModelFileName" << endl;
		return 0;
	}
	char* modelFileName = argv[1];
	char* outputModelFileName = argv[2];
	ioFile iofC;
	if (!iofC.check(modelFileName, 1)) {
		return 1;
	}
	if (iofC.check(outputModelFileName, 0)) {
		cerr << "Prototype exists" << endl;
		return 1;
	}

	NeuralModel* model;
	int i;
	READMODEL(model, 1, modelFileName);

	LinearNCE* outputLayer;
	if (model->name == OVN_NCE) {
		outputLayer = static_cast<NgramModelNCE*>(model)->outputLayer;
	} else if (model->name == WTOVN_NCE) {
		outputLayer = static_cast<NgramWordTranslationModelNCE*>(model)->outputLayer;
	} else {
		cerr << "addNULLNCE.exe is not suitable with model type " << model->name << endl;
		cerr << "Use addNULL.exe instead" << endl;
		exit(1);
	}

	int NULLindex = model->inputVoc->index("NULL");

	if (NULLindex == ID_UNK) {
		model->inputVoc->add("NULL", model->inputVoc->wordNumber);
		floatTensor newWeight;
		floatTensor subW;

		newWeight.resize(model->baseNetwork->lkt->weight.size[0],
				model->baseNetwork->lkt->weight.size[1] + 1);
		newWeight = 0;
		subW.sub(newWeight, 0, model->baseNetwork->lkt->weight.size[0] - 1, 0,
				model->baseNetwork->lkt->weight.size[1] - 1);
		subW.copy(model->baseNetwork->lkt->weight);

		delete[] model->baseNetwork->lkt->weight.data;

		model->baseNetwork->lkt->weight.data = newWeight.data;
		newWeight.haveMemory = 0;
		model->baseNetwork->lkt->weight.size[1]++;
		model->baseNetwork->lkt->weight.length +=
				model->baseNetwork->lkt->weight.size[0];
	}
	NULLindex = model->outputVoc->index("NULL");
	if (NULLindex == ID_UNK) {
		model->outputVoc->add("NULL", model->outputVoc->wordNumber);

		floatTensor newWeight;
		floatTensor newBias;
		newWeight.resize(outputLayer->weight.size[0],
				outputLayer->weight.size[1] + 1);
		newWeight = 0;
		floatTensor subWeight;
		subWeight.sub(newWeight, 0, outputLayer->weight.size[0] - 1,
				0, outputLayer->weight.size[1] - 1);
		subWeight.copy(outputLayer->weight);
		delete[] outputLayer->weight.data;
		outputLayer->weight.data = newWeight.data;
		newWeight.haveMemory = 0;

		newBias.resize(outputLayer->bias.size[0],
				outputLayer->bias.size[1] + 1);
		newBias = 0;
		floatTensor subBias;
		subBias.sub(newBias, 0, outputLayer->bias.size[0] - 1, 0,
				outputLayer->bias.size[1] - 1);
		subBias.copy(outputLayer->bias);
		delete[] outputLayer->bias.data;
		outputLayer->bias.data = newBias.data;
		newBias.haveMemory = 0;

		outputLayer->weight.size[1]++;
		outputLayer->weight.length =
				outputLayer->weight.size[0]
						* outputLayer->weight.size[1];
		outputLayer->bias.size[0]++;
		outputLayer->bias.length++;
	}
	ioFile iof;
	iof.takeWriteFile(outputModelFileName);
	model->write(&iof, 1);
	delete model;
}

