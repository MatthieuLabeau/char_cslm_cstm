#include "mainModel.H"

NgramModel* readModel(int blockSize, char* sourceModelFileName) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramModel* sourceModel = new NgramModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize);
  return sourceModel;
}

NgramModelNCE* readModelNCE(int blockSize, char* sourceModelFileName) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramModelNCE* sourceModel = new NgramModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize);
  return sourceModel;
}

NgramCharModel* readCharModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModel* sourceModel = new NgramCharModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

NgramCharModelNCE* readCharModelNCE(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModelNCE* sourceModel = new NgramCharModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

int main(int argc, char *argv[]) {
	if (argc != 6) {
		cout << "modelFileName blockSize textFileName textType charWordHMap" << endl;
		cout
				<< "textType: n:normal(text), l:list of ngram (words), id:list of ngram (ids)"
				<< endl;
		return 0;
	} else {
	  time_t start, end;
	  char* modelFileName = argv[1];
	  int blockSize = atoi(argv[2]);
	  char* textFileName = argv[3];
	  string textType = argv[4];
	  char* charWordHMap = argv[5];

	  if (textType != "n" && textType != "l" && textType != "id"
	      && textType != "r") {
	    cerr << "What is textType?" << endl;
	    return 0;
	  }
	  
	  ioFile iofC;
	  if (!iofC.check(modelFileName, 1)) {
	    return 1;
	  }
	  if (!iofC.check(textFileName, 1)) {
	    return 1;
	  }
	  ioFile file;
	  string name = file.recognition(modelFileName);
	  NeuralModel* model;
	  if (name == CE || name == CWE || name == CWE_L) {
	    model = readCharModel(blockSize, modelFileName, charWordHMap);
	  } else if (name == CE_NCE || name == CWE_NCE || name == CWE_NCE_L 
		     || name == CE_CE_NCE || name == CWE_CE_NCE || name == CE_CWE_NCE
		     || name == CWE_CWE_NCE || name == LKT_CE_NCE || name == LKT_CWE_NCE) {
	    model = readCharModelNCE(blockSize, modelFileName, charWordHMap);
	  } else if (name == OVN_NCE || name == OVN_NCE_AG) {
	    model = readModelNCE(blockSize, modelFileName);
	  } else {
	    model = readModel(blockSize, modelFileName);
	  }
	  time(&start);
	  float perplexity = model->computePerplexity(model->dataSet, textFileName, textType);
	  float noUnkPerplexity = model->computePerplexity(model->noUnkDataSet, textFileName, textType);
	  time(&end);
	  /*
	  cout << "With model " << modelFileName << ", perplexity of "
	       << textFileName << " is " << perplexity << " (" << model->dataSet->ngramNumber
	       << " ngrams)" << endl;
	  cout << "Perplexity of "
	       << modelFileName << " with no unknown words is " << noUnkPerplexity << " ("
	       << model->noUnkDataSet->ngramNumber << " ngrams)" << endl;
	  cout << "Finish after " << difftime(end, start) / 60 << " minutes"
               << endl;
	  */
	  float unnmPerplexity;
	  float unnmNoUnkPerplexity;
	  if (name == OVN_NCE || name == OVN_NCE_AG || name == CE_NCE 
	      || name == CWE_NCE || name == CWE_NCE_L || name == CE_CE_NCE 
	      || name == CWE_CE_NCE || name == CE_CWE_NCE || name == CWE_CWE_NCE
	      || name == LKT_CE_NCE || name == LKT_CWE_NCE) {
	    static_cast<NgramModelNCE*>(model)->forwardUnnormalizedProbability(model->dataSet->dataTensor, model->dataSet->probTensor, 0);
	    model->dataSet->computePerplexity();
	    unnmPerplexity = model->dataSet->perplexity;
	    static_cast<NgramModelNCE*>(model)->forwardUnnormalizedProbability(model->noUnkDataSet->dataTensor, model->noUnkDataSet->probTensor, 0);
            model->noUnkDataSet->computePerplexity();
	    unnmNoUnkPerplexity = model->noUnkDataSet->perplexity;
	  } else if (name == WTOVN_NCE) {
	    static_cast<NgramWordTranslationModelNCE*>(model)->forwardUnnormalizedProbability(model->dataSet->dataTensor, model->dataSet->probTensor);
	    model->dataSet->computePerplexity();
	    unnmPerplexity = model->dataSet->perplexity;
	    static_cast<NgramWordTranslationModelNCE*>(model)->forwardUnnormalizedProbability(model->noUnkDataSet->dataTensor, model->noUnkDataSet->probTensor);
            model->noUnkDataSet->computePerplexity();
            unnmNoUnkPerplexity = model->noUnkDataSet->perplexity;
	  } else {
	    cerr << "Computation of unnormalized probabilities on "<< name << " is impossible" << endl;
	    exit(1);
	  }
	  /*
	  cout << "Unnormalized score on "
               << textFileName << " is " << unnmPerplexity << " (" << model->dataSet->ngramNumber
               << " ngrams)" << endl;
          cout << "Unnormalized score on "
               << modelFileName << " with no unknown words is " << unnmNoUnkPerplexity << " ("
               << model->noUnkDataSet->ngramNumber << " ngrams)" << endl;
	  */
	  char outputFile[STRING_SIZE];
	  string fileNamePath(modelFileName);
	  string fileName(modelFileName);
	  size_t pos = fileName.find_last_not_of("0123456789");
	  string iteration = fileName.substr(pos + 1);
	  string path = fileNamePath.erase(pos + 1);
	  
	  ofstream out;
	  strcpy(outputFile, path.c_str());
	  strcat(outputFile, "out.perp");
	  out.open(outputFile, ios::app);
	  out << iteration << " " << perplexity << endl;
	  out.close();

	  strcpy(outputFile, path.c_str());
          strcat(outputFile, "out.noUnkPerp");
          out.open(outputFile, ios::app);
	  out << iteration << " " << noUnkPerplexity << endl;
	  out.close();

	  strcpy(outputFile, path.c_str());
          strcat(outputFile, "out.unnmPerp");
          out.open(outputFile, ios::app);
	  out << iteration << " " << unnmPerplexity << endl;
	  out.close();

	  strcpy(outputFile, path.c_str());
          strcat(outputFile, "out.unnmNoUnkPerp");
          out.open(outputFile, ios::app);
	  out << iteration << " " << unnmNoUnkPerplexity << endl;
	  out.close();

	  delete model;
	}
	return 0;
}

