#include "mainModel.H"
#include "ioFile.H"

int main(int argc, char* argv[]) {
	if (argc != 6) {
		cout << "fileName outputFileName blockSize oldName newName" << endl;
		return 1;
	}
	char* fileName = argv[1];
	char* outputFileName = argv[2];
	ioFile modelFile;
	modelFile.takeReadFile(fileName);
	int blockSize = atoi(argv[3]);
	string oldName = argv[4];
	string newName = argv[5];
	NeuralModel* model;
	if (oldName == WTOVN || oldName == WTOVN_DISCRIM) {
		model = new NgramWordTranslationModel();
	} else if (oldName == OVN || oldName == OVN_DISCRIM || oldName == OVN_AG) {
		model = new NgramModel();
	} else if (oldName == WTOVN_NCE || oldName == WTOVN_NCE_DISCRIM) {
		model = new NgramWordTranslationModelNCE();
	} else {
		model = new NgramModel();
	}
	DEBUG(cout << "newName: " << newName << endl;)
	model->read(&modelFile, 1, blockSize);
	DEBUG(cout << "name: " << model->name << endl;)
	if (model->name != oldName) {
		cout << "Type is not good, the program will exit" << endl;
		return 1;
	} else {
		model->name = newName;

		// change the names of corresponding modules with AdaGrad
		if (model->name == OVN_AG) {
			// for test
			cout << "ovn2ovnb::main change to AG" << endl;
			Embeddings* tmp = model->baseNetwork->lkt;
			model->baseNetwork->lkt = new LookupTable_AG(tmp);
			delete tmp;
			for (int i = 0; i < model->baseNetwork->size; i +=
					model->hiddenStep) {
				Module* tmp = model->baseNetwork->modules[i];
				model->baseNetwork->modules[i] = new Linear_AG(tmp);
				delete tmp;
			}
			for (int i = 0; i < model->outputNetworkNumber; i++) {
				ProbOutput* tmp = model->outputNetwork[i];
				model->outputNetwork[i] = new LinearSoftmax_AG(tmp);
				delete tmp;
			}
		}

		// change output network to DISCRIM if necessary
		if (oldName == WTOVN && newName == WTOVN_DISCRIM) {
			// for test
			cout << "ovn2ovnb::main change to DISCRIM" << endl;
			for (int i = 0; i < model->outputNetworkNumber; i++) {
				ProbOutput* tmp = model->outputNetwork[i];
				model->outputNetwork[i] = new LinearSoftmax_discrim(tmp);
				delete tmp;
			}
		}
	}
	ioFile outputFile;
	outputFile.takeWriteFile(outputFileName);
	model->write(&outputFile, 1);
	delete model;
	return 0;
}
