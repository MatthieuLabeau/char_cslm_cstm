#include "mainModel.H"
void getHiddenCode(char* input, intTensor& outputTensor) {
	char hidden[260];
	strcpy(hidden, input);
	int hiddenNumber = 1;
	for (int i = 0; i < strlen(input); i++) {
		if (hidden[i] == '_') {
			hidden[i] = ' ';
			hiddenNumber++;
		}
	}
	string strHidden = hidden;
	istringstream streamHidden(strHidden);
	string word;
	outputTensor.resize(hiddenNumber, 1);
	hiddenNumber = 0;
	while (streamHidden >> word) {
		outputTensor(hiddenNumber) = atoi(word.c_str());
		hiddenNumber++;
	}
}

NeuralModel* readModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NeuralModel* sourceModel;
  if (name_sourceModel == OVN || name_sourceModel == OVN_AG || name_sourceModel == ROVN 
      || name_sourceModel == CN || name_sourceModel == OVNB || name_sourceModel == MAXOVN
      || name_sourceModel == LBL) {
    sourceModel = new NgramModel();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize);
  } else if (name_sourceModel == OVN_NCE || name_sourceModel == OVN_NCE_AG) {
    sourceModel = new NgramModelNCE();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize);
  } else if (name_sourceModel == CE || name_sourceModel == CWE 
	     || name_sourceModel == CWE_L || name_sourceModel == CE_AG) {
    sourceModel = new NgramCharModel();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  } else if (name_sourceModel == CE_NCE || name_sourceModel == CWE_NCE || name_sourceModel == CWE_NCE_L
	     || name_sourceModel == CE_CE_NCE || name_sourceModel == CWE_CE_NCE || name_sourceModel == LKT_CE_NCE
	     || name_sourceModel == LKT_CWE_NCE || name_sourceModel == CE_CWE_NCE || name_sourceModel == CWE_CWE_NCE
	     || name_sourceModel == CE_NCE_AG || name_sourceModel == CE_CE_NCE_AG || name_sourceModel == CE_H_NCE_AG
	     || name_sourceModel == CE_CWE_NCE_AG || name_sourceModel == CWE_NCE_AG || name_sourceModel == LKT_CWE_NCE_AG 
	     || name_sourceModel == CWE_CWE_NCE_AG) {
    sourceModel = new NgramCharModelNCE();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  }
  return sourceModel;
}

int main(int argc, char *argv[]) {
  //Create model
  if (argc != 16 && argc != 12) {
    cout << "type charVocFileName inputVocFileName outputVocFileName charWordMapFileName mapIUnk mapOUnk"
	 << " n dimensionSize charDimensionSize nonLinearType"
	 << " hiddenLayerSizeCode codeWordFileName outputNetworkSizeFileName outputModelFileName" << endl;
    cout << " or in case of pre-training : " << endl;
    cout << "type PreTrainedModel charVocFileName inputVocFileName outputVocFileName charWordMapFileName"
	 << " charDimensionSize nonLinearType"
	 << " codeWordFileName outputNetworkSizeFileName outputModelFileName"
	 << endl;
    return 1;
  } else if (argc == 16) {
    srand48(time(NULL));
    srand(time(NULL));
    //NeuralModel* modelPrototype;
    string name = argv[1];
  
    char* charVocFileName = argv[2];
    char* contextVocFileName = argv[3];
    char* predictVocFileName = argv[4];
    char* charWordMapFileName = argv[5];
    int mapIUnk = atoi(argv[6]);
    int mapOUnk = atoi(argv[7]);
    int n = atoi(argv[8]);
    int BOS = n - 1;
    int charDimensionSize = atoi(argv[9]);
    int dimensionSize = atoi(argv[10]);
	  
    string nonLinearType = argv[11];
    if (nonLinearType != TANH && nonLinearType != SIGM
	&& nonLinearType != LINEAR && nonLinearType != RELU)  {
      cerr << "Which activation do you want?" << endl;
      return 1;
    }
    
    char* hiddenLayerSizeCode = argv[12];
    char* codeWordFileName = argv[13];
    char* outputNetworkSizeFileName = argv[14];
    char* outputModelFileName = argv[15];
    int blockSize = 1;
    ioFile iof;
    if (!iof.check(contextVocFileName, 1)) {
      return 1;
    }
    if (!iof.check(predictVocFileName, 1)) {
      return 1;
    }
    if (strcmp(codeWordFileName, "xxx")) {
      if (!iof.check(codeWordFileName, 1)) {
	return 1;
      }
    }
    if (strcmp(outputNetworkSizeFileName, "xxx")) {
      if (!iof.check(outputNetworkSizeFileName, 1)) {
	return 1;
      }
    }
    if (iof.check(outputModelFileName, 0)) {
      cerr << "Prototype exists" << endl;
      return 1;
    }
    intTensor hiddenLayerSizeArray;
    getHiddenCode(hiddenLayerSizeCode, hiddenLayerSizeArray);
    ioFile mIof;
    if (name == CE_NCE || name == CWE_NCE || name == CWE_NCE_L || name == CE_CE_NCE || name == CWE_CE_NCE 
	|| name == LKT_CE_NCE || name == LKT_CWE_NCE || name == CE_CWE_NCE || name == CWE_CWE_NCE
	|| name == CE_NCE_AG || name == CE_H_NCE_AG || name == CE_CE_NCE_AG || name == CE_CWE_NCE_AG
	|| name == CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
      // nce training
      NgramCharModelNCE* modelPrototype = new NgramCharModelNCE(name, charWordMapFileName, charVocFileName, contextVocFileName,
								predictVocFileName, mapIUnk, mapOUnk, BOS, blockSize, n,
								charDimensionSize, dimensionSize, nonLinearType, hiddenLayerSizeArray);
      mIof.takeWriteFile(outputModelFileName);
      modelPrototype->write(&mIof, 1);
      mIof.freeWriteFile();
      delete modelPrototype;
    } else if (name == CE || name == CWE || name == CWE_L || name == CE_AG) {
      NgramCharModel* modelPrototype = new NgramCharModel(name, charWordMapFileName, charVocFileName, contextVocFileName,
							  predictVocFileName, mapIUnk, mapOUnk, BOS, blockSize, n,
							  charDimensionSize, dimensionSize, nonLinearType, hiddenLayerSizeArray,
							  codeWordFileName, outputNetworkSizeFileName);
      mIof.takeWriteFile(outputModelFileName);
      modelPrototype->write(&mIof, 1);
      mIof.freeWriteFile();
      delete modelPrototype;
    } else if (name == OVN_NCE || name == OVN_NCE_AG) {
      // nce training
      NgramModelNCE* modelPrototype = new NgramModelNCE(name, contextVocFileName,
							predictVocFileName, mapIUnk, mapOUnk, BOS, blockSize, n,
							dimensionSize, nonLinearType, hiddenLayerSizeArray);
      mIof.takeWriteFile(outputModelFileName);
      modelPrototype->write(&mIof, 1);
      mIof.freeWriteFile();
      delete modelPrototype;
    } else {
      NgramModel* modelPrototype = new NgramModel(name, contextVocFileName,
						  predictVocFileName, mapIUnk, mapOUnk, BOS, blockSize, n,
						  dimensionSize, nonLinearType, hiddenLayerSizeArray,
						  codeWordFileName, outputNetworkSizeFileName);
      mIof.takeWriteFile(outputModelFileName);
      modelPrototype->write(&mIof, 1);
      mIof.freeWriteFile();
      delete modelPrototype;
      
    }
    return 0;
  } else if (argc == 12) {
    time_t start, end;
    string name = argv[1];
    char* preTrainedModelFileName = argv[2];
    char* charVocFileName = argv[3];
    char* contextVocFileName = argv[4];
    char* predictVocFileName = argv[5];
    char* charWordHMap = argv[6];
    int charDimensionSize = atoi(argv[7]);
    string nonLinearType = argv[8];
    if (nonLinearType != TANH && nonLinearType != SIGM
        && nonLinearType != LINEAR && nonLinearType != RELU)  {
      cerr << "Which activation do you want?" << endl;
      return 1;
    }
    char* codeWordFileName = argv[9];
    char* outputNetworkSizeFileName = argv[10];
    char* outputModelFileName = argv[11];

    ioFile iofC;
    if (!iofC.check(preTrainedModelFileName, 1)) {
      return 1;
    }
    if (!iofC.check(contextVocFileName, 1)) {
      return 1;
    }
    if (!iofC.check(predictVocFileName, 1)) {
      return 1;
    }
    if (strcmp(charVocFileName, "xxx")) {
      if (!iofC.check(charVocFileName, 1)) {
        return 1;
      }
    }
    if (strcmp(charWordHMap, "xxx")) {
      if (!iofC.check(charWordHMap, 1)) {
        return 1;
      }
    }
    if (strcmp(codeWordFileName, "xxx")) {
      if (!iofC.check(codeWordFileName, 1)) {
        return 1;
      }
    }
    if (strcmp(outputNetworkSizeFileName, "xxx")) {
      if (!iofC.check(outputNetworkSizeFileName, 1)) {
        return 1;
      }
    }
    if (iofC.check(outputModelFileName, 0)) {
      cerr << "Prototype exists" << endl;
      return 1;
    }

    NeuralModel* preTrainedModel = readModel(1, preTrainedModelFileName, charWordHMap);
    ioFile mIof;
    if (name == OVN_NCE || name == OVN_NCE_AG) {
      // NCE training from pre-trained non-NCE model
      NgramModelNCE* modelPrototype = new NgramModelNCE(name, contextVocFileName,
                                                        predictVocFileName, preTrainedModel->mapIUnk, preTrainedModel->mapOUnk,
							preTrainedModel->BOS, preTrainedModel->blockSize, preTrainedModel->n,
                                                        preTrainedModel->dimensionSize, nonLinearType, preTrainedModel->hiddenLayerSizeArray);
      //Lookup Table
      modelPrototype->baseNetwork->lkt->weight.copy(preTrainedModel->baseNetwork->lkt->weight);
      //Hidden Layers
      if (preTrainedModel->name == OVN || preTrainedModel->name == OVN_AG) {
	for (int i = 0; i < preTrainedModel->baseNetwork->size; i=i+2) {
	  modelPrototype->baseNetwork->modules[i]->weight.copy(preTrainedModel->baseNetwork->modules[i]->weight);
	  modelPrototype->baseNetwork->modules[i]->bias.copy(preTrainedModel->baseNetwork->modules[i]->bias);
	}
      }
      //Output Layer : three solutions
      //Initialized randomly: do nothing
      //Initialize bias from unigram data: maybe call other command ? 
      //Initialize from pre-trained model
      if (preTrainedModel->outputVoc->wordNumber == modelPrototype->outputVoc->wordNumber) {
	modelPrototype->outputLayer->weight.copy(preTrainedModel->outputNetwork[0]->weight);
	modelPrototype->outputLayer->bias.copy(preTrainedModel->outputNetwork[0]->bias);
      } else if (preTrainedModel->outputVoc->wordNumber < modelPrototype->outputVoc->wordNumber) {
	floatTensor selectWeight;
	selectWeight.sub(modelPrototype->outputLayer->weight, 0, modelPrototype->outputLayer->weight.size[0] - 1, 0, preTrainedModel->outputVoc->wordNumber - 1);
	selectWeight.copy(preTrainedModel->outputNetwork[0]->weight);
	floatTensor selectBias;
	selectBias.sub(modelPrototype->outputLayer->bias, 0, preTrainedModel->outputVoc->wordNumber - 1, 0, 0);
	selectBias.copy(preTrainedModel->outputNetwork[0]->bias);
      }
      mIof.takeWriteFile(outputModelFileName);
      modelPrototype->write(&mIof, 1);
      mIof.freeWriteFile();
      delete modelPrototype;
    }
    if (name == CE_H_NCE || name == CE_H_NCE_AG) {
      NgramCharModelNCE* modelPrototype = new NgramCharModelNCE(name, charWordHMap, charVocFileName, 
								contextVocFileName, predictVocFileName,
								preTrainedModel->mapIUnk, preTrainedModel->mapOUnk, 
								preTrainedModel->BOS, preTrainedModel->blockSize, preTrainedModel->n,
								charDimensionSize, preTrainedModel->dimensionSize,
								nonLinearType, preTrainedModel->hiddenLayerSizeArray);
      HybridNCE* out_h = static_cast<HybridNCE*>(modelPrototype->outputLayer);
      NgramCharModelNCE* preTrainedModelNCE = static_cast<NgramCharModelNCE*>(preTrainedModel);     
      out_h->setFrequentEmbeddings(preTrainedModelNCE->outputLayer->weight, preTrainedModelNCE->outputLayer->bias);

      //Add char weight and word weight to model
      // Copy Char Embeddings
      CharEmbeddings* clkt = static_cast<CharEmbeddings*>(preTrainedModelNCE->baseNetwork->lkt);
      CharEmbeddings* in_clkt = static_cast<CharEmbeddings*>(modelPrototype->baseNetwork->lkt);
      in_clkt->cLkt->weight.copy(clkt->cLkt->weight);
      in_clkt->convModule->weight.copy(clkt->convModule->weight);
      in_clkt->convModule->bias.copy(clkt->convModule->bias);

      // Copy hidden weight
      for (int i = 0; i < modelPrototype->baseNetwork->size; i=i+2) {
	modelPrototype->baseNetwork->modules[i]->weight.copy(preTrainedModelNCE->baseNetwork->modules[i]->weight);
	modelPrototype->baseNetwork->modules[i]->bias.copy(preTrainedModelNCE->baseNetwork->modules[i]->bias);
      }

      mIof.takeWriteFile(outputModelFileName);
      modelPrototype->write(&mIof, 1);
      mIof.freeWriteFile();
      delete modelPrototype;
    }
    return 0;
  }
}

