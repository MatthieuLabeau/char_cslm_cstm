#include "mainModel.H"

NgramModel* readModel(int blockSize, char* sourceModelFileName) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramModel* sourceModel = new NgramModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize);
  return sourceModel;
}

NgramModelNCE* readModelNCE(int blockSize, char* sourceModelFileName) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramModelNCE* sourceModel = new NgramModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize);
  return sourceModel;
}

NgramCharModel* readCharModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;					       
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);	
  cout << name_sourceModel << endl;			      
  NgramCharModel* sourceModel = new NgramCharModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);           
  cout << sourceModelFileName << endl;                          
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

NgramCharModelNCE* readCharModelNCE(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModelNCE* sourceModel = new NgramCharModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

int sequenceTrain(char* prefixModel, char* prefixData, int maxExampleNumber,
		  char* trainingFileName, char* validationFileName, string validType,
		  string learningRateType, int minIteration, int maxIteration,
		  int update_lkt, string update_hiddens, int update_output_layer,
		  char* charWordHMap, float e, float cache) {
  outils otl;
  int write_model = 1;
  char inputModelFileName[260];
  char convertStr[260];
  int iteration;
  int gz = 0;
  for (iteration = maxIteration; iteration >= minIteration - 2; iteration--) {
    sprintf(convertStr, "%d", iteration);
    strcpy(inputModelFileName, prefixModel);
    strcat(inputModelFileName, convertStr);
    ioFile iof;
    if (!iof.check(inputModelFileName, 0)) {
      strcat(inputModelFileName, ".gz");
      if (iof.check(inputModelFileName, 0)) {
	gz = 1;
	break;
      }
    } else {
      gz = 0;
      break;
    }
  }
  if (iteration == minIteration - 3) {
    cerr << "Can not find training model " << minIteration - 1 << endl;
    return 1;
  } else if (iteration == maxIteration) {
    cerr << "All is done" << endl;
    return 1;
  }

  sprintf(convertStr, "%d", iteration);
  strcpy(inputModelFileName, prefixModel);
  strcat(inputModelFileName, convertStr);
  if (gz) {
    strcat(inputModelFileName, ".gz");
  }
  ioFile file;
  string name = file.recognition(inputModelFileName);
  if (name == JWTOVN) {
    MultiplesNeuralModel* model;
    READMODEL_MULTIPLE(model, 0, inputModelFileName);
    model->sequenceTrain(prefixModel, gz, prefixData, maxExampleNumber,
			 trainingFileName, validationFileName, validType,
			 learningRateType, iteration + 1, maxIteration, update_lkt,
			 update_hiddens, update_output_layer);
    delete model;
    return 0;
  } else if (name == CE || name == CWE || name == CWE_L || name == CE_H || name == CE_AG) {
    NgramCharModel* model = readCharModel(0, inputModelFileName, charWordHMap);
    model->sequenceTrain(prefixModel, gz, prefixData, maxExampleNumber,
			 trainingFileName, validationFileName, validType,
			 learningRateType, iteration + 1, maxIteration, update_lkt,
			 update_hiddens, update_output_layer, write_model, charWordHMap, e, cache);
    delete model;
    return 0;
  } else if (name == CE_NCE || name == CWE_NCE || name == CWE_NCE_L || name == CE_CE_NCE || name == CWE_CE_NCE
	     || name == LKT_CE_NCE || name == LKT_CWE_NCE || name == CE_CWE_NCE || name == CWE_CWE_NCE 
	     || name == CE_H_NCE || name == CE_NCE_AG || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	     || name == CE_CWE_NCE_AG || name == CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    NgramCharModelNCE* model = readCharModelNCE(0, inputModelFileName, charWordHMap);
    model->sequenceTrain(prefixModel, gz, prefixData, maxExampleNumber,
                         trainingFileName, validationFileName, validType,
                         learningRateType, iteration + 1, maxIteration, update_lkt,
                         update_hiddens, update_output_layer, write_model, charWordHMap, e, cache); 
    delete model;
    return 0;
  } else if (name == OVN_NCE || name == OVN_NCE_AG) {
    NgramModelNCE* model = readModelNCE(0, inputModelFileName);
    model->sequenceTrain(prefixModel, gz, prefixData, maxExampleNumber,
			 trainingFileName, validationFileName, validType,
			 learningRateType, iteration + 1, maxIteration, update_lkt,
			 update_hiddens, update_output_layer, write_model, charWordHMap, e, cache);
    delete model;
    return 0;
  } else {
    NgramModel* model = readModel(0, inputModelFileName);
    model->sequenceTrain(prefixModel, gz, prefixData, maxExampleNumber,
                         trainingFileName, validationFileName, validType,
                         learningRateType, iteration + 1, maxIteration, update_lkt,
                         update_hiddens, update_output_layer, write_model, charWordHMap, e, cache);
    delete model;
    return 0;
  }
}

int main(int argc, char *argv[]) {
  if (argc != 14 && argc != 16) {
    cout
      << "prefixModel prefixData maxExampleNumber trainingFileName, validationFileName validType learningRateType minIteration maxIteration update_lkt update_hiddens update_output_layer charWordHMap"
      << endl;
    cout << "validType: n(normal-text), l(ngram list), id (binary id ngram)"
	 << endl;
    return 0;
  }
  char* prefixModel = argv[1];
  char* prefixData = argv[2];
  int maxExampleNumber = atoi(argv[3]);
  char* trainingFileName = argv[4];
  char* validationFileName = argv[5];
  string validType = argv[6];
  if (validType != "n" && validType != "l" && validType != "id")

    {
      cerr << "Which validType do you want?" << endl;
      return 1;
    }

  string learningRateType = argv[7];
  if (learningRateType != LEARNINGRATE_NORMAL
      && learningRateType != LEARNINGRATE_DOWN
      && learningRateType != LEARNINGRATE_ADJUST
      && learningRateType != LEARNINGRATE_AG
      && learningRateType != LEARNINGRATE_CAG
      && learningRateType != LEARNINGRATE_BAG
      && learningRateType != LEARNINGRATE_DBAG
      && learningRateType != LEARNINGRATE_NCE
      && learningRateType != LEARNINGRATE_NCE_ADJUST
      && learningRateType != LEARNINGRATE_NCE_BAG) {
    cerr << "Which learningRateType do you want?" << endl;
    return 1;
  }
  int minIteration = atoi(argv[8]);
  int maxIteration = atoi(argv[9]);
  int update_lkt = atoi(argv[10]);
  string update_hiddens = string(argv[11]);
  int update_output_layer = atoi(argv[12]);
  char* charWordHMap = argv[13];
  float e;
  float cache;
  if (argc == 16) {
    e = atof(argv[14]);
    cache = atof(argv[15]);
  } else {
    e = 0.000001;
    cache = 0.95;
  }
  srand(time(NULL));
  sequenceTrain(prefixModel, prefixData, maxExampleNumber, trainingFileName,
		validationFileName, validType, learningRateType, minIteration,
		maxIteration, update_lkt, update_hiddens, update_output_layer,
		charWordHMap, e, cache);
  return 0;
}

