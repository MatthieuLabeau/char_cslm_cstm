#include "mainModel.H"

NeuralModel* readModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NeuralModel* sourceModel;
  if (name_sourceModel == OVN || name_sourceModel == OVN_AG || name_sourceModel == ROVN
      || name_sourceModel == CN || name_sourceModel == OVNB || name_sourceModel == MAXOVN
      || name_sourceModel == LBL) {
    sourceModel = new NgramModel();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize);
  } else if (name_sourceModel == OVN_NCE || name_sourceModel == OVN_NCE_AG) {
    sourceModel = new NgramModelNCE();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize);
  } else if (name_sourceModel == CE || name_sourceModel == CWE || name_sourceModel == CWE_L) {
    sourceModel = new NgramCharModel();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  } else if (name_sourceModel == CE_NCE || name_sourceModel == CWE_NCE || name_sourceModel == CWE_NCE_L
             || name_sourceModel == CE_CE_NCE || name_sourceModel == CWE_CE_NCE || name_sourceModel == LKT_CE_NCE
             || name_sourceModel == LKT_CWE_NCE || name_sourceModel == CE_CWE_NCE || name_sourceModel == CWE_CWE_NCE) {
    sourceModel = new NgramCharModelNCE();
    iof_sourceModel.takeReadFile(sourceModelFileName);
    cout << sourceModelFileName << endl;
    sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  }
  return sourceModel;
}

int main(int argc, char *argv[]) {
  if (argc != 5) {
                cout
		  << "modelFileName blockSize resampledFileName charWordHMap"
		  << endl;
                return 0;
  } else {
    time_t start, end;
    char* modelFileName = argv[1];
    int blockSize = atoi(argv[2]);
    char* validFileName = argv[3];
    char* charWordHMap = argv[4];

    ioFile iofC;
    if (!iofC.check(modelFileName, 1)) {
      return 1;
    }
    if (!iofC.check(validFileName, 1)) {
      return 1;
    }
    if (strcmp(charWordHMap, "xxx")) {
      if (!iofC.check(charWordHMap, 1)) {
        return 1;
      }
    }

    NeuralModel* model = readModel(blockSize, modelFileName, charWordHMap);
    if (model->name == OVN_NCE || model->name == OVN_NCE_AG) {
      time(&start);
      NgramModelNCE* modelNCE = static_cast<NgramModelNCE*>(model);
      modelNCE->monitorNCE(validFileName, modelFileName, 1);
      time(&end);
      cout << "Finish after " << difftime(end, start) / 60 << " minutes"
	   << endl;
      delete model;
    }
    return 0;
  }
}
