/*
 * text2Prob_ngram.cc
 *
 *  Created on: Oct 29, 2015
 *      Author: dokhanh
 */

#include "mainModel.H"

int main(int argc, char *argv[]) {
	if (argc != 8) {
		cout
				<< "modelFileName blockSize incrUnk textFileName file_fprobs file_ngramNumber_for_each_hypo normalize"
				<< endl;
		return 0;
	} else {
		time_t start, end;
		// for text2Prob_ngram, addAuxil = 0
		int addAuxil = 0;
		char* modelFileName = argv[1];
		int blockSize = atoi(argv[2]);
		float incrUnk = pow(10, atof(argv[3]));
		char* textFileName = argv[4];
		char* outputFileName = argv[5];
		char* file_ngramNumber_name = argv[6];
		int normalize = atoi(argv[7]);
		ioFile iofC;
		if (!iofC.check(modelFileName, 1)) {
			return 1;
		}
		if (!iofC.check(textFileName, 1)) {
			return 1;
		}
		if (iofC.check(outputFileName, 0)) {
			cerr << "prob file exists" << endl;
			return 1;
		}

		NeuralModel* model;
		READMODEL(model, blockSize, modelFileName);
		model->incrUnk = incrUnk;
		time(&start);

		ioFile iof;
		iof.format = TEXT;
		iof.takeReadFile((char*) textFileName);
		ioFile iofO;
		iofO.format = TEXT;
		iofO.takeWriteFile(outputFileName);
		ioFile iof_ngramNumber;
		iof_ngramNumber.format = TEXT;
		iof_ngramNumber.takeWriteFile(file_ngramNumber_name);
		int readLineNumber = 0;
		int old_ngram_number = 0;
		int current_ngram_number = 0;

		char * maxNgramNumberEnv;
		int maxNgramNumber = BLOCK_NGRAM_NUMBER;
		maxNgramNumberEnv = getenv("BLOCK_NGRAM_NUMBER");
		if (maxNgramNumberEnv != NULL) {
			maxNgramNumber = atoi(maxNgramNumberEnv);
		}

		while (!iof.getEOF()) {
			old_ngram_number = model->dataSet->ngramNumber;
			model->dataSet->addLine(&iof, addAuxil);
			readLineNumber++;
			// write ngram number
			current_ngram_number = model->dataSet->ngramNumber
					- old_ngram_number;
			if (current_ngram_number != 0) {
				iof_ngramNumber.writeInt(current_ngram_number);
			}
			// for test
			//cout << "text2Prob::main readLineNumber: " << readLineNumber << endl;
			//model->dataSet->writeReBiNgram();
#if PRINT_DEBUG
			if (readLineNumber % NLINEPRINT == 0 && readLineNumber != 0) {
				cout << readLineNumber << " ... " << flush;
			}
#endif
			if (model->dataSet->ngramNumber
					> maxNgramNumber - MAX_WORD_PER_SENTENCE) {
				cout << "Compute " << model->dataSet->ngramNumber << " ngrams"
						<< endl;
				model->dataSet->createTensor();
//				forwardProbability(model, normalize);
				model->forwardProbability(model->dataSet->dataTensor,
						model->dataSet->probTensor, normalize);
				for (int i = 0; i < model->dataSet->probTensor.length; i++) {
					iofO.writeFloat(model->dataSet->probTensor(i));
				}
				model->dataSet->reset();
			}
		}
		if (model->dataSet->ngramNumber != 0) {
			cout << "Compute with " << model->dataSet->ngramNumber << " ngrams"
					<< endl;
			model->dataSet->createTensor();
//			forwardProbability(model, normalize);
			model->forwardProbability(model->dataSet->dataTensor,
					model->dataSet->probTensor, normalize);
			for (int i = 0; i < model->dataSet->probTensor.length; i++) {
				iofO.writeFloat(model->dataSet->probTensor(i));
			}
		}

#if PRINT_DEBUG
		cout << endl;
#endif
		time(&end);
		cout << "Finish after " << difftime(end, start) / 60 << " minutes"
				<< endl;
		delete model;
	}
	return 0;
}
