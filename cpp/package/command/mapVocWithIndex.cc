/*
 * mapVocWithIndex.cc
 *
 *  Created on: Sep 16, 2014
 *      Author: dokhanh
 */

#include "mainModel.H"
int main(int argc, char *argv[]) {
	if (argc != 6) {
		cerr << "vocFileName baseVocFileName indexFileName mapOutputFileName defaultFreq"
				<< endl;
		return 1;
	}
	char* vocFileName = argv[1];
	char* baseVocFileName = argv[2];
	char* indexFileName = argv[3];
	char* mapOutputFileName = argv[4];
	string defaultFreq = argv[5];

	ioFile iof;
	if (!iof.check(vocFileName, 1)) {
		return 1;
	}
	if (!iof.check(baseVocFileName, 1)) {
		return 1;
	}
	if (!iof.check(indexFileName, 1)) {
		return 1;
	}
	if (iof.check(mapOutputFileName, 0)) {
		cerr << "mapOutput file exists" << endl;
		return 1;
	}

	SoulVocab* baseVoc = new SoulVocab(baseVocFileName, indexFileName, 0);
	iof.format = TEXT;
	iof.takeReadFile(vocFileName);
	iof.takeWriteFile(mapOutputFileName);
	string line;
	string index;
	string df("xxx");
	while (!iof.getEOF()) {
		if (iof.getLine(line)) {
			index = baseVoc->acp(line);
			if (index.compare(df) == 0) {
				index = defaultFreq;
			}
			iof.writeString(index);
		}
	}
	delete baseVoc;
}
