#include "mainModel.H"
#include <boost/lexical_cast.hpp>
string2CCharFunction

void forwardProbability(NeuralModel* model) {
	if (model->name == WTOVN_DISCRIM) {
		model->forwardProbability(model->dataSet->dataTensor,
				model->dataSet->probTensor);
	} else if (model->name == WTOVN_NCE_DISCRIM) {
		static_cast<NgramWordTranslationModelNCE*>(model)->efficientForwardUnnormalizedProbability(
				model->dataSet->dataTensor, model->dataSet->probTensor);
	} else {
		cerr
				<< "sequenceTrain_nbestList::forwardProbability model name generates unpredictable behaviors : "
				<< model->name << endl;
		exit(1);
	}
}

int text2Prob_hypo(NeuralModel* model, int blockSize, int inUnk,
		char* tupleFileName, char* fprobs_out, char* ngramNumber_out) {
	// always
	int addAuxil = 1;
	ioFile iofC;
	if (!iofC.check(tupleFileName, 1)) {
		return 1;
	}
	if (iofC.check(fprobs_out, 0)) {
		cerr << "prob file exists" << endl;
		return 1;
	}
	if (model->dataSet->ngramNumber != 0) {
		model->dataSet->reset();
	}
	if (model->blockSize != blockSize) {
		model->changeBlockSize(blockSize);
	}
	model->incrUnk = inUnk;
	time_t start, end;
	time(&start);
	ioFile iof;
	iof.format = TEXT;
	iof.takeReadFile(tupleFileName);
	ioFile iofO;
	iofO.format = TEXT;
	iofO.takeWriteFile(fprobs_out);
	ioFile iof_ngramNumber;
	iof_ngramNumber.format = TEXT;
	iof_ngramNumber.takeWriteFile(ngramNumber_out);
	int readLineNumber = 0;
	int old_ngram_number = 0;
	int current_ngram_number = 0;

	char * maxNgramNumberEnv;
	int maxNgramNumber = BLOCK_NGRAM_NUMBER;
	maxNgramNumberEnv = getenv("BLOCK_NGRAM_NUMBER");
	if (maxNgramNumberEnv != NULL) {
		maxNgramNumber = atoi(maxNgramNumberEnv);
	}

	while (!iof.getEOF()) {
		old_ngram_number = model->dataSet->ngramNumber;
		model->dataSet->addLine(&iof, addAuxil);
		readLineNumber++;
// write ngram number
		current_ngram_number = model->dataSet->ngramNumber - old_ngram_number;
		if (current_ngram_number > 0) {
			iof_ngramNumber.writeInt(current_ngram_number);
		}

#if PRINT_DEBUG
		if (readLineNumber % NLINEPRINT == 0 && readLineNumber != 0) {
			cout << readLineNumber << " ... " << flush;
		}
#endif
		if (model->dataSet->ngramNumber > maxNgramNumber - MAX_WORD_PER_SENTENCE) {
			cout << "Compute " << model->dataSet->ngramNumber << " ngrams"
					<< endl;
			model->dataSet->createTensor();
			forwardProbability(model);
			for (int i = 0; i < model->dataSet->probTensor.length; i++) {
				iofO.writeFloat(model->dataSet->probTensor(i));
			}
			model->dataSet->reset();
		}
	}
	if (model->dataSet->ngramNumber != 0) {
		cout << "Compute with " << model->dataSet->ngramNumber << " ngrams"
				<< endl;
		model->dataSet->createTensor();
		forwardProbability(model);
		for (int i = 0; i < model->dataSet->probTensor.length; i++) {
			iofO.writeFloat(model->dataSet->probTensor(i));
		}
	}

#if PRINT_DEBUG
	cout << endl;
#endif
	time(&end);
	cout << "Finish after " << difftime(end, start) / 60 << " minutes" << endl;
	model->dataSet->reset();
	return 0;
}

int wc(char* fileName) {
	ioFile file;
	file.format = TEXT;
	file.takeReadFile(fileName);
	int nb_line = 0;
	string line;
	while (!file.getEOF()) {
		if (file.getLine(line)) {
			nb_line += 1;
		}
	}
	return nb_line;
}

int run_exe_mira(char* file_nbest, char* file_score_soul, char* command_dir,
		char* dir, char* weights_out) {
	ioFile iofC;
	string cmd;
	string refName = string(file_nbest) + ".ref";
	if (!iofC.check(string2CChar(refName), 1)) {
		cerr << "sequenceTrain_nbestList::run_exe_mira " << refName
				<< " does not exist" << endl;
		return 1;
	}
	string weights_link = string(dir) + "/0/weights";
	if (iofC.check(string2CChar(weights_link), 0)) {
		cout << "sequenceTrain_nbestList::run_exe_mira " << weights_link
				<< " exists" << endl;
		cmd = "cp " + weights_link + " " + string(weights_out);
		cout << cmd << endl;
		system(cmd.c_str());
		return 1;
	}
	cmd = "exe_mira.sh " + string(file_nbest) + " " + refName + " "
			+ string(file_score_soul) + " " + string(command_dir) + " "
			+ string(dir) + " " + string(weights_out);
	cout << cmd << endl;
	system(cmd.c_str());
	return 0;
}

int run_score_nn_nbest(char* fprobs_file_name, char* ngramNumber_file_name,
		char* score_soul_file_name) {
	string cmd = "score_nn_nbest.py " + string(fprobs_file_name) + " "
			+ string(ngramNumber_file_name) + " "
			+ string(score_soul_file_name);
	cout << cmd << endl;
	system(cmd.c_str());
	return 0;
}

int run_score_ncode_nbest(char* file_nbest, char* file_score_soul,
		char* file_weights, char* file_ncode_soul) {
	string cmd = "score_ncode_nbest.py " + string(file_nbest) + " "
			+ string(file_score_soul) + " " + string(file_weights) + " "
			+ string(file_ncode_soul);
	cout << cmd << endl;
	system(cmd.c_str());
	return 0;
}

int run_ranking_hypothesis_mm(char* index_file, char* sBLEU_file,
		char* score_soul_file, char* rankingHyp_out, float rate_pos,
		char* file_tuple, char* file_tuple_coef, char* file_tuple_coef_dest,
		float alpha, char* obj_fun_type) {
	string cmd_ranking = "ranking_hypothesis_mm.py " + string(index_file) + " "
			+ string(sBLEU_file) + " " + string(score_soul_file) + " "
			+ string(rankingHyp_out) + " "
			+ boost::lexical_cast<string>(rate_pos) + " " + string(file_tuple)
			+ " " + string(file_tuple_coef) + " " + string(file_tuple_coef_dest)
			+ " " + boost::lexical_cast<string>(alpha) + " "
			+ string(obj_fun_type);
	cout << cmd_ranking << endl;
	system(cmd_ranking.c_str());
	return 0;
}

float rankingHyp_eval(char* rankingHyp_file, char* obj_fun_type) {
	ioFile ranking_file;
	ranking_file.format = TEXT;
	ranking_file.takeReadFile(rankingHyp_file);
	string line;
	string word;
	float sum = 0;
	int read_nsca = 0;
	int read_ns = 0;
	int read_p = 0;
	int read_coef = 0;
	int read_cls_sBLEU = 0;
	int read_rp = 0;
	int read_sbp = 0;
	int read_xbleu = 0;
	float nsca = 0;
	float ns = 0;
	float p = 0;
	float coef = 0;
	int cls_sBLEU = 0;
	float rp = 0;
	float sbp = 0;
	float xbleu = 0;
	int nb_line = 0;
	while (!ranking_file.getEOF()) {
		if (ranking_file.getLine(line)) {

			istringstream streamLine(line);
			while (streamLine >> word) {
				if (read_p == 1) {
					p = (float) atof(word.c_str());
					read_p = 0;
				} else if (read_nsca == 1) {
					nsca = (float) atof(word.c_str());
					read_nsca = 0;
				} else if (read_coef == 1) {
					coef = (float) atof(word.c_str());
					read_coef = 0;
				} else if (read_cls_sBLEU == 1) {
					cls_sBLEU = (int) atoi(word.c_str());
					read_cls_sBLEU = 0;
				} else if (read_rp == 1) {
					rp = (float) atof(word.c_str());
					read_rp = 0;
				} else if (read_ns == 1) {
					ns = (float) atof(word.c_str());
					read_ns = 0;
				} else if (read_sbp == 1) {
					sbp = (float) atof(word.c_str());
					read_sbp = 0;
				} else if (read_xbleu == 1) {
					xbleu = (float) atof(word.c_str());
					read_xbleu = 0;
				}
				if (word == "p") {
					read_p = 1;
				} else if (word == "nsca") {
					read_nsca = 1;
				} else if (word == "coef") {
					read_coef = 1;
				} else if (word == "cls_sBLEU") {
					read_cls_sBLEU = 1;
				} else if (word == "rp") {
					read_rp = 1;
				} else if (word == "ns") {
					read_ns = 1;
				} else if (word == "sbp") {
					read_sbp = 1;
				} else if (word == "xbleu") {
					read_xbleu = 1;
				}
			}

			if (string(obj_fun_type) == "PRO"
					|| string(obj_fun_type) == "PRO_sbleu"
					|| string(obj_fun_type) == "max_margin") {
				sum -= coef * nsca;
			} else if (string(obj_fun_type) == "CLL_word") {
				sum -= coef * ns;
			} else if (string(obj_fun_type) == "xBLEU") {
				sum -= xbleu;
			} else if (string(obj_fun_type) == "ListNet") {
				sum -= sbp * log(p);
			} else if (string(obj_fun_type) == "CLL") {
				// oracle
				if (cls_sBLEU == 0) {
					sum -= log(p);
				}
			} else {
				cout << "What kind of objective function ?" << endl;
				exit(1);
			}
			nb_line += 1;
		}
	}
	return sum / nb_line;
}

float rankingHyp_eval_model(NeuralModel* model, char* vad_nbest,
		char* vad_tuple, char* fprobs_out, char* ngramNumber_out,
		char* score_soul_out, char* ncode_soul_out, char* weights,
		char* index_file, char* sBLEU_file, char* rankingHyp_out,
		float rate_pos, float alpha, char* obj_fun_type) {
	int blockSize = 128;
	int iuk = 0;
	cout
			<< "sequenceTrain_nbestList::rankingHyp_eval_model start computing neural scores... "
			<< endl;
	text2Prob_hypo(model, blockSize, iuk, vad_tuple, fprobs_out,
			ngramNumber_out);

	run_score_nn_nbest(fprobs_out, ngramNumber_out, score_soul_out);

	run_score_ncode_nbest(vad_nbest, score_soul_out, weights, ncode_soul_out);

	run_ranking_hypothesis_mm(index_file, sBLEU_file, ncode_soul_out,
			rankingHyp_out, rate_pos, "xxx", "xxx", "xxx", alpha, obj_fun_type);
	return rankingHyp_eval(rankingHyp_out, obj_fun_type);
}

float validation(NeuralModel* model, char* vad_prefix, char* weights,
		char* modelDir, char* vad_base, float rate_pos, float alpha,
		char* obj_fun_type, int iteration) {
	string vad_tuple = string(vad_prefix) + ".tuple";
	string vad_index = string(vad_prefix) + ".index";
	string vad_sBLEU = string(vad_prefix) + ".sBLEU";
	string fprobs_out = string(modelDir) + string(vad_base) + ".fprobs."
			+ boost::lexical_cast<string>(iteration);
	string ngramNumber_out = string(modelDir) + string(vad_base)
			+ ".ngramNumber." + boost::lexical_cast<string>(iteration);
	string score_soul_out = string(modelDir) + string(vad_base) + ".score_soul."
			+ boost::lexical_cast<string>(iteration);
	string ncode_soul_out = string(modelDir) + string(vad_base) + ".ncode_soul."
			+ boost::lexical_cast<string>(iteration);
	string rankingHyp_out = string(modelDir) + string(vad_base) + ".rankingHyp."
			+ boost::lexical_cast<string>(iteration);

	return rankingHyp_eval_model(model, vad_prefix, string2CChar(vad_tuple),
			string2CChar(fprobs_out), string2CChar(ngramNumber_out),
			string2CChar(score_soul_out), string2CChar(ncode_soul_out), weights,
			string2CChar(vad_index), string2CChar(vad_sBLEU),
			string2CChar(rankingHyp_out), rate_pos, alpha, obj_fun_type);
}

int main(int argc, char *argv[]) {
	if (argc != 20) {
		cout
				<< "modelDir min_iteration max_iteration nbest_prefix tuple_prefix sBLEU_prefix index_prefix data_prefix weights validation_prefix rate_pos alpha obj_fun_type lr_schema INDEX_KEEP INDEX_MIRA update_lkt update_hiddens update_output_layer"
				<< endl;
		return 0;
	}
	// inputs
	char* modelDir = argv[1];
	int min_iteration = atoi(argv[2]);
	int max_iteration = atoi(argv[3]);
	char* nbest_prefix = argv[4];
	char* tuple_prefix = argv[5];
	char* sBLEU_prefix = argv[6];
	char* index_prefix = argv[7];
	char* data_prefix = argv[8];
	char* weights = argv[9];
	char* validation_prefix = argv[10];
	float rate_pos = atof(argv[11]);
	float alpha = atof(argv[12]);
	char* obj_fun_type = argv[13];
	char* lr_schema = argv[14];
	int INDEX_KEEP = atoi(argv[15]);
	int INDEX_MIRA = atoi(argv[16]);
	int update_lkt = atoi(argv[17]);
	string update_hiddens = string(argv[18]);
	int update_output_layer = atoi(argv[19]);
	int mapIUnk = 1, mapOUnk = 1, times = 10, gz = 0,
			maxExampleNumber = 0, pre_iteration, pre_pre_iteration, inUnk = 0,
			blockSize;
	floatTensor parasTensor(1, 6);
	string validType = "n", learningRateType = "d";
	ioFile iofc, modelIof, parasIof, iofO;
	parasIof.format = TEXT;
	string cmd, dir_mira, par_file_name, model_file_name_pre, nbest_file_name,
			tuple_file_name, fprobs_out, ngramNumber_out, score_soul_out,
			ncode_soul_out, vad_index, vad_sBLEU, rankingHyp_out, tuple,
			tuple_coef, tuple_coef_dest, data_file;
	float learningRateForRd, learningRateForParas, learningRateDecay,
			weightDecay;

	outils otl;
	int iteration = min_iteration - 1;

	string model_name = string(modelDir)
			+ boost::lexical_cast<string>(iteration);
	NeuralModel* model;
	cout << "sequenceTrain_nbestList::main start charging model " << iteration
			<< endl;
	time_t start, end;
	time(&start);
	READMODEL(model, 0, string2CChar(model_name));
	time(&end);
	cout << "Finish reading the model after " << difftime(end, start) / 60
			<< " minutes" << endl;
	// read first hyper-parameters
	par_file_name = string(modelDir)
			+ boost::lexical_cast<string>(min_iteration - 1) + ".par";
	parasIof.takeReadFile(string2CChar(par_file_name));
	parasTensor.read(&parasIof);
	learningRateForRd = parasTensor(0);
	learningRateForParas = parasTensor(1);
	learningRateDecay = parasTensor(2);
	weightDecay = parasTensor(3);
	blockSize = (int) parasTensor(4);
	model->changeBlockSize(blockSize);
	model->setWeightDecay(weightDecay);
	parasIof.freeReadFile();

	char* vad_base = "validation";
	char* vad_base_mira = "validation.mira";
	char* command_dir = "xxx";

	cout << "rate_pos : " << rate_pos << endl;
	cout << "alpha : " << alpha << endl;
	cout << "learning rate : " << learningRateForParas << endl;
	cout << "blockSize : " << blockSize << endl;
	cout << "weightDecay : " << weightDecay << endl;

	cout << "Validation on " << validation_prefix << endl;
	time(&start);
	float pre_obj = validation(model, validation_prefix, weights, modelDir,
			vad_base, rate_pos, alpha, obj_fun_type, iteration);
	cout << "pre_obj : " << pre_obj << endl;
	time(&end);
	cout << "Finish validation after " << difftime(end, start) / 60
			<< " minutes" << endl;

	// run MIRA if needed
	if (INDEX_MIRA != 0) {
		cout << "Run MIRA on " << validation_prefix << endl;
		time(&start);
		string score_soul_out = string(modelDir) + string(vad_base)
				+ ".score_soul." + boost::lexical_cast<string>(iteration);
		dir_mira = string(modelDir) + "mira."
				+ boost::lexical_cast<string>(iteration);
		run_exe_mira(validation_prefix, string2CChar(score_soul_out),
				command_dir, string2CChar(dir_mira), weights);
		time(&end);
		cout << "Finish MIRA after " << difftime(end, start) / 60 << " minutes"
				<< endl;

		cmd = "ln -s " + string(modelDir) + string(vad_base) + ".fprobs."
				+ boost::lexical_cast<string>(iteration) + " "
				+ string(modelDir) + string(vad_base_mira) + ".fprobs."
				+ boost::lexical_cast<string>(iteration);
		cout << cmd << endl;
		system(cmd.c_str());

		cmd = "ln -s " + string(modelDir) + string(vad_base) + ".ngramNumber."
				+ boost::lexical_cast<string>(iteration) + " "
				+ string(modelDir) + string(vad_base_mira) + ".ngramNumber."
				+ boost::lexical_cast<string>(iteration);
		cout << cmd << endl;
		system(cmd.c_str());

		// recomputing the objective function with the new weights
		cout << "Validation on " << validation_prefix << endl;
		time(&start);
		pre_obj = validation(model, validation_prefix, weights, modelDir,
				vad_base_mira, rate_pos, alpha, obj_fun_type, iteration);
		cout << "pre_obj : " << pre_obj << endl;
		time(&end);
		cout << "Finish validation after " << difftime(end, start) / 60
				<< " minutes" << endl;
	}

	cout << "Create dataset..." << endl;
	time(&start);
	DataSet* dataSet = model->dataSet;

	time(&end);
	cout << "Dataset, input voc and output voc created after "
			<< difftime(end, start) / 60 << " minutes" << endl;

	cout << "Start training..." << endl;

	for (iteration = min_iteration; iteration <= max_iteration; iteration++) {
		cout << "Start iteration " << iteration << endl;
		pre_iteration = iteration - 1;
		pre_pre_iteration = iteration - 2;

		// read hyper-parameters
		par_file_name = string(modelDir)
				+ boost::lexical_cast<string>(pre_iteration) + ".par";
		parasIof.takeReadFile(string2CChar(par_file_name));
		parasTensor.read(&parasIof);
		learningRateForRd = parasTensor(0);
		learningRateForParas = parasTensor(1);
		learningRateDecay = parasTensor(2);
		weightDecay = parasTensor(3);
		blockSize = (int) parasTensor(4);
		model->changeBlockSize(blockSize);
		model->setWeightDecay(weightDecay);
		parasIof.freeReadFile();

		nbest_file_name = string(nbest_prefix)
				+ boost::lexical_cast<string>(iteration);

		tuple_file_name = string(tuple_prefix)
				+ boost::lexical_cast<string>(iteration);

		fprobs_out = string(modelDir) + "fprobs."
				+ boost::lexical_cast<string>(iteration);

		ngramNumber_out = string(modelDir) + "ngramNumber."
				+ boost::lexical_cast<string>(iteration);

		data_file = string(data_prefix)
				+ boost::lexical_cast<string>(iteration);
		if (iofc.check(string2CChar(data_file), 0)) {
			cout << "binary file already exists" << endl;
		} else {
			// firstly, soul score over hypotheses
			cout << "Recomputing neural network score..." << endl;
			text2Prob_hypo(model, blockSize, inUnk,
					string2CChar(tuple_file_name), string2CChar(fprobs_out),
					string2CChar(ngramNumber_out));

			score_soul_out = string(modelDir) + "score_soul."
					+ boost::lexical_cast<string>(iteration);

			run_score_nn_nbest(string2CChar(fprobs_out),
					string2CChar(ngramNumber_out),
					string2CChar(score_soul_out));

			ncode_soul_out = string(modelDir) + "ncode_soul."
					+ boost::lexical_cast<string>(iteration);

			run_score_ncode_nbest(string2CChar(nbest_file_name),
					string2CChar(score_soul_out), weights,
					string2CChar(ncode_soul_out));

			vad_index = string(index_prefix)
					+ boost::lexical_cast<string>(iteration);

			vad_sBLEU = string(sBLEU_prefix)
					+ boost::lexical_cast<string>(iteration);

			rankingHyp_out = string(modelDir) + "rankingHyp."
					+ boost::lexical_cast<string>(iteration);

			tuple = string(tuple_prefix)
					+ boost::lexical_cast<string>(iteration);

			tuple_coef = string(modelDir)
					+ boost::lexical_cast<string>(iteration) + ".coef";

			tuple_coef_dest = string(modelDir)
					+ boost::lexical_cast<string>(iteration) + ".coef.dest";

			cout << "Ranking hypotheses..." << endl;
			run_ranking_hypothesis_mm(string2CChar(vad_index),
					string2CChar(vad_sBLEU), string2CChar(ncode_soul_out),
					string2CChar(rankingHyp_out), rate_pos, string2CChar(tuple),
					string2CChar(tuple_coef), string2CChar(tuple_coef_dest),
					alpha, obj_fun_type);

			int current_nb_data = wc(string2CChar(tuple_coef));
			if (current_nb_data == 0) {
				cout << "Error = 0, stop training at iteration : " << iteration
						<< endl;
				model_name = string(modelDir)
						+ boost::lexical_cast<string>(iteration);

				modelIof.takeWriteFile(string2CChar(model_name));
				model->write(&modelIof, 1);
				modelIof.freeWriteFile();
				break;
			}
			cout << "Make data train file..." << endl;
			time(&start);
			if (dataSet->ngramNumber != 0) {
				cout << "dataSet not initialized" << endl;
				dataSet->reset();
			}
			dataSet->resamplingDataDes(string2CChar(tuple_coef_dest),
					model->ngramType);
			if (SHUFFLE == 1) {
				cout << "shuffle epoch: " << iteration << " with "
						<< dataSet->ngramNumber << " ngrams" << endl;
				dataSet->shuffle(times);
			}
			cout << "write to file : " << data_file << endl;
			iofO.takeWriteFile(string2CChar(data_file));
			if (model->name == WTOVN_DISCRIM) {
				dataSet->writeReBiNgram(&iofO);
			} else if (model->name == WTOVN_NCE_DISCRIM) {
				static_cast<NgramDiscrimWordTranslationDataSet*>(dataSet)->writeReBiNgramNCE(
						&iofO);
			} else {
				cerr
						<< "sequenceTrain_nbestList::main model name is not correct : "
						<< model->name << endl;
				exit(1);
			}
			iofO.freeWriteFile();
			dataSet->reset();
			time(&end);
			cout << "Finish making data train file after "
					<< difftime(end, start) / 60 << " minutes" << endl;
		}

		cout << "Training..." << endl;
		time(&start);
		/*model->sequenceTrain(modelDir, gz, data_prefix, maxExampleNumber, "xxx",
		 "xxx", validType, learningRateType, iteration, iteration,
		 update_lkt, update_hiddens, update_output_layer, write_model);*/
		model->train(string2CChar(data_file), maxExampleNumber, iteration,
				learningRateType, learningRateForParas, learningRateDecay,
				update_lkt, update_hiddens, update_output_layer);
		time(&end);
		cout << "Finish after " << difftime(end, start) / 60 << " minutes"
				<< endl;

		// validation
		if (iteration % INDEX_KEEP == 0) {
			model_name = string(modelDir)
					+ boost::lexical_cast<string>(iteration);

			modelIof.takeWriteFile(string2CChar(model_name));
			model->write(&modelIof, 1);
			modelIof.freeWriteFile();
			cout << "validation on " << validation_prefix << endl;
			time(&start);
			float obj = validation(model, validation_prefix, weights, modelDir,
					vad_base, rate_pos, alpha, obj_fun_type, iteration);
			cout << "obj : " << obj << endl;
			if (string(lr_schema) == "adjust" && (isinf(obj) || isnan(obj) || (obj > pre_obj))) {
				cout << "Warning: objective function increases" << endl;
				// divide the learning by 2
				cout << "Divide the learning rate by 2" << endl;
				parasTensor(0) = parasTensor(0) / parasTensor(2);
				parasTensor(1) = parasTensor(1) / parasTensor(2);
				if (UNDO == 1) {
					cout << "Back to the precedent model" << endl;
					model_file_name_pre = string(modelDir)
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP);

					int modelC = iofc.check(string2CChar(model_file_name_pre),
							0);
					if (!modelC) {
						cerr << "WARNING: Train model file "
								<< model_file_name_pre << " does not exists"
								<< endl;
						return 0;
					}
					modelIof.takeReadFile(string2CChar(model_file_name_pre));
					model->read(&modelIof, 0, blockSize);

					// copy file fprobs, because we return to precedent evaluation
					cmd = "cp " + string(modelDir) + string(vad_base)
							+ ".fprobs."
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP) + " "
							+ string(modelDir) + string(vad_base) + ".fprobs."
							+ boost::lexical_cast<string>(iteration);
					cout << cmd << endl;
					system(cmd.c_str());

					// copy file ngramNumber
					cmd = "cp " + string(modelDir) + string(vad_base)
							+ ".ngramNumber."
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP) + " "
							+ string(modelDir) + string(vad_base)
							+ ".ngramNumber."
							+ boost::lexical_cast<string>(iteration);
					cout << cmd << endl;
					system(cmd.c_str());

					// copy file score_soul
					cmd = "cp " + string(modelDir) + string(vad_base)
							+ ".score_soul."
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP) + " "
							+ string(modelDir) + string(vad_base)
							+ ".score_soul."
							+ boost::lexical_cast<string>(iteration);
					cout << cmd << endl;
					system(cmd.c_str());

					// copy file ncode_soul
					cmd = "cp " + string(modelDir) + string(vad_base)
							+ ".ncode_soul."
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP) + " "
							+ string(modelDir) + string(vad_base)
							+ ".ncode_soul."
							+ boost::lexical_cast<string>(iteration);
					cout << cmd << endl;
					system(cmd.c_str());

					// copy file rankingHyp
					cmd = "cp " + string(modelDir) + string(vad_base)
							+ ".rankingHyp."
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP) + " "
							+ string(modelDir) + string(vad_base)
							+ ".rankingHyp."
							+ boost::lexical_cast<string>(iteration);
					cout << cmd << endl;
					system(cmd.c_str());

					// copy model
					cmd = "cp " + string(modelDir)
							+ boost::lexical_cast<string>(
									iteration - INDEX_KEEP) + " "
							+ string(modelDir)
							+ boost::lexical_cast<string>(iteration);
					cout << cmd << endl;
					system(cmd.c_str());

					// return perplexity to the precedent value
					obj = pre_obj;
				} else {
					cout << "We do not back to the precedent model" << endl;
				}
			}
			pre_obj = obj;
			time(&end);
			cout << "Finish validation after " << difftime(end, start) / 60
					<< " minutes" << endl;

			// run MIRA if needed
			if (INDEX_MIRA != 0 && iteration % INDEX_MIRA == 0) {
				cout << "Run MIRA on " << validation_prefix << endl;
				time(&start);
				score_soul_out = string(modelDir) + string(vad_base)
						+ ".score_soul."
						+ boost::lexical_cast<string>(iteration);

				dir_mira = string(modelDir) + "mira."
						+ boost::lexical_cast<string>(iteration);

				run_exe_mira(validation_prefix, string2CChar(score_soul_out),
						command_dir, string2CChar(dir_mira), weights);
				time(&end);
				cout << "Finish MIRA after " << difftime(end, start) / 60
						<< " minutes" << endl;

				cmd = "ln -s " + string(modelDir) + string(vad_base)
						+ ".fprobs." + boost::lexical_cast<string>(iteration)
						+ " " + string(modelDir) + string(vad_base_mira)
						+ ".fprobs." + boost::lexical_cast<string>(iteration);
				cout << cmd << endl;
				system(cmd.c_str());

				cmd = "ln -s " + string(modelDir) + string(vad_base)
						+ ".ngramNumber."
						+ boost::lexical_cast<string>(iteration) + " "
						+ string(modelDir) + string(vad_base_mira)
						+ ".ngramNumber."
						+ boost::lexical_cast<string>(iteration);
				cout << cmd << endl;
				system(cmd.c_str());

				// recomputing the objective function with the new weights
				cout << "Validation on " << validation_prefix << endl;
				time(&start);
				pre_obj = validation(model, validation_prefix, weights,
						modelDir, vad_base_mira, rate_pos, alpha, obj_fun_type,
						iteration);
				cout << "pre_obj : " << pre_obj << endl;
				time(&end);
				cout << "Finish validation after " << difftime(end, start) / 60
						<< " minutes" << endl;
			}
		}
		par_file_name = string(modelDir)
				+ boost::lexical_cast<string>(iteration) + ".par";

		parasIof.takeWriteFile(string2CChar(par_file_name));
		parasTensor.write(&parasIof);
		cout << parasTensor(0) << " to " << par_file_name << endl;
		parasIof.freeWriteFile();
	}
	delete model;
}
