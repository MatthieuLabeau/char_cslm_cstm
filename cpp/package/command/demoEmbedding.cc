/*
 * demoEmbedding.cc
 *
 *  Created on: Feb 3, 2015
 *      Author: dokhanh
 */

#include<iostream>
#include<mainModule.H>
#include<map>
#include<cmath>

using namespace std;

/*
 #define tr(container, it)\
	for (typeof(container.begin()) it = container.begin() ; it != container.end() ; it ++)
 */

SPLIT_STRING

int main(int argc, char* argv[]) {
	if (argc != 3) {
		cout << "vocFile embeddingFile" << endl;
		exit(1);
	}
	// inputs
	char* vocFileName = argv[1];
	char* embeddingFile = argv[2];
	ioFile vocFile;
	vocFile.takeReadFile(vocFileName);
	string line;
	map<string, int> voc;
	while (vocFile.getEOF() == false) {
		vocFile.getLine(line);
		vector<string> lines = split_string(line, string(" "));
		voc[lines[0]] = atoi(lines[1].c_str());
		//DEBUG(cout << "lines[1] : " << lines[1] << " lines[0] : " << lines[0] << endl;)
	}
	vocFile.freeReadFile();

	floatTensor embed;
	ioFile embedFile;
	embedFile.takeReadFile(embeddingFile);
	embed.read(&embedFile);
	embedFile.freeReadFile();
	string word;
	map<float, string> distance;
	int id, id1, nbClos, i;
	map<float, string>::iterator run;
	float dist;
	floatTensor ts, ts1, ts2(embed.size[1], 1);
	do {
		cout << "Entrer un mot : " << endl;
		cin >> word;
		id = voc[word];
		cout << "Embedding : " << endl;
		for (i = 0 ; i < embed.size[1] ; i ++) {
			cout << embed(id, i) << " ";
		}
		cout << endl;
		distance.clear();
		tr(voc, it)
		{
			id1 = it->second;
			//DEBUG(cout << id1 << endl;)
			ts.select(embed, 0, id);
			ts1.select(embed, 0, id1);
			if (id == id1) {
				distance[0] = it->first;
			} else {
				ts.axpy(ts1, -1);
				dist = ts.dot(ts);
				distance[dist] = it->first;
				ts.axpy(ts1, 1);
			}
		}

		cout << "Nb of neighbor words : " << endl;
		cin >> nbClos;
		i = 0;
		run = distance.begin();
		while (i < nbClos) {
			cout << run->first << " " << run->second << endl;
			i++;
			run++;
		}
	} while (1);
	return 0;
}
