#include "mainModel.H"

NgramCharModel* readCharModel(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModel* sourceModel = new NgramCharModel();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

NgramCharModelNCE* readCharModelNCE(int blockSize, char* sourceModelFileName, char* hmf) {
  ioFile iof_sourceModel;
  string name_sourceModel = iof_sourceModel.recognition(sourceModelFileName);
  cout << name_sourceModel << endl;
  NgramCharModelNCE* sourceModel = new NgramCharModelNCE();
  iof_sourceModel.takeReadFile(sourceModelFileName);
  cout << sourceModelFileName << endl;
  sourceModel->read(&iof_sourceModel, 1, blockSize, hmf);
  return sourceModel;
}

int main(int argc, char *argv[]) {
  if (argc != 8) {
    cout << "charModelFileName charVocFileName inputVocFileName outputVocFileName charWordHMapFileName nonLinearType outputModelFileName" << endl;
    return 0;
  }
  time_t start, end;
  char* charModelFileName = argv[1];
  char* charVocFileName = argv[2];
  char* contextVocFileName = argv[3];
  char* predictVocFileName = argv[4];
  char* charWordHMap = argv[5];
  string nonLinearType = argv[6];
  char* outputModelFileName = argv[7];
  
  ioFile iofC;
  if (!iofC.check(charModelFileName, 1)) {
    return 1;
  }
  if (!iofC.check(charVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(contextVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(predictVocFileName, 1)) {
    return 1;
  }
  if (!iofC.check(charWordHMap, 1)) {
    return 1;
  }
  if (iofC.check(outputModelFileName, 0)) {
    cerr << "Prototype exists" << endl;
    return 1;
  }
  ioFile iofChar;
  string nameChar = iofChar.recognition(charModelFileName);
  NeuralModel* charModel;
  int charDimensionSize;
  int charWordNumber;
  if (nameChar == CE) {
    charModel = readCharModel(1, charModelFileName, charWordHMap);
    NgramCharModel* charModelCast = static_cast<NgramCharModel*>(charModel);
    charDimensionSize = charModelCast->charDimensionSize;
    charWordNumber = charModelCast->charVoc->wordNumber;
  } else if (nameChar == CE_NCE) {
    charModel = readCharModelNCE(1, charModelFileName, charWordHMap);
    NgramCharModelNCE* charModelCast = static_cast<NgramCharModelNCE*>(charModel);
    charDimensionSize = charModelCast->charDimensionSize;
    charWordNumber = charModelCast->charVoc->wordNumber;
  } else {
    cerr << "The first model needs to be of type CE or CE_NCE" << endl;
    return 1;
  }

  intTensor hiddenLayerSizeArray;
  hiddenLayerSizeArray.resize(1, 1);
  hiddenLayerSizeArray(0) = charModel->hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);

  ioFile mIof;
  //Create model here
  string name="ce_ce_nce";
  NgramCharModelNCE* model = new NgramCharModelNCE(name, charWordHMap, charVocFileName, contextVocFileName, predictVocFileName,
						   charModel->mapIUnk, charModel->mapOUnk, charModel->BOS, charModel->blockSize, charModel->n,
						   charDimensionSize, hiddenLayerSizeArray(0), nonLinearType, hiddenLayerSizeArray);
  //Add char weight and word weight to model
  if (charWordNumber != model->charVoc->wordNumber) {
    cerr << "Error: Wrong character vocabulary" << endl;
    return 1;
  }
  
  CharEmbeddings* clkt = static_cast<CharEmbeddings*>(charModel->baseNetwork->lkt);
  CharEmbeddings* in_clkt = static_cast<CharEmbeddings*>(model->baseNetwork->lkt);
  CharNCE* out_NCE = static_cast<CharNCE*>(model->outputLayer);
  CharEmbeddings* out_clkt = static_cast<CharEmbeddings*>(out_NCE->charEmbeddings);
  in_clkt->cLkt->weight.copy(clkt->cLkt->weight);
  //in_clkt->convModule->weight.copy(clkt->convModule->weight);
  out_clkt->cLkt->weight.copy(clkt->cLkt->weight);
  //out_clkt->convModule->weight.copy(clkt->convModule->weight);
  //for (int i = 0; i < model->baseNetwork->size; i=i+2) {
  //model->baseNetwork->modules[i]->weight.copy(charModel->baseNetwork->modules[i]->weight);
  //}

  mIof.takeWriteFile(outputModelFileName);
  model->write(&mIof, 1);
  mIof.freeWriteFile();
  delete model;
  return 0;
}
