/*
 * train2freq.cc
 *
 *  Created on: Mar 24, 2015
 *      Author: dokhanh
 */

#include<stdlib.h>
#include<map>
#include<text.H>
#include<soulConfig.H>
#include<boost/lexical_cast.hpp>

using namespace std;

Strip(read, Int, int)
Strip(write, Int, int)

map<int, int>* countOutWord(char* trainFileName, string name) {
	map<int, int>* freqMap = new map<int, int>();
	ioFile trainFile, ioFc;
	if (!ioFc.check(trainFileName, 1)) {
		cerr << "train file does not exist" << endl;
		exit(1);
	}
	trainFile.takeReadFile(trainFileName);
	int ngramNumber;
	trainFile.readInt(ngramNumber);
	int N;
	trainFile.readInt(N);

	int nbBlock = ngramNumber / NCE_RESAMPLING_NGRAM_NUMBER;
	int remainingNgramNumber = ngramNumber % NCE_RESAMPLING_NGRAM_NUMBER;

	intTensor readTensor(NCE_RESAMPLING_NGRAM_NUMBER, N);
	floatTensor coef(NCE_RESAMPLING_NGRAM_NUMBER, 1);
	int idBlock, idNgram, nbNgramFinished = 0, nextShow = 1, current;
	float percent;
	for (idBlock = 0; idBlock < nbBlock; idBlock++) {
		readIntTrainFile(trainFile, readTensor, coef, N, name);
		for (idNgram = 0; idNgram < readTensor.size[0]; idNgram++) {
			if (freqMap->find(readTensor(idNgram, N - 1)) == freqMap->end()) {
				// initialize
				(*freqMap)[readTensor(idNgram, N - 1)] = 1;
			} else {
				// increment
				current = (*freqMap)[readTensor(idNgram, N - 1)];
				(*freqMap)[readTensor(idNgram, N - 1)] = current + 1;
			}
		}
# if PRINT_DEBUG
		nbNgramFinished += NCE_RESAMPLING_NGRAM_NUMBER;
		percent = (float) nbNgramFinished / ngramNumber;
		if (percent > nextShow * CONSTPRINT) {
			cout << percent << " ... " << flush;
		}
		while (percent > nextShow * CONSTPRINT) {
			nextShow += 1;
		}
# endif
	}
	// the last block
	if (remainingNgramNumber != 0) {
		readTensor.resize(remainingNgramNumber, N);
		readIntTrainFile(trainFile, readTensor, coef, N, name);
		for (idNgram = 0; idNgram < readTensor.size[0]; idNgram++) {
			if (freqMap->find(readTensor(idNgram, N - 1)) == freqMap->end()) {
				// initialize
				(*freqMap)[readTensor(idNgram, N - 1)] = 1;
			} else {
				// increment
				current = (*freqMap)[readTensor(idNgram, N - 1)];
				(*freqMap)[readTensor(idNgram, N - 1)] = current + 1;
			}
		}
	}
	trainFile.freeReadFile();
	return freqMap;
}

int main(int argc, char* argv[]) {
	if (argc != 4) {
		cout << "trainFileName type outFileName" << endl;
		return 1;
	}
	char* trainFileName = argv[1];
	string name = argv[2];
	char* outFileName = argv[3];
	map<int, int>* freqMap = countOutWord(trainFileName, name);
	int lastIdVoc = freqMap->rbegin()->first, id, freq;
	DEBUG(cout << "lastIdVoc : " << lastIdVoc << endl;)
	ioFile outFile;
	outFile.takeWriteFile(outFileName);
	outFile.format = TEXT;
	for (id = 0 ; id <= lastIdVoc ; id ++) {
		if (freqMap->find(id) == freqMap->end()) {
			// OOV
			freq = atoi(DEFAULT_FREQ);
		} else {
			freq = (*freqMap)[id];
		}
		outFile.writeString(boost::lexical_cast<string>(freq));
	}
	outFile.freeWriteFile();
	delete freqMap;
}
