HEADERS += \
    ../package/text/DataSet.H \
    ../package/text/FunctionDataSet.H \
    ../package/text/NgramDataSet.H \
    ../package/text/NgramDiscrimDataSet.H \
    ../package/text/NgramDiscrimWordTranslationDataSet.H \
    ../package/text/NgramPhraseTranslationDataSet.H \
    ../package/text/NgramRankDataSet.H \
    ../package/text/NgramWordTranslationDataSet.H \
    ../package/text/RecurrentDataSet.H \
    ../package/text/text.H \
    ../package/text/Vocab.H

SOURCES += \
    ../package/text/DataSet.cc \
    ../package/text/FunctionDataSet.cc \
    ../package/text/NgramDataSet.cc \
    ../package/text/NgramDiscrimDataSet.cc \
    ../package/text/NgramDiscrimWordTranslationDataSet.cc \
    ../package/text/NgramPhraseTranslationDataSet.cc \
    ../package/text/NgramRankDataSet.cc \
    ../package/text/NgramWordTranslationDataSet.cc \
    ../package/text/RecurrentDataSet.cc \
    ../package/text/Vocab.cc
