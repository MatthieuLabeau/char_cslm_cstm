/******************************************************************                                                                   
 Structure OUtput Layer (SOUL) Language Model Toolkit                                                                                 
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS                                                                                      
*******************************************************************/

#include "mainModule.H"

CharEmbeddings::CharEmbeddings() {
}

CharEmbeddings::CharEmbeddings(int nDimension, int inputSize, int cIndex, int cDimension, string activation, int blockSize, int oneClass, outils *otl) {
  this->name = "CharEmbeddings";
  this->activation = activation;
  this->cLkt = new CharLookupTable(cIndex, cDimension, inputSize, blockSize, oneClass, otl);
  // We will create the intermediate layers once the file containing the mapping character n-grams -> word has been read.
  this->poolModule = new Pooling(inputSize, nDimension, blockSize);
  if (activation == TANH) {
    this->actPoolModule = new Tanh(nDimension * inputSize, blockSize);
  }
  else if (activation == RELU) {
    this->actPoolModule = new Relu(nDimension * inputSize, blockSize);
  }
  else {
    this->actPoolModule = new Sigmoid(nDimension * inputSize, blockSize);
  }
  this->inputSize = inputSize;
  this->blockSize = blockSize;
  this->dimensionSize = nDimension;
  this->charDimensionSize = cDimension;
  this->update_lkt = 1;
  output.resize(inputSize * dimensionSize, blockSize);
}

CharEmbeddings::~CharEmbeddings() {
  delete cLkt;
  delete convModule;
  delete actModule;
  delete poolModule;
  delete actPoolModule;
}

void CharEmbeddings::getHashMaps(char* hmf) { 
  cLkt->getHashMaps(hmf);
  /* We need to create the first layers with the maximum possible blocksize,
     (longest word * nChar * n * bloackSize), to avoid memory allocation issues
     then it will change at each forward, being the sum of the word length in the block */
  this->convModule = new Linear(charDimensionSize * cLkt->nChar, dimensionSize, cLkt->maxLength * inputSize * blockSize, otl); 
  if (activation == TANH) {
    this->actModule = new Tanh(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
  else if (activation == RELU) {
    this->actModule = new Relu(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
  else {
    this->actModule = new Sigmoid(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
}


void CharEmbeddings::changeBlockSize(int blockSize) {
  this->blockSize = blockSize;
  cLkt->changeBlockSize(blockSize);
  poolModule->changeBlockSize(blockSize);
  actPoolModule->changeBlockSize(blockSize);
}


void CharEmbeddings::reset() {
  cLkt->reset();
}

void CharEmbeddings::init1class() {
  cLkt->init1class();
}

floatTensor& CharEmbeddings::forward(intTensor& input) {
  this->input = input;
  currentOutput = cLkt->forward(input);
  //cout << "cLkt weight:" << cLkt->weight.averageSquare() << endl;
  //cout << "cLkt out:" << currentOutput.averageSquare() << endl;
  // Once forward has been called in cLkt we know the total length of the tensor to go through conv/actModule and we can resize them
  convModule->changeBlockSize(cLkt->totLength);
  // actModule->changeBlockSize(cLkt->totLength);
  currentOutput = convModule->forward(currentOutput);
  //currentOutput = actModule->forward(currentOutput);
  //cout << "conv weight:" << convModule->weight.averageSquare() << endl;
  //cout << "conv out:" << currentOutput.averageSquare() << endl;
  // Similarly, we need info from cLkt to use Pooling, which has no parameters and will be used only for backward
  poolModule->wordLength = cLkt->wordLength;
  poolModule->wordStart = cLkt->wordStart;
  output = actPoolModule->output;
  currentOutput = poolModule->forward(currentOutput);
  //cout << "pool out:" << currentOutput.averageSquare() << endl;
  currentOutput = actPoolModule->forward(currentOutput);
  //cout << "act out:" << currentOutput.averageSquare() << endl;
  output.copy(currentOutput);
  //cout << "output :" << currentOutput.averageSquare() << endl;
  return output;
}


floatTensor& CharEmbeddings::backward(floatTensor& gradOutput) {
  currentGradOutput = gradOutput;
  //cout << "gradoutput :" << gradOutput.averageSquare() << endl;
  currentGradOutput = actPoolModule->backward(currentGradOutput);
  //cout << "act gradoutput :" << currentGradOutput.averageSquare() << endl;
  currentGradOutput = poolModule->backward(currentGradOutput);
  //cout << "pool gradoutput :" << currentGradOutput.averageSquare() << endl;
  //currentGradOutput = actModule->backward(currentGradOutput);
  currentGradOutput = convModule->backward(currentGradOutput);
  //cout << "conv gradoutput :" << currentGradOutput.averageSquare() << endl;
  currentGradOutput = cLkt->backward(currentGradOutput);
  return currentGradOutput;
}


void CharEmbeddings::updateParameters(float learningRate) {
  convModule->updateParameters(learningRate); 
  if (update_lkt == 1) {
    cLkt->updateParameters(learningRate);
  }
}

// TODO
void CharEmbeddings::read(ioFile *iof) {
  iof->readString(name);
  cout << name << endl;
  cLkt->read(iof);
  convModule->read(iof);
  //actModule->read(iof);
  poolModule->read(iof);
  actPoolModule->read(iof);
}
void CharEmbeddings::write(ioFile *iof) {
  iof->writeString(name);
  cLkt->write(iof);
  convModule->write(iof);
  //actModule->write(iof);
  poolModule->write(iof);
  actPoolModule->write(iof);
}

