/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS

 Specific class for language model. This layer aims to project context
 words to a space, then concatenate them to create a new layer
 Input is the indices of context words (n - 1 integers)
 Output is the vector after concatenation (n - 1) x dimensionSize floats
 *******************************************************************/

#include "mainModule.H"

LookupTable::LookupTable() {

}

LookupTable::LookupTable(int indexNumber, int nDimension, int inputSize,
		int blockSize, int oneClass, outils* otl) {
	name = "LookupTable";

	weight.resize(nDimension, indexNumber);
	output.resize(nDimension * inputSize, blockSize);
	this->otl = otl;
	this->blockSize = blockSize;
	if (!oneClass) {
		reset();
	} else {
		init1class();
	}
	this->dimensionSize = nDimension;
	this->indexNumber = indexNumber;
}

LookupTable::~LookupTable() {
}

void LookupTable::reset() {
	weight.uniform(LKT_INIT0, LKT_INIT1, otl);
}

void LookupTable::getHashMaps(char* hmf) {
}

void LookupTable::updateParameters(float learningRate) {
	int x0, x1;
	for (int i = 0; i < input.size[1]; i++) {
		x0 = 0;
		x1 = dimensionSize - 1;
		for (int j = 0; j < input.size[0]; j++) {
			selectWeight.select(weight, 1, input(j, i));
			selectGradWeight.sub(gradWeight, x0, x1, i, i);
			if (weightDecay != 0) {
				// y = y - lr * wd * y
				selectWeight.scal(1 - learningRate * weightDecay);
			}
			selectWeight.axpy(selectGradWeight, -learningRate);
			x0 += dimensionSize;
			x1 += dimensionSize;
		}
	}
}

void LookupTable::read(ioFile* iof) {
	iof->readString(name);
	weight.read(iof);
}
void LookupTable::write(ioFile* iof) {
	iof->writeString(name);
	weight.write(iof);
}
