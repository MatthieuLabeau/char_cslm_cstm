#include "mainModule.H"

CharLookupTable_AG::CharLookupTable_AG() {
}

CharLookupTable_AG::CharLookupTable_AG(int charNumber, int charDimension,
				       int inputSize, int blockSize, int oneClass, outils* otl) :
  CharLookupTable(charNumber, charDimension, inputSize, blockSize, oneClass,
		  otl) {
  name = "CharLookupTable_AG";
  ag_type = "no_type";
  use_cache = 0;
}

CharLookupTable_AG::~CharLookupTable_AG() {
}

void CharLookupTable_AG::resetAG() {
  cumulGradWeight = INIT_VALUE_ADAG;
}

void CharLookupTable_AG::initAG(string type){
  if (ag_type != LEARNINGRATE_AG
      && ag_type != LEARNINGRATE_CAG
      && ag_type != LEARNINGRATE_BAG
      && ag_type != LEARNINGRATE_DBAG
      && ag_type != LEARNINGRATE_NCE_BAG) {
    ag_type = type;
  } else {
    cout << "Warning: CharLookupTable_AG has learning type " << ag_type
         << " and can't be attributed " << type
         << ". Learning will continue as " << ag_type
         << endl;
  }
  if (ag_type == LEARNINGRATE_AG) {
    cumulGradWeight.resize(weight);
    squareSelectGradWeight.resize(dimensionSize, 1);  
  } else if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.resize(1, indexNumber);
    squareSelectGradWeight.resize(1, 1);
  } else {
    cout << "Learning strategy "
         << ag_type
         << " is not working with Adagrad"
         << endl;
    exit(1);
  }
  
  cumulGradWeight = INIT_VALUE_ADAG;
  
  learningRateAfterADAGWeight.resize(squareSelectGradWeight);
  util1Weight.resize(squareSelectGradWeight);
  util1Weight = 1;
}

void CharLookupTable_AG::updateParameters(float learningRate) {
  int x0, x1;
  for (int l=0; l < input.size[1]; l++) {
    for (int n=0; n < input.size[0]; n++) {
      currentInput = readTensors[input(n,l)];
      for (int i = 0; i < currentInput.size[1]; i++) {
	x0 = 0;
	x1 = dimensionSize - 1;
	for (int j = 0; j < currentInput.size[0]; j++) {
	  selectWeight.select(weight, 1, currentInput(j, i));
	  selectGradWeight.sub(gradWeight, x0, x1, i + wordStart[l*inputSize + n], i + wordStart[l*inputSize + n]);
	  selectCumulGradWeight.select(cumulGradWeight, 1, currentInput(j, i));
	  if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
	    squareSelectGradWeight = selectGradWeight.averageSquare();
	  } else if (ag_type == LEARNINGRATE_AG) {
	    squareSelectGradWeight.square(selectGradWeight);
	  }
	  
	  if (use_cache == 1) {
	    selectCumulGradWeight.scal(cache);
	    //selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0-cache);
	    selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0);
	  } else {
	    selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0);
	  }

	  squareSelectGradWeight.squareRoot(selectCumulGradWeight);
	  squareSelectGradWeight.axpy(util1Weight, e);
	  learningRateAfterADAGWeight = learningRate;
	  learningRateAfterADAGWeight.div(squareSelectGradWeight);
	  if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
	    selectWeight.axpy(selectGradWeight, -sqrt(learningRateAfterADAGWeight.averageSquare()));
	  } else if (ag_type == LEARNINGRATE_AG) {
	    learningRateAfterADAGWeight.product(selectGradWeight);
	    selectWeight.axpy(learningRateAfterADAGWeight, -1.0);
	  }
	  x0 += dimensionSize;
	  x1 += dimensionSize;
	}
      }
    }
  }
}

void CharLookupTable_AG::read(ioFile* iof) {
  CharLookupTable::read(iof);
  /*
  iof->readString(ag_type);
  if (ag_type == LEARNINGRATE_AG
      || ag_type == LEARNINGRATE_CAG
      || ag_type == LEARNINGRATE_BAG
      || ag_type == LEARNINGRATE_DBAG
      || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.read(iof);
  }
  */
}

void CharLookupTable_AG::write(ioFile* iof) {
  CharLookupTable::write(iof);
  /*
  iof->writeString(ag_type);
  if (ag_type == LEARNINGRATE_AG
      || ag_type == LEARNINGRATE_CAG
      || ag_type == LEARNINGRATE_BAG
      || ag_type == LEARNINGRATE_DBAG
      || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.write(iof);
  }
  */
}

float CharLookupTable_AG::sqrtCumulGradWeight() {
  return sqrt(cumulGradWeight.averageSquare());
}
