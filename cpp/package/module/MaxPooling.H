/******************************************************************                                                                                                                                      
Structure OUtput Layer (SOUL) Language Model Toolkit                                                                                                                                                       
(C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS                                                                                                                                                        
*******************************************************************/

class MaxPooling : public Pooling {
public:
  intTensor maxInfo;
  floatTensor selectMax;

  MaxPooling();
  MaxPooling(int inputSize, int dimensionSize, int blockSize);

  ~MaxPooling();

  floatTensor& forward(floatTensor& input);
  floatTensor& backward(floatTensor& gradOutput);
};
