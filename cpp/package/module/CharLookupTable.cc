/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS
*******************************************************************/

#include "mainModule.H"

CharLookupTable::CharLookupTable() {
}

CharLookupTable::CharLookupTable(int charNumber, int charDimension, int inputSize, int blockSize, int oneClass, outils* otl) {
  name="CharLookupTable";
  // Create an array to keep track of the length of each word and their start index
  //this->indexLength = new int[nIndex];
  this->nIndex = 0;
  this->indexLength = new int[nIndex];
  this->wordLength = new int[inputSize*blockSize];
  this->wordStart = new int[inputSize*blockSize];
  // Create an array to keep the character n-grams for each word                                                                                                                                
  this->readTensors = new intTensor[nIndex];
  // Keep track of size of character n-grams and dimension of character embeddings
  this->indexNumber = charNumber;
  this->dimensionSize = charDimension;
  this->blockSize = blockSize;
  this->inputSize = inputSize;
  this->nIndex = nIndex;
  // Resize the embeddings and output accordingly
  weight.resize(charDimension, charNumber);
  //output.resize(charDimension * nChar, maxLength * inputSize * blockSize);
  this->weightDecay = 0;
  this->otl = otl;
  if (!oneClass) {
    reset();
  } else {
    init1class();
  }
}

CharLookupTable::~CharLookupTable() {
  delete wordLength;
  delete wordStart;
  delete[] readTensors;
  delete indexLength;
}

void CharLookupTable::init1class() {
  floatTensor initRealTensor;
  initRealTensor.resize(weight.size[0], 1);
  initRealTensor.uniform(LKT_INIT0, LKT_INIT1, otl);
  floatTensor initSelectWeight;
  int i;
  for (i = 0; i < weight.size[1]; i++) {
    initSelectWeight.select(weight, 1, i);
    initSelectWeight.copy(initRealTensor);
  }
}

void CharLookupTable::changeBlockSize(int blockSize) {
  this->blockSize = blockSize;
  //delete wordLength;
  this->wordLength = new int[inputSize*blockSize];
  //delete wordStart;
  this->wordStart = new int[inputSize*blockSize];
}

vector<string> * CharLookupTable::tokenize(string * s, const string SEP){
  //cout << "line:" << *s<< endl;                                                                                                                                                                            
  size_t pos = s->find(SEP);
  size_t org = 0;
  vector<string> * parse = new vector<string>();
  while (pos != string::npos){
    parse->push_back(s->substr(org,pos-org));
    //cout << "extract " << parse->size() << " " <<  parse->back() << endl;                                                                                                                                  
    org = pos+SEP.length();
    pos = s->find(SEP,org);
  }
  parse->push_back(s->substr(org,s->size()-org));
  //cout << "extract " << parse->size() << " " <<  parse->back() << endl;                                                                                                                                    
  return parse;
}

void CharLookupTable::getHashMaps(char* hmf) {
  //File opening
  ifstream hmfile;
  hmfile.open(hmf);
  if(! hmfile){
    cerr << "ERROR - cannot open file cLkt" << hmf <<endl;
    exit(1);
  }
  string line;
  const string SSPACE = " ";
  stringstream ss;

  //First line
  std::getline(hmfile, line);
  ss << line;
  ss >> this->nIndex;
  ss.clear();
  this->indexLength = new int[nIndex];
  this->readTensors = new intTensor[nIndex];

  //Second line
  std::getline(hmfile, line);
  ss << line;
  ss >> this->nChar;
  ss.clear();

  //Third line
  std::getline(hmfile, line);
  ss << line;
  ss >> this->maxLength;
  ss.clear();
  
  int j = 0;
  //Hashmaps - but they really are indexed intTensor (easier to use)
  while (std::getline(hmfile, line)) {
    vector<string> * tokens = tokenize(&line, SSPACE);
    int index, length;
    // Get the first number: index of the word
    ss.clear();
    ss << (*tokens)[0];
    ss >> index; 
    // Get the second number : length of the word and keep it in lengthHashMap
    ss.clear();
    ss << (*tokens)[1];
    ss >> length;
    indexLength[index] = length;
    // Get the rest of the numbers into a int array that will be put in an intTensor
    readTensors[j].resize(nChar, length);
    for (int i=2; i<tokens->size(); i++){                                                                                                                                                  
      ss.clear();                                                                                                                                                               
      ss << (*tokens)[i];                                                                                                                                                                        
      ss >> readTensors[j].data[i-2];                                                             
    }
    delete tokens;
    j++;
  }
  hmfile.close();
  output.resize(dimensionSize * nChar, maxLength * inputSize * blockSize);
}

floatTensor& CharLookupTable::forward(intTensor& input) {

  this->input = input;
  // First, use the lengthHashMap to build wordLen
  for (int l=0; l < input.size[1]; l++) {
    for (int n=0; n < input.size[0]; n++) {
      //cout << input(n,l) << endl;
      wordLength[l*inputSize + n] = indexLength[input(n,l)];
      if (l*nChar + n == 0) {
	wordStart[l*inputSize + n] = 0;
      }
      else {
	wordStart[l*inputSize + n] = wordStart[l*inputSize + n - 1] + wordLength[l*inputSize + n - 1];
      }
    }
  }
  // Resize output with the size of a character n-gram embedding * the sum of all word length 
  this->totLength = wordStart[input.size[1]*inputSize-1] + wordLength[input.size[1]*inputSize-1];
  output.resize(dimensionSize * nChar, totLength);
  output = 0;

  //For each word n-gram in blockSize -
  int x0, x1;
  for (int l=0; l < input.size[1]; l++) {  
    //For each word in the n-gram -
    for (int n=0; n < input.size[0]; n++) {
      //cout << input(n,l) << ' ' << n << ' ' << l << endl;
      this->currentInput = readTensors[input(n,l)];
      //For each char n-gram in the word length -
      for (int i = 0; i < currentInput.size[1]; i++) {
	x0 = 0;
	x1 = dimensionSize - 1;
	//For each char in the char n-gram -
	for (int j = 0; j < currentInput.size[0]; j++) {
	  //Get the right char embedding in the right place in output
	  selectOutput.sub(output, x0, x1, i + wordStart[l*inputSize + n], i + wordStart[l*inputSize + n]);
	  selectWeight.select(weight, 1, currentInput(j,i));
	  selectOutput.copy(selectWeight);
	  x0 += dimensionSize;
	  x1 += dimensionSize;
	}
      }
    }
  }
  return output;
}

floatTensor&
CharLookupTable::forward(floatTensor& input)
{
  cout << "Wrong call, must call with input: int" << endl;
  return input;
}

floatTensor&
CharLookupTable::backward(floatTensor& gradOutput) {
  gradWeight = gradOutput;
  return gradWeight;
}

void CharLookupTable::updateParameters(float learningRate) {
  int x0, x1;
  for (int l=0; l < input.size[1]; l++) {
    for (int n=0; n < input.size[0]; n++) {
      //cout << input(n,l) << ' ' << n << ' ' << l <<  endl;
      currentInput = readTensors[input(n,l)];
      for (int i = 0; i < currentInput.size[1]; i++) {
	x0 = 0;
	x1 = dimensionSize - 1;
	for (int j = 0; j < currentInput.size[0]; j++) {
	  selectWeight.select(weight, 1, currentInput(j, i));
	  selectGradWeight.sub(gradWeight, x0, x1, i + wordStart[l*inputSize + n], i + wordStart[l*inputSize + n]);
	  if (weightDecay != 0) {
	    selectWeight.scal(1 - learningRate * weightDecay);
	  }
	  selectWeight.axpy(selectGradWeight, -learningRate);
	}
      }
    }
  }
}

void CharLookupTable::reset(){
  weight.uniform(LKT_INIT0, LKT_INIT1, otl);
}
void CharLookupTable::read(ioFile *iof) {
  iof->readString(name);
  cout << name << endl;
  weight.read(iof);
}
void CharLookupTable::write(ioFile *iof) {
  iof->writeString(name);
  weight.write(iof);
}

float CharLookupTable::distance2(Module* anotherCLkt) {
  CharLookupTable* validEmbed = dynamic_cast<CharLookupTable*>(anotherCLkt);
  if (validEmbed == NULL) {
    DEBUG(cerr << "anotherCLkt is not a good type" << endl;)
      exit(1);
  }
  floatTensor distanceMatrix;
  distanceMatrix.copy(this->weight);
  distanceMatrix.axpy(validEmbed->weight, -1);
  return distanceMatrix.sumSquared();
}
