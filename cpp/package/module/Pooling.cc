/******************************************************************                                                                                                                                         
 Structure OUtput Layer (SOUL) Language Model Toolkit                                                                                                                                                       
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS                                                                                                                                                          
*******************************************************************/

#include "mainModule.H"

Pooling::Pooling(int inputSize, int dimensionSize, int blockSize) {
  name="Pooling";
  output.resize(inputSize * dimensionSize, blockSize);
  output = 0;
  this->inputSize = inputSize;
  this->dimensionSize = dimensionSize;
  this->blockSize = blockSize;
  // Will contain info about word boundaries - to know which part of the input tensor it needs to 'pool'
  this->wordLength = new int[inputSize*blockSize];
  this->wordStart = new int[inputSize*blockSize];
}

Pooling::~Pooling() {
  //delete wordLength;
  //delete wordStart;
}

void Pooling::changeBlockSize(int blockSize) {
  this->blockSize = blockSize;
  output.resize(inputSize * dimensionSize, blockSize);
  output = 0;
  delete wordLength;
  this->wordLength = new int[inputSize*blockSize];
  delete wordStart;
  this->wordStart = new int[inputSize*blockSize];
}

floatTensor&
Pooling::forward(floatTensor& input) {
  output = 0;
  this->input = input;
  int x0, x1;
  //For each word in the block
  for (int i = 0; i < blockSize; i++) {
    x0 = 0;
    x1 = dimensionSize - 1;
    // For each word in the n-gram
    for (int j = 0; j < inputSize; j++) {   
      selectOutput.sub(output, x0, x1, i, i);
      // We take the corresponding part in the input (indicated by wordStart/wordLength), and output the mean for this word 
      for (int l = 0; l < wordLength[i*inputSize + j]; l++) {
	selectInput.select(input, 1, wordStart[i*inputSize + j] + l);
	selectOutput.axpy(selectInput, 1.0);
      }
      float scale = (float)1/(float)wordLength[i*inputSize + j];
      selectOutput.scal(scale);
      x0 += dimensionSize;
      x1 += dimensionSize;
    }   
  }
  return output;
}

floatTensor&
Pooling::backward(floatTensor& gradOutput) {
  this->gradOutput = gradOutput;
  gradInput.resize(input.size[0],input.size[1]);
  int x0, x1, x2, x3;
  for (int i = 0; i < blockSize; i++) {
    x0 = 0;
    x1 = dimensionSize - 1;
    for (int j = 0; j < inputSize; j++) {
      for (int l = 0; l < wordLength[i*inputSize + j]; l++) {
	selectGradInput.select(gradInput, 1, wordStart[i*inputSize + j] + l);
	selectGradOutput.sub(gradOutput, x0, x1, i, i);
	selectGradInput.copy(selectGradOutput);
	float scale = (float)1/(float)wordLength[i*inputSize+j];
	selectGradInput.scal(scale);
      }
      x0 += dimensionSize;
      x1 += dimensionSize;
    }
  }
  return gradInput;
}

void Pooling::updateParameters(float learningRate) {}

void Pooling::read(ioFile* iof) {
  iof->readString(name);
}
void Pooling::write(ioFile* iof) {
  iof->writeString(name);
}
float Pooling::distance2(Module* anotherModule) {
  return 0;
}


