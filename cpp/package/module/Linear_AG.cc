#include "mainModule.H"

Linear_AG::Linear_AG() {

}

Linear_AG::Linear_AG(int inputSize, int outputSize, int blockSize, outils* otl) :
		Linear(inputSize, outputSize, blockSize, otl) {
	name = "Linear_AG";
	ag_type = "no_type";
	use_cache = 0;

	gradWeight.resize(weight);
	gradBias.resize(bias);
}

Linear_AG::Linear_AG(Module* orig) {
	Linear* real_orig;
	if (real_orig = dynamic_cast<Linear*>(orig)) {
	  name = "Linear_AG";
	  // Initialize parameters
	  blockSize = real_orig->blockSize;
	  weightDecay = real_orig->weightDecay;
	  weight.copy_and_delete(real_orig->weight);
	  bias.copy_and_delete(real_orig->bias);
	  V1col.copy_and_delete(real_orig->V1col);
	  gradInput.copy_and_delete(real_orig->gradInput);
	  output.copy_and_delete(real_orig->output);
	  
	  otl = real_orig->otl;
	  // for ADAG
	  ag_type = "no_type";
	  use_cache = 0;

	  gradWeight.resize(weight);
	  gradBias.resize(bias);
	} else {
	  DEBUG(cerr << " argument does not have good type" << endl;)
	    exit(1);
	}
}

Linear_AG::~Linear_AG() {
}

void Linear_AG::initAG(string type){
  if (ag_type != LEARNINGRATE_AG
      && ag_type != LEARNINGRATE_CAG
      && ag_type != LEARNINGRATE_BAG
      && ag_type != LEARNINGRATE_DBAG
      && ag_type != LEARNINGRATE_NCE_BAG) {
    ag_type = type;
  } else {
    cout << "Warning: Linear_AG has learning type " << ag_type
         << " and can't be attributed " << type
         << ". Learning will continue as " << ag_type
         << endl;
  }
  if (ag_type == LEARNINGRATE_AG || ag_type == LEARNINGRATE_CAG) {
    utilGradWeight.resize(weight);
    cumulGradWeight.resize(weight);
    cumulGradBias.resize(bias);
    /*else if (ag_type == LEARNINGRATE_CAG) {
    util1Col.resize(weight.size[0], 1);
    util1Col = 1;
    utilGradWeight.resize(weight);
    cumulGradWeight.resize(bias);
    cumulGradBias.resize(bias);*/
  } else if (ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.resize(1, 1);
    cumulGradBias.resize(1, 1);
  } else {
    cout << "Learning strategy "
         << ag_type
         << " is not working with Adagrad"
         << endl;
    exit(1);
  }

  cumulGradWeight = INIT_VALUE_ADAG;
  cumulGradBias = INIT_VALUE_ADAG;

  squareGradWeight.resize(cumulGradWeight);
  squareGradBias.resize(cumulGradBias);

  learningRateAfterADAGWeight.resize(cumulGradWeight);
  learningRateAfterADAGBias.resize(cumulGradBias);
  
  util1Weight.resize(cumulGradWeight);
  util1Bias.resize(cumulGradBias);
  util1Weight = 1;
  util1Bias = 1;
}

void Linear_AG::reset() {
	Linear::reset();
	cumulGradWeight = 0;
	cumulGradBias = 0;
}

void Linear_AG::resetAG() {
  cumulGradWeight = INIT_VALUE_ADAG;
  cumulGradBias = INIT_VALUE_ADAG;
}

void Linear_AG::updateParameters(float learningRate) {
  gradWeight.gemm(input, 'N', gradOutput, 'T', 1, 0);
  gradBias.gemv(gradOutput, 'N', V1col, 1, 0);
  
  //Compute gradient history 
  if (ag_type == LEARNINGRATE_AG || ag_type == LEARNINGRATE_CAG) {
    // parameter by parameter
    squareGradWeight.square(gradWeight);
    squareGradBias.square(gradBias);
    /*else if (ag_type == LEARNINGRATE_CAG) {
    // column by column
    squareGradWeight.gemv(gradWeight, 'T', util1Col, 1, 0);
    squareGradWeight.scal((float) 1 / util1Col.size[0]);
    squareGradWeight.square(squareGradWeight);
    squareGradBias.square(gradBias);*/
  } else if (ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
    // by tensor
    squareGradWeight = gradWeight.averageSquare();
    squareGradBias = gradBias.averageSquare();
  }
  if (use_cache == 1) {
    cumulGradWeight.scal(cache);
    //cumulGradWeight.axpy(squareGradWeight, 1.0-cache);
    cumulGradWeight.axpy(squareGradWeight, 1.0); 
    cumulGradBias.scal(cache);
    //cumulGradBias.axpy(squareGradBias, 1.0-cache);
    cumulGradBias.axpy(squareGradBias, 1.0);
  } else {
    cumulGradWeight.axpy(squareGradWeight, 1.0);
    cumulGradBias.axpy(squareGradBias, 1.0);
  }

  // compute adapted learning rates give this history
  learningRateAfterADAGWeight = learningRate;
  squareGradWeight.squareRoot(cumulGradWeight);
  squareGradWeight.axpy(util1Weight, e);
  learningRateAfterADAGWeight.div(squareGradWeight);
  
  learningRateAfterADAGBias = learningRate;
  squareGradBias.squareRoot(cumulGradBias);
  squareGradBias.axpy(util1Bias, e);
  learningRateAfterADAGBias.div(squareGradBias);

  // update parameters
  if (ag_type == LEARNINGRATE_AG || ag_type == LEARNINGRATE_CAG) {
    utilGradWeight.copy(gradWeight);
    utilGradWeight.axpy(weight, weightDecay);
    utilGradWeight.product(learningRateAfterADAGWeight);
    weight.axpy(utilGradWeight, -1.0);
    learningRateAfterADAGBias.product(gradBias);
    bias.axpy(learningRateAfterADAGBias, -1.0);
    /*} else if (ag_type == LEARNINGRATE_CAG) {
    utilGradWeight = 0;
    utilGradWeight.ger(util1Col, learningRateAfterADAGWeight, 1);
    gradWeight.axpy(weight, weightDecay);
    utilGradWeight.product(gradWeight);
    gradWeight.axpy(weight, -weightDecay);
    weight.axpy(utilGradWeight, -1.0);
    learningRateAfterADAGBias.product(gradBias);
    bias.axpy(learningRateAfterADAGBias, -1.0);*/
  } else if (ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
    weight.scal(1 - sqrt(learningRateAfterADAGWeight.averageSquare()) * weightDecay);
    weight.axpy(gradWeight, -sqrt(learningRateAfterADAGWeight.averageSquare()));
    bias.axpy(gradBias, -sqrt(learningRateAfterADAGBias.averageSquare()));
  }
}

void Linear_AG::read(ioFile *iof) {
	Linear::read(iof);
	/*
	iof->readString(ag_type);
	if (ag_type == LEARNINGRATE_AG
	    || ag_type == LEARNINGRATE_CAG 
	    || ag_type == LEARNINGRATE_BAG
	    || ag_type == LEARNINGRATE_DBAG
	    || ag_type == LEARNINGRATE_NCE_BAG) {
	  cumulGradWeight.read(iof);
	  cumulGradBias.read(iof);
	}
	*/
}

void Linear_AG::write(ioFile * iof) {
	Linear::write(iof);
	/*
	iof->writeString(ag_type);
	if (ag_type == LEARNINGRATE_AG
            || ag_type == LEARNINGRATE_CAG
            || ag_type == LEARNINGRATE_BAG
            || ag_type == LEARNINGRATE_DBAG
	    || ag_type == LEARNINGRATE_NCE_BAG) {
	  cumulGradWeight.write(iof);
	  cumulGradBias.write(iof);
	}
	*/
}

float Linear_AG::sqrtCumulGradWeight() {
  return sqrt(cumulGradWeight.averageSquare());
}

float Linear_AG::sqrtCumulGradBias() {
  return sqrt(cumulGradBias.averageSquare());
}
