/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS
*******************************************************************/

#include "mainModule.H"

CharWordEmbeddings_AG::CharWordEmbeddings_AG() {
}

CharWordEmbeddings_AG::CharWordEmbeddings_AG(int wIndex, int nDimension, int inputSize, int cIndex, int cDimension, string activation, int blockSize, int oneClass, outils *otl) {
  name="CharWordEmbeddings";
  this->wIndex = wIndex;
  this->inputSize = inputSize;
  this->blockSize = blockSize;
  this->dimensionSize = nDimension;
  this->charDimensionSize = cDimension;
  this->update_lkt = 1;
  this->activation = activation;
  this->cLkt = new CharLookupTable_AG(cIndex, cDimension, inputSize, blockSize, oneClass, otl);
  this->Lkt = new LookupTable_AG(wIndex, dimensionSize/2, inputSize, blockSize, oneClass, otl);
  this->poolModule = new Pooling(inputSize, nDimension/2, blockSize);
  if (activation == TANH) {
    this->actPoolModule = new Tanh(nDimension/2 * inputSize, blockSize);
  }
  else if (activation == RELU) {
    this->actPoolModule = new Relu(nDimension/2 * inputSize, blockSize);
  }
  else {
    this->actPoolModule = new Sigmoid(nDimension/2 * inputSize, blockSize);
  }
  output.resize(inputSize * dimensionSize, blockSize);
  restrictedInput.resize(inputSize, blockSize);
  charGradOutput.resize(inputSize * dimensionSize / 2, blockSize);
  wordGradOutput.resize(inputSize * dimensionSize / 2, blockSize);
}

CharWordEmbeddings_AG::~CharWordEmbeddings_AG() {
}

void CharWordEmbeddings_AG::getHashMaps(char* hmf) {
  cLkt->getHashMaps(hmf);
  /* We need to create the first layers with the maximum possible blocksize,
     (longest word * nChar * n * bloackSize), to avoid memory allocation issues
     then it will change at each forward, being the sum of the word length in the block */
  this->convModule = new Linear_AG(charDimensionSize * cLkt->nChar, dimensionSize/2, cLkt->maxLength * inputSize * blockSize, otl);
  if (activation == TANH) {
    this->actModule = new Tanh(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
  else if (activation == RELU) {
    this->actModule = new Relu(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
  else {
    this->actModule = new Sigmoid(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
}

void CharWordEmbeddings_AG::resetAG() {
  LookupTable_AG* Lkt_AG =  static_cast<LookupTable_AG*>(this->Lkt);
  CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(this->cLkt);
  Linear_AG* conv_AG = static_cast<Linear_AG*>(this->convModule);
  Lkt_AG->resetAG();
  cLkt_AG->resetAG();
  conv_AG->resetAG();
}

void CharWordEmbeddings_AG::initAG(string type){
  LookupTable_AG* Lkt_AG =  static_cast<LookupTable_AG*>(this->Lkt);
  CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(this->cLkt);
  Linear_AG* conv_AG = static_cast<Linear_AG*>(this->convModule);
  Lkt_AG->initAG(type);
  cLkt_AG->initAG(type);
  conv_AG->initAG(type);
}
