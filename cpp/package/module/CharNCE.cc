#include "mainModule.H"

CharNCE::CharNCE() {}

CharNCE::CharNCE(int cIndex, int cDimension,
		 int inputSize, int outputSize, int blockSize, string activation,
		 int word, outils *otl) {
  name = "CharNCE";
  this->blockSize = blockSize;
  this->nceOutputSize = 0;

  //LinearNCE initialization - without weight ! 
  softmaxV1row.resize(outputSize, 1);
  softmaxV1row = 1;
  softmaxVCol.resize(blockSize, 1);
  allWords.resize(1, outputSize);
  allWeight.resize(inputSize, outputSize);
  gradInput.resize(inputSize, blockSize);
  output.resize(outputSize, blockSize);
  normalizeOutput.resize(output);
  gradOutput.resize(output);
  preOutput.resize(outputSize, blockSize);
  weight.resize(0,0);
  weight = 0;
  
  if (word == 0) { 
    this->charEmbeddings = new CharEmbeddings(inputSize, 1, cIndex, cDimension, activation, outputSize, 1, otl);
  } else {
    this->charEmbeddings = new CharWordEmbeddings(outputSize, inputSize, 1, cIndex, cDimension, activation, outputSize, 1, otl);
  }
  this->otl = otl;
}

CharNCE::~CharNCE() {
}


void CharNCE::reset() {
  charEmbeddings->reset();
}

void CharNCE::changeNceOutputSize(int nceOutputSize) {
  //LinearNCE - NCE initialization
  if (this->nceOutputSize != nceOutputSize) {
    this->nceOutputSize = nceOutputSize;
  }
  int inputSize = gradInput.size[0];
  nceWeight.resize(inputSize, blockSize * nceOutputSize);
  wordLign.resize(1, blockSize * nceOutputSize);
  nceOutput.resize(nceOutputSize, blockSize);
  ncePreOutput.resize(nceOutputSize, blockSize);
  gradCE.resize(nceWeight);
  gradNceOutput.resize(nceOutput);
  //For charEmbeddings
  charEmbeddings->changeBlockSize(nceOutputSize * blockSize);
}


void CharNCE::changeCEBlockSize() {
  charEmbeddings->changeBlockSize(nceOutputSize * blockSize);
}


void CharNCE::changeBlockSize(int blockSize) {
  //LinearNCE changeBlockSize
  this->blockSize = blockSize;
  int inputSize = gradInput.size[0];
  int outputSize = output.size[0];
  softmaxVCol.resize(blockSize, 1);
  gradInput.resize(inputSize, blockSize);
  output.resize(outputSize, blockSize);
  normalizeOutput.resize(output);
  if (nceOutputSize != 0) {
    nceOutput.resize(nceOutputSize, blockSize);
    ncePreOutput.resize(nceOutputSize, blockSize);
    nceWeight.resize(inputSize, blockSize * nceOutputSize);
    wordLign.resize(1, blockSize * nceOutputSize);
    gradNceOutput.resize(nceOutput);
    gradCE.resize(nceWeight);
  }
  gradOutput.resize(output);
  preOutput.resize(outputSize, blockSize);
  //For charEmbeddings
  charEmbeddings->changeBlockSize(nceOutputSize * blockSize);
}

floatTensor& CharNCE::nceForward(floatTensor& input, intTensor& word) {
  if (word.size[0] != nceOutputSize || word.size[1] != blockSize) {
    cout << "CharrNCE::nceForward word tensor is not in good size, the good size is " << nceOutputSize << " x " << blockSize << ", but word is of " << word.size[0] << " x " << word.size[1] << endl;
    cout << "CharNCE::nceForward word : " << endl;
    word.write();
    exit(1);
  }
  int inputSize = gradInput.size[0];
  this->input = input;
  this->outputWord = word;
  //ncePreOutput = 0;
  
  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
    for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
      if (word(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
	wordLign.data[idBlockSize * nceOutputSize + idNceOutputSize] = word(idNceOutputSize, idBlockSize);
      } else {
	wordLign.data[idBlockSize * nceOutputSize + idNceOutputSize] = 0;
      }
    }
  }
  nceWeight = charEmbeddings->forward(wordLign);

  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {  
    localNceWeight.sub(nceWeight, 0, inputSize - 1, idBlockSize*nceOutputSize, (idBlockSize+1)*nceOutputSize - 1);
    localNcePreOutput.select(ncePreOutput, 1, idBlockSize);
    localInput.select(input, 1, idBlockSize);
    // Then, preOutput = preOutput + weight^T x input
    localNcePreOutput.gemm(localNceWeight, 'T', localInput, 'N', 1, 0);
  }
  nceOutput.mexp(ncePreOutput);
  //cout << "nceForward here" << endl;
  return nceOutput;
}

float CharNCE::efficientNceForward(floatTensor& input, int word) {

}

float CharNCE::computeF(intTensor& word, floatTensor& logPropTensor) {
  float f = 0;
  int k = nceOutputSize - 1;
  if (k > 0) {
    for (idBlockSize = 0; idBlockSize < outputWord.size[1]; idBlockSize++) {
      for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
	if (word(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
	  float modelProp = nceOutput(idNceOutputSize, idBlockSize);
	  float noiseProp = exp(logPropTensor(idNceOutputSize, idBlockSize));
	  if (idNceOutputSize == 0) {
	    f -= log(modelProp / (modelProp + k * noiseProp));
	  } else {
	    f -= log(k * noiseProp / (modelProp + k * noiseProp));
	  }
	}
      }
    }
  } else if (k == 0) {
    for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
      if (word(0, idBlockSize) != SIGN_NOT_WORD) {
	f -= logPropTensor(0, idBlockSize) * log(nceOutput(0, idBlockSize));
      }
    }
  }
  return f;
}

floatTensor& CharNCE::backward(intTensor& outputWord, floatTensor& logPropTensor) {
  int inputSize = gradInput.size[0];
  int k = nceOutputSize - 1;
  if (k > 0) {
    for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
      for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
	if (outputWord(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
	  float modelProp = nceOutput(idNceOutputSize, idBlockSize);
	  float noiseProp = exp(logPropTensor(idNceOutputSize, idBlockSize));
	  if (idNceOutputSize == 0) {
	    // positive example
	    gradNceOutput(idNceOutputSize, idBlockSize) = -k * noiseProp / (modelProp + k * noiseProp);
	  } else {
	    // negative example
	    gradNceOutput(idNceOutputSize, idBlockSize) = modelProp / (modelProp + k * noiseProp);  
	    if (isnan(gradNceOutput(idNceOutputSize, idBlockSize))) {
	      gradNceOutput(idNceOutputSize, idBlockSize) = 1.0;
	    }    
	  }
	} else {
	  gradNceOutput(idNceOutputSize, idBlockSize) = 0;
	}
      }
    }
  } else {
    if (k == 0) {
      for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
	if (outputWord(0, idBlockSize) != SIGN_NOT_WORD) {
	  gradNceOutput(0, idBlockSize) = -logPropTensor(0, idBlockSize);
	} else {
	  gradNceOutput(0, idBlockSize) = 0;
	}
      }
    }
  }
  // Compute gradCE - gradient that will be propagated to CharEmbeddings
  gradCE = 0; // Need to put it to 0 since we use axpy ? 
  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
    for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
      if (outputWord(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
	localGradCE.select(gradCE, 1, nceOutputSize*idBlockSize + idNceOutputSize);
	localInput.select(input, 1, idBlockSize);
	localGradCE.axpy(localInput, gradNceOutput(idNceOutputSize, idBlockSize));
      }
    }
  }
  charEmbeddings->backward(gradCE);
  // Compute gradInput - gradient that will be returned and propagated to the inferior layers 
  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
    localGradNceOutput.select(gradNceOutput, 1, idBlockSize);
    localGradInput.select(gradInput, 1, idBlockSize);
    localNceWeight.sub(nceWeight, 0, inputSize - 1, idBlockSize*nceOutputSize, (idBlockSize+1)*nceOutputSize - 1);
    localGradInput.gemm(localNceWeight, 'N', localGradNceOutput, 'N', 1, 0);
  }
  //cout << "backward here" << endl;
  return gradInput;
}


void CharNCE::updateParameters(float learningRate, intTensor& word) {
  /*
  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
    for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
      int consideredWord = word(idNceOutputSize, idBlockSize);
      if (consideredWord != SIGN_NOT_WORD) {
	bias(consideredWord) = bias(consideredWord) - learningRate * gradNceOutput(idNceOutputSize, idBlockSize);
      }
    }
  }
  */
  //cout << "updateParameters1 here" << endl;
  charEmbeddings->updateParameters(learningRate);
}


void CharNCE::updateParameters(float learningRate) {
  cout << "updateParameters2 here" << endl;
  charEmbeddings->updateParameters(learningRate);
}

void CharNCE::computeAllWeight() {
  cout << "computeAllWeight here" << endl;
  int outputSize = output.size[0];
  charEmbeddings->changeBlockSize(outputSize);
  for (int idOutputSize = 0; idOutputSize < outputSize; idOutputSize++) {
    allWords.data[idOutputSize] = idOutputSize;
  }
  allWeight = charEmbeddings->forward(allWords);
  cout << allWeight.averageSquare() << endl;
}

floatTensor& CharNCE::forward(floatTensor& input) {
  // Il faut appeller computeAllWeight avant forward
  //this->computeAllWeight();
  this->input = input;
  preOutput = 0;
  preOutput.gemm(allWeight, 'T', input, 'N', 1, 1);
  // output = e^(preOutput), we obtain unnormalized probabilities
  output.mexp(preOutput);
  //cout << "CharNCE forward: " << output.averageSquare() << endl;
  return output;
}


floatTensor& CharNCE::normalizeForward(floatTensor& input) {
  output = forward(input);
  // softmaxVCol contains the sum for each column
  softmaxVCol.gemv(output, 'T', softmaxV1row, 1, 0);
  // For each column, divide by the sum to have
  // for each element e^x_i / \sum_j e^x_j
  for (int i = 0; i < output.size[1]; i++) {
    localNormalizeOutput.select(normalizeOutput, 1, i);
    localOutput.select(output, 1, i);
    localNormalizeOutput.copy(localOutput);
    localNormalizeOutput.scal(1.0 / softmaxVCol(i));
  }
  //cout << "normalizeForward here" << endl;
  return normalizeOutput;
}
/*
float CharNCE::coefNorm(floatTensor& input, ioFile* file) { 
  // write only text
  if (file->format != TEXT) {
    file->format = TEXT;
  }
  output = forward(input);
  // softmaxVCol contains the sum for each column
  softmaxVCol.gemv(output, 'T', softmaxV1row, 1, 0);
  for (int i = 0 ; i < output.size[1] ; i ++) {
    file->writeString(boost::lexical_cast<string>(softmaxVCol(i)));
  }
}
*/

float CharNCE::computeNormalizedF(floatTensor& input, intTensor& word) {
  //Only for evaluation!
  normalizeOutput = normalizeForward(input);
  float f = 0;
  for (int idBlockSize = 0 ; idBlockSize < word.length ; idBlockSize ++) {
    if (word(idBlockSize) != SIGN_NOT_WORD) {
      f -= log(normalizeOutput(word(idBlockSize), idBlockSize));
    }
  }
  return f;
}

float CharNCE::distance2(Module* anotherModule) {
  return 0;
}
void CharNCE::read(ioFile *iof) {
  iof->readString(name);
  charEmbeddings->read(iof);
}

void CharNCE::write(ioFile * iof) {
  iof->writeString(name);
  charEmbeddings->write(iof);
}
