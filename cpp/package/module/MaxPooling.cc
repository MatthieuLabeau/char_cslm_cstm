/******************************************************************                                                                                                                                         
 Structure OUtput Layer (SOUL) Language Model Toolkit                                                                                                                                                       
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS                                                                                                                                                          
*******************************************************************/

#include "mainModule.H"

MaxPooling::MaxPooling(int inputSize, int dimensionSize, int blockSize) :
  Pooling(inputSize, dimensionSize, blockSize) {
  name="MaxPooling";
  maxInfo.resize(inputSize * dimensionSize, blockSize);
}

MaxPooling::~MaxPooling() {
  //delete wordLength;
  //delete wordStart;
}

floatTensor&
MaxPooling::forward(floatTensor& input) {
  output = 0;
  this->input = input;
  int x0, x1;
  //For each word in the block
  for (int i = 0; i < blockSize; i++) {
    // For each word in the n-gram
    for (int j = 0; j < inputSize; j++) {   
      int s = wordStart[i*inputSize + j];
      int l =  wordLength[i*inputSize + j];
      // For each feature
      for (int k = 0; k < dimensionSize; k++) {
	selectMax.sub(input, k, k, s, s+l);
	maxInfo(i, j*dimensionSize + k) = selectMax.imax();
	output(i, j*dimensionSize + k) = selectMax.data[maxInfo(i*inputSize + j, k)];
      }
    }   
  }
  return output;
}

floatTensor&
MaxPooling::backward(floatTensor& gradOutput) {
  this->gradOutput = gradOutput;
  gradInput.resize(input.size[0],input.size[1]);
  for (int i = 0; i < blockSize; i++) {
    for (int j = 0; j < inputSize; j++) {
      int s = wordStart[i*inputSize + j];
      int l =  wordLength[i*inputSize + j];
      for (int k = 0; k < dimensionSize; k++) {
	gradInput.data[maxInfo(i*inputSize + j, k)] =  gradOutput(i, j*dimensionSize + k);
      }
    }
  }
  return gradInput;
}



