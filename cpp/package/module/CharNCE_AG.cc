#include "mainModule.H"

CharNCE_AG::CharNCE_AG() {}

CharNCE_AG::CharNCE_AG(int cIndex, int cDimension,
		       int inputSize, int outputSize, int blockSize, string activation,
		       int word, outils *otl) {
  name = "CharNCE_AG";
  this->blockSize = blockSize;
  this->nceOutputSize = 0;

  //LinearNCE initialization - without weight !                                                                                                                                                             
  softmaxV1row.resize(outputSize, 1);
  softmaxV1row = 1;
  softmaxVCol.resize(blockSize, 1);
  allWords.resize(1, outputSize);
  allWeight.resize(inputSize, outputSize);
  gradInput.resize(inputSize, blockSize);
  output.resize(outputSize, blockSize);
  normalizeOutput.resize(output);
  gradOutput.resize(output);
  preOutput.resize(outputSize, blockSize);
  weight.resize(0,0);
  weight = 0;

  if (word == 0) {  
    this->charEmbeddings = new CharEmbeddings_AG(inputSize, 1, cIndex, cDimension, activation, outputSize, 1, otl);
  } else {
    this->charEmbeddings = new CharWordEmbeddings_AG(outputSize, inputSize, 1, cIndex, cDimension, activation, outputSize, 1, otl);
  }
  this->otl = otl;
}

CharNCE_AG::~CharNCE_AG() {
}

void CharNCE_AG::resetAG() {
  CharEmbeddings_AG* nowords = dynamic_cast<CharEmbeddings_AG*>(charEmbeddings);
  CharWordEmbeddings_AG* words = dynamic_cast<CharWordEmbeddings_AG*>(charEmbeddings);
  if (nowords) {
    nowords->resetAG();
  } else {
    words->resetAG();
  }
}

void CharNCE_AG::initAG(string type) {
  CharEmbeddings_AG* nowords = dynamic_cast<CharEmbeddings_AG*>(charEmbeddings);
  CharWordEmbeddings_AG* words = dynamic_cast<CharWordEmbeddings_AG*>(charEmbeddings);
  if (nowords) {
    nowords->initAG(type);
  } else {
    words->initAG(type);
  }
}
