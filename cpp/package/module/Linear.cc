/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS

 Linear Layer.
 *******************************************************************/
#include "mainModule.H"

Linear::Linear() {

}

Linear::Linear(int inputSize, int outputSize, int blockSize, outils* otl) {
	name = "Linear";
	// Initialize parameters
	this->blockSize = blockSize;
	weightDecay = 0;
	weight.resize(inputSize, outputSize);
	bias.resize(outputSize, 1);
	V1col.resize(blockSize, 1);
	V1col = 1;
	gradInput.resize(inputSize, blockSize);
	output.resize(outputSize, blockSize);

	this->otl = otl;
	reset();
}

Linear::~Linear() {
}

void Linear::to_ag() {
	name = "Linear_AG";
}

void Linear::changeBlockSize(int blockSize) {
	// Need to change memory size for some parameters
	this->blockSize = blockSize;
	int inputSize = gradInput.getSize(0);
	int outputSize = output.getSize(0);
	V1col.resize(blockSize, 1);
	V1col = 1;
	gradInput.resize(inputSize, blockSize);
	output.resize(outputSize, blockSize);

}

void Linear::reset() {
	weight.uniform(LINEAR_INIT0, LINEAR_INIT1, otl);
	bias.uniform(LINEAR_INIT0, LINEAR_INIT1, otl);
}

floatTensor&
Linear::forward(floatTensor& input) {
	this->input = input;
	output = 0;
	output.ger(bias, V1col, 1);
	output.gemm(weight, 'T', input, 'N', 1, 1);
	return output;
}

floatTensor&
Linear::backward(floatTensor& gradOutput) {
	// Keep gradOutput for later update
	this->gradOutput = gradOutput;

	// gradInput = weight x gradOutput
	gradInput.gemm(weight, 'N', gradOutput, 'N', 1, 0);
	return gradInput;
}

void Linear::updateParameters(float learningRate) {
	// weight = - learningRate x input x gradOutput^T
	//        + weight - learningRate * weightDecay * weight
	weight.gemm(input, 'N', gradOutput, 'T', -learningRate,
			1 - learningRate * weightDecay);
	// bias = -learningRate x gradOutput x V1col + bias
	bias.gemv(gradOutput, 'N', V1col, -learningRate, 1 - learningRate * weightDecay);
}

float Linear::distance2(Module* anotherLinear) {
	Linear* validModule = dynamic_cast<Linear*>(anotherLinear);
	if (validModule == NULL) {
		cerr << "Linear::distance2 anotherLinear is not a good type" << endl;
		exit(1);
	}
	floatTensor distMatrix;
	distMatrix.copy(this->weight);
	distMatrix.axpy(validModule->weight, -1);
	float res1 = distMatrix.sumSquared();
	distMatrix.resize(this->bias);
	distMatrix.copy(this->bias);
	distMatrix.axpy(validModule->bias, -1);
	return res1 + distMatrix.sumSquared();
}

void Linear::read(ioFile* iof) {
	iof->readString(name);
	weight.read(iof);
	bias.read(iof);
}

void Linear::write(ioFile* iof) {
	iof->writeString(name);
	weight.write(iof);
	bias.write(iof);
}
