class LinearSoftmax_discrim : public LinearSoftmax {
public:
	LinearSoftmax_discrim();
	LinearSoftmax_discrim(int inputSize, int outputSize, int blockSize, outils* otl);
	LinearSoftmax_discrim(ProbOutput* orig);
	~LinearSoftmax_discrim();
	float
	computeF(intTensor& word, floatTensor& coefTensor);
	floatTensor&
	backward(intTensor& word, floatTensor& coefTensor);
};
