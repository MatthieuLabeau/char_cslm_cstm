/*
 * LinearNCE_AG.cc
 *
 *  Created on: Jun 30, 2015
 *      Author: dokhanh
 */




#include "mainModule.H"
#include <boost/lexical_cast.hpp>

LinearNCE_AG::LinearNCE_AG() {
//	nceWeight = NULL;
}

LinearNCE_AG::LinearNCE_AG(int inputSize, int outputSize, int blockSize,
		outils* otl) : LinearNCE(inputSize, outputSize, blockSize, otl) {
  // ADAG
  name = "LinearNCE_AG";
  ag_type = "no_type";
  use_cache = 0;
}

LinearNCE_AG::~LinearNCE_AG() {
  /*if (nceWeight != NULL) {
    delete[] nceWeight;
    }*/
}

void LinearNCE_AG::initAG(string type){
  if (ag_type != LEARNINGRATE_AG
      && ag_type != LEARNINGRATE_CAG
      && ag_type != LEARNINGRATE_BAG
      && ag_type != LEARNINGRATE_DBAG
      && ag_type != LEARNINGRATE_NCE_BAG) {
    ag_type = type;
  } else {
    cout << "Warning: Linear_AG has learning type " << ag_type
         << " and can't be attributed " << type
         << ". Learning will continue as " << ag_type
         << endl;
  }
  if (ag_type == LEARNINGRATE_AG) {
    cumulGradWeight.resize(weight);
    cumulGradBias.resize(bias);
    squareSelectGradWeight.resize(weight.size[0], 1);
    utilLocalInput.resize(weight.size[0], 1);
  } else if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.resize(1, weight.size[1]);
    cumulGradBias.resize(1, weight.size[1]);
    squareSelectGradWeight.resize(1, 1);
  } else {
    cout << "Learning strategy "
         << ag_type
         << " is not working with Adagrad"
         << endl;
    exit(1);
  }
  cumulGradWeight = INIT_VALUE_ADAG;
  cumulGradBias = INIT_VALUE_ADAG;

  learningRateAfterADAGWeight.resize(squareSelectGradWeight);
  util1Weight.resize(squareSelectGradWeight);
  util1Weight = 1;
}

void LinearNCE_AG::reset() {
  LinearNCE::reset();
  cumulGradWeight = 0;
  cumulGradBias = 0;
}

void LinearNCE_AG::resetAG() {
  cumulGradWeight = INIT_VALUE_ADAG;
  cumulGradBias = INIT_VALUE_ADAG;
}

void LinearNCE_AG::updateParameters(float learningRate, intTensor& word) {
  int outputSize = bias.length;
  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
    for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
      int consideredWord = word(idNceOutputSize, idBlockSize);
      if (consideredWord != SIGN_NOT_WORD) {
	//Weight
	localWeight.select(weight, 1, consideredWord);
	localInput.select(input, 1, idBlockSize);
	selectCumulGradWeight.select(cumulGradWeight, 1, consideredWord);

	if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
	  squareSelectGradWeight = localInput.averageSquare() * (gradNceOutput(idNceOutputSize, idBlockSize) * gradNceOutput(idNceOutputSize, idBlockSize)) / sqrt(outputSize); 
	} else if (ag_type == LEARNINGRATE_AG) {
	  squareSelectGradWeight.square(localInput);
	  squareSelectGradWeight.scal((gradNceOutput(idNceOutputSize, idBlockSize) * gradNceOutput(idNceOutputSize, idBlockSize)) / sqrt(outputSize)); 
	}
	
	if (use_cache == 1) {
	  selectCumulGradWeight.scal(cache);
	  //selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0-cache);
	  selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0);
	} else {
	  selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0);
	}

	squareSelectGradWeight.squareRoot(selectCumulGradWeight);
	squareSelectGradWeight.axpy(util1Weight, e);
	learningRateAfterADAGWeight = learningRate;
	learningRateAfterADAGWeight.div(squareSelectGradWeight);

	if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
	  localWeight.scal(1 - sqrt(learningRateAfterADAGWeight.averageSquare()) * weightDecay);
	  localWeight.axpy(localInput, -sqrt(learningRateAfterADAGWeight.averageSquare()) * gradNceOutput(idNceOutputSize, idBlockSize));
	} else if (ag_type == LEARNINGRATE_AG) {
	  utilLocalInput.copy(localInput);
	  utilLocalInput.axpy(localWeight, weightDecay);
	  utilLocalInput.product(learningRateAfterADAGWeight);
	  localWeight.axpy(utilLocalInput, -gradNceOutput(idNceOutputSize, idBlockSize));
	}
	//Bias
	if (use_cache == 1) {
	  cumulGradBias(consideredWord) *= cache; 
	  cumulGradBias(consideredWord) += (1-cache) * gradNceOutput(idNceOutputSize, idBlockSize) * gradNceOutput(idNceOutputSize, idBlockSize) / sqrt(outputSize);
	} else {
	  cumulGradBias(consideredWord) += gradNceOutput(idNceOutputSize, idBlockSize) * gradNceOutput(idNceOutputSize, idBlockSize) / sqrt(outputSize);
	}
	learningRateAfterADAGBias = learningRate / sqrt(cumulGradBias(consideredWord) + e);
	bias(consideredWord) -= learningRateAfterADAGBias * gradNceOutput(idNceOutputSize, idBlockSize);
      }
    }
  }
}

void LinearNCE_AG::read(ioFile* iof) {
  LinearNCE::read(iof);
  /*
  if (ag_type == LEARNINGRATE_AG
      || ag_type == LEARNINGRATE_CAG
      || ag_type == LEARNINGRATE_BAG
      || ag_type == LEARNINGRATE_DBAG
      || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.read(iof);
    cumulGradBias.read(iof);
  }
  */
}

void LinearNCE_AG::write(ioFile* iof) {
  LinearNCE::write(iof);
  /*
  if (ag_type == LEARNINGRATE_AG
      || ag_type == LEARNINGRATE_CAG
      || ag_type == LEARNINGRATE_BAG
      || ag_type == LEARNINGRATE_DBAG
      || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.write(iof);
    cumulGradBias.write(iof);
  }
  */
}

float LinearNCE_AG::sqrtCumulGradWeight() {
	return sqrt(cumulGradWeight.averageSquare());
}

float LinearNCE_AG::sqrtCumulGradBias() {
	return sqrt(cumulGradBias.averageSquare());
}
