#include "mainModule.H"

LookupTable_AG::LookupTable_AG() {

}

LookupTable_AG::LookupTable_AG(int indexNumber, int dimensionSize,
			       int inputSize, int blockSize, int oneClass, outils* otl) :
  LookupTable(indexNumber, dimensionSize, inputSize, blockSize, oneClass,
	      otl) {
  name = "LookupTable_AG";
  ag_type = "no_type";
  use_cache = 0;
}

LookupTable_AG::LookupTable_AG(Embeddings* orig) {
        name = "LookupTable_AG";
	ag_type = "no_type";
	use_cache = 0;
	weight.copy_and_delete(orig->weight);
	output.copy_and_delete(orig->output);
	otl = orig->otl;
	this->blockSize = orig->blockSize;
	this->dimensionSize = orig->dimensionSize;
	this->indexNumber = orig->indexNumber;
}

LookupTable_AG::~LookupTable_AG() {
}

void LookupTable_AG::resetAG() {
  cumulGradWeight = INIT_VALUE_ADAG;
}

void LookupTable_AG::initAG(string type){
  if (ag_type != LEARNINGRATE_AG
      && ag_type != LEARNINGRATE_CAG
      && ag_type != LEARNINGRATE_BAG
      && ag_type != LEARNINGRATE_DBAG
      && ag_type != LEARNINGRATE_NCE_BAG) {
    ag_type = type;
  } else {
    cout << "Warning: LookupTable_AG has learning type " << ag_type
         << " and can't be attributed " << type
         << ". Learning will continue as " << ag_type
         << endl;
  }
  if (ag_type == LEARNINGRATE_AG) {
    cumulGradWeight.resize(weight);
    squareSelectGradWeight.resize(dimensionSize, 1);  
  } else if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
    cumulGradWeight.resize(1, indexNumber);
    squareSelectGradWeight.resize(1, 1);
  } else {
    cout << "Learning strategy "
         << ag_type
         << " is not working with Adagrad"
         << endl;
    exit(1);
  }
  
  cumulGradWeight = INIT_VALUE_ADAG;
  
  learningRateAfterADAGWeight.resize(squareSelectGradWeight);
  util1Weight.resize(squareSelectGradWeight);
  util1Weight = 1;
}

void LookupTable_AG::updateParameters(float learningRate) {
  int x0, x1;
  for (int i = 0; i < input.size[1]; i++) {
    x0 = 0;
    x1 = dimensionSize - 1;
    for (int j = 0; j < input.size[0]; j++) {
      selectWeight.select(weight, 1, input(j, i));
      selectGradWeight.sub(gradWeight, x0, x1, i, i);
      selectCumulGradWeight.select(cumulGradWeight, 1, input(j, i));
      if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
	squareSelectGradWeight = selectGradWeight.averageSquare();
      } else if (ag_type == LEARNINGRATE_AG) {
	squareSelectGradWeight.square(selectGradWeight);
      }

      if (use_cache == 1) {
	selectCumulGradWeight.scal(cache);
	//selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0-cache);
	selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0);
      } else {
	selectCumulGradWeight.axpy(squareSelectGradWeight, 1.0);
      }      

      squareSelectGradWeight.squareRoot(selectCumulGradWeight);
      squareSelectGradWeight.axpy(util1Weight, e);
      learningRateAfterADAGWeight = learningRate;
      learningRateAfterADAGWeight.div(squareSelectGradWeight);     
      if (ag_type == LEARNINGRATE_CAG || ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG || ag_type == LEARNINGRATE_NCE_BAG) {
	selectWeight.axpy(selectGradWeight, -sqrt(learningRateAfterADAGWeight.averageSquare()));
      } else if (ag_type == LEARNINGRATE_AG) {
	learningRateAfterADAGWeight.product(selectGradWeight);
	selectWeight.axpy(learningRateAfterADAGWeight, -1.0);
      }
      x0 += dimensionSize;
      x1 += dimensionSize;
    }
  }
}


void LookupTable_AG::read(ioFile* iof) {
	LookupTable::read(iof);
	/*
	iof->readString(ag_type);
        if (ag_type == LEARNINGRATE_AG
            || ag_type == LEARNINGRATE_CAG
            || ag_type == LEARNINGRATE_BAG
            || ag_type == LEARNINGRATE_DBAG
	    || ag_type == LEARNINGRATE_NCE_BAG) {
          cumulGradWeight.read(iof);
	}
	*/
}

void LookupTable_AG::write(ioFile* iof) {
	LookupTable::write(iof);
	/*
	iof->writeString(ag_type);
        if (ag_type == LEARNINGRATE_AG
            || ag_type == LEARNINGRATE_CAG
            || ag_type == LEARNINGRATE_BAG
            || ag_type == LEARNINGRATE_DBAG
	    || ag_type == LEARNINGRATE_NCE_BAG) {
          cumulGradWeight.write(iof);
        }
	*/
}

float LookupTable_AG::sqrtCumulGradWeight() {
	return sqrt(cumulGradWeight.averageSquare());
}
