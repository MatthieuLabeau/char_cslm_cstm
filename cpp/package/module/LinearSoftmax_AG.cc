#include "mainModule.H"

LinearSoftmax_AG::LinearSoftmax_AG() {

}

LinearSoftmax_AG::LinearSoftmax_AG(int inputSize, int outputSize, int blockSize,
		outils* otl) :
		LinearSoftmax(inputSize, outputSize, blockSize, otl) {
	name = "LinearSoftmax_AG";
	ag_type = "no_type";
	use_cache = 0;

        gradWeight.resize(weight);
        gradBias.resize(bias);
}

LinearSoftmax_AG::~LinearSoftmax_AG() {

}

LinearSoftmax_AG::LinearSoftmax_AG(ProbOutput* orig) {
	name = "LinearSoftmax_AG";
	blockSize = orig->blockSize;
	weightDecay = orig->weightDecay;
	weight.copy_and_delete(orig->weight);
	bias.copy_and_delete(orig->bias);
	V1col.copy_and_delete(orig->V1col);
	softmaxV1row.copy_and_delete(orig->softmaxV1row);
	softmaxVCol.copy_and_delete(orig->softmaxVCol);
	gradInput.copy_and_delete(orig->gradInput);
	output.copy_and_delete(orig->output);
	gradOutput.copy_and_delete(orig->gradOutput);
	preOutput.copy_and_delete(orig->preOutput);

	otl = orig->otl;
	// for ADAG
	ag_type = "no_type";
	use_cache = 0;
	gradWeight.resize(weight);
	gradBias.resize(bias);
}

void LinearSoftmax_AG::initAG(string type){
  if (ag_type != LEARNINGRATE_AG
      && ag_type != LEARNINGRATE_CAG
      && ag_type != LEARNINGRATE_BAG
      && ag_type != LEARNINGRATE_DBAG) {
    ag_type = type;
  } else {
    cout << "Warning: Linear_AG has learning type " << ag_type
         << " and can't be attributed " << type
         << ". Learning will continue as " << ag_type
         << endl;
  }
  if (ag_type == LEARNINGRATE_AG) {
    utilGradWeight.resize(weight);
    cumulGradWeight.resize(weight);
    cumulGradBias.resize(bias);
  } else if (ag_type == LEARNINGRATE_CAG) {
    utilGradWeight.resize(weight);
    cumulGradWeight.resize(bias);
    cumulGradBias.resize(bias);
    util1Col.resize(weight.size[0],1);
    util1Col = 1;
  } else if (ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG) {
    cumulGradWeight.resize(1, 1);
    cumulGradBias.resize(1, 1);
  } else {
    cout << "Learning strategy "
         << ag_type
         << " is not working with Adagrad"
         << endl;
    exit(1);
  }
  cumulGradWeight = INIT_VALUE_ADAG;
  cumulGradBias = INIT_VALUE_ADAG;
 
  squareGradWeight.resize(cumulGradWeight);
  squareGradBias.resize(cumulGradBias);

  learningRateAfterADAGWeight.resize(cumulGradWeight);
  learningRateAfterADAGBias.resize(cumulGradBias);

  util1Weight.resize(cumulGradWeight);
  util1Bias.resize(cumulGradBias);
  util1Weight = 1;
  util1Bias = 1;
}

void LinearSoftmax_AG::reset() {
  LinearSoftmax::reset();
  this->cumulGradWeight = 0;
  this->cumulGradBias = 0;
}

void LinearSoftmax_AG::resetAG() {
  cumulGradWeight = INIT_VALUE_ADAG;
  cumulGradBias = INIT_VALUE_ADAG;
}

void LinearSoftmax_AG::updateParameters(float learningRate) {
  gradWeight.gemm(input, 'N', gradOutput, 'T', 1, 0);
  gradBias.gemv(gradOutput, 'N', V1col, 1, 0);
	
  //Compute gradient history
  if (ag_type == LEARNINGRATE_AG) {
    // parameter by parameter
    squareGradWeight.square(gradWeight);
    squareGradBias.square(gradBias);
  } else if (ag_type == LEARNINGRATE_CAG) {
    // column by column
    squareGradWeight.gemv(gradWeight, 'T',util1Col, 1, 0);
    squareGradWeight.scal((float)1 / util1Col.size[0]);
    squareGradWeight.square(squareGradWeight);
    squareGradBias.square(gradBias);
  } else if (ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG) {
    // by tensor
    squareGradWeight = gradWeight.averageSquare();
    squareGradBias = gradBias.averageSquare();
  }

  if (use_cache == 1) {
    cumulGradWeight.scal(cache);
    //cumulGradWeight.axpy(squareGradWeight, 1.0-cache);
    cumulGradWeight.axpy(squareGradWeight, 1.0); 
    cumulGradBias.scal(cache);
    //cumulGradBias.axpy(squareGradBias, 1.0-cache);
    cumulGradBias.axpy(squareGradBias, 1.0);
  } else {
    cumulGradWeight.axpy(squareGradWeight, 1.0);
    cumulGradBias.axpy(squareGradBias, 1.0);
  }

  // compute adapted learning rates give this history
  learningRateAfterADAGWeight = learningRate;
  //squareGradWeight.copy(cumulGradWeight);
  squareGradWeight.squareRoot(cumulGradWeight);
  squareGradWeight.axpy(util1Weight, e);
  learningRateAfterADAGWeight.div(squareGradWeight);

  learningRateAfterADAGBias = learningRate;
  //squareGradBias.copy(cumulGradBias);
  squareGradBias.squareRoot(cumulGradBias);
  squareGradBias.axpy(util1Bias, e);
  learningRateAfterADAGBias.div(squareGradBias);

  // update parameters
  if (ag_type == LEARNINGRATE_AG) {
    utilGradWeight.copy(gradWeight);
    utilGradWeight.axpy(weight, weightDecay);
    utilGradWeight.product(learningRateAfterADAGWeight);
    weight.axpy(utilGradWeight, -1.0);
    learningRateAfterADAGBias.product(gradBias);
    bias.axpy(learningRateAfterADAGBias, -1.0);
  } else if (ag_type == LEARNINGRATE_CAG) {
    utilGradWeight = 0;
    utilGradWeight.ger(util1Col, learningRateAfterADAGWeight, 1);
    gradWeight.axpy(weight, weightDecay);
    utilGradWeight.product(gradWeight);
    gradWeight.axpy(weight, -weightDecay);
    weight.axpy(utilGradWeight, -1.0);
    learningRateAfterADAGBias.product(gradBias);
    bias.axpy(learningRateAfterADAGBias, -1.0);
  } else if (ag_type == LEARNINGRATE_BAG || ag_type == LEARNINGRATE_DBAG) {
    weight.scal(1 - sqrt(learningRateAfterADAGWeight.averageSquare()) * weightDecay);
    weight.axpy(gradWeight, -sqrt(learningRateAfterADAGWeight.averageSquare()));
    bias.axpy(gradBias, -sqrt(learningRateAfterADAGBias.averageSquare()));
  }
}

void LinearSoftmax_AG::read(ioFile *iof) {
	LinearSoftmax::read(iof);
	/*
        if (ag_type == LEARNINGRATE_AG
            || ag_type == LEARNINGRATE_CAG
            || ag_type == LEARNINGRATE_BAG
            || ag_type == LEARNINGRATE_DBAG) {
          cumulGradWeight.read(iof);
          cumulGradBias.read(iof);
        }
	*/
}

void LinearSoftmax_AG::write(ioFile * iof) {
	LinearSoftmax::write(iof);
	/*
        if (ag_type == LEARNINGRATE_AG
            || ag_type == LEARNINGRATE_CAG
            || ag_type == LEARNINGRATE_BAG
            || ag_type == LEARNINGRATE_DBAG) {
          cumulGradWeight.write(iof);
          cumulGradBias.write(iof);
        }
	*/
}

float LinearSoftmax_AG::sqrtCumulGradWeight() {
  return sqrt(cumulGradWeight.averageSquare());
}

float LinearSoftmax_AG::sqrtCumulGradBias() {
  return sqrt(cumulGradBias.averageSquare());
}
