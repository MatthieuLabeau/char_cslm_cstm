/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS

 General class for module (layer)
 *******************************************************************/

#include "mainModule.H"

Module::Module()
{
}
Module::~Module()
{
	// for test
	//cout << "Module::~Module here" << endl;
}

void Module::to_ag() {
	cerr << "Warning : default Module::to_ag, does no thing" << endl;
}

void
Module::shareWeight(floatTensor& weight)
{
	this->weight.tieData(weight);
}

float
Module::sqrtCumulGradWeight() {
	cerr << "Module::sqrtCumulGradWeight default function, return 0" << endl;
	return 0;
}

float
Module::sqrtCumulGradBias() {
	cerr << "Module::sqrtCumulGradBias default function, return 0" << endl;
	return 0;
}

void
Module::copy(Module* anotherModule) {
	if (anotherModule->weight.haveMemory == 1) {
		weight.copy(anotherModule->weight);
	}
	if (anotherModule->bias.haveMemory == 1) {
		bias.copy(anotherModule->bias);
	}
}
