#include "mainModule.H"
#include <boost/lexical_cast.hpp>

LinearNCE::LinearNCE() {
	nceWeight = NULL;
}

LinearNCE::LinearNCE(int inputSize, int outputSize, int blockSize,
		outils* otl) {
	nceWeight = NULL;
	name = "LinearNCE";
	this->blockSize = blockSize;
	// in the beginning, no variables related to nceOutputSize will be initialized
	this->nceOutputSize = 0;
	weightDecay = 0;
	weight.resize(inputSize, outputSize);
	bias.resize(outputSize, 1);
	V1col.resize(blockSize, 1);
	V1col = 1;
	softmaxV1row.resize(outputSize, 1);
	softmaxV1row = 1;
	softmaxVCol.resize(blockSize, 1);
	gradInput.resize(inputSize, blockSize);
	output.resize(outputSize, blockSize);
	normalizeOutput.resize(output);
	gradOutput.resize(output);
	//copyOutput.resize(output);
	preOutput.resize(outputSize, blockSize);

	this->otl = otl;
	reset();
}

void LinearNCE::changeNceOutputSize(int nceOutputSize) {
	if (this->nceOutputSize != nceOutputSize) {
		this->nceOutputSize = nceOutputSize;
		if (nceWeight != NULL) {
			delete[] nceWeight;
		}
		// nceWeight connecting only to output words we need to consider
		nceWeight = new floatTensor[blockSize];
		for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
			nceWeight[idBlockSize].resize(gradInput.size[0], nceOutputSize);
		}
		nceBias.resize(nceOutputSize, 1);
		nceSoftmaxV1row.resize(nceOutputSize, 1);
		nceOutput.resize(nceOutputSize, blockSize);
		ncePreOutput.resize(nceOutputSize, blockSize);
		nceSoftmaxV1row.resize(nceOutputSize, 1);
		nceSoftmaxV1row = 1;
		nceNormalizeOutput.resize(nceOutput);
		gradNceOutput.resize(nceOutput);
	}
}

LinearNCE::~LinearNCE() {
	if (nceWeight != NULL) {
		delete[] nceWeight;
	}
}

void LinearNCE::reset() {
	weight.uniform(LINEAR_INIT0, LINEAR_INIT1, otl);
	bias.uniform(LINEAR_INIT0, LINEAR_INIT1, otl);
}

void LinearNCE::changeBlockSize(int blockSize) {
	this->blockSize = blockSize;
	V1col.resize(blockSize, 1);
	V1col = 1;
	int inputSize = gradInput.size[0];
	int outputSize = output.size[0];
	softmaxVCol.resize(blockSize, 1);
	gradInput.resize(inputSize, blockSize);
	output.resize(outputSize, blockSize);
	normalizeOutput.resize(output);
	if (nceOutputSize != 0) {
		nceOutput.resize(nceOutputSize, blockSize);
		ncePreOutput.resize(nceOutputSize, blockSize);
		nceNormalizeOutput.resize(nceOutput);
		gradNceOutput.resize(nceOutput);
		if (nceWeight != NULL) {
			delete[] nceWeight;
		}
		nceWeight = new floatTensor[blockSize];
		for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
			nceWeight[idBlockSize].resize(inputSize, nceOutputSize);
		}
	}
	gradOutput.resize(output);
	preOutput.resize(outputSize, blockSize);
}

floatTensor&
LinearNCE::forward(floatTensor& input) {
	this->input = input;
	// preOutput is the same as output in Linear.cc
	preOutput = 0;
	// preOutput = block_size bias columns
	// replicate the bias vector
	preOutput.ger(bias, V1col, 1);

	// Then, preOutput = preOutput + weight^T x input
	preOutput.gemm(weight, 'T', input, 'N', 1, 1);

	// output = e^(preOutput), we obtain unnormalized probabilities
	output.mexp(preOutput);

	return output;
}

floatTensor&
LinearNCE::normalizeForward(floatTensor& input) {
  output = forward(input);
  // softmaxVCol contains the sum for each column
  softmaxVCol.gemv(output, 'T', softmaxV1row, 1, 0);
  // For each column, divide by the sum to have
  // for each element e^x_i / \sum_j e^x_j
  for (int i = 0; i < output.size[1]; i++) {
    localNormalizeOutput.select(normalizeOutput, 1, i);
    localOutput.select(output, 1, i);
    localNormalizeOutput.copy(localOutput);
    localNormalizeOutput.scal(1.0 / softmaxVCol(i));
  }
  return normalizeOutput;
}

float LinearNCE::monitorNorm() {
  float f = 0;
  softmaxVCol.gemv(output, 'T', softmaxV1row, 1, 0);
  for (int i = 0 ; i < output.size[1] ; i ++) {
    f += softmaxVCol(i);
  }
  return f;
}

float LinearNCE::monitorScoreEntropy() {
  float f = 0;
  //copyOutput.copy(output);
  output.product(preOutput);
  softmaxVCol.gemv(output, 'T', softmaxV1row, 1, 0);
  for (int i = 0 ; i < output.size[1] ; i ++) {
    f += softmaxVCol(i);
  }
  return f; 
}

void LinearNCE::coefNorm(floatTensor& input, ioFile* file) {
	// write only text
	if (file->format != TEXT) {
		file->format = TEXT;
	}
	output = forward(input);
	// softmaxVCol contains the sum for each column
	softmaxVCol.gemv(output, 'T', softmaxV1row, 1, 0);
	for (int i = 0 ; i < output.size[1] ; i ++) {
		file->writeString(boost::lexical_cast<string>(softmaxVCol(i)));
	}
}

floatTensor&
LinearNCE::nceForward(floatTensor& input, intTensor& word) {
	if (word.size[0] != nceOutputSize || word.size[1] != blockSize) {
		cout
				<< "LinearNCE::nceForward word tensor is not in good size, the good size is "
				<< nceOutputSize << " x " << blockSize << ", but word is of "
				<< word.size[0] << " x " << word.size[1] << endl;
		cout << "LinearNCE::nceForward word : " << endl;
		word.write();
		exit(1);
	}
	// for test
	//cout << "LinearNCE::nceForward here" << endl;
	this->input = input;
	ncePreOutput = 0;
	// for test
	//cout << "LinearNCE::nceForward here 2" << endl;
	for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
		for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize;
				idNceOutputSize++) {
			localNceWeight.select(nceWeight[idBlockSize], 1, idNceOutputSize);
			if (word(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
				localWeight.select(weight, 1,
						word(idNceOutputSize, idBlockSize));
				localNceWeight.copy(localWeight);
				nceBias(idNceOutputSize) = bias(
						word(idNceOutputSize, idBlockSize));
				// ok, we finish fill in nceWeight and nceBias
			} else {
				localNceWeight = 0;
				nceBias(idNceOutputSize) = 0;
			}
		}
		// add bias first
		localNcePreOutput.select(ncePreOutput, 1, idBlockSize);
		localNcePreOutput.copy(nceBias);
		localInput.select(input, 1, idBlockSize);
		// Then, preOutput = preOutput + weight^T x input
		localNcePreOutput.gemm(nceWeight[idBlockSize], 'T', localInput, 'N', 1,
				1);
	}
	nceOutput.mexp(ncePreOutput);
	return nceOutput;
}

float LinearNCE::efficientNceForward(floatTensor& input, int word) {
	float output;
	if (word != SIGN_NOT_WORD) {
		localWeight.select(weight, 1, word);
		output = localWeight.dot(input) + bias(word);
	} else {
		output = 0;
	}
	output = exp(output);
	return output;
}

float LinearNCE::computeF(intTensor& outputWord, floatTensor& logPropTensor) {
  float f = 0;
  int k = nceOutputSize - 1;
  if (k > 0) {
    for (idBlockSize = 0; idBlockSize < outputWord.size[1]; idBlockSize++) {
      for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize; idNceOutputSize++) {
	if (outputWord(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
	  float modelProp = nceOutput(idNceOutputSize, idBlockSize);
	  if (isinf(modelProp)) {
	    modelProp = 1.0e38;
	  }
	  float noiseProp = exp(logPropTensor(idNceOutputSize, idBlockSize));
	  if (idNceOutputSize == 0) {				  
	    f -= log(modelProp / (modelProp + k * noiseProp));
	  } else {
	    f -= log(k * noiseProp / (modelProp + k * noiseProp));
	  }
	}
      }
    }
  } else if (k == 0) {
    for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
      if (outputWord(0, idBlockSize) != SIGN_NOT_WORD) {
	f -= logPropTensor(0, idBlockSize)
	  * log(nceOutput(0, idBlockSize));
      }
    }
  }
  return f;
}

floatTensor&
LinearNCE::normalizeNceForward(floatTensor& input, intTensor& word) {
	if (word.size[0] != nceOutputSize || word.size[1] != blockSize) {
		cout
				<< "LinearNCE::nceForward word tensor is not in good size, the good size is "
				<< nceOutputSize << " x " << blockSize << ", but word is of "
				<< word.size[0] << " x " << word.size[1] << endl;
		exit(1);
	}
	nceOutput = nceForward(input, word);
	// softmaxVCol contains the sum for each column
	softmaxVCol.gemv(nceOutput, 'T', nceSoftmaxV1row, 1, 0);
	// For each column, divide by the sum to have
	// for each element e^x_i / \sum_j e^x_j
	for (int i = 0; i < nceOutput.size[1]; i++) {
		localNormalizeOutput.select(nceNormalizeOutput, 1, i);
		localOutput.select(nceOutput, 1, i);
		localNormalizeOutput.copy(localOutput);
		localNormalizeOutput.scal(1.0 / softmaxVCol(i));
	}
	return nceNormalizeOutput;
}

floatTensor&
LinearNCE::backward(floatTensor& word) {
	cerr << "ERROR: backward of LinearNCE for floatTensor" << endl;
	exit(1);
}

floatTensor&
LinearNCE::backward(intTensor& word, floatTensor& logNoiseProps) {
	// firstly, compute gradNceOutput
	float modelProp, noiseProp;
	int k = nceOutputSize - 1;
	if (k > 0) {
		for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
			for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize;
					idNceOutputSize++) {
				if (word(idNceOutputSize, idBlockSize) != SIGN_NOT_WORD) {
					modelProp = nceOutput(idNceOutputSize, idBlockSize);
					noiseProp = exp(
							logNoiseProps(idNceOutputSize, idBlockSize));
					if (idNceOutputSize == 0) {
						// positive example
						gradNceOutput(idNceOutputSize, idBlockSize) = -k
								* noiseProp / (modelProp + k * noiseProp);
					} else {
						// negative example
						gradNceOutput(idNceOutputSize, idBlockSize) = modelProp
								/ (modelProp + k * noiseProp);
						if (isnan(gradNceOutput(idNceOutputSize, idBlockSize))) {
						  gradNceOutput(idNceOutputSize, idBlockSize) = 1.0;
						}
						if (isnan(gradNceOutput(idNceOutputSize, idBlockSize))) {
						  cout << word(idNceOutputSize, idBlockSize) << " " << modelProp << " " << noiseProp << endl;
						}
					}
				} else {
					gradNceOutput(idNceOutputSize, idBlockSize) = 0;
				}
			}
		}
	} else {
		if (k == 0) {
			for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
				if (word(0, idBlockSize) != SIGN_NOT_WORD) {
					gradNceOutput(0, idBlockSize) = -logNoiseProps(0,
							idBlockSize);
				} else {
					gradNceOutput(0, idBlockSize) = 0;
				}
			}
		}
	}
	for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
		localGradNceOutput.select(gradNceOutput, 1, idBlockSize);
		localGradInput.select(gradInput, 1, idBlockSize);
		localGradInput.gemm(nceWeight[idBlockSize], 'N', localGradNceOutput,
				'N', 1, 0);
	}
	return gradInput;
}

void LinearNCE::updateParameters(float learningRate) {
	cerr
			<< "LinearNCE::updateParameters this function does not run with only argument learningRate, the correct form : updateParameters(float learningRate, intTensor& word)"
			<< endl;
	exit(1);
}

void LinearNCE::updateParameters(float learningRate, intTensor& word) {
	for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
		for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize;
				idNceOutputSize++) {
			int consideredWord = word(idNceOutputSize, idBlockSize);
			if (consideredWord != SIGN_NOT_WORD) {
				localWeight.select(weight, 1, consideredWord);
				localInput.select(input, 1, idBlockSize);
				localWeight.scal(1 - learningRate * weightDecay);
				localWeight.axpy(localInput,
						-learningRate
								* gradNceOutput(idNceOutputSize, idBlockSize));
				bias(consideredWord) = bias(consideredWord)
						- learningRate
								* gradNceOutput(idNceOutputSize, idBlockSize);
			}
		}
	}
}

float LinearNCE::distance2(Module* anotherOutput) {
	LinearNCE* validModule = dynamic_cast<LinearNCE*>(anotherOutput);
	if (validModule == NULL) {
		cerr << "LinearNCE::distance2 anotherOutput is not a good type" << endl;
		exit(1);
	}
	floatTensor distMatrix;
	distMatrix.copy(this->weight);
	distMatrix.axpy(validModule->weight, -1);
	float res1 = distMatrix.sumSquared();
	distMatrix.resize(this->bias);
	distMatrix.copy(this->bias);
	distMatrix.axpy(validModule->bias, -1);
	return res1 + distMatrix.sumSquared();
}

void LinearNCE::read(ioFile* iof) {
	iof->readString(name);
	weight.read(iof);
	bias.read(iof);
}

void LinearNCE::write(ioFile* iof) {
	iof->writeString(name);
	weight.write(iof);
	bias.write(iof);
}
