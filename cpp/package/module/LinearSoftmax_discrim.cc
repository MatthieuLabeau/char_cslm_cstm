#include "mainModule.H"

LinearSoftmax_discrim::LinearSoftmax_discrim() {

}

LinearSoftmax_discrim::LinearSoftmax_discrim(int inputSize, int outputSize,
		int blockSize, outils* otl) :
		LinearSoftmax(inputSize, outputSize, blockSize, otl) {
	name = "LinearSoftmax_discrim";
}

LinearSoftmax_discrim::LinearSoftmax_discrim(ProbOutput* orig) {
	name = "LinearSoftmax_discrim";
	blockSize = orig->blockSize;
	weightDecay = orig->weightDecay;
	weight.copy_and_delete(orig->weight);
	bias.copy_and_delete(orig->bias);
	V1col.copy_and_delete(orig->V1col);
	softmaxV1row.copy_and_delete(orig->softmaxV1row);
	softmaxVCol.copy_and_delete(orig->softmaxVCol);
	gradInput.copy_and_delete(orig->gradInput);
	output.copy_and_delete(orig->output);
	gradOutput.copy_and_delete(orig->gradOutput);
	preOutput.copy_and_delete(orig->preOutput);

	otl = orig->otl;
}

LinearSoftmax_discrim::~LinearSoftmax_discrim() {

}

float LinearSoftmax_discrim::computeF(intTensor& word,
		floatTensor& coefTensor) {
	// output was computed by forward function
	float f = 0;
	for (int idBlockSize = 0; idBlockSize < word.length; idBlockSize++) {
		if (word(idBlockSize) != SIGN_NOT_WORD) {
			f -= coefTensor(idBlockSize)
					* log(output(word(idBlockSize), idBlockSize));
		}
	}
	return f;
}

floatTensor&
LinearSoftmax_discrim::backward(intTensor& word, floatTensor& coefTensor) {
	// gradOutput for weight and bias of the Linear part,
	// computed from the output after softmax (not the output of
	// the Linear part
	// coefTensor: this tensor is only for NCE algorithm
	// for test
	//cout << "LinearSoftmax::backward here" << endl;
	gradOutput.copy(output);
	// for test
	//cout << "LinearSoftmax::backward here1" << endl;
	int i;
	this->input = input;
	// for test
	//cout << "LinearSoftmax::backward here2" << endl;
	for (i = 0; i < blockSize; i++) {
		// If taking account this n-gram
		// In some cases, for some examples in the block, we don't
		// want to update with them, e.g., blockSize = 128 but in the
		// last block, we have only 78, predicted word for 50 *
		// last examples should be set to SIGN_NOT_WORD
		// If using, subtract its value in gradOutput 1
		selectGradOutput.select(gradOutput, 1, i);
		if (word(i) != SIGN_NOT_WORD) {
			gradOutput(word(i), i) -= 1;
			// multiply the gradient corresponding to word(i) with coefTensor(i)
			// here is the only difference between LinearSoftmax_discrim and LinearSoftmax
			selectGradOutput.scal(coefTensor(i));
		}
		// Not use, all values = 0
		else {
			selectGradOutput = 0;
		}
	}
	// now we have calculated gradOutput, from which we calculate gradInput easily with the traditional chain rule
	gradInput.gemm(weight, 'N', gradOutput, 'N', 1, 0);
	return gradInput;
}
