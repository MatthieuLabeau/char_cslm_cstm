#include "mainModule.H"
#include <boost/lexical_cast.hpp>

HybridNCE::HybridNCE() {
  nceWeight = NULL;
}

HybridNCE::HybridNCE(int inputSize, int outputSize, int blockSize,
		     outils* otl) {
  //nceWeight = NULL;
  name = "HybridNCE";
  this->blockSize = blockSize;
  this->nceOutputSize = 0;
  this->shortListSize = 0;

  weightDecay = 0;
  weight.resize(inputSize, outputSize);
  bias.resize(outputSize, 1);
  bias = 0;
  V1col.resize(blockSize, 1);
  V1col = 1;
  softmaxV1row.resize(outputSize, 1);
  softmaxV1row = 1;
  softmaxVCol.resize(blockSize, 1);
  gradInput.resize(inputSize, blockSize);
  output.resize(outputSize, blockSize);
  normalizeOutput.resize(output);
  gradOutput.resize(output);
  preOutput.resize(outputSize, blockSize);

  this->otl = otl;
  reset();
}

HybridNCE::~HybridNCE() {
}

void HybridNCE::reset() {
  weight.uniform(LINEAR_INIT0, LINEAR_INIT1, otl);
}

void HybridNCE::updateParameters(float learningRate, intTensor& word) {
  for (idBlockSize = 0; idBlockSize < blockSize; idBlockSize++) {
    for (idNceOutputSize = 0; idNceOutputSize < nceOutputSize;
	 idNceOutputSize++) {
      int consideredWord = word(idNceOutputSize, idBlockSize);
      //check the word is among the frequent ones (small voc)
      if (consideredWord != SIGN_NOT_WORD && consideredWord < shortListSize) {
	localWeight.select(weight, 1, consideredWord);
	localInput.select(input, 1, idBlockSize);
	localWeight.scal(1 - learningRate * weightDecay);
	localWeight.axpy(localInput,
			 -learningRate
			 * gradNceOutput(idNceOutputSize, idBlockSize));
	bias(consideredWord) = bias(consideredWord)
	  - learningRate
	  * gradNceOutput(idNceOutputSize, idBlockSize);
      }
    }
  }
}

void HybridNCE::setFrequentEmbeddings(floatTensor& wordEmbeddings, floatTensor& wordBias) {
  this->shortListSize = wordEmbeddings.size[1];
  if (wordEmbeddings.size[0] != weight.size[0]) {
    cout << "HybridNCE::setFrequentEmbeddings wrong size: "
         << weight.size[0] << " x " << shortListSize << " but input embeddings are of size "
         << wordEmbeddings.size[0] << " x " << wordEmbeddings.size[1] << endl;
    exit(1);
  }
  selectWeight.sub(weight, 0, weight.size[0] - 1, 0, shortListSize - 1); 
  selectWeight.copy(wordEmbeddings);
  selectBias.sub(bias, 0, shortListSize - 1, 0, 0);
  selectBias.copy(wordBias);
}

void HybridNCE::updateRareEmbeddings(floatTensor& charEmbeddings) {
  if (charEmbeddings.size[0] != weight.size[0] || charEmbeddings.size[1] != weight.size[1] - shortListSize ) {
    cout << "HybridNCE::updateRareEmbeddings wrong size: "
	 << weight.size[0] << " x " << weight.size[1] - shortListSize << " but input embeddings are of size "
	 << charEmbeddings.size[0] << " x " << charEmbeddings.size[1] << endl;
    exit(1);
  }
  selectWeight.sub(weight, 0, weight.size[0] - 1, shortListSize, weight.size[1] - 1);
  selectWeight.copy(charEmbeddings);
}

void HybridNCE::read(ioFile* iof) {
  iof->readString(name);
  iof->readInt(shortListSize);
  weight.read(iof);
  bias.read(iof);
}

void HybridNCE::write(ioFile* iof) {
  iof->writeString(name);
  iof->writeInt(shortListSize);
  weight.write(iof);
  bias.write(iof);
}
