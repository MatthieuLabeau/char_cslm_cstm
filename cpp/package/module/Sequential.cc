/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS

 Specific module used to build a sequential network.  The methods
 forward, backward, and updateParameters only call sequentially the
 corresponding methods of each module. The add method aims to add a
 new module, and the last added module is the output (the first
 added module is the input of the network).
 *******************************************************************/
#include "mainModule.H"
SPLIT_STRING
Sequential::Sequential(int maxSize) {
	size = 0;
	modules = new Module*[maxSize];
}

Sequential::~Sequential() {
	delete lkt;
	for (int idel = 0; idel < size; idel++) {
		delete modules[idel];
	}
	delete[] modules;
}
void Sequential::changeBlockSize(int blockSize) {
	this->blockSize = blockSize;
	int i;
	for (i = 0; i < size; i++) {
		modules[i]->changeBlockSize(blockSize);
	}
	lkt->changeBlockSize(blockSize);
	// Update output
	output = modules[size - 1]->output;
}

void Sequential::add(Module* module) {
	size++;
	modules[size - 1] = module;
	output = module->output;
}

floatTensor&
Sequential::forward(intTensor& input) {
	int i;
	//CharEmbeddings* clkt = dynamic_cast<CharEmbeddings*>(lkt);
	//CharWordEmbeddings* cwlkt = dynamic_cast<CharWordEmbeddings*>(lkt);
	/*
	if (clkt) {
	  currentOutput = clkt->forward(input);
	} else if (cwlkt) {
	  currentOutput = cwlkt->forward(input);
	  } else {*/
	currentOutput = lkt->forward(input);
	//}
	for (i = 0; i < size; i++) {
		currentOutput = modules[i]->forward(currentOutput);
	}
	return currentOutput;
}

floatTensor&
Sequential::backward(floatTensor& gradOutput) {
	currentGradOutput = gradOutput;

	Module* currentModule = modules[size - 1];
	Module* previousModule;
	int i;
	for (i = size - 2; i > -1; i--) {
		previousModule = modules[i];
		currentGradOutput = currentModule->backward(currentGradOutput);
		//cout << "Hidden gradOutput:" << currentGradOutput.averageSquare() << endl;
		currentModule = previousModule;
	}
	currentGradOutput = currentModule->backward(currentGradOutput);
	// Also backward with Lookup Table
	currentGradOutput = lkt->backward(currentGradOutput);
	return currentGradOutput;
}

void Sequential::updateParameters(float learningRate, int update_lkt,
		string update_hiddens) {
	int i;
	string delim = "_";
	vector<string> vector_update_hiddens = split_string(update_hiddens, delim);
	//DEBUG(
		//	cout << "vector_update_hiddens : " << endl; tr(vector_update_hiddens, it) {cout << *it << endl;})
	if (vector_update_hiddens.size() != size) {
		cout
				<< "Sequential::updateParameter ERROR: size of update_hiddens is not compatible"
				<< endl;
		cout << "Sequential::updateParameter vector_update_hiddens.size() = "
				<< vector_update_hiddens.size() << endl;
		cout << "While inner size is " << size << endl;
		cout << "update_hidden = " << update_hiddens << endl;
		exit(1);
	} else {
		for (i = 0; i < size; i++) {
			//DEBUG(cout << "i : " << i << endl;)
			//DEBUG(
				//	cout << "atoi : " << atoi((vector_update_hiddens.at(i)).c_str()) << endl;)
			if (atoi((vector_update_hiddens.at(i)).c_str()) == 1) {
				modules[i]->updateParameters(learningRate);
			}
		}
		if (update_lkt == 1) {
			// Also update with Lookup Table if update_lkt == 1
			lkt->updateParameters(learningRate);
		}
	}
}

void Sequential::read(ioFile* iof) {
	int i;
	lkt->read(iof);
	for (i = 0; i < size; i++) {
		modules[i]->read(iof);
	}
}
void Sequential::write(ioFile* iof) {
	int i;
	lkt->write(iof);
	for (i = 0; i < size; i++) {
		modules[i]->write(iof);
	}
}

void Sequential::write(ioFile* iof, string name) {
	lkt->write(iof);
	for (int i = 0; i < size; i++) {
		modules[i]->write(iof);
	}
}
