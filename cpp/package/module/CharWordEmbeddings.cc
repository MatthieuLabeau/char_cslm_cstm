/******************************************************************                                                                                                                                         
 Structure OUtput Layer (SOUL) Language Model Toolkit                                                                                                                                                       
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS                                                                                                                                                            
*******************************************************************/

#include "mainModule.H"

CharWordEmbeddings::CharWordEmbeddings() {
}

CharWordEmbeddings::CharWordEmbeddings(int wIndex, int nDimension, int inputSize, int cIndex, int cDimension, string activation, int blockSize, int oneClass, outils *otl) {
  name="CharWordEmbeddings";
  this->wIndex = wIndex;
  this->inputSize = inputSize;
  this->blockSize = blockSize;
  this->dimensionSize = nDimension;
  this->charDimensionSize = cDimension;
  this->update_lkt = 1;
  this->activation = activation;
  this->cLkt = new CharLookupTable(cIndex, cDimension, inputSize, blockSize, oneClass, otl);
  this->Lkt = new LookupTable(wIndex, dimensionSize/2, inputSize, blockSize, oneClass, otl); 
  this->poolModule = new Pooling(inputSize, nDimension/2, blockSize);
  if (activation == TANH) {
    this->actPoolModule = new Tanh(nDimension/2 * inputSize, blockSize);
  }
  else if (activation == RELU) {
    this->actPoolModule = new Relu(nDimension/2 * inputSize, blockSize);
  }
  else {
    this->actPoolModule = new Sigmoid(nDimension/2 * inputSize, blockSize);
  }
  output.resize(inputSize * dimensionSize, blockSize);
  restrictedInput.resize(inputSize, blockSize);
  charGradOutput.resize(inputSize * dimensionSize / 2, blockSize);
  wordGradOutput.resize(inputSize * dimensionSize / 2, blockSize);
}

CharWordEmbeddings::~CharWordEmbeddings() {
  delete Lkt;
  delete cLkt;
  delete convModule;
  delete actModule;
  delete poolModule;
  delete actPoolModule;
}

void CharWordEmbeddings::getHashMaps(char* hmf) { 
  cLkt->getHashMaps(hmf);
  this->convModule = new Linear(charDimensionSize * cLkt->nChar, dimensionSize/2, cLkt->maxLength * inputSize * blockSize, otl); 
  if (activation == TANH) {
    this->actModule = new Tanh(dimensionSize/2, cLkt->maxLength * inputSize * blockSize);
  }
  else if (activation == RELU) {
    this->actModule = new Relu(dimensionSize/2, cLkt->maxLength * inputSize * blockSize);
  }
  else {
  this->actModule = new Sigmoid(dimensionSize/2, cLkt->maxLength * inputSize * blockSize);
  }
}

void CharWordEmbeddings::changeBlockSize(int blockSize) {
  this->blockSize = blockSize;
  output.resize(inputSize * dimensionSize, blockSize);
  restrictedInput.resize(inputSize, blockSize);
  charGradOutput.resize(inputSize * dimensionSize / 2, blockSize);
  wordGradOutput.resize(inputSize * dimensionSize / 2, blockSize);
  Lkt->changeBlockSize(blockSize);
  cLkt->changeBlockSize(blockSize);
  poolModule->changeBlockSize(blockSize);
  actPoolModule->changeBlockSize(blockSize);
}


void CharWordEmbeddings::reset() {
  Lkt->reset();
  cLkt->reset();
}

void CharWordEmbeddings::init1class() {
  Lkt->init1class();
  cLkt->init1class();
}

floatTensor& CharWordEmbeddings::forward(intTensor& input) {
  this->input = input;
  this->restrictedInput.copy(input);
  charCurrentOutput = cLkt->forward(input);
  //cout << "cLkt weight:" << cLkt->weight.averageSquare() << endl;
  //cout << "cLkt out:" << charCurrentOutput.averageSquare() << endl;
  // Once forward has been called in cLkt we know the total length of the tensor to go through conv/actModule and we can resize them
  convModule->changeBlockSize(cLkt->totLength);
  //actModule->changeBlockSize(cLkt->totLength);
  charCurrentOutput = convModule->forward(charCurrentOutput);
  //cout << "conv weight:" << convModule->weight.averageSquare() << endl;
  //cout << "conv out:" << charCurrentOutput.averageSquare() << endl;
  //charCurrentOutput = actModule->forward(charCurrentOutput);
  //cout << "act out:" << charCurrentOutput.averageSquare() << endl;
  // Similarly, we need info from cLkt to use Pooling, which has no parameters and will be used only for backward
  poolModule->wordLength = cLkt->wordLength;
  poolModule->wordStart = cLkt->wordStart;
  charCurrentOutput = poolModule->forward(charCurrentOutput);
  //cout << "pool out:" << charCurrentOutput.averageSquare() << endl;
  charCurrentOutput = actPoolModule->forward(charCurrentOutput);
  //cout << "act out:" << charCurrentOutput.averageSquare() << endl;
  // We restrict the input to the lookuptable vocabulary, get its output 
  restrictedInput.restrict(wIndex-1);
  wordCurrentOutput = Lkt->forward(restrictedInput);
  //cout << "Lkt weight:" << Lkt->weight.averageSquare() << endl;
  //cout << "Lkt out:" << wordCurrentOutput.averageSquare() << endl;
  // Concatenate the two
  selectCurrentOutput.sub(output, 0, inputSize * (dimensionSize/2) - 1, 0, blockSize-1);
  selectCurrentOutput.copy(charCurrentOutput);
  selectCurrentOutput.sub(output, inputSize * (dimensionSize/2), inputSize * dimensionSize - 1, 0, blockSize-1);
  selectCurrentOutput.copy(wordCurrentOutput);
  //cout << "output :" << selectCurrentOutput.averageSquare() << endl;
  return output;
}


floatTensor& CharWordEmbeddings::backward(floatTensor& gradOutput) {
  selectCurrentGradOutput.sub(gradOutput, 0, inputSize * (dimensionSize/2) - 1, 0, blockSize-1);
  charGradOutput.copy(selectCurrentGradOutput);
  selectCurrentGradOutput.sub(gradOutput, inputSize * (dimensionSize/2), inputSize * dimensionSize - 1, 0, blockSize-1);   
  wordGradOutput.copy(selectCurrentGradOutput);
  //cout << "gradoutput :" << gradOutput.averageSquare() << endl;
  // On the word lookup table
  wordCurrentGradOutput = Lkt->backward(wordGradOutput);
  // On the character lookup table
  charCurrentGradOutput = actPoolModule->backward(charGradOutput);
  //cout << "act gradoutput :" << charCurrentGradOutput.averageSquare() << endl;
  charCurrentGradOutput = poolModule->backward(charCurrentGradOutput);
  //cout << "pool gradoutput :" << charCurrentGradOutput.averageSquare() << endl;
  //charCurrentGradOutput = actModule->backward(charCurrentGradOutput);
  //cout << "act gradoutput :" << charCurrentGradOutput.averageSquare() << endl;
  charCurrentGradOutput = convModule->backward(charCurrentGradOutput);
  //cout << "conv gradoutput :" << charCurrentGradOutput.averageSquare() << endl;
  charCurrentGradOutput = cLkt->backward(charCurrentGradOutput);
  return gradOutput;
}


void CharWordEmbeddings::updateParameters(float learningRate) {
  convModule->updateParameters(learningRate); 
  if (update_lkt == 1) {
    Lkt->updateParameters(learningRate);
    cLkt->updateParameters(learningRate);
  }
}

void CharWordEmbeddings::read(ioFile *iof) {
  iof->readString(name);
  cout << name << endl;
  Lkt->read(iof);
  cLkt->read(iof);
  convModule->read(iof);
  //actModule->read(iof);
  poolModule->read(iof);
  actPoolModule->read(iof);
}
void CharWordEmbeddings::write(ioFile *iof) {
  iof->writeString(name);
  Lkt->write(iof);
  cLkt->write(iof);
  convModule->write(iof);
  //actModule->write(iof);
  poolModule->write(iof);
  actPoolModule->write(iof);
}
