

class LookupTable_AG : public LookupTable {
public: 
        float e;
        float cache;
        string ag_type;
        int use_cache;

        floatTensor util1Weight;
        floatTensor cumulGradWeight;
        floatTensor selectCumulGradWeight;
        floatTensor squareSelectGradWeight;
        floatTensor learningRateAfterADAGWeight;

	LookupTable_AG();

	LookupTable_AG(int nIndex, int nDimension, int inputSize, int blockSize,
		      int oneClass, outils* otl);

	// upgrade an ordinary LookupTable to LookupTable_AG
	LookupTable_AG(Embeddings* orig);

	~LookupTable_AG();

        void
        initAG(string type);
         
        void
	resetAG();
       
	void
	updateParameters(float learningRate);

	void
	read(ioFile *iof);
	void
	write(ioFile * iof);

	float
	sqrtCumulGradWeight();
};
