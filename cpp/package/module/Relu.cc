#include "mainModule.H"

Relu::Relu(int size0, int size1)
{
  blockSize = size1;
  output.resize(size0, size1);
  gradInput.resize(size0, size1);
}

Relu::~Relu()
{
}

void Relu::to_ag() {

}

void
Relu::changeBlockSize(int blockSize)
{
  this->blockSize = blockSize;
  int size0 = output.size[0];
  int size1 = blockSize;
  output.resize(size0, size1);
  gradInput.resize(size0, size1);
}

floatTensor&
Relu::forward(floatTensor& input)
{
  output.relu(input);
  return output;
}

floatTensor&
Relu::backward(floatTensor& gradOutput)
{
  gradInput.invrelu(output);
  gradInput.product(gradOutput);
  return gradInput;
}
void
Relu::updateParameters(float learningRate)
{
}
void
Relu::read(ioFile* iof)
{
  iof->readString(name);
}
void
Relu::write(ioFile* iof)
{
  iof->writeString((char*) "Relu");
}

float
Relu::distance2(Module* anotherModule) {
	return 0;
}
