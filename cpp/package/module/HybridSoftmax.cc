#include "mainModule.H"

HybridSoftmax::HybridSoftmax() {
}

HybridSoftmax::HybridSoftmax(int inputSize, int outputSize, int blockSize,
    outils* otl) {
	name = "HybridSoftmax";
	this->blockSize = blockSize;
	this->shortListSize = 0;
	weightDecay = 0;
	weight.resize(inputSize, outputSize);
	selectfWeight.resize(weight);
	bias.resize(outputSize, 1);
	selectfBias.resize(bias);
	bias = 0;
	V1col.resize(blockSize, 1);
	V1col = 1;
	softmaxV1row.resize(outputSize, 1);
	softmaxV1row = 1;
	softmaxVCol.resize(blockSize, 1);
	gradInput.resize(inputSize, blockSize);
	output.resize(outputSize, blockSize);
	gradOutput.resize(output);
	selectfGradOutput.resize(output);
	preOutput.resize(outputSize, blockSize);

	this->otl = otl;
	reset();
}

HybridSoftmax::~HybridSoftmax() {
}

void HybridSoftmax::reset() {
  weight.uniform(LINEAR_INIT0, LINEAR_INIT1, otl);
}

void HybridSoftmax::updateParameters(float learningRate) {
  if (shortListSize > 0) {
    selectfWeight.sub(weight, 0, weight.size[0] - 1, 0, shortListSize - 1);
    selectfBias.sub(bias, 0, shortListSize - 1, 0, 0);
    selectfGradOutput.sub(gradOutput, 0, shortListSize - 1, 0, gradOutput.size[1] - 1);
    
    selectfWeight.gemm(input, 'N', selectfGradOutput, 'T', -learningRate, 1 - learningRate * weightDecay);
    selectfBias.gemv(selectfGradOutput, 'N', V1col, -learningRate, 1);
  }
}

void HybridSoftmax::setFrequentEmbeddings(floatTensor& wordEmbeddings, floatTensor& wordBias) {
  this->shortListSize = wordEmbeddings.size[1];
  if (wordEmbeddings.size[0] != weight.size[0]) {
    cout << "HybridSoftmax::setFrequentEmbeddings wrong size: "
         << weight.size[0] << " x " << shortListSize << " but input embeddings are of size "
         << wordEmbeddings.size[0] << " x " << wordEmbeddings.size[1] << endl;
    exit(1);
  }
  selectfWeight.sub(weight, 0, weight.size[0] - 1, 0, shortListSize - 1);
  selectfWeight.copy(wordEmbeddings);
  selectfBias.sub(bias, 0, shortListSize - 1, 0, 0);
  selectfBias.copy(wordBias);
}

void HybridSoftmax::updateRareEmbeddings(floatTensor& charEmbeddings) {
  if (charEmbeddings.size[0] != weight.size[0] || charEmbeddings.size[1] != weight.size[1] - shortListSize ) {
    cout << "HybridSoftmax::updateRareEmbeddings wrong size: "
         << weight.size[0] << " x " << weight.size[1] - shortListSize << " but input embeddings are of size "
         << charEmbeddings.size[0] << " x " << charEmbeddings.size[1] << endl;
    exit(1);
  }
  selectfWeight.sub(weight, 0, weight.size[0] - 1, shortListSize, weight.size[1] - 1);
  selectfWeight.copy(charEmbeddings);
}

void HybridSoftmax::read(ioFile* iof) {
	iof->readString(name);
	iof->readInt(shortListSize);
	weight.read(iof);
	bias.read(iof);
}
void HybridSoftmax::write(ioFile* iof) {
	iof->writeString(name);
	iof->writeInt(shortListSize);
	weight.write(iof);
	bias.write(iof);
}
