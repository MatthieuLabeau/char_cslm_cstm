#include "mainModule.H"

CharEmbeddings_AG::CharEmbeddings_AG() {
}

CharEmbeddings_AG::CharEmbeddings_AG(int nDimension, int inputSize, int cIndex, int cDimension, string activation, int blockSize,
				     int oneClass,  outils *otl) {
  this->name = "CharEmbeddings_AG";
  this->activation = activation;
  this->cLkt = new CharLookupTable_AG(cIndex, cDimension, inputSize, blockSize, oneClass, otl);
  // We will create the intermediate layers once the file containing the mapping character n-grams -> word has been read.
  this->poolModule = new Pooling(inputSize, nDimension, blockSize);
  if (activation == TANH) {
    this->actPoolModule = new Tanh(nDimension * inputSize, blockSize);
  }
  else if (activation == RELU) {
    this->actPoolModule = new Relu(nDimension * inputSize, blockSize);
  }
  else {
    this->actPoolModule = new Sigmoid(nDimension * inputSize, blockSize);
  }
  this->inputSize = inputSize;
  this->blockSize = blockSize;
  this->dimensionSize = nDimension;
  this->charDimensionSize = cDimension;
  this->update_lkt = 1;
  output.resize(inputSize * dimensionSize, blockSize);
}

CharEmbeddings_AG::~CharEmbeddings_AG() {
}

void CharEmbeddings_AG::getHashMaps(char* hmf) { 
  cLkt->getHashMaps(hmf);
  /* We need to create the first layers with the maximum possible blocksize,
     (longest word * nChar * n * bloackSize), to avoid memory allocation issues
     then it will change at each forward, being the sum of the word length in the block */
  this->convModule = new Linear_AG(charDimensionSize * cLkt->nChar, dimensionSize, cLkt->maxLength * inputSize * blockSize, otl); 
  if (activation == TANH) {
    this->actModule = new Tanh(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
  else if (activation == RELU) {
    this->actModule = new Relu(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
  else {
    this->actModule = new Sigmoid(dimensionSize, cLkt->maxLength * inputSize * blockSize);
  }
}

void CharEmbeddings_AG::resetAG() {
  CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(this->cLkt);
  Linear_AG* conv_AG = static_cast<Linear_AG*>(this->convModule);
  cLkt_AG->resetAG();
  conv_AG->resetAG();
}

void CharEmbeddings_AG::initAG(string type){
  CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(this->cLkt);
  Linear_AG* conv_AG = static_cast<Linear_AG*>(this->convModule);
  cLkt_AG->initAG(type);
  conv_AG->initAG(type);
}
