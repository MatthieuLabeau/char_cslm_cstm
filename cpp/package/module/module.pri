HEADERS += \
    ../package/module/BLinear.H \
    ../package/module/Embeddings.H \
    ../package/module/FunctionSequential.H \
    ../package/module/Linear.H \
    ../package/module/Linear_AG.H \
    ../package/module/LinearNCE.H \
    ../package/module/LinearNCE_AG.H \
    ../package/module/LinearSoftmax.H \
    ../package/module/LinearSoftmax_AG.H \
    ../package/module/LinearSoftmax_discrim.H \
    ../package/module/LookupTable.H \
    ../package/module/LookupTable_AG.H \
    ../package/module/mainModule.H \
    ../package/module/MaxLinear.H \
    ../package/module/Module.H \
    ../package/module/ProbOutput.H \
    ../package/module/RLinear.H \
    ../package/module/RRLinear.H \
    ../package/module/Sequential.H \
    ../package/module/Sigmoid.H \
    ../package/module/Tanh.H

SOURCES += \
    ../package/module/BLinear.cc \
    ../package/module/Embeddings.cc \
    ../package/module/FunctionSequential.cc \
    ../package/module/Linear.cc \
    ../package/module/Linear_AG.cc \
    ../package/module/LinearNCE.cc \
    ../package/module/LinearNCE_AG.cc \
    ../package/module/LinearSoftmax.cc \
    ../package/module/LinearSoftmax_AG.cc \
    ../package/module/LinearSoftmax_discrim.cc \
    ../package/module/LookupTable.cc \
    ../package/module/LookupTable_AG.cc \
    ../package/module/MaxLinear.cc \
    ../package/module/Module.cc \
    ../package/module/ProbOutput.cc \
    ../package/module/RLinear.cc \
    ../package/module/RRLinear.cc \
    ../package/module/Sequential.cc \
    ../package/module/Sigmoid.cc \
    ../package/module/Tanh.cc
