/******************************************************************                                                                                                                                      
Structure OUtput Layer (SOUL) Language Model Toolkit                                                                                                                                                       
(C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS                                                                                                                                                        
*******************************************************************/

class Pooling : public Module {
public:
  floatTensor selectOutput;
  floatTensor input;
  floatTensor selectInput;
  floatTensor selectGradInput;
  floatTensor selectGradOutput;
  int inputSize;
  int dimensionSize;
  int blockSize;
  int* wordLength;
  int* wordStart;

  Pooling();
  Pooling(int inputSize, int dimensionSize, int blockSize);

  virtual ~Pooling();

  virtual floatTensor& forward(floatTensor& input);
  virtual floatTensor& backward(floatTensor& gradOutput);

  void updateParameters(float learningRate);
  void changeBlockSize(int blockSize);
  void read(ioFile* iof);
  void write(ioFile* iof);
  float distance2(Module* anotherModule);

};
