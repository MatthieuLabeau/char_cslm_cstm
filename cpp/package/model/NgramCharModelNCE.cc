/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS
 Class for n-gram neural network language model
*******************************************************************/
#include "mainModel.H"

NgramCharModelNCE::NgramCharModelNCE() {
}

NgramCharModelNCE::~NgramCharModelNCE() {
  delete charVoc;
}

void NgramCharModelNCE::changeNceOutputSize(int nceOutputSize) {
  if (this->nceOutputSize != nceOutputSize) {
    this->nceOutputSize = nceOutputSize;
    this->outputLayer->changeNceOutputSize(nceOutputSize);
  }
}

void NgramCharModelNCE::allocation() {
  recurrent = 0;
  otl = new outils();
  otl->sgenrand(time(NULL) + getpid());
  // hiddenStep = 1 if linear, 2 if non linear
  hiddenStep = 1;
  if (nonLinearType == TANH || nonLinearType == SIGM || nonLinearType == RELU) {
    hiddenStep = 2;
  }
  
  // create baseNetwork with one Embeddings and hiddenLayerSizeArray.length * hiddenStep consecutive modules
  baseNetwork = new Sequential(hiddenLayerSizeArray.length * hiddenStep);
  
  int i;
  // Lookup table with classical or one vector initialization
  if (name == CE_NCE || name == CE_CE_NCE || name == CE_CWE_NCE || name == CE_H_NCE) {
    baseNetwork->lkt = new CharEmbeddings(dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else if (name == CWE_NCE || name == CWE_NCE_L || name == CWE_CE_NCE || name == CWE_CWE_NCE) {
    baseNetwork->lkt =  new CharWordEmbeddings(outputVoc->wordNumber, dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else if (name == CE_NCE_AG || name == CE_H_NCE_AG || name == CE_CE_NCE_AG || name == CE_CWE_NCE_AG) {
    baseNetwork->lkt = new CharEmbeddings_AG(dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else if (name == CWE_NCE_AG ||  name == CWE_CWE_NCE_AG) {
    baseNetwork->lkt = new CharWordEmbeddings_AG(outputVoc->wordNumber, dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else if (name == LKT_CWE_NCE_AG) {
    baseNetwork->lkt = new LookupTable_AG(inputVoc->wordNumber, dimensionSize, n - 1, blockSize, 1, otl); 
  } else {
    baseNetwork->lkt = new LookupTable(inputVoc->wordNumber, dimensionSize, n - 1, blockSize, 1, otl);
  }
  if (name == CE_H_NCE || name == CE_H_NCE_AG) {
    CharEmbeddingsForOutput = new CharEmbeddings(dimensionSize, 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    CharEmbeddings* inputCE = static_cast<CharEmbeddings*>(baseNetwork->lkt);
    CharEmbeddings* inputL = static_cast<CharEmbeddings*>(CharEmbeddingsForOutput);
    inputL->getHashMaps(hmf);
    inputL->cLkt->weight.tieData(inputCE->cLkt->weight); 
    inputL->convModule->weight.tieData(inputCE->convModule->weight);
    inputL->convModule->bias.tieData(inputCE->convModule->bias);
  }
  Module* module;
  if (name == CE_NCE_AG || name == CWE_NCE_AG  || name == CE_CE_NCE_AG || name == CE_H_NCE_AG || name == CE_CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    module = new Linear_AG((n - 1) * dimensionSize, hiddenLayerSizeArray(0), blockSize, otl);
  } else {
    module = new Linear((n - 1) * dimensionSize, hiddenLayerSizeArray(0), blockSize, otl);
  }
  baseNetwork->add(module);
  
  if (dimensionSize != hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1)) {
    cerr << "WARNING: last hidden layer size !=  projection dimension, use projection dimension" << endl;
  }
  hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1) = dimensionSize;
  
  // Add non linear activation
  if (nonLinearType == TANH) {
    module = new Tanh(hiddenLayerSizeArray(0), blockSize); // non linear
    baseNetwork->add(module);
  } else if (nonLinearType == SIGM) {
    module = new Sigmoid(hiddenLayerSizeArray(0), blockSize); // non linear
    baseNetwork->add(module);
  } else if (nonLinearType == RELU) {
      module = new Relu(hiddenLayerSizeArray(0), blockSize); // non linear
      baseNetwork->add(module);
  }
  // Add several hidden layers with linear or non linear activation
  for (i = 1; i < hiddenLayerSizeArray.size[0]; i++) {
    if (name == CE_NCE_AG || name == CWE_NCE_AG || name == CE_CE_NCE_AG || name == CE_H_NCE_AG || name == CE_CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
      module = new Linear_AG((n - 1) * dimensionSize, hiddenLayerSizeArray(0), blockSize, otl);
    } else {
      module = new Linear(hiddenLayerSizeArray(i - 1), hiddenLayerSizeArray(i), blockSize, otl);
    }
    baseNetwork->add(module);
    if (nonLinearType == TANH) {
      module = new Tanh(hiddenLayerSizeArray(i), blockSize); // non linear
      baseNetwork->add(module);
    } else if (nonLinearType == SIGM) {
      module = new Sigmoid(hiddenLayerSizeArray(i), blockSize); // non linear
      baseNetwork->add(module);
    } else if (nonLinearType == RELU) {
      module = new Relu(hiddenLayerSizeArray(i), blockSize); // non linear
      baseNetwork->add(module);
    }
  }
  
  // probabilities of ngram in each mini-batch
  probabilityOne.resize(blockSize, 1);
  if (name == CWE_CE_NCE || name == CE_CE_NCE || name == LKT_CE_NCE) {
    this->outputLayer = new CharNCE(charVoc->wordNumber, charDimensionSize,
				    hiddenLayerSize, outputVoc->wordNumber, blockSize, nonLinearType, 0, otl);
    CharNCE* cNCE = static_cast<CharNCE*>(this->outputLayer);
    cNCE->charEmbeddings->getHashMaps(hmf);
  } else if (name == LKT_CWE_NCE ||  name == CE_CWE_NCE || name == CWE_CWE_NCE) { 
    this->outputLayer = new CharNCE(charVoc->wordNumber, charDimensionSize,
                                    hiddenLayerSize, outputVoc->wordNumber, blockSize, nonLinearType, 1, otl);
    CharNCE* cNCE = static_cast<CharNCE*>(this->outputLayer);
    cNCE->charEmbeddings->getHashMaps(hmf);
  } else if (name == CE_CE_NCE_AG) {
    this->outputLayer = new CharNCE_AG(charVoc->wordNumber, charDimensionSize,
				       hiddenLayerSize, outputVoc->wordNumber, blockSize, nonLinearType, 0, otl);
    CharNCE_AG* cNCE = static_cast<CharNCE_AG*>(this->outputLayer);
    cNCE->charEmbeddings->getHashMaps(hmf);
  } else if (name == LKT_CWE_NCE_AG || name == CE_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    this->outputLayer = new CharNCE_AG(charVoc->wordNumber, charDimensionSize,
                                       hiddenLayerSize, outputVoc->wordNumber, blockSize, nonLinearType, 1, otl);
    CharNCE_AG* cNCE = static_cast<CharNCE_AG*>(this->outputLayer);
    cNCE->charEmbeddings->getHashMaps(hmf);
  } else if (name == CE_H_NCE) {
    this->outputLayer = new HybridNCE(hiddenLayerSize, outputVoc->wordNumber, blockSize, otl);
  } else if (name == CE_H_NCE_AG) {
    this->outputLayer = new HybridNCE_AG(hiddenLayerSize, outputVoc->wordNumber, blockSize, otl);
  } else if (name == CE_NCE_AG || name == CWE_NCE_AG) {
    this->outputLayer = new LinearNCE_AG(hiddenLayerSize, outputVoc->wordNumber, blockSize, otl);
  } else {
    this->outputLayer = new LinearNCE(hiddenLayerSize, outputVoc->wordNumber, blockSize, otl);
  }
  cout << "[INFO] create an output Layer of size : " 
       << this->outputLayer->weight.size[0]  << "," 
       << this->outputLayer->weight.size[1] << " / " 
       << this->outputLayer->weight.stride[0]  << ","
       << this->outputLayer->weight.stride[1]  <<  endl;
  
  // contextFeature is the output of the last hidden layer, also the feature of all history
  contextFeature = baseNetwork->output;
  gradContextFeature.resize(contextFeature);
  if (name == CWE_L) {
    CharWordEmbeddings* cwlkt = static_cast<CharWordEmbeddings*>(baseNetwork->lkt);
    outputLayer->weight.tieData(cwlkt->Lkt->weight);
  }
  dataSet = new NgramDataSet(ngramType, n, BOS, inputVoc, outputVoc, mapIUnk, mapOUnk, BLOCK_NGRAM_NUMBER);
  noUnkDataSet = new NgramDataSet(ngramType, n, BOS, inputVoc, outputVoc, mapIUnk, 0, BLOCK_NGRAM_NUMBER);
}

NgramCharModelNCE::NgramCharModelNCE(string name, char* charWordMapFileString, char* charVocFileString, char* inputVocFileString,
				     char* outputVocFileString, int mapIUnk, int mapOUnk, int BOS,
				     int blockSize, int n, int charDimensionSize, int dimensionSize, string nonLinearType,
				     intTensor& hiddenLayerSizeArray) {
  recurrent = 0;
  this->name = name;
  this->ngramType = 0;
  // Read vocabularies
  this->hmf = charWordMapFileString;
  this->charVoc = new SoulVocab(charVocFileString);
  this->inputVoc = new SoulVocab(inputVocFileString);
  this->outputVoc = new SoulVocab(outputVocFileString);
  this->mapIUnk = mapIUnk;
  this->mapOUnk = mapOUnk;
  this->BOS = BOS;
  this->blockSize = blockSize;
  this->n = n;
  if (BOS > n - 1) {
    this->BOS = n - 1;
  }

  this->charDimensionSize = charDimensionSize;
  this->dimensionSize = dimensionSize;
  this->nonLinearType = nonLinearType;

  this->hiddenLayerSizeArray.resize(hiddenLayerSizeArray);
  this->hiddenLayerSizeArray.copy(hiddenLayerSizeArray);
  hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
  hiddenNumber = hiddenLayerSizeArray.length;
  allocation();
}

void NgramCharModelNCE::read(ioFile* iof, int allocation, int blockSize, char* hmf) {
  string readFormat;
  this->hmf = hmf;
  iof->readString(name);
  // for test
  cout << "NgramCharModelNCE::read name: " << name << endl;
  iof->readString(readFormat);
  iof->readInt(ngramType);
  if (allocation == 1) {
    charVoc = new SoulVocab();
    inputVoc = new SoulVocab();
    outputVoc = new SoulVocab();
  }
  iof->readInt(charVoc->wordNumber);
  iof->readInt(inputVoc->wordNumber);
  iof->readInt(outputVoc->wordNumber);
  iof->readInt(mapIUnk);
  iof->readInt(mapOUnk);
  iof->readInt(BOS);
  if (blockSize != 0) {
    this->blockSize = blockSize;
  } else {
    this->blockSize = DEFAULT_BLOCK_SIZE;
  }
  iof->readInt(n);
  iof->readInt(charDimensionSize);
  iof->readInt(dimensionSize);
  iof->readInt(hiddenNumber);
  iof->readString(nonLinearType);
  hiddenLayerSizeArray.resize(hiddenNumber, 1);
  hiddenLayerSizeArray.read(iof);
  hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
  if (allocation == 1) {
    // for test
    //cout << "NgramCharModel::read here" << endl;
    this->allocation();
    // for test
    //cout << "NgramCharModel::read here1" << endl;
  }
  baseNetwork->read(iof);
  // for test
  cout << "NgramCharModelNCE::read here2" << endl;
  int i;

  outputLayer->read(iof);
  if (allocation == 1) {
    charVoc->read(iof);
    inputVoc->read(iof);
    outputVoc->read(iof);
  }
  cout << "NgramCharModelNCE: Read: " << name << " " << ngramType << " " << inputVoc->wordNumber << " " << outputVoc->wordNumber << " " <<  charVoc->wordNumber << " " << blockSize << " " << n << " " << charDimensionSize << " " << dimensionSize << " " << hiddenNumber << " " << nonLinearType << endl;

}

void NgramCharModelNCE::write(ioFile* iof, int closeFile) {
  // for test
  //cout << "NgramCharModel::write here8" << endl;
  cout << "NgramCharModelNCE: Write: " << name << " " << ngramType << " " << inputVoc->wordNumber << " " << outputVoc->wordNumber << " " << charVoc->wordNumber << " " << blockSize << " " << n << " " << charDimensionSize << " " << dimensionSize << " " << hiddenNumber << " " << nonLinearType << endl;

  iof->writeString(name);
  iof->writeString(iof->format);
  iof->writeInt(ngramType);
  iof->writeInt(charVoc->wordNumber);
  iof->writeInt(inputVoc->wordNumber);
  iof->writeInt(outputVoc->wordNumber);
  iof->writeInt(mapIUnk);
  iof->writeInt(mapOUnk);
  iof->writeInt(BOS);
  iof->writeInt(n);
  iof->writeInt(charDimensionSize);
  iof->writeInt(dimensionSize);
  iof->writeInt(hiddenNumber);
  iof->writeString(nonLinearType);
  hiddenLayerSizeArray.write(iof);
  // for test
  //cout << "NgramModel::write here10" << endl;
  baseNetwork->write(iof);
  // for test
  //cout << "NgramModel::write here11" << endl;
  int i;
  if (name != LBL) {
    for (i = 0; i < outputNetworkSize.size[0]; i++) {
      outputNetwork[i]->write(iof);
    }
  }
  outputLayer->write(iof);
  charVoc->write(iof);
  inputVoc->write(iof);
  outputVoc->write(iof);
  if (closeFile == 1) {
    iof->freeWriteFile();
  }
}
