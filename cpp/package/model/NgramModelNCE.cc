#include "mainModel.H"

NgramModelNCE::NgramModelNCE() {

}

void NgramModelNCE::changeNceOutputSize(int nceOutputSize) {
	if (this->nceOutputSize != nceOutputSize) {
		this->nceOutputSize = nceOutputSize;
		this->outputLayer->changeNceOutputSize(nceOutputSize);
	}
}

void NgramModelNCE::changeBlockSize(int blockSize) {
	if (this->blockSize != blockSize) {
		this->blockSize = blockSize;
		baseNetwork->changeBlockSize(blockSize);
		contextFeature = baseNetwork->output;
		gradContextFeature.resize(contextFeature);
		if (!recurrent || !cont) {
			dataSet->blockSize = blockSize;
			noUnkDataSet->blockSize = blockSize;
		}
		// we dont have outputNetwork, but outputLayer
		this->outputLayer->changeBlockSize(blockSize);
		probabilityOne.resize(blockSize, 1);
	}
}

void NgramModelNCE::allocation() {
	recurrent = 0;
	otl = new outils();
	otl->sgenrand(time(NULL) + getpid());
	hiddenStep = 1;
	if (nonLinearType == TANH || nonLinearType == SIGM || nonLinearType == RELU) {
		hiddenStep = 2;
	}
	baseNetwork = new Sequential(hiddenLayerSizeArray.length * hiddenStep);
	int i;
	if (name == OVN_NCE_AG) {
		baseNetwork->lkt = new LookupTable_AG(inputVoc->wordNumber,
				dimensionSize, n - 1, blockSize, 1, otl);
	} else if (name == OVN_NCE) {
		baseNetwork->lkt = new LookupTable(inputVoc->wordNumber, dimensionSize,
				n - 1, blockSize, 1, otl);
	} else {
		cerr << "name is not correct : " << name << endl;
		exit(1);
	}
	cout << "[INFO] create a LookupTable of size : " 
	     << baseNetwork->lkt->weight.size[0]  << "," 
	     << baseNetwork->lkt->weight.size[1] << " / " 
	     << baseNetwork->lkt->weight.stride[0]  << ","
	     << baseNetwork->lkt->weight.stride[1]  <<  endl;
	Module* module;
	if (name == OVN_NCE_AG) {
		module = new Linear_AG((n - 1) * dimensionSize, hiddenLayerSizeArray(0),
				blockSize, otl);
	} else if (name == OVN_NCE) {
		module = new Linear((n - 1) * dimensionSize, hiddenLayerSizeArray(0),
				blockSize, otl);
	} else {
		cerr << "name is not correct : " << name << endl;
		exit(1);
	}
	baseNetwork->add(module);
	// Add non linear activation
	if (nonLinearType == TANH) {
		module = new Tanh(hiddenLayerSizeArray(0), blockSize); // non linear
		baseNetwork->add(module);
	} else if (nonLinearType == SIGM) {
		module = new Sigmoid(hiddenLayerSizeArray(0), blockSize); // non linear
		baseNetwork->add(module);
	}else if (nonLinearType == RELU) {
		module = new Relu(hiddenLayerSizeArray(0), blockSize); // non linear
		baseNetwork->add(module);
	}
	for (i = 1; i < hiddenLayerSizeArray.size[0]; i++) {
		if (name == OVN_NCE_AG) {
			module = new Linear_AG(hiddenLayerSizeArray(i - 1),
					hiddenLayerSizeArray(i), blockSize, otl);
		} else if (name == OVN_NCE) {
			module = new Linear(hiddenLayerSizeArray(i - 1),
					hiddenLayerSizeArray(i), blockSize, otl);
		} else {
			cerr << "name is not correct : " << name << endl;
			exit(1);
		}
		baseNetwork->add(module);
		if (nonLinearType == TANH) {
			module = new Tanh(hiddenLayerSizeArray(i), blockSize); // non linear
			baseNetwork->add(module);
		} else if (nonLinearType == SIGM) {
			module = new Sigmoid(hiddenLayerSizeArray(i), blockSize); // non linear
			baseNetwork->add(module);
		} else if (nonLinearType == RELU) {
			module = new Relu(hiddenLayerSizeArray(i), blockSize); // non linear
			baseNetwork->add(module);
		}
	}
	cout << "[INFO]: Build a Sequential model of size :" << baseNetwork->size << endl;
	// probabilities of ngram in each mini-batch
	probabilityOne.resize(blockSize, 1);
	if (name == OVN_NCE_AG) {
		this->outputLayer = new LinearNCE_AG(hiddenLayerSize,
				outputVoc->wordNumber, blockSize, otl);
	} else if (name == OVN_NCE) {
		this->outputLayer = new LinearNCE(hiddenLayerSize,
				outputVoc->wordNumber, blockSize, otl);
	} else {
		cerr << "name is not correct : " << name << endl;
		exit(1);
	}
	cout << "[INFO] create an output Layer of size : " 
	     << this->outputLayer->weight.size[0]  << "," 
	     << this->outputLayer->weight.size[1] << " / " 
	     << this->outputLayer->weight.stride[0]  << ","
	     << this->outputLayer->weight.stride[1]  <<  endl;
	
	// contextFeature is the output of the last hidden layer, also the feature of all history
	contextFeature = baseNetwork->output;
	gradContextFeature.resize(contextFeature);
	dataSet = new NgramDataSet(ngramType, n, BOS, inputVoc, outputVoc, mapIUnk,
				   mapOUnk, BLOCK_NGRAM_NUMBER);
	noUnkDataSet = new NgramDataSet(ngramType, n, BOS, inputVoc, outputVoc, mapIUnk,
					0, BLOCK_NGRAM_NUMBER);
}

NgramModelNCE::~NgramModelNCE() {
	delete baseNetwork;
	delete outputLayer;
	delete inputVoc;
	delete outputVoc;
	delete dataSet;
	delete noUnkDataSet;
}

NgramModelNCE::NgramModelNCE(string name, int inputSize, int outputSize,
		int blockSize, int n, int projectionDimension, string nonLinearType,
		intTensor& hiddenLayerSizeArray) {
	recurrent = 0;
	this->name = name;
	this->ngramType = 0;
	// for test
	//cout << "NgramModelNCE::NgramModelNCE here" << endl;
	inputVoc = new SoulVocab();
	outputVoc = new SoulVocab();
	// vocabulary contains only 0, 1, 2, ...
	for (int i = 0; i < inputSize; i++) {
		stringstream out;
		out << i;
		inputVoc->add(out.str(), i);
	}
	for (int i = 0; i < outputSize; i++) {
		stringstream out;
		out << i;
		outputVoc->add(out.str(), i);
	}
	// for test
	//cout << "NgramModelNCE::NgramModelNCE here 1" << endl;
	this->mapIUnk = 1;
	this->mapOUnk = 1;
	// why?
	this->BOS = 1;
	this->blockSize = blockSize;
	this->nceOutputSize = 0;
	this->n = n;
	this->dimensionSize = projectionDimension;
	this->nonLinearType = nonLinearType;
	this->hiddenLayerSizeArray = hiddenLayerSizeArray;

	hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
	hiddenNumber = hiddenLayerSizeArray.length;
	// for test
	//cout << "NgramModelNCE::NgramModelNCE here 2" << endl;
	allocation();
}

NgramModelNCE::NgramModelNCE(string name, char* inputVocFileName,
		char* outputVocFileName, int mapIUnk, int mapOUnk, int BOS,
		int blockSize, int n, int projectionDimension, string nonLinearType,
		intTensor& hiddenLayerSizeArray) {
	recurrent = 0;
	this->name = name;
	this->ngramType = 0;
	// Read vocabularies
	this->inputVoc = new SoulVocab(inputVocFileName);
	this->outputVoc = new SoulVocab(outputVocFileName);
	this->mapIUnk = mapIUnk;
	this->mapOUnk = mapOUnk;
	this->BOS = BOS;
	this->blockSize = blockSize;
	this->nceOutputSize = 0;
	this->n = n;
	if (BOS > n - 1) {
		this->BOS = n - 1;
	}

	this->dimensionSize = projectionDimension;
	this->nonLinearType = nonLinearType;

	this->hiddenLayerSizeArray.resize(hiddenLayerSizeArray);
	this->hiddenLayerSizeArray.copy(hiddenLayerSizeArray);
	hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
	hiddenNumber = hiddenLayerSizeArray.length;
	allocation();
}

void NgramModelNCE::firstTime() {

}

void NgramModelNCE::firstTime(intTensor& context) {

}

int NgramModelNCE::train(char* dataFileName, int maxExampleNumber,
		int iteration, string learningRateType, float learningRate,
		float learningRateDecay, int update_lkt, string update_hiddens,
		int update_output_layer) {
	firstTime();
	// For HybridNCE only ! 
	if (name == CE_H_NCE || name == CE_H_NCE_AG) {
	  cout << "Computing rare words output embeddings from character layer" << endl;
	  HybridNCE* outputL = static_cast<HybridNCE*>(outputLayer);
	  NgramCharModelNCE* charModel = static_cast<NgramCharModelNCE*>(this);
	  CharEmbeddings* inputL = static_cast<CharEmbeddings*>(charModel->CharEmbeddingsForOutput);

	  intTensor rareWords;
	  floatTensor rareWeight;
	  int rareSize = outputL->weight.size[1] - outputL->shortListSize;

	  rareWords.resize(1, rareSize);
	  //rareWeight.resize(dimensionSize, rareSize);
	  
	  for (int idOutputSize = outputL->shortListSize; idOutputSize < outputL->weight.size[1]; idOutputSize++) {
	    rareWords.data[idOutputSize - outputL->shortListSize] = idOutputSize;
	  }
	  
	  inputL->changeBlockSize(rareSize);
	  rareWeight = inputL->forward(rareWords);
	  outputL->updateRareEmbeddings(rareWeight);
	}
	
	ioFile dataIof;
	dataIof.takeReadFile(dataFileName);
	// Read header 3 integers
	int ngramNumber;
	dataIof.readInt(ngramNumber);
	// N is order in data file, can be larger than n, order of model
	int N;
	dataIof.readInt(N);
	if (N < n) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << n << endl;
		exit(1);
	}
	// k is the number of negative examples, in the model, this number is stored as nceOutputSize = k + 1
	int k;
	dataIof.readInt(k);
	if (nceOutputSize != k + 1) {
		this->changeNceOutputSize(k + 1);
		// ready for training
	}
	// maxExampleNumber = 0 => use all ngrams
	if (maxExampleNumber > ngramNumber || maxExampleNumber == 0) {
		maxExampleNumber = ngramNumber;
	}
	float currentLearningRate;
	// nstep is the number of seen examples
	int nstep;
	// nstep at the beginning: number of examples used in
	// previous iterations, here being computed approximately
	nstep = maxExampleNumber * (iteration - 1);
	// comments for NCE training
	// we have N - 1 context words, 1 positive output word and k negative output words, so N + k = N + nceOutputSize - 1 words in total
	intTensor readTensor(blockSize, N + nceOutputSize - 1);
	// for each output words (positive or negative, we need to store its log-probability from the noise distribution)
	floatTensor logPropTensor(blockSize, nceOutputSize);
	// these tensors do not contain their own data, but point to different parts of readTensor
	intTensor context;
	intTensor word;
	floatTensor subLogPropTensor;
	context.sub(readTensor, 0, blockSize - 1, N - n, N - 2);
	context.t();
	word.sub(readTensor, 0, blockSize - 1, N - 1, N + nceOutputSize - 2);
	word.t();
	subLogPropTensor.sub(logPropTensor, 0, blockSize - 1, 0, nceOutputSize - 1);
	subLogPropTensor.t();
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	int blockNumber = maxExampleNumber / blockSize;
	int remainingNumber = maxExampleNumber - blockSize * blockNumber;
	int i;
	for (i = 0; i < blockNumber; i++) {
		readStripInt(dataIof, readTensor, logPropTensor, N);
		if (dataIof.getEOF()) {
			break;
		}
		currentExampleNumber += blockSize;
		currentLearningRate = takeCurrentLearningRate(learningRate,
				learningRateType, nstep, learningRateDecay);
		trainOne(context, word, subLogPropTensor, currentLearningRate,
				blockSize, update_lkt, update_hiddens, update_output_layer);
		nstep += blockSize;
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	CharNCE* charOut = dynamic_cast<CharNCE*>(this->outputLayer);
	// The last block wit less number of examples than blockSize
	if (remainingNumber != 0 && !dataIof.getEOF()
	    && not (charOut)) {
		context = 0;
		word = SIGN_NOT_WORD;
		logPropTensor = 0;
		// read only remainingNumber examples, copy to readTensor
		intTensor lastReadTensor(remainingNumber, N + nceOutputSize - 1);
		readStripInt(dataIof, lastReadTensor, logPropTensor, N);
		intTensor subReadTensor;
		subReadTensor.sub(readTensor, 0, remainingNumber - 1, 0,
				N + nceOutputSize - 2);
		subReadTensor.copy(lastReadTensor);
		currentLearningRate = takeCurrentLearningRate(learningRate,
				learningRateType, nstep, learningRateDecay);
		trainOne(context, word, subLogPropTensor, currentLearningRate,
				remainingNumber, update_lkt, update_hiddens,
				update_output_layer);
	}
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

float NgramModelNCE::monitorNCE(char* dataFileName, char* prefixModel, int all) {
  //Very similar to ComputeF
  //Will not only compute the NCE Objective for the data, but also do a complete forward
  // and compute: Log Z, entropy (KL(.||uniform)) and some specific values

  firstTime();
  ioFile dataIof;
  dataIof.takeReadFile(dataFileName);
  int ngramNumber;
  dataIof.readInt(ngramNumber);
  int N;
  dataIof.readInt(N);
  if (N < n) {
    cerr << "ERROR: N in data is wrong:" << N << " < " << n << endl;
    exit(1);
  }
  int k;
  dataIof.readInt(k);
  if (nceOutputSize != k + 1) {
    this->changeNceOutputSize(k + 1);
  }

  int maxExampleNumber = ngramNumber;

  intTensor readTensor(blockSize, N + nceOutputSize - 1);
  floatTensor logPropTensor(blockSize, nceOutputSize);
  intTensor context;
  intTensor word;
  floatTensor subLogPropTensor;
  context.sub(readTensor, 0, blockSize - 1, N - n, N - 2);
  context.t();
  word.sub(readTensor, 0, blockSize - 1, N - 1, N + nceOutputSize - 2);
  word.t();
  subLogPropTensor.sub(logPropTensor, 0, blockSize - 1, 0, nceOutputSize - 1);
  subLogPropTensor.t();
  int currentExampleNumber = 0;
  int percent = 1;
  float aPercent = maxExampleNumber * CONSTPRINT;
  float iPercent = aPercent * percent;
  int blockNumber = maxExampleNumber / blockSize;
  int remainingNumber = maxExampleNumber - blockSize * blockNumber;
  int i;
  float f = 0;
  float z = 0;
  float h = 0;

  char outputF[STRING_SIZE];
  strcpy(outputF, prefixModel);
  strcat(outputF, "out.score");
  ofstream outputFs;
  outputFs.open(outputF, ios_base::app);
  
  if (name != CE_CE_NCE_AG && name != CE_CWE_NCE_AG 
      && name != LKT_CWE_NCE_AG && name != CWE_CWE_NCE_AG) {
    char outputZ[STRING_SIZE];
    strcpy(outputZ, prefixModel);
    strcat(outputZ, "out.norm");
    ofstream outputZs;
    outputZs.open(outputZ, ios_base::app);
    
    char outputH[STRING_SIZE];
    strcpy(outputH, prefixModel);
    strcat(outputH, "out.ent");
    ofstream outputHs;
    outputHs.open(outputH, ios_base::app);
    
    float z = 0;
    float h = 0;
  }
  
  //if (name == CE_CE_NCE_AG || name == CE_CWE_NCE_AG) {
  //  static_cast<CharNCE_AG*>(outputLayer)->computeAllWeight();
  //}

  for (i = 0; i < blockNumber; i++) {
    readStripInt(dataIof, readTensor, logPropTensor, N);
    if (dataIof.getEOF()) {
      break;
    }
    currentExampleNumber += blockSize;
  
    firstTime(context);
    baseNetwork->forward(context);
    //Usual computation of the score                                                                                                                                                                       
    outputLayer->nceForward(contextFeature, word);
    if (all) {
      f = outputLayer->computeF(word, subLogPropTensor);
      if (isinf(f)) {
	  int testLookup = baseNetwork->lkt->weight.testInf();
	  int testHidden = baseNetwork->modules[0]->weight.testInf();
	  int testOut = outputLayer->weight.testInf();
	  int testBiasHidden = baseNetwork->modules[0]->bias.testInf();
	  int testBiasOut = outputLayer->weight.testInf();
	  if ( testLookup || testHidden || testOut || testBiasHidden || testBiasOut) {
	    cout << "Inf score was caused by an inf parameter: exit validation" << endl;
	    exit(1);
	  }
	}
      outputFs << i << " " << context.data[0] << " " << context.data[1] << " " << context.data[2] << " " << word.data[0] << " " << f << endl;      
      //outputFs << i << " " <<  f << endl;
    } else {   
      f += outputLayer->computeF(word, subLogPropTensor);
    }
    if (name != CE_CE_NCE_AG && name != CE_CWE_NCE_AG
	&& name != LKT_CWE_NCE_AG && name != CWE_CWE_NCE_AG) {
      //We then do a complete forward and get the values we want. 
      outputLayer->forward(contextFeature);
      if (all) {
	z = outputLayer->monitorNorm();
	//outputZs << i << " " << context.data[0] << " " << context.data[1] << " " << context.data[2] << " " << word.data[0] << " " << z << endl;
	//outputZs << i << " " << z << endl;
	h = outputLayer->monitorScoreEntropy();
	//outputHs << i << " " << context.data[0] << " " << context.data[1] << " " << context.data[2] << " " << word.data[0] << " " << h << endl;
	//outputHs << i << " " << h << endl;
      } else {
	z += outputLayer->monitorNorm();
	h += outputLayer->monitorScoreEntropy();  
      }
    }
  }
  
  if (remainingNumber != 0 && !dataIof.getEOF()) {
    context = 0;
    word = SIGN_NOT_WORD;
    logPropTensor = 0;
    intTensor lastReadTensor(remainingNumber, N + nceOutputSize - 1);
    readStripInt(dataIof, lastReadTensor, logPropTensor, N);
    intTensor subReadTensor;
    subReadTensor.sub(readTensor, 0, remainingNumber - 1, 0,
		      N + nceOutputSize - 2);
    subReadTensor.copy(lastReadTensor);

    firstTime(context);
    baseNetwork->forward(context);
    //Usual computation of the score
    outputLayer->nceForward(contextFeature, word);
    if (all) {
      f = outputLayer->computeF(word, subLogPropTensor);
      //outputFs << i << " " << context.data[0] << " " << context.data[1] << " " << context.data[2] << " " << word.data[0] << " " << f << endl;
      //outputFs << f << endl;
      //outputFs.close();
    } else {
      f += outputLayer->computeF(word, subLogPropTensor);
    }
    if (name != CE_CE_NCE_AG && name != CE_CWE_NCE_AG
	&& name != LKT_CWE_NCE_AG && name != CWE_CWE_NCE_AG) {
      //We then do a complete forward and get the values we want.                                                                                                                                        
      outputLayer->forward(contextFeature);
      if (all) {
	z = outputLayer->monitorNorm();
	//outputZs << i << " " << context.data[0] << " " << context.data[1] << " " << context.data[2] << " " << word.data[0] << " " << z << endl;
	//outputZs << z << endl;
	//outputZs.close();
	h = outputLayer->monitorScoreEntropy();
	//outputHs << i << " " << context.data[0] << " " << context.data[1] << " " << context.data[2] << " " << word.data[0] << " " << h << endl;
	//outputHs << h << endl;
	//outputHs.close();
      } else {
	z += outputLayer->monitorNorm();
	h += outputLayer->monitorScoreEntropy();
      }
    }
  }

  if (not all) {
    //We keep print these values to log files
    outputFs << f / ngramNumber << endl;
    outputFs.close();
    char outputF[STRING_SIZE];
    strcpy(outputF, prefixModel);
    strcat(outputF, "out.score");
    ofstream outputFs;
    outputFs.open(outputF, ios_base::app);

    if (name != CE_CE_NCE_AG && name != CE_CWE_NCE_AG
	&& name != LKT_CWE_NCE_AG && name != CWE_CWE_NCE_AG) {
      char outputZ[STRING_SIZE];
      strcpy(outputZ, prefixModel);
      strcat(outputZ, "out.norm");
      ofstream outputZs;
      outputZs.open(outputZ, ios_base::app);

      char outputH[STRING_SIZE];
      strcpy(outputH, prefixModel);
      strcat(outputH, "out.ent");
      ofstream outputHs;
      outputHs.open(outputH, ios_base::app);

      outputZs << z / ngramNumber << endl;
      outputZs.close();
      outputHs << h / ngramNumber << endl;
      outputHs.close();
    }
  }
  
  return f / ngramNumber;
}

float NgramModelNCE::computeF(char* dataFileName) {
	firstTime();
	ioFile dataIof;
	dataIof.takeReadFile(dataFileName);
	// Read header 3 integers
	int ngramNumber;
	dataIof.readInt(ngramNumber);
	// N is order in data file, can be larger than n, order of model
	int N;
	dataIof.readInt(N);
	if (N < n) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << n << endl;
		exit(1);
	}
	// k is the number of negative examples, in the model, this number is stored as nceOutputSize = k + 1
	int k;
	dataIof.readInt(k);
	if (nceOutputSize != k + 1) {
		this->changeNceOutputSize(k + 1);
		// ready for training
	}

	int maxExampleNumber = ngramNumber;

	// for test
	//cout << "NgramModelNCE::computeF here" << endl;
	// comments for NCE training
	// we have N - 1 context words, 1 positive output word and k negative output words, so N + k = N + nceOutputSize - 1 words in total
	intTensor readTensor(blockSize, N + nceOutputSize - 1);
	// for each output words (positive or negative, we need to store its log-probability from the noise distribution)
	floatTensor logPropTensor(blockSize, nceOutputSize);
	// these tensors do not contain their own data, but point to different parts of readTensor
	intTensor context;
	intTensor word;
	floatTensor subLogPropTensor;
	context.sub(readTensor, 0, blockSize - 1, N - n, N - 2);
	context.t();
	word.sub(readTensor, 0, blockSize - 1, N - 1, N + nceOutputSize - 2);
	word.t();
	subLogPropTensor.sub(logPropTensor, 0, blockSize - 1, 0, nceOutputSize - 1);
	subLogPropTensor.t();
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	int blockNumber = maxExampleNumber / blockSize;
	int remainingNumber = maxExampleNumber - blockSize * blockNumber;
	int i;
	float f = 0;
	for (i = 0; i < blockNumber; i++) {
		readStripInt(dataIof, readTensor, logPropTensor, N);
		if (dataIof.getEOF()) {
			break;
		}
		currentExampleNumber += blockSize;
		f += this->computeOneF(context, word, subLogPropTensor);
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	// The last block wit less number of examples than blockSize
	if (remainingNumber != 0 && !dataIof.getEOF()) {
		context = 0;
		word = SIGN_NOT_WORD;
		logPropTensor = 0;
		// read only remainingNumber examples, copy to readTensor
		intTensor lastReadTensor(remainingNumber, N + nceOutputSize - 1);
		readStripInt(dataIof, lastReadTensor, logPropTensor, N);
		intTensor subReadTensor;
		subReadTensor.sub(readTensor, 0, remainingNumber - 1, 0,
				N + nceOutputSize - 2);
		subReadTensor.copy(lastReadTensor);
		// for test
		//cout << "NgramModelNCE::computeF here 4" << endl;
		f += this->computeOneF(context, word, subLogPropTensor);
	}
#if PRINT_DEBUG
	cout << endl;
#endif
	return f;
}

int NgramModelNCE::trainTest(intTensor& wordTensor, floatTensor& logPropTensor,
		int iteration, string learningRateType, float learningRate,
		float learningRateDecay, int update_lkt, string update_hiddens,
		int update_output_layer) {
	int nceOutputSize = logPropTensor.size[1];
	int N = wordTensor.size[1] - nceOutputSize + 1;
	firstTime();
	if (this->nceOutputSize != nceOutputSize) {
		this->changeNceOutputSize(nceOutputSize);
		// ready for training
	}

	if (N < n) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << n << endl;
		exit(1);
	}

	int maxExampleNumber = wordTensor.size[0];

	float currentLearningRate;
	// for test
	//cout << "NgramModel::train here 1" << endl;
	// nstep is the number of seen examples
	int nstep;
	// nstep at the beginning: number of examples used in
	// previous iterations, here being computed approximately
	nstep = maxExampleNumber * (iteration - 1);

	intTensor context;
	intTensor outputWord;
	floatTensor subLogPropTensor;
	int firstWord = 0, lastWord = 0;
	int cont = 1;
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	while (cont == 1) {
		firstWord = lastWord;
		lastWord += blockSize;
		if (lastWord >= maxExampleNumber) {
			// reach the end of the input tensor
			lastWord = maxExampleNumber;
			cont = 0;
		}
		context.sub(wordTensor, firstWord, lastWord - 1, N - n, N - 2);
		context.t();
		outputWord.sub(wordTensor, firstWord, lastWord - 1, N - 1,
				N + nceOutputSize - 2);
		outputWord.t();
		subLogPropTensor.sub(logPropTensor, firstWord, lastWord - 1, 0,
				nceOutputSize - 1);
		subLogPropTensor.t();
		currentExampleNumber += (lastWord - firstWord);
		currentLearningRate = takeCurrentLearningRate(learningRate,
				learningRateType, nstep, learningRateDecay);
		if (lastWord - firstWord == blockSize) {
			// the batch's size is respected
			this->trainOne(context, outputWord, subLogPropTensor,
					currentLearningRate, lastWord - firstWord, update_lkt,
					update_hiddens, update_output_layer);
		} else {
			intTensor sizedContext(n - 1, blockSize);
			// important
			sizedContext = 0;
			intTensor sizedOutputWord(nceOutputSize, blockSize);
			// important
			sizedOutputWord = SIGN_NOT_WORD;
			floatTensor sizedSubLogPropTensor(nceOutputSize, blockSize);
			// less important
			sizedSubLogPropTensor = 0;
			intTensor subSizedContext, subSizedOutputWord;
			floatTensor subSizedSubLogPropTensor;
			subSizedContext.sub(sizedContext, 0, n - 2, 0,
					lastWord - firstWord - 1);
			subSizedContext.copy(context);
			subSizedOutputWord.sub(sizedOutputWord, 0, nceOutputSize - 1, 0,
					lastWord - firstWord - 1);
			subSizedOutputWord.copy(outputWord);
			subSizedSubLogPropTensor.sub(sizedSubLogPropTensor, 0,
					nceOutputSize - 1, 0, lastWord - firstWord - 1);
			subSizedSubLogPropTensor.copy(subLogPropTensor);
			this->trainOne(sizedContext, sizedOutputWord, sizedSubLogPropTensor,
					currentLearningRate, lastWord - firstWord, update_lkt,
					update_hiddens, update_output_layer);
		}
		nstep += lastWord - firstWord;
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	return 1;
}

float NgramModelNCE::computeF(intTensor& wordTensor,
		floatTensor& logPropTensor) {
	float f = 0;
	int nceOutputSize = logPropTensor.size[1];
	int N = wordTensor.size[1] - nceOutputSize + 1;
	firstTime();
	if (this->nceOutputSize != nceOutputSize) {
		this->changeNceOutputSize(nceOutputSize);
		// ready for training
	}

	if (N < n) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << n << endl;
		exit(1);
	}

	int maxExampleNumber = wordTensor.size[0];

	intTensor context;
	intTensor outputWord;
	floatTensor subLogPropTensor;
	int firstWord = 0, lastWord = 0;
	int cont = 1;
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	while (cont == 1) {
		firstWord = lastWord;
		lastWord += blockSize;
		if (lastWord >= maxExampleNumber) {
			// reach the end of the input tensor
			lastWord = maxExampleNumber;
			cont = 0;
		}
		context.sub(wordTensor, firstWord, lastWord - 1, N - n, N - 2);
		context.t();
		outputWord.sub(wordTensor, firstWord, lastWord - 1, N - 1,
				N + nceOutputSize - 2);
		outputWord.t();
		subLogPropTensor.sub(logPropTensor, firstWord, lastWord - 1, 0,
				nceOutputSize - 1);
		subLogPropTensor.t();
		currentExampleNumber += (lastWord - firstWord);

		if (lastWord - firstWord == blockSize) {
			// the batch's size is respected
			f += this->computeOneF(context, outputWord, subLogPropTensor);
		} else {
			intTensor sizedContext(n - 1, blockSize);
			// important
			sizedContext = 0;
			intTensor sizedOutputWord(nceOutputSize, blockSize);
			// important
			sizedOutputWord = SIGN_NOT_WORD;
			floatTensor sizedSubLogPropTensor(nceOutputSize, blockSize);
			// less important
			sizedSubLogPropTensor = 0;
			intTensor subSizedContext, subSizedOutputWord;
			floatTensor subSizedSubLogPropTensor;
			subSizedContext.sub(sizedContext, 0, n - 2, 0,
					lastWord - firstWord - 1);
			subSizedContext.copy(context);
			subSizedOutputWord.sub(sizedOutputWord, 0, nceOutputSize - 1, 0,
					lastWord - firstWord - 1);
			subSizedOutputWord.copy(outputWord);
			subSizedSubLogPropTensor.sub(sizedSubLogPropTensor, 0,
					nceOutputSize - 1, 0, lastWord - firstWord - 1);
			subSizedSubLogPropTensor.copy(subLogPropTensor);
			f += this->computeOneF(sizedContext, sizedOutputWord,
					sizedSubLogPropTensor);
		}
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	return f;
}

int NgramModelNCE::forwardProbability(intTensor& ngramTensor,
		floatTensor& probTensor) {
	int ngramNumber = ngramTensor.size[0];
	int nextId;
	int idWord;
	float localProb;
	intTensor context;
	intTensor bContext(n - 1, blockSize);
	intTensor selectBContext;
	intTensor selectContext;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, n - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, n + 2);
	intTensor word;
	word.select(ngramTensor, 1, n - 1);
	intTensor order;
	order.select(ngramTensor, 1, n + 1);
	int ngramId = 0;
	int ngramId2 = 0;
	int rBlockSize;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	//For CharNCE
	CharNCE* charOutput = dynamic_cast<CharNCE*>(this->outputLayer);
	CharNCE_AG* charOutputAg = dynamic_cast<CharNCE_AG*>(this->outputLayer);
	if (charOutput) {
	  charOutput->computeAllWeight();
	} else if (charOutputAg) {
	  charOutputAg->computeAllWeight();
	}
	do {
		ngramId2 = ngramId;
		rBlockSize = 0;
		// Read blockSize different contexts in ngramTensor
		while (rBlockSize < blockSize && ngramId < ngramNumber) {
			selectBContext.select(bContext, 1, rBlockSize);
			selectContext.select(context, 0, ngramId);
			selectBContext.copy(selectContext);
			// advance
			ngramId = contextFlag(ngramId);
			rBlockSize++;
		}
		rBlockSize = 0;
		firstTime(bContext);
		baseNetwork->forward(bContext);
		mainProb = this->outputLayer->normalizeForward(contextFeature);

		// run through each context in bContext
		while (rBlockSize < blockSize && ngramId2 < ngramNumber) {
			// the last n-gram with the same context
			nextId = contextFlag(ngramId2);
			for (; ngramId2 < nextId; ngramId2++) {
				// process each n-gram until nextId
				if (order(ngramId2) != SIGN_NOT_WORD) {
					// output word of ngramId2-th n-gram
					idWord = word(ngramId2);
					localProb = mainProb(idWord, rBlockSize);
					if (incrUnk != 1) {
						if (idWord == outputVoc->unk) {
							localProb = localProb * incrUnk;
						}
					}
					// Sometimes we have 0 probability and we want to write small number
					// but not zero
					if (localProb != 0) {
						probTensor(order(ngramId2)) = localProb;
					} else {
						probTensor(order(ngramId2)) = PROBZERO;
					}
				}
			}
			rBlockSize++;
		}
#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
	if (charOutput) {
          charOutput->changeCEBlockSize();
        } else if (charOutputAg) {
	  charOutputAg->changeCEBlockSize();
	}
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

int NgramModelNCE::forwardProbability(intTensor& ngramTensor,
		floatTensor& probTensor, int normalize) {
	if (normalize == 1) {
		return forwardProbability(ngramTensor, probTensor);
	} else if (normalize == 0) {
		return efficientForwardUnnormalizedProbability(ngramTensor, probTensor);
	} else {
		cerr << "Function : " << __PRETTY_FUNCTION__ << " File : " << __FILE__
				<< " line : " << __LINE__ << endl;
		cerr << "What is the value of normalize ? 0 or 1" << endl;
		return 2;
	}
}

int NgramModelNCE::forwardCoefNorm(intTensor& ngramTensor, ioFile* iof) {
	// computing
	int ngramNumber = ngramTensor.size[0];
	intTensor context;
	intTensor bContext(n - 1, blockSize);
	intTensor selectBContext;
	intTensor selectContext;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, n - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, n + 2);
	int ngramId = 0;
	int rBlockSize;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	do {
		rBlockSize = 0;
		// Read blockSize different contexts in ngramTensor
		while (rBlockSize < blockSize && ngramId < ngramNumber) {
			selectBContext.select(bContext, 1, rBlockSize);
			selectContext.select(context, 0, ngramId);
			selectBContext.copy(selectContext);
			// advance
			ngramId = contextFlag(ngramId);
			rBlockSize++;
		}
		rBlockSize = 0;
		firstTime(bContext);
		baseNetwork->forward(bContext);
		this->outputLayer->coefNorm(contextFeature, iof);
#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

int NgramModelNCE::forwardUnnormalizedProbability(intTensor& ngramTensor,
		floatTensor& probTensor, int normalized) {
	// for test
	time_t start, end;
	// with NCE, we only proceed the computation with blockSize = 1, because the number
	// of output words for each context is not the same, so I think it will be more
	// convenient to only consider 1 context each time
	changeBlockSize(1);
	int ngramNumber = ngramTensor.size[0];
	int idWord;
	float localProb;
	intTensor context;
	intTensor bContext;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, n - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, n + 2);
	intTensor word, localWord;
	word.select(ngramTensor, 1, n - 1);
	intTensor order;
	order.select(ngramTensor, 1, n + 1);
	int ngramId = 0, ngramId2 = 0, runNgram;
	int nbOutputWords;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	bContext = 0;
	CharNCE* charOutput = dynamic_cast<CharNCE*>(this->outputLayer);
        if (charOutput && normalized == 1) {
          charOutput->computeAllWeight();
        }
	do {
		ngramId2 = ngramId;
		bContext.select(context, 0, ngramId);

		//bContext.t();
		// advance
		ngramId = contextFlag(ngramId);
		// number of output words of which outputs need to be computed
		nbOutputWords = ngramId - ngramId2;
		firstTime(bContext);
		baseNetwork->forward(bContext);

		changeNceOutputSize(nbOutputWords);
		localWord.sub(word, ngramId2, ngramId - 1, 0, 0);

		// for test
		//time(&start);
		if (normalized == 0) {
			// no normalization
			mainProb = this->outputLayer->nceForward(contextFeature, localWord);
		} else if (normalized == 1) {
			mainProb = this->outputLayer->normalizeNceForward(contextFeature,
					localWord);
		} else {
			cerr
					<< "NgramModelNCE::forwardUnnormalizedProbability undefined normalized value"
					<< endl;
			cerr << "normalized should be 0 or 1" << endl;
			exit(1);
		}
		// for test
		//time(&end);
		//cout << "nceForward after " << difftime(end, start) << " seconds" << endl;

		for (runNgram = ngramId2; runNgram < ngramId; runNgram++) {
			if (order(runNgram) != SIGN_NOT_WORD) {
				idWord = word(runNgram);
				localProb = mainProb(runNgram - ngramId2, 0);
				if (incrUnk != 1) {
					if (idWord == outputVoc->unk) {
						localProb = localProb * incrUnk;
					}
				}
				if (localProb != 0) {
					probTensor(order(runNgram)) = localProb;
				} else {
					probTensor(order(runNgram)) = PROBZERO;
				}
			}
		}
#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

int NgramModelNCE::efficientForwardUnnormalizedProbability(
		intTensor& ngramTensor, floatTensor& probTensor) {
	// for test
	time_t start, end;
	// with NCE, we only proceed the computation with blockSize = 1, because the number
	// of output words for each context is not the same, so I think it will be more
	// convenient to only consider 1 context each time
	int ngramNumber = ngramTensor.size[0];
	int idWord;
	float localProb;
	intTensor context, selectContext;
	intTensor bContext(n - 1, blockSize), selectBContext;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, n - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, n + 2);
	intTensor word, localWord;
	word.select(ngramTensor, 1, n - 1);
	intTensor order;
	order.select(ngramTensor, 1, n + 1);
	int ngramId = 0, ngramId2 = 0, rBlockSize, nextId;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	bContext = 0;
	do {
		ngramId2 = ngramId;
		rBlockSize = 0;
		// Read blockSize different contexts in ngramTensor
		while (rBlockSize < blockSize && ngramId < ngramNumber) {
			selectBContext.select(bContext, 1, rBlockSize);
			selectContext.select(context, 0, ngramId);
			selectBContext.copy(selectContext);
			ngramId = contextFlag(ngramId);
			rBlockSize++;
		}
		rBlockSize = 0;
		//bContext.t();
		// advance
		firstTime(bContext);
		baseNetwork->forward(bContext);
		while (rBlockSize < blockSize && ngramId2 < ngramNumber) {
			nextId = contextFlag(ngramId2);
			selectContextFeature.select(contextFeature, 1, rBlockSize);
			for (; ngramId2 < nextId; ngramId2++) {
				if (order(ngramId2) != SIGN_NOT_WORD) {
					idWord = word(ngramId2);
					localProb = this->outputLayer->efficientNceForward(
							selectContextFeature, idWord);
					if (incrUnk != 1) {
						if (idWord == outputVoc->unk) {
							localProb = localProb * incrUnk;
						}
					}
					if (localProb != 0) {
						probTensor(order(ngramId2)) = localProb;
					} else {
						probTensor(order(ngramId2)) = PROBZERO;
					}
				}
			}
			rBlockSize++;
		}
#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

void NgramModelNCE::trainOne(intTensor& context, intTensor& outputWord,
		floatTensor& logPropTensor, float learningRate, int update_lkt,
		string update_hiddens, int update_output_layer) {
	this->trainOne(context, outputWord, logPropTensor, learningRate, blockSize,
			update_lkt, update_hiddens, update_output_layer);
}

void NgramModelNCE::trainOne(intTensor& context, intTensor& outputWord,
		floatTensor& logPropTensor, float learningRate, int subBlockSize,
		int update_lkt, string update_hiddens, int update_output_layer) {
  //time_t start, end;
  // firstTime is required only for recurrent models, see RRLinear
  firstTime(context);
  // Forward from lkt to the last hidden layer
  baseNetwork->forward(context);
  //cout << "Hidden weight:" << baseNetwork->modules[0]->weight.averageSquare() << endl;
  //cout << "Hidden out:" << baseNetwork->modules[1]->output.averageSquare() << endl;
  //cout << "Hidden Act out:" << baseNetwork->output.averageSquare() << endl;
  outputLayer->nceForward(contextFeature, outputWord);
  gradContextFeature = outputLayer->backward(outputWord, logPropTensor);
  //cout << "Out gradOutput:" << gradContextFeature.averageSquare() << endl;
  outputLayer->updateParameters(learningRate, outputWord);
  // Now gradContextFeature = sum of gradients of outputNetworks
  baseNetwork->backward(gradContextFeature);
  baseNetwork->updateParameters(learningRate, update_lkt, update_hiddens);
}

float NgramModelNCE::computeOneF(intTensor& context, intTensor& outputWord,
		floatTensor& logPropTensor) {
	firstTime(context);

	// Forward from lkt to the last hidden layer
	baseNetwork->forward(context);
	outputLayer->nceForward(contextFeature, outputWord);

	return outputLayer->computeF(outputWord, logPropTensor);
}

void NgramModelNCE::read(ioFile* iof, int allocation, int blockSize) {
	string readFormat;
	iof->readString(name);
	iof->readString(readFormat);
	iof->readInt(ngramType);
	inputVoc = new SoulVocab();
	outputVoc = new SoulVocab();
	iof->readInt(inputVoc->wordNumber);
	iof->readInt(outputVoc->wordNumber);
	iof->readInt(mapIUnk);
	iof->readInt(mapOUnk);
	iof->readInt(BOS);
	if (blockSize != 0) {
		this->blockSize = blockSize;
	} else {
		this->blockSize = DEFAULT_BLOCK_SIZE;
	}
	iof->readInt(n);
	iof->readInt(dimensionSize);
	iof->readInt(hiddenNumber);
	iof->readString(nonLinearType);
	hiddenLayerSizeArray.resize(hiddenNumber, 1);
	hiddenLayerSizeArray.read(iof);
	hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
	if (allocation) {
		// for test
		//cout << "NgramModel::read here" << endl;
		this->allocation();
		// for test
		//cout << "NgramModel::read here1" << endl;
	}
	baseNetwork->read(iof);
	// for test
	//cout << "NgramModel::read here2" << endl;
	int i;

	// only in the case of LBL model, we do not read the outputNetwork, because the outputNetwork weights are pointed to lkt right?
	outputLayer->read(iof);
	inputVoc->read(iof);
	outputVoc->read(iof);

	cout << "NgramModelNCE: Read: " << name << " " << ngramType << " " << inputVoc->wordNumber << " " << outputVoc->wordNumber << " " << blockSize << " " << n << " " << dimensionSize << " " << hiddenNumber << " " << nonLinearType << endl;
}

void NgramModelNCE::write(ioFile* iof, int closeFile) {
	// for test
	//cout << "NgramModel::write here8" << endl;
        cout << "NgramModelNCE: Write: " << name << " " << ngramType << " " << inputVoc->wordNumber << " " << outputVoc->wordNumber << " " << blockSize << " " << n << " " << dimensionSize << " " << hiddenNumber << " " << nonLinearType << endl;
  
        iof->writeString(name);
	iof->writeString(iof->format);
	iof->writeInt(ngramType);
	iof->writeInt(inputVoc->wordNumber);
	iof->writeInt(outputVoc->wordNumber);
	iof->writeInt(mapIUnk);
	iof->writeInt(mapOUnk);
	iof->writeInt(BOS);
	iof->writeInt(n);
	iof->writeInt(dimensionSize);
	iof->writeInt(hiddenNumber);
	iof->writeString(nonLinearType);
	hiddenLayerSizeArray.write(iof);
	// for test
	//cout << "NgramModel::write here10" << endl;
	baseNetwork->write(iof);
	// for test
	//cout << "NgramModel::write here11" << endl;
	outputLayer->write(iof);
	inputVoc->write(iof);
	outputVoc->write(iof);
	if (closeFile == 1) {
		iof->freeWriteFile();
	}
}

void NgramModelNCE::writeOutput(char* prefix) {
	body_writeOutput_nce
}

distance2NCE(NgramModelNCE)

void NgramModelNCE::showStateAG() {
	if (name == OVN_NCE_AG) {
		cout << "lkt cumul: " << baseNetwork->lkt->sqrtCumulGradWeight()
				<< endl;
		cout << "linear cumulWeight: "
				<< baseNetwork->modules[0]->sqrtCumulGradWeight() << endl;
		cout << "linear cumulBias: "
				<< baseNetwork->modules[0]->sqrtCumulGradBias() << endl;
		cout << "linearNCE cumulWeight: " << outputLayer->sqrtCumulGradWeight()
				<< endl;
		cout << "linearNCE cumulBias: " << outputLayer->sqrtCumulGradBias()
				<< endl;
	}
}

float NgramModelNCE::takeCurrentLearningRate(float learningRate,
		string learningRateType, int nstep, float learningRateDecay) {
	float currentLearningRate = 0;
	if (learningRateType == LEARNINGRATE_NORMAL) {
		currentLearningRate = learningRate / (1 + nstep * learningRateDecay);
	} else if (learningRateType == LEARNINGRATE_DOWN) {
		currentLearningRate = learningRate;
	} else if (learningRateType == LEARNINGRATE_ADJUST) {
		currentLearningRate = learningRate;
	} else if (learningRateType == LEARNINGRATE_NCE) {
	  currentLearningRate = learningRate;
	} else if (learningRateType == LEARNINGRATE_NCE_ADJUST) {
          currentLearningRate = learningRate;
	} else {
	  if (name == OVN_NCE_AG || name == CE_NCE_AG || name == CWE_NCE_AG
	      || name == CE_CE_NCE_AG || name == CE_H_NCE_AG || name == CE_CWE_NCE_AG 
	      || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
	    if (learningRateType == LEARNINGRATE_AG
		|| learningRateType == LEARNINGRATE_CAG
		|| learningRateType == LEARNINGRATE_BAG
		|| learningRateType == LEARNINGRATE_NCE_BAG) {
	      currentLearningRate = learningRate;
	    } else if (learningRateType == LEARNINGRATE_DBAG) {
	      
	      currentLearningRate = learningRate
		* outputLayer->sqrtCumulGradWeight();
	      
	    } else {
	      DEBUG(
		    cout << "learningRateType " << learningRateType << " does not match" << endl;)
		exit(1);
	    }
	  } else {
	    DEBUG(cout << "what is your learning rate type?" << endl;)
	      exit(1);
	  }
	}
	return currentLearningRate;
}
