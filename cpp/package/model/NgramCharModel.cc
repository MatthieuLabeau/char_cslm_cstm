/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS

 Class for n-gram neural network language model
*******************************************************************/
#include "mainModel.H"

NgramCharModel::NgramCharModel() {
}

NgramCharModel::~NgramCharModel() {
  delete charVoc;
}

void NgramCharModel::allocation() {
  recurrent = 0;
  otl = new outils();
  otl->sgenrand(time(NULL) + getpid());
  // hiddenStep = 1 if linear, 2 if non linear
  hiddenStep = 1;
  if (nonLinearType == TANH || nonLinearType == SIGM || nonLinearType == RELU) {
    hiddenStep = 2;
  }

  // create baseNetwork with one Embeddings and hiddenLayerSizeArray.length * hiddenStep consecutive modules
  baseNetwork = new Sequential(hiddenLayerSizeArray.length * hiddenStep);

  int i;
  // Lookup table with classical or one vector initialization
  if (name == CE || name == CE_H) {
    baseNetwork->lkt = new CharEmbeddings(dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else if (name == CWE || name == CWE_L) {
    baseNetwork->lkt =  new CharWordEmbeddings(outputVoc->wordNumber, dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else if (name == CE_AG) {
    baseNetwork->lkt = new CharEmbeddings_AG(dimensionSize, n - 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    baseNetwork->lkt->getHashMaps(hmf);
  } else {
    baseNetwork->lkt = new LookupTable(outputVoc->wordNumber, dimensionSize, n - 1, blockSize, 1, otl);
  }
  if (name == CE_H) {
    CharEmbeddingsForOutput = new CharEmbeddings(dimensionSize, 1, charVoc->wordNumber, charDimensionSize, nonLinearType, blockSize, 1, otl);
    CharEmbeddings* inputCE = static_cast<CharEmbeddings*>(baseNetwork->lkt);
    CharEmbeddings* inputL = static_cast<CharEmbeddings*>(CharEmbeddingsForOutput);
    inputL->getHashMaps(hmf);
    inputL->cLkt->weight.tieData(inputCE->cLkt->weight);
    inputL->convModule->weight.tieData(inputCE->convModule->weight);
    inputL->convModule->bias.tieData(inputCE->convModule->bias);
  }

  /*
  CharEmbeddings* clkt = dynamic_cast<CharEmbeddings*>(baseNetwork->lkt);
  CharWordEmbeddings* cwlkt = dynamic_cast<CharWordEmbeddings*>(baseNetwork->lkt);
  if (clkt) {
    clkt->getHashMaps(hmf);
  } else if (cwlkt) {
    cwlkt->getHashMaps(hmf);
    } */
  
  Module* module;
  if (name == CE_AG) {
    module = new Linear_AG((n - 1) * dimensionSize, hiddenLayerSizeArray(0), blockSize, otl);
  } else {
    module = new Linear((n - 1) * dimensionSize, hiddenLayerSizeArray(0), blockSize, otl);
  }
  baseNetwork->add(module);
  
  if (dimensionSize != hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1)) {
    cerr << "WARNING: last hidden layer size !=  projection dimension, use projection dimension" << endl;
  }
  hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1) = dimensionSize;

  // Add non linear activation
  if (nonLinearType == TANH) {
    module = new Tanh(hiddenLayerSizeArray(0), blockSize); // non linear
    baseNetwork->add(module);
  } else if (nonLinearType == SIGM) {
    module = new Sigmoid(hiddenLayerSizeArray(0), blockSize); // non linear
    baseNetwork->add(module);
  } else if (nonLinearType == RELU) {
    module = new Relu(hiddenLayerSizeArray(0), blockSize); // non linear
    baseNetwork->add(module);
  }
  // Add several hidden layers with linear or non linear activation
  for (i = 1; i < hiddenLayerSizeArray.size[0]; i++) {
    if (name == CE_AG) {
      module = new Linear_AG((n - 1) * dimensionSize, hiddenLayerSizeArray(0), blockSize, otl);
    } else {
      module = new Linear(hiddenLayerSizeArray(i - 1), hiddenLayerSizeArray(i), blockSize, otl);
    }
    baseNetwork->add(module);
    if (nonLinearType == TANH) {
      module = new Tanh(hiddenLayerSizeArray(i), blockSize); // non linear
      baseNetwork->add(module);
    } else if (nonLinearType == SIGM) {
      module = new Sigmoid(hiddenLayerSizeArray(i), blockSize); // non linear
      baseNetwork->add(module);
    } else if (nonLinearType == RELU) {
      module = new Relu(hiddenLayerSizeArray(i), blockSize); // non linear
      baseNetwork->add(module);
    }
  }
  
  // probabilities of ngram in each mini-batch
  probabilityOne.resize(blockSize, 1);
  int outputNetworkNumber = outputNetworkSize.size[0];
  // Create outputNetwork => softmax layers for tree
  outputNetwork = new ProbOutput*[outputNetworkNumber];
  ProbOutput* sl;
  if (name == CE_H) {
    sl = new HybridSoftmax(hiddenLayerSize, outputNetworkSize(0), blockSize, otl);
  } else if (name == CE_AG) {
    sl = new LinearSoftmax_AG(hiddenLayerSize, outputNetworkSize(0), blockSize, otl);
  } else {
    sl = new LinearSoftmax(hiddenLayerSize, outputNetworkSize(0), blockSize, otl);
  }

  outputNetwork[0] = sl;
  for (i = 1; i < outputNetworkNumber; i++) {
    sl = new LinearSoftmax(hiddenLayerSize, outputNetworkSize(i), 1, otl);
    outputNetwork[i] = sl;
  }
  doneForward.resize(outputNetworkNumber, 1);
  // contextFeature is the output of the last hidden layer, also the feature of all history
  contextFeature = baseNetwork->output;
  gradContextFeature.resize(contextFeature);
  localCodeWord.resize(blockSize, maxCodeWordLength);
  if (name == CWE_L) {
    CharWordEmbeddings* cwlkt = static_cast<CharWordEmbeddings*>(baseNetwork->lkt);
    outputNetwork[0]->weight.tieData(cwlkt->Lkt->weight);
  }
  // Create NgramDataSet to manipulate data
  dataSet = new NgramDataSet(ngramType, n, BOS, inputVoc, outputVoc, mapIUnk, mapOUnk, BLOCK_NGRAM_NUMBER);
  noUnkDataSet = new NgramDataSet(ngramType, n, BOS, inputVoc, outputVoc, mapIUnk, 0, BLOCK_NGRAM_NUMBER);
}



NgramCharModel::NgramCharModel(string name, char* charWordMapFileString, char* charVocFileString, char* inputVocFileString,
			       char* outputVocFileString, int mapIUnk, int mapOUnk, int BOS,
			       int blockSize, int n, int charDimensionSize, int dimensionSize, string nonLinearType,
			       intTensor& hiddenLayerSizeArray, char* codeWordFileString,
			       char* outputNetworkSizeFileString) {
  recurrent = 0;
  this->name = name;
  this->ngramType = 0;
  // Read vocabularies
  this->hmf = charWordMapFileString;
  this->charVoc = new SoulVocab(charVocFileString);
  this->inputVoc = new SoulVocab(inputVocFileString);
  this->outputVoc = new SoulVocab(outputVocFileString);
  this->mapIUnk = mapIUnk;
  this->mapOUnk = mapOUnk;
  this->BOS = BOS;
  this->blockSize = blockSize;
  this->n = n;
  if (BOS > n - 1) {
    this->BOS = n - 1;
  }

  this->charDimensionSize = charDimensionSize;
  this->dimensionSize = dimensionSize;
  this->nonLinearType = nonLinearType;
  ioFile readIof;
  // Read a tree structure, codeWord, outputNetworkSize
  if (!strcmp(codeWordFileString, "xxx")) {
    // default length is 2, there is only one output layer
    codeWord.resize(outputVoc->wordNumber, 2);
    codeWord = 0;
    for (int wordIndex = 0; wordIndex < outputVoc->wordNumber;
	 wordIndex++) {
      codeWord(wordIndex, 1) = wordIndex;
    }
  } else {
    readIof.takeReadFile(codeWordFileString);
    codeWord.read(&readIof);
  }
  if (!strcmp(outputNetworkSizeFileString, "xxx")) {
    outputNetworkSize.resize(1, 1);
    outputNetworkSize(0) = outputVoc->wordNumber;
  } else {
    readIof.takeReadFile(outputNetworkSizeFileString);
    outputNetworkSize.read(&readIof);
  }
  this->hiddenLayerSizeArray.resize(hiddenLayerSizeArray);
  this->hiddenLayerSizeArray.copy(hiddenLayerSizeArray);
  hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
  hiddenNumber = hiddenLayerSizeArray.length;
  maxCodeWordLength = this->codeWord.size[1];
  outputNetworkNumber = outputNetworkSize.size[0];
  allocation();
}

void NgramCharModel::read(ioFile* iof, int allocation, int blockSize, char* hmf) {
  string readFormat;
  this->hmf = hmf;
  iof->readString(name);
  iof->readString(readFormat);
  iof->readInt(ngramType);
  if (allocation == 1) {
    charVoc = new SoulVocab();
    inputVoc = new SoulVocab();
    outputVoc = new SoulVocab();
  }
  iof->readInt(charVoc->wordNumber);
  iof->readInt(inputVoc->wordNumber);
  iof->readInt(outputVoc->wordNumber);
  iof->readInt(mapIUnk);
  iof->readInt(mapOUnk);
  iof->readInt(BOS);
  if (blockSize != 0) {
    this->blockSize = blockSize;
  } else {
    this->blockSize = DEFAULT_BLOCK_SIZE;
  }
  iof->readInt(n);
  iof->readInt(charDimensionSize);
  iof->readInt(dimensionSize);
  iof->readInt(hiddenNumber);
  iof->readString(nonLinearType);
  iof->readInt(maxCodeWordLength);
  iof->readInt(outputNetworkNumber);
  if (allocation == 1) {
    codeWord.resize(outputVoc->wordNumber, maxCodeWordLength);
    outputNetworkSize.resize(outputNetworkNumber, 1);
  }
  codeWord.read(iof);
  outputNetworkSize.read(iof);
  hiddenLayerSizeArray.resize(hiddenNumber, 1);
  hiddenLayerSizeArray.read(iof);
  hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
  if (allocation == 1) {
    this->allocation();
  }
  baseNetwork->read(iof);
  int i;
  // only in the case of LBL model, we do not read the outputNetwork, because the outputNetwork weights are pointed to lkt right?
  if (name != CWE_L) {
    for (i = 0; i < outputNetworkSize.size[0]; i++) {
      outputNetwork[i]->read(iof);
    }
  }
  if (allocation == 1) {
    charVoc->read(iof);
    inputVoc->read(iof);
    outputVoc->read(iof);
  }
}

void NgramCharModel::write(ioFile* iof, int closeFile) {
  // for test
  //cout << "NgramCharModel::write here8" << endl;
  iof->writeString(name);
  iof->writeString(iof->format);
  iof->writeInt(ngramType);
  iof->writeInt(charVoc->wordNumber);
  iof->writeInt(inputVoc->wordNumber);
  iof->writeInt(outputVoc->wordNumber);
  iof->writeInt(mapIUnk);
  iof->writeInt(mapOUnk);
  iof->writeInt(BOS);
  iof->writeInt(n);
  iof->writeInt(charDimensionSize);
  iof->writeInt(dimensionSize);
  iof->writeInt(hiddenNumber);
  iof->writeString(nonLinearType);
  iof->writeInt(maxCodeWordLength);
  iof->writeInt(outputNetworkNumber);
  //iof->writeString(hmf)
  // for test
  //cout << "NgramModel::write here9" << endl;
  codeWord.write(iof);
  outputNetworkSize.write(iof);
  hiddenLayerSizeArray.write(iof);
  // for test
  //cout << "NgramModel::write here10" << endl;
  baseNetwork->write(iof);
  // for test
  //cout << "NgramModel::write here11" << endl;
  int i;
  if (name != CWE_L) {
    for (i = 0; i < outputNetworkSize.size[0]; i++) {
      outputNetwork[i]->write(iof);
    }
  }
  charVoc->write(iof);
  inputVoc->write(iof);
  outputVoc->write(iof);
  if (closeFile == 1) {
    iof->freeWriteFile();
  }
}
