#include "mainModel.H"

NgramWordTranslationModelNCE::NgramWordTranslationModelNCE() {

}

NgramWordTranslationModelNCE::~NgramWordTranslationModelNCE() {
	delete baseNetwork;
	delete outputLayer;
	delete inputVoc;
	delete outputVoc;
	delete dataSet;
}

void NgramWordTranslationModelNCE::changeBlockSize(int blockSize) {
	if (this->blockSize != blockSize) {
		this->blockSize = blockSize;
		baseNetwork->changeBlockSize(blockSize);
		contextFeature = baseNetwork->output;
		gradContextFeature.resize(contextFeature);
		if (!recurrent || !cont) {
			dataSet->blockSize = blockSize;
		}
		// we dont have outputNetwork, but outputLayer
		this->outputLayer->changeBlockSize(blockSize);
		probabilityOne.resize(blockSize, 1);
	}
}

void NgramWordTranslationModelNCE::changeNceOutputSize(int nceOutputSize) {
	if (this->nceOutputSize != nceOutputSize) {
		this->nceOutputSize = nceOutputSize;
		this->outputLayer->changeNceOutputSize(nceOutputSize);
	}
}

void NgramWordTranslationModelNCE::allocation() {
	otl = new outils();
	otl->sgenrand(time(NULL));
	hiddenStep = 1;
	if (nonLinearType == TANH) {
		hiddenStep = 2;
	} else if (nonLinearType == SIGM) {
		hiddenStep = 2;
	}

	baseNetwork = new Sequential(hiddenLayerSizeArray.length * hiddenStep);

	int i;
	baseNetwork->lkt = new LookupTable(inputVoc->wordNumber, dimensionSize,
			nm - 1, blockSize, 1, otl);

	Module* module;
	module = new Linear((nm - 1) * dimensionSize, hiddenLayerSizeArray(0),
			blockSize, otl);
	baseNetwork->add(module);

	if (nonLinearType == TANH) {
		module = new Tanh(hiddenLayerSizeArray(0), blockSize); // non linear
		baseNetwork->add(module);
	} else if (nonLinearType == SIGM) {
		module = new Sigmoid(hiddenLayerSizeArray(0), blockSize); // non linear
		baseNetwork->add(module);
	}
	for (i = 1; i < hiddenLayerSizeArray.size[0]; i++) {
		module = new Linear(hiddenLayerSizeArray(i - 1),
				hiddenLayerSizeArray(i), blockSize, otl);
		baseNetwork->add(module);
		if (nonLinearType == TANH) {
			module = new Tanh(hiddenLayerSizeArray(i), blockSize); // non linear
			baseNetwork->add(module);
		} else if (nonLinearType == SIGM) {
			module = new Sigmoid(hiddenLayerSizeArray(i), blockSize); // non linear
			baseNetwork->add(module);
		}
	}
	probabilityOne.resize(blockSize, 1);
	int outputNetworkNumber = outputNetworkSize.size[0];

	this->outputLayer = new LinearNCE(hiddenLayerSize, outputVoc->wordNumber,
			blockSize, otl);
	// contextFeature is the output of the last hidden layer, also the feature of all history
	contextFeature = baseNetwork->output;
	gradContextFeature.resize(contextFeature);
	if (name == WTOVN_NCE) {
		dataSet = new NgramWordTranslationDataSet(ngramType, n, BOS, inputVoc,
				outputVoc, mapIUnk, mapOUnk, BLOCK_NGRAM_NUMBER);
	} else if (name == WTOVN_NCE_DISCRIM) {
		dataSet = new NgramDiscrimWordTranslationDataSet(ngramType, n, BOS,
				inputVoc, outputVoc, mapIUnk, mapOUnk, BLOCK_NGRAM_NUMBER);
	} else {
		cerr << "NgramWordTranslationModelNCE::allocation error model name : "
				<< name << endl;
	}
}

NgramWordTranslationModelNCE::NgramWordTranslationModelNCE(string name,
		int ngramType, int inputSize, int outputSize, int blockSize, int n,
		int projectionDimension, string nonLinearType,
		intTensor& hiddenLayerSizeArray) {
	recurrent = 0;
	this->name = name;
	this->ngramType = ngramType;
	inputVoc = new SoulVocab();
	outputVoc = new SoulVocab();
	for (int i = 0; i < inputSize; i++) {
		stringstream out;
		out << i;
		inputVoc->add(out.str(), i);
	}
	for (int i = 0; i < outputSize; i++) {
		stringstream out;
		out << i;
		outputVoc->add(out.str(), i);
	}
	this->mapIUnk = 1;
	this->mapOUnk = 1;
	this->BOS = n;
	this->blockSize = blockSize;
	this->nceOutputSize = 0;
	this->n = n;
	nm = n * 2;
	this->dimensionSize = projectionDimension;
	this->nonLinearType = nonLinearType;
	this->hiddenLayerSizeArray = hiddenLayerSizeArray;
	hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
	hiddenNumber = hiddenLayerSizeArray.length;
	allocation();
}

NgramWordTranslationModelNCE::NgramWordTranslationModelNCE(string name,
		int ngramType, char* inputVocFileName, char* outputVocFileName,
		int mapIUnk, int mapOUnk, int BOS, int blockSize, int n,
		int projectionDimension, string nonLinearType,
		intTensor& hiddenLayerSizeArray) {
	recurrent = 0;
	this->name = name;
	this->ngramType = ngramType;
	this->inputVoc = new SoulVocab(inputVocFileName);
	this->outputVoc = new SoulVocab(outputVocFileName);
	this->mapIUnk = mapIUnk;
	this->mapOUnk = mapOUnk;
	this->BOS = BOS;
	this->blockSize = blockSize;
	this->nceOutputSize = 0;
	this->n = n;
	nm = n * 2;
	if (BOS > n) {
		this->BOS = n;
	}

	this->dimensionSize = projectionDimension;
	this->nonLinearType = nonLinearType;
	this->hiddenLayerSizeArray.resize(hiddenLayerSizeArray);
	this->hiddenLayerSizeArray.copy(hiddenLayerSizeArray);
	hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
	hiddenNumber = hiddenLayerSizeArray.length;
	allocation();
}

void NgramWordTranslationModelNCE::firstTime() {

}
void NgramWordTranslationModelNCE::firstTime(intTensor& context) {

}

int NgramWordTranslationModelNCE::train(char* dataFileName,
		int maxExampleNumber, int iteration, string learningRateType,
		float learningRate, float learningRateDecay, int update_lkt,
		string update_hiddens, int update_output_layer) {
	firstTime();
	ioFile dataIof;
	dataIof.takeReadFile(dataFileName);
	int ngramNumber;
	dataIof.readInt(ngramNumber);
	int N;
	dataIof.readInt(N);
	if (N < nm) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << nm << endl;
		exit(1);
	}
	// k is the number of negative examples, in the model, this number is stored as nceOutputSize = k + 1
	int k;
	dataIof.readInt(k);
	if (nceOutputSize != k + 1) {
		this->changeNceOutputSize(k + 1);
		// ready for training
	}
	if (maxExampleNumber > ngramNumber || maxExampleNumber == 0) {
		maxExampleNumber = ngramNumber;
	}
	float currentLearningRate;
	int nstep;
	nstep = maxExampleNumber * (iteration - 1);
	intTensor readTensor(blockSize, N + nceOutputSize - 1);
	floatTensor logPropTensor(blockSize, nceOutputSize);
	intTensor context;
	intTensor word;
	floatTensor subLogPropTensor;
	context.sub(readTensor, 0, blockSize - 1, N - nm, N - 2);
	context.t();
	word.sub(readTensor, 0, blockSize - 1, N - 1, N + nceOutputSize - 2);
	word.t();
	subLogPropTensor.sub(logPropTensor, 0, blockSize - 1, 0, nceOutputSize - 1);
	subLogPropTensor.t();
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	int blockNumber = maxExampleNumber / blockSize;
	int remainingNumber = maxExampleNumber - blockSize * blockNumber;
	int i;
	cout << maxExampleNumber << " examples" << endl;
	for (i = 0; i < blockNumber; i++) {
		//Read one line and then train
		this->readStripInt(dataIof, readTensor, logPropTensor, N); // read file n gram for word and context
		if (dataIof.getEOF()) {
			break;
		}
		currentExampleNumber += blockSize;
		currentLearningRate = this->takeCurrentLearningRate(learningRate,
				learningRateType, nstep, learningRateDecay);
		trainOne(context, word, subLogPropTensor, currentLearningRate,
				blockSize, update_lkt, update_hiddens, update_output_layer);
		nstep += blockSize;
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	if (remainingNumber != 0 && !dataIof.getEOF()) {
		context = 0;
		word = SIGN_NOT_WORD;
		logPropTensor = 0;
		intTensor lastReadTensor(remainingNumber, N + nceOutputSize - 1);
		this->readStripInt(dataIof, lastReadTensor, logPropTensor, N);
		intTensor subReadTensor;
		subReadTensor.sub(readTensor, 0, remainingNumber - 1, 0,
				N + nceOutputSize - 2);
		subReadTensor.copy(lastReadTensor);
		currentLearningRate = this->takeCurrentLearningRate(learningRate,
				learningRateType, nstep, learningRateDecay);
		trainOne(context, word, subLogPropTensor, currentLearningRate,
				remainingNumber, update_lkt, update_hiddens,
				update_output_layer);
	}
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

float NgramWordTranslationModelNCE::computeF(char* dataFileName) {
	float f = 0;
	firstTime();
	ioFile dataIof;
	dataIof.takeReadFile(dataFileName);
	int ngramNumber;
	dataIof.readInt(ngramNumber);
	int N;
	dataIof.readInt(N);
	if (N < nm) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << nm << endl;
		exit(1);
	}
	int k;
	dataIof.readInt(k);
	if (nceOutputSize != k + 1) {
		this->changeNceOutputSize(k + 1);
		// ready for training
	}

	int maxExampleNumber = ngramNumber;
	intTensor readTensor(blockSize, N + nceOutputSize - 1);
	floatTensor logPropTensor(blockSize, nceOutputSize);
	intTensor context;
	intTensor word;
	floatTensor subLogPropTensor;
	context.sub(readTensor, 0, blockSize - 1, N - nm, N - 2);
	context.t();
	word.sub(readTensor, 0, blockSize - 1, N - 1, N + nceOutputSize - 2);
	word.t();
	subLogPropTensor.sub(logPropTensor, 0, blockSize - 1, 0, nceOutputSize - 1);
	subLogPropTensor.t();
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	int blockNumber = maxExampleNumber / blockSize;
	int remainingNumber = maxExampleNumber - blockSize * blockNumber;
	int i;
	for (i = 0; i < blockNumber; i++) {
		//Read one line and then train
		this->readStripInt(dataIof, readTensor, logPropTensor, N); // read file n gram for word and context
		if (dataIof.getEOF()) {
			break;
		}
		currentExampleNumber += blockSize;
		f += this->computeOneF(context, word, subLogPropTensor);
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	if (remainingNumber != 0 && !dataIof.getEOF()) {
		context = 0;
		word = SIGN_NOT_WORD;
		logPropTensor = 0;
		intTensor lastReadTensor(remainingNumber, N + nceOutputSize - 1);
		this->readStripInt(dataIof, lastReadTensor, logPropTensor, N);
		intTensor subReadTensor;
		subReadTensor.sub(readTensor, 0, remainingNumber - 1, 0,
				N + nceOutputSize - 2);
		subReadTensor.copy(lastReadTensor);
		f += this->computeOneF(context, word, subLogPropTensor);
	}
#if PRINT_DEBUG
	cout << endl;
#endif
	return f;
}

float NgramWordTranslationModelNCE::computeF(intTensor& wordTensor,
		floatTensor& logPropTensor) {
	float f = 0;
	int nceOutputSize = logPropTensor.size[1];
	int N = wordTensor.size[1] - nceOutputSize + 1;
	firstTime();
	if (this->nceOutputSize != nceOutputSize) {
		this->changeNceOutputSize(nceOutputSize);
		// ready for training
	}

	if (N < nm) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << nm << endl;
		exit(1);
	}

	int maxExampleNumber = wordTensor.size[0];

	intTensor context;
	intTensor outputWord;
	floatTensor subLogPropTensor;
	int firstWord = 0, lastWord = 0;
	int cont = 1;
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	while (cont == 1) {
		firstWord = lastWord;
		lastWord += blockSize;
		if (lastWord >= maxExampleNumber) {
			// reach the end of the input tensor
			lastWord = maxExampleNumber;
			cont = 0;
		}
		context.sub(wordTensor, firstWord, lastWord - 1, N - nm, N - 2);
		context.t();
		outputWord.sub(wordTensor, firstWord, lastWord - 1, N - 1,
				N + nceOutputSize - 2);
		outputWord.t();
		subLogPropTensor.sub(logPropTensor, firstWord, lastWord - 1, 0,
				nceOutputSize - 1);
		subLogPropTensor.t();
		currentExampleNumber += (lastWord - firstWord);

		if (lastWord - firstWord == blockSize) {
			// the batch's size is respected
			f += this->computeOneF(context, outputWord, subLogPropTensor);
		} else {
			intTensor sizedContext(nm - 1, blockSize);
			// important
			sizedContext = 0;
			intTensor sizedOutputWord(nceOutputSize, blockSize);
			// important
			sizedOutputWord = SIGN_NOT_WORD;
			floatTensor sizedSubLogPropTensor(nceOutputSize, blockSize);
			// less important
			sizedSubLogPropTensor = 0;
			intTensor subSizedContext, subSizedOutputWord;
			floatTensor subSizedSubLogPropTensor;
			subSizedContext.sub(sizedContext, 0, nm - 2, 0,
					lastWord - firstWord - 1);
			subSizedContext.copy(context);
			subSizedOutputWord.sub(sizedOutputWord, 0, nceOutputSize - 1, 0,
					lastWord - firstWord - 1);
			subSizedOutputWord.copy(outputWord);
			subSizedSubLogPropTensor.sub(sizedSubLogPropTensor, 0,
					nceOutputSize - 1, 0, lastWord - firstWord - 1);
			subSizedSubLogPropTensor.copy(subLogPropTensor);
			f += this->computeOneF(sizedContext, sizedOutputWord,
					sizedSubLogPropTensor);
		}
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	return f;
}

int NgramWordTranslationModelNCE::trainTest(intTensor& wordTensor,
		floatTensor& logPropTensor, int iteration, string learningRateType,
		float learningRate, float learningRateDecay, int update_lkt,
		string update_hiddens, int update_output_layer) {
	int nceOutputSize = logPropTensor.size[1];
	int N = wordTensor.size[1] - nceOutputSize + 1;
	firstTime();
	if (this->nceOutputSize != nceOutputSize) {
		this->changeNceOutputSize(nceOutputSize);
		// ready for training
	}

	if (N < nm) {
		cerr << "ERROR: N in data is wrong:" << N << " < " << nm << endl;
		exit(1);
	}

	int maxExampleNumber = wordTensor.size[0];

	float currentLearningRate;
	// for test
	//cout << "NgramModel::train here 1" << endl;
	// nstep is the number of seen examples
	int nstep;
	// nstep at the beginning: number of examples used in
	// previous iterations, here being computed approximately
	nstep = maxExampleNumber * (iteration - 1);

	intTensor context;
	intTensor outputWord;
	floatTensor subLogPropTensor;
	int firstWord = 0, lastWord = 0;
	int cont = 1;
	int currentExampleNumber = 0;
	int percent = 1;
	float aPercent = maxExampleNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	while (cont == 1) {
		firstWord = lastWord;
		lastWord += blockSize;
		if (lastWord >= maxExampleNumber) {
			// reach the end of the input tensor
			lastWord = maxExampleNumber;
			cont = 0;
		}
		context.sub(wordTensor, firstWord, lastWord - 1, N - nm, N - 2);
		context.t();
		outputWord.sub(wordTensor, firstWord, lastWord - 1, N - 1,
				N + nceOutputSize - 2);
		outputWord.t();
		subLogPropTensor.sub(logPropTensor, firstWord, lastWord - 1, 0,
				nceOutputSize - 1);
		subLogPropTensor.t();
		currentExampleNumber += (lastWord - firstWord);
		currentLearningRate = takeCurrentLearningRate(learningRate,
				learningRateType, nstep, learningRateDecay);
		if (lastWord - firstWord == blockSize) {
			// the batch's size is respected
			this->trainOne(context, outputWord, subLogPropTensor,
					currentLearningRate, lastWord - firstWord, update_lkt,
					update_hiddens, update_output_layer);
		} else {
			intTensor sizedContext(nm - 1, blockSize);
			// important
			sizedContext = 0;
			intTensor sizedOutputWord(nceOutputSize, blockSize);
			// important
			sizedOutputWord = SIGN_NOT_WORD;
			floatTensor sizedSubLogPropTensor(nceOutputSize, blockSize);
			// less important
			sizedSubLogPropTensor = 0;
			intTensor subSizedContext, subSizedOutputWord;
			floatTensor subSizedSubLogPropTensor;
			subSizedContext.sub(sizedContext, 0, nm - 2, 0,
					lastWord - firstWord - 1);
			subSizedContext.copy(context);
			subSizedOutputWord.sub(sizedOutputWord, 0, nceOutputSize - 1, 0,
					lastWord - firstWord - 1);
			subSizedOutputWord.copy(outputWord);
			subSizedSubLogPropTensor.sub(sizedSubLogPropTensor, 0,
					nceOutputSize - 1, 0, lastWord - firstWord - 1);
			subSizedSubLogPropTensor.copy(subLogPropTensor);
			this->trainOne(sizedContext, sizedOutputWord, sizedSubLogPropTensor,
					currentLearningRate, lastWord - firstWord, update_lkt,
					update_hiddens, update_output_layer);
		}
		nstep += lastWord - firstWord;
#if PRINT_DEBUG
		if (currentExampleNumber > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) currentExampleNumber / maxExampleNumber << " ... "
					<< flush;
		}
#endif
	}
	return 1;
}

void NgramWordTranslationModelNCE::trainOne(intTensor& context,
		intTensor& outputWord, floatTensor& logPropTensor, float learningRate,
		int update_lkt, string update_hiddens, int update_output_layer) {
	this->trainOne(context, outputWord, logPropTensor, learningRate, blockSize,
			update_lkt, update_hiddens, update_output_layer);
}

void NgramWordTranslationModelNCE::trainOne(intTensor& context,
		intTensor& outputWord, floatTensor& logPropTensor, float learningRate,
		int subBlockSize, int update_lkt, string update_hiddens,
		int update_output_layer) {
	firstTime(context);
	baseNetwork->forward(context);
	// for test
	//cout << "NgramWordTranslationModelNCE::trainOne here 2" << endl;

	outputLayer->nceForward(contextFeature, outputWord);
	// for test
	//cout << "NgramWordTranslationModelNCE::trainOne here 3" << endl;
	gradContextFeature = outputLayer->backward(outputWord, logPropTensor);
	// for test
	//cout << "NgramWordTranslationModelNCE::trainOne here 4" << endl;
	baseNetwork->backward(gradContextFeature);
	// for test
	//cout << "NgramWordTranslationModelNCE::trainOne here 5" << endl;
	if (update_output_layer == 1) {
		outputLayer->updateParameters(learningRate, outputWord);
		// for test
		//cout << "NgramWordTranslationModelNCE::trainOne here 6" << endl;
	}
	baseNetwork->updateParameters(learningRate, update_lkt, update_hiddens);
	// for test
	//cout << "NgramWordTranslationModelNCE::trainOne here 7" << endl;
}

// objective function of a mini-batch
float NgramWordTranslationModelNCE::computeOneF(intTensor& context,
		intTensor& outputWord, floatTensor& logPropTensor) {
	firstTime(context);

	// Forward from lkt to the last hidden layer
	baseNetwork->forward(context);
	outputLayer->nceForward(contextFeature, outputWord);

	return outputLayer->computeF(outputWord, logPropTensor);
}

// Compute probabilities for n-gram in ngramTensor, output to probTensor
int NgramWordTranslationModelNCE::forwardProbability(intTensor& ngramTensor,
		floatTensor& probTensor) {
	int bkBlockSize = blockSize;
	if (cont && recurrent) {
		changeBlockSize(1);
	}
	firstTime();
	int i;
	float localProb;
	int idWord;
	int ngramNumber = ngramTensor.size[0];
	intTensor bContext(nm - 1, blockSize);
	intTensor selectContext;
	intTensor selectBContext;
	intTensor context;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, nm - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, nm + 2);
	intTensor word;
	word.select(ngramTensor, 1, nm - 1);
	intTensor order;
	order.select(ngramTensor, 1, nm + 1);
	int ngramId = 0;
	int ngramId2 = 0;
	int rBlockSize;
	int nextId;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	bContext = 0;
	do {
		ngramId2 = ngramId;
		rBlockSize = 0;

		while (rBlockSize < blockSize && ngramId < ngramNumber) {
			selectBContext.select(bContext, 1, rBlockSize);
			selectContext.select(context, 0, ngramId);
			selectBContext.copy(selectContext);
			ngramId = contextFlag(ngramId);
			rBlockSize++;
		}
		rBlockSize = 0;
		firstTime(bContext);
		baseNetwork->forward(bContext);
		mainProb = this->outputLayer->normalizeForward(contextFeature);
		while (rBlockSize < blockSize && ngramId2 < ngramNumber) {
			nextId = contextFlag(ngramId2);
			for (; ngramId2 < nextId; ngramId2++) {
				if (order(ngramId2) != SIGN_NOT_WORD) {
					idWord = word(ngramId2);
					localProb = mainProb(idWord, rBlockSize);
					if (incrUnk != 1) {
						if (idWord == outputVoc->unk) {
							localProb = localProb * incrUnk;
						}
					}
					if (localProb != 0) {
						probTensor(order(ngramId2)) = localProb;
					} else {
						probTensor(order(ngramId2)) = PROBZERO;
					}
				}
			}
			rBlockSize++;
		}
#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
#if PRINT_DEBUG
	cout << endl;
#endif
	if (cont && recurrent) {
		changeBlockSize(bkBlockSize);
	}
	return 1;
}

int NgramWordTranslationModelNCE::forwardProbability(intTensor& ngramTensor,
		floatTensor& probTensor, int normalize) {
	if (normalize == 1) {
		return forwardProbability(ngramTensor, probTensor);
	} else if (normalize == 0) {
		return efficientForwardUnnormalizedProbability(ngramTensor, probTensor);
	} else {
		cerr << "Function : " << __PRETTY_FUNCTION__ << " File : " << __FILE__
				<< " line : " << __LINE__ << endl;
		cerr << "What is the value of normalize ? 0 or 1" << endl;
		return 2;
	}
}

int NgramWordTranslationModelNCE::forwardUnnormalizedProbability(
		intTensor& ngramTensor, floatTensor& probTensor) {
	// with NCE, we only proceed the computation with blockSize = 1, because the number
	// of output words for each context is not the same, so I think it will be more
	// convenient to only consider 1 context each time
	changeBlockSize(1);
	firstTime();
	int i;
	float localProb;
	int idWord;
	int ngramNumber = ngramTensor.size[0];
	intTensor bContext;
	intTensor context;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, nm - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, nm + 2);
	intTensor word, localWord;
	word.select(ngramTensor, 1, nm - 1);
	intTensor order;
	order.select(ngramTensor, 1, nm + 1);
	int ngramId = 0, ngramId2 = 0, runNgram;
	int nbOutputWords;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	bContext = 0;
	do {
		ngramId2 = ngramId;

		bContext.select(context, 0, ngramId);
		//bContext.t();
		// the next id where there is a new different context
		ngramId = contextFlag(ngramId);
		// number of output words of which outputs need to be computed
		nbOutputWords = ngramId - ngramId2;
		firstTime(bContext);
		baseNetwork->forward(bContext);

		changeNceOutputSize(nbOutputWords);
		localWord.sub(word, ngramId2, ngramId - 1, 0, 0);

		// no normalization
		mainProb = this->outputLayer->nceForward(contextFeature, localWord);

		for (runNgram = ngramId2; runNgram < ngramId; runNgram++) {
			if (order(runNgram) != SIGN_NOT_WORD) {
				idWord = word(runNgram);
				localProb = mainProb(runNgram - ngramId2, 0);
				if (incrUnk != 1) {
					if (idWord == outputVoc->unk) {
						localProb = localProb * incrUnk;
					}
				}
				if (localProb != 0) {
					probTensor(order(runNgram)) = localProb;
				} else {
					probTensor(order(runNgram)) = PROBZERO;
				}
			}
		}

#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;
}

int NgramWordTranslationModelNCE::efficientForwardUnnormalizedProbability(
		intTensor& ngramTensor, floatTensor& probTensor) {
	firstTime();
	float localProb;
	int idWord;
	int ngramNumber = ngramTensor.size[0];
	intTensor bContext(nm - 1, blockSize);
	intTensor selectContext;
	intTensor selectBContext;
	intTensor context;
	context.sub(ngramTensor, 0, ngramNumber - 1, 0, nm - 2);
	intTensor contextFlag;
	contextFlag.select(ngramTensor, 1, nm + 2);
	intTensor word;
	word.select(ngramTensor, 1, nm - 1);
	intTensor order;
	order.select(ngramTensor, 1, nm + 1);
	int ngramId = 0;
	int ngramId2 = 0;
	int rBlockSize;
	int nextId;
	int percent = 1;
	float aPercent = ngramNumber * CONSTPRINT;
	float iPercent = aPercent * percent;
	bContext = 0;
	do {
		ngramId2 = ngramId;
		rBlockSize = 0;

		while (rBlockSize < blockSize && ngramId < ngramNumber) {
			selectBContext.select(bContext, 1, rBlockSize);
			selectContext.select(context, 0, ngramId);
			selectBContext.copy(selectContext);
			ngramId = contextFlag(ngramId);
			rBlockSize++;
		}
		rBlockSize = 0;
		firstTime(bContext);
		baseNetwork->forward(bContext);
		while (rBlockSize < blockSize && ngramId2 < ngramNumber) {
			nextId = contextFlag(ngramId2);
			selectContextFeature.select(contextFeature, 1, rBlockSize);
			for (; ngramId2 < nextId; ngramId2++) {
				if (order(ngramId2) != SIGN_NOT_WORD) {
					idWord = word(ngramId2);
					localProb = this->outputLayer->efficientNceForward(
							selectContextFeature, idWord);
					if (incrUnk != 1) {
						if (idWord == outputVoc->unk) {
							localProb = localProb * incrUnk;
						}
					}
					if (localProb != 0) {
						probTensor(order(ngramId2)) = localProb;
					} else {
						probTensor(order(ngramId2)) = PROBZERO;
					}
				}
			}
			rBlockSize++;
		}
#if PRINT_DEBUG
		if (ngramId > iPercent) {
			percent++;
			iPercent = aPercent * percent;
			cout << (float) ngramId / ngramNumber << " ... " << flush;
		}
#endif
	} while (ngramId < ngramNumber);
#if PRINT_DEBUG
	cout << endl;
#endif
	return 1;

}

//IO functions
void NgramWordTranslationModelNCE::read(ioFile* iof, int allocation,
		int blockSize) {
	string readFormat;
	iof->readString(name);
	iof->readString(readFormat);
	iof->readInt(ngramType);
	inputVoc = new SoulVocab();
	outputVoc = new SoulVocab();
	iof->readInt(inputVoc->wordNumber);
	iof->readInt(outputVoc->wordNumber);
	iof->readInt(mapIUnk);
	iof->readInt(mapOUnk);
	iof->readInt(BOS);

	if (blockSize != 0) {
		this->blockSize = blockSize;
	} else {
		this->blockSize = DEFAULT_BLOCK_SIZE;
	}
	iof->readInt(n);
	nm = n * 2;
	iof->readInt(dimensionSize);
	iof->readInt(hiddenNumber);
	iof->readString(nonLinearType);

	hiddenLayerSizeArray.resize(hiddenNumber, 1);
	hiddenLayerSizeArray.read(iof);

	hiddenLayerSize = hiddenLayerSizeArray(hiddenLayerSizeArray.length - 1);
	if (allocation) {
		this->allocation();
	}

	baseNetwork->read(iof);

	int i;
	outputLayer->read(iof);
	inputVoc->read(iof);
	outputVoc->read(iof);
}

void NgramWordTranslationModelNCE::write(ioFile* iof, int closeFile) {
	iof->writeString(name);
	iof->writeString(iof->format);
	iof->writeInt(ngramType);
	iof->writeInt(inputVoc->wordNumber);
	iof->writeInt(outputVoc->wordNumber);
	iof->writeInt(mapIUnk);
	iof->writeInt(mapOUnk);
	iof->writeInt(BOS);
	iof->writeInt(n);
	iof->writeInt(dimensionSize);
	iof->writeInt(hiddenNumber);
	iof->writeString(nonLinearType);
	hiddenLayerSizeArray.write(iof);
	baseNetwork->write(iof);
	outputLayer->write(iof);
	inputVoc->write(iof);
	outputVoc->write(iof);
	if (closeFile == 1) {
		iof->freeWriteFile();
	}
}

void NgramWordTranslationModelNCE::writeOutput(char* prefix) {
	body_writeOutput_nce
}

distance2NCE(NgramWordTranslationModelNCE)
