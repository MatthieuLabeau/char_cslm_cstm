HEADERS += \
    #package/model/FunctionModel.H \
    ../package/model/JointNgramWordTranslationModel.H \
    ../package/model/mainModel.H \
    ../package/model/MultiplesNeuralModel.H \
    ../package/model/NeuralModel.H \
    ../package/model/NgramModel.H \
    ../package/model/NgramModelNCE.H \
    ../package/model/NgramPhraseTranslationModel.H \
    ../package/model/NgramRankModel.H \
    ../package/model/NgramWordTranslationModel.H \
    ../package/model/NgramWordTranslationModelNCE.H \
    ../package/model/RecurrentModel.H

SOURCES += \
    #package/model/FunctionModel.cc \
    ../package/model/JointNgramWordTranslationModel.cc \
    ../package/model/MultiplesNeuralModel.cc \
    ../package/model/NeuralModel.cc \
    ../package/model/NgramModel.cc \
    ../package/model/NgramModelNCE.cc \
    ../package/model/NgramPhraseTranslationModel.cc \
    ../package/model/NgramRankModel.cc \
    ../package/model/NgramWordTranslationModel.cc \
    ../package/model/NgramWordTranslationModelNCE.cc \
    ../package/model/RecurrentModel.cc
