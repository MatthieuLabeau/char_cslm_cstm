#include "mainModel.H"

class NgramWordTranslationModelNCE: public NeuralModel {
public:
	// ngramType 0: trgSrc, 1: trg, 2: srcTrg, 3: src

	int nm;
	int nceOutputSize;
	LinearNCE* outputLayer;

	NgramWordTranslationModelNCE();
	~NgramWordTranslationModelNCE();

	void
	allocation();

	void
	changeBlockSize(int blockSize);

	void
	changeNceOutputSize(int nceOutputSize);

	NgramWordTranslationModelNCE(string name, int ngramType, int inputSize,
			int outputSize, int blockSize, int n, int projectionDimension,
			string nonLinearType, intTensor& hiddenLayerSizeArray); // for test only

	NgramWordTranslationModelNCE(string name, int ngramType,
			char* inputVocFileName, char* outputVocFileName, int mapCUnk,
			int mapPUnk, int BOS, int blockSize, int n, int projectionDimension,
			string nonLinearType, intTensor& hiddenLayerSizeArray);

	void
	firstTime();
	void
	firstTime(intTensor& context);

	int
	train(char* dataFileName, int maxExampleNumber, int iteration,
			string learningRateType, float learningRate,
			float learningRateDecay, int update_lkt, string update_hiddens,
			int update_output_layer);

	float
	computeF(char* dataFileName);

	float
	computeF(intTensor& wordTensor, floatTensor& logPropTensor);

	int trainTest(intTensor& wordTensor, floatTensor& logPropTensor,
			int iteration, string learningRateType, float learningRate,
			float learningRateDecay, int update_lkt, string update_hiddens,
			int update_output_layer);

	void
	trainOne(intTensor& context, intTensor& outputWord,
			floatTensor& logPropTensor, float learningRate, int update_lkt,
			string update_hiddens, int update_output_layer);

	void
	trainOne(intTensor& context, intTensor& outputWord,
			floatTensor& logPropTensor, float learningRate, int subBlockSize,
			int update_lkt, string update_hiddens, int update_output_layer);

	// objective function of a mini-batch
	float
	computeOneF(intTensor& context, intTensor& outputWord,
			floatTensor& logPropTensor);

	// Compute probabilities for n-gram in ngramTensor, output to probTensor
	int
	forwardProbability(intTensor& ngramTensor, floatTensor& probTensor);

	int
	forwardProbability(intTensor& ngramTensor, floatTensor& probTensor, int normalize);

	int
	forwardUnnormalizedProbability(intTensor& ngramTensor, floatTensor& probTensor);

	int
	efficientForwardUnnormalizedProbability(intTensor& ngramTensor, floatTensor& probTensor);

	//IO functions
	void
	read(ioFile* iof, int allocation, int blockSize);

	void
	write(ioFile* iof, int closeFile);

	void
	writeOutput(char* prefix);

	float
	distance2(NeuralModel* anotherModel);
};
