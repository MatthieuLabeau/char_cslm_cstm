/******************************************************************
 Structure OUtput Layer (SOUL) Language Model Toolkit
 (C) Copyright 2009 - 2012 Hai-Son LE LIMSI-CNRS

 Abstract class for neural network language model,
 functions are described in NeuralModel.H
*******************************************************************/
#include "mainModel.H"

NeuralModel::NeuralModel() {
  // By default, don't modify p(<unk>)
  incrUnk = 1;
}

NeuralModel::~NeuralModel() {
}

int NeuralModel::decodeWord(intTensor& word) {
  return decodeWord(word, blockSize);
}
int NeuralModel::decodeWord(intTensor& word, int subBlockSize) {
  int nBlockSize;
  for (nBlockSize = 0; nBlockSize < subBlockSize; nBlockSize++) {
    if (word(nBlockSize) != SIGN_NOT_WORD) {
      selectCodeWord.select(codeWord, 0, word(nBlockSize));
      selectLocalCodeWord.select(localCodeWord, 0, nBlockSize);
      selectLocalCodeWord.copy(selectCodeWord);
    } else {
      selectLocalCodeWord.select(localCodeWord, 0, nBlockSize);
      selectLocalCodeWord = SIGN_NOT_WORD;
    }
  }
  for (nBlockSize = subBlockSize; nBlockSize < blockSize; nBlockSize++) {
    selectLocalCodeWord.select(localCodeWord, 0, nBlockSize);
    selectLocalCodeWord = SIGN_NOT_WORD;
  }
  return 1;
}

void NeuralModel::setWeight(char* layerString, floatTensor& tensor) {
  if (!strcmp(layerString, "projection")) {
    baseNetwork->lkt->weight.copy(tensor);
  } else if (!strcmp(layerString, "prediction")) {
    outputNetwork[0]->weight.copy(tensor);
  }
}
floatTensor&
NeuralModel::getWeight(char* layerString) {
  if (!strcmp(layerString, "projection")) {
    return baseNetwork->lkt->weight;
  } else if (!strcmp(layerString, "prediction")) {
    return outputNetwork[0]->weight;
  }
  return baseNetwork->lkt->weight;
}

void NeuralModel::setAGparams(float e, float cache, int use_cache) {  
  //Embeddings
  if (name == OVN_AG || name == OVN_NCE_AG || name == LKT_CWE_NCE_AG){
    LookupTable_AG* lkt = static_cast<LookupTable_AG*>(this->baseNetwork->lkt);
    lkt->e = e;
    lkt->cache = cache;
    lkt->use_cache = use_cache;
  } else if (name == CE_AG || name == CE_NCE_AG
	     || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	     || name == CE_CWE_NCE_AG ) {
    CharEmbeddings_AG* clkt = static_cast<CharEmbeddings_AG*>(this->baseNetwork->lkt);
    CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(clkt->cLkt);
    Linear_AG* conv_AG = static_cast<Linear_AG*>(clkt->convModule);
    cLkt_AG->e = e;
    cLkt_AG->cache = cache;
    cLkt_AG->use_cache = use_cache;
    conv_AG->e = e;
    conv_AG->cache = cache;
    conv_AG->use_cache = use_cache;
  } else if (name == CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    CharWordEmbeddings_AG* clkt = static_cast<CharWordEmbeddings_AG*>(this->baseNetwork->lkt);
    CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(clkt->cLkt);
    LookupTable_AG* Lkt_AG = static_cast<LookupTable_AG*>(clkt->Lkt);
    Linear_AG* conv_AG = static_cast<Linear_AG*>(clkt->convModule);
    cLkt_AG->e = e;
    cLkt_AG->cache = cache;
    cLkt_AG->use_cache = use_cache;
    Lkt_AG->e = e;
    Lkt_AG->cache = cache;
    Lkt_AG->use_cache = use_cache;
    conv_AG->e = e;
    conv_AG->cache = cache;
    conv_AG->use_cache = use_cache;
  }
  //Hidden Layers
  for (int i = 0; i < baseNetwork->size; i=i+2) {
    if (name == OVN_AG || name == OVN_NCE_AG
	|| name == CE_AG || name == CE_NCE_AG
	|| name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	|| name == CE_CWE_NCE_AG || name == CWE_NCE_AG 
	|| name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG){
      Linear_AG* h = static_cast<Linear_AG*>(this->baseNetwork->modules[i]);
      h->e = e;
      h->cache = cache;
      h->use_cache = use_cache;
    }
  }
  //Output
  if (name == OVN_AG || name == CE_AG){
    for (int i = 0; i < outputNetworkNumber; i++) {
      LinearSoftmax_AG* out = static_cast<LinearSoftmax_AG*>(this->outputNetwork[i]);
      out->e = e;
      out->cache = cache;
      out->use_cache = use_cache;
    }
  } else if (name == OVN_NCE_AG || name == CE_NCE_AG || name == CWE_NCE_AG){
    LinearNCE_AG* out = static_cast<LinearNCE_AG*>(static_cast<NgramModelNCE*>(this)->outputLayer);
    out->e = e;
    out->cache = cache;
    out->use_cache = use_cache;
  } else if (name == CE_H_NCE_AG) {
    HybridNCE_AG* out = static_cast<HybridNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer);
    out->e = e;
    out->cache = cache;
    out->use_cache = use_cache;
  } else if (name == CE_CE_NCE_AG) {
    CharNCE_AG* out = static_cast<CharNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer);    
    CharEmbeddings_AG* out_clkt = static_cast<CharEmbeddings_AG*>(out->charEmbeddings);
    CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(out_clkt->cLkt);
    Linear_AG* conv_AG = static_cast<Linear_AG*>(out_clkt->convModule);
    cLkt_AG->e = e;
    cLkt_AG->cache = cache;
    cLkt_AG->use_cache = use_cache;
    conv_AG->e = e;
    conv_AG->cache = cache;
    conv_AG->use_cache = use_cache;
  } else if (name == CE_CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    CharNCE_AG* out = static_cast<CharNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer);
    CharWordEmbeddings_AG* out_clkt = static_cast<CharWordEmbeddings_AG*>(out->charEmbeddings);
    LookupTable_AG* Lkt_AG = static_cast<LookupTable_AG*>(out_clkt->Lkt);
    CharLookupTable_AG* cLkt_AG = static_cast<CharLookupTable_AG*>(out_clkt->cLkt);
    Linear_AG* conv_AG = static_cast<Linear_AG*>(out_clkt->convModule);
    Lkt_AG->e = e;
    Lkt_AG->cache = cache;
    Lkt_AG->use_cache = use_cache;
    cLkt_AG->e = e;
    cLkt_AG->cache = cache;
    cLkt_AG->use_cache = use_cache;
    conv_AG->e = e;
    conv_AG->cache = cache;
    conv_AG->use_cache = use_cache;
  }
}

void NeuralModel::resetAGparams() {
  //Embeddings                                                                                                                                                                                              
  if (name == OVN_AG || name == OVN_NCE_AG || name == LKT_CWE_NCE_AG){
    static_cast<LookupTable_AG*>(this->baseNetwork->lkt)->resetAG();
  } else if (name == CE_AG || name == CE_NCE_AG
             || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	     || name == CE_CWE_NCE_AG ) {
    static_cast<CharEmbeddings_AG*>(this->baseNetwork->lkt)->resetAG();
  } else if (name == CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    static_cast<CharWordEmbeddings_AG*>(this->baseNetwork->lkt)->resetAG();
  }
  //Hidden Layers                      
  for (int i = 0; i < baseNetwork->size; i=i+2) {
    if (name == OVN_AG || name == OVN_NCE_AG
	|| name == CE_AG || name == CE_NCE_AG
        || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	|| name == CE_CWE_NCE_AG || name == CWE_NCE_AG
        || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG){
      static_cast<Linear_AG*>(this->baseNetwork->modules[i])->resetAG();
    }
  }
  //Output                                                                                                                                                                                  
  if (name == OVN_AG || name == CE_AG){
    for (int i = 0; i < outputNetworkNumber; i++) {
      static_cast<LinearSoftmax_AG*>(this->outputNetwork[i])->resetAG();
    }
  } else if (name == OVN_NCE_AG || name == CE_NCE_AG || name == CWE_NCE_AG){
    static_cast<LinearNCE_AG*>(static_cast<NgramModelNCE*>(this)->outputLayer)->resetAG();
  } else if (name == CE_H_NCE_AG) {
    static_cast<HybridNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer)->resetAG();
  } else if (name == CE_CE_NCE_AG || name == CE_CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    static_cast<CharNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer)->resetAG();
  }
}


void NeuralModel::setAGtype(string type) {
  //Embeddings
  if (name == OVN_AG || name == OVN_NCE_AG || name == LKT_CWE_NCE_AG){
    static_cast<LookupTable_AG*>(this->baseNetwork->lkt)->initAG(type);
  } else if (name == CE_AG || name == CE_NCE_AG
             || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	     || name == CE_CWE_NCE_AG) {
    static_cast<CharEmbeddings_AG*>(this->baseNetwork->lkt)->initAG(type);
  } else if (name == CWE_NCE_AG || name == CWE_CWE_NCE_AG){
    static_cast<CharWordEmbeddings_AG*>(this->baseNetwork->lkt)->initAG(type);
  }
  //Hidden Layers
  for (int i = 0; i < baseNetwork->size; i=i+2) {
    if (name == OVN_AG || name == OVN_NCE_AG
	|| name == CE_AG || name == CE_NCE_AG
        || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	|| name == CE_CWE_NCE_AG || name == CWE_NCE_AG
        || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG){
      static_cast<Linear_AG*>(this->baseNetwork->modules[i])->initAG(type);
    }
  }
  //Output
  if (name == OVN_AG || name == CE_AG){
    for (int i = 0; i < outputNetworkNumber; i++) {
      static_cast<LinearSoftmax_AG*>(this->outputNetwork[i])->initAG(type);
    }
  } else if (name == OVN_NCE_AG || name == CE_NCE_AG || name == CWE_NCE_AG){
    static_cast<LinearNCE_AG*>(static_cast<NgramModelNCE*>(this)->outputLayer)->initAG(type);
  }  else if (name == CE_H_NCE_AG) {
    static_cast<HybridNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer)->initAG(type);
  } else if (name == CE_CE_NCE_AG || name == CE_CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    static_cast<CharNCE_AG*>(static_cast<NgramCharModelNCE*>(this)->outputLayer)->initAG(type);
  }
}


void NeuralModel::setWeightDecay(float weightDecay) {
  // For LBL, weight of lkt and outputNetwork[0] are tied,
  // so we set weightDecay only for outputNetwork[0], unless
  // weights are updated twice with weightDecay
  if ( name == OVN || name == OVN_NCE 
       || name == OVN_AG || name == OVN_NCE_AG
       || name == LKT_CE_NCE || name == LKT_CWE_NCE
       || name == LKT_CWE_NCE_AG) {
    baseNetwork->lkt->weightDecay = weightDecay;
  }
  else if (name == CE || name == CE_NCE || name == CE_CE_NCE || name == CE_H_NCE
	   || name == CE_AG || name == CE_NCE_AG || name == CE_H_NCE_AG 
	   || name == CE_CE_NCE_AG || name == CE_CWE_NCE_AG) { 
    CharEmbeddings* clkt = static_cast<CharEmbeddings*>(baseNetwork->lkt);
    clkt->cLkt->weightDecay = weightDecay;
    clkt->convModule->weightDecay = weightDecay;
  }
  else if (name == CWE || name == CWE_NCE || name == CWE_L
	   || name == CWE_NCE_L || name == CWE_CE_NCE || name == CWE_CWE_NCE 
	   || name == CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    CharWordEmbeddings* cwlkt = static_cast<CharWordEmbeddings*>(baseNetwork->lkt);
    cwlkt->cLkt->weightDecay = weightDecay;
    cwlkt->convModule->weightDecay = weightDecay;
    if (name != CWE_L && name != CWE_NCE_L) {
      cwlkt->Lkt->weightDecay = weightDecay;
    }
  }
  for (int i = 0; i < baseNetwork->size; i++) {
    baseNetwork->modules[i]->weightDecay = weightDecay;
  }
  if (name == OVN || name == OVN_AG
      || name == CE || name == CE_AG
      || name == CWE || name == CWE_L) {
    for (int i = 0; i < outputNetworkNumber; i++) {
      outputNetwork[i]->weightDecay = weightDecay;
    }
  } else if (name == OVN_NCE || name == OVN_NCE_AG || name == CE_NCE
	     || name == CWE_NCE || name == CWE_NCE_L || name == CE_CE_NCE
	     || name == CWE_CE_NCE || name == LKT_CE_NCE || name == LKT_CWE_NCE
	     || name == CWE_CE_NCE || name == CWE_CWE_NCE || name == CE_H_NCE
	     || name == CE_NCE_AG || name == CWE_NCE_AG || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	     || name == CE_CWE_NCE_AG || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    static_cast<NgramModelNCE*>(this)->outputLayer->weightDecay = weightDecay;
  } else if (name == WTOVN_NCE || name == WTOVN_NCE_DISCRIM) {
    static_cast<NgramWordTranslationModelNCE*>(this)->outputLayer->weightDecay = weightDecay;
  }
}

void NeuralModel::changeBlockSize(int blockSize) {
  DEBUG(cerr << "Default function, does nothing" << endl;)
    }

void NeuralModel::trainOne(intTensor& context, intTensor& word,
			   floatTensor& coefTensor, float learningRate, int update_lkt,
			   string update_hiddens, int update_output_layer) {
  trainOne(context, word, coefTensor, learningRate, blockSize, update_lkt,
	   update_hiddens, update_output_layer);
}

void NeuralModel::trainOne(intTensor& context, intTensor& word,
			   floatTensor& coefTensor, float learningRate, int subBlockSize,
			   int update_lkt, string update_hiddens, int update_output_layer) {
  time_t start, end;
  intTensor localWord;
  intTensor idLocalWord(1, 1);
  int idParent;
  int i;
  // Copy codeWord of predicted words into localCodeWord
  decodeWord(word, subBlockSize);

  // firstTime is required only for recurrent models, see RRLinear
  firstTime(context);

  // Forward from lkt to the last hidden layer
  baseNetwork->forward(context);

  // Initialize the gradient for the last hidden layer
  gradContextFeature = 0;
  int rBlockSize;
  // Forward for the main softmax layer outputNetwork[0]
  // localWord is the indices of top classes of prediced words,
  // the second line of localCodeWord
  localWord.select(localCodeWord, 1, 1);
  outputNetwork[0]->forward(contextFeature);

  // gradInput is the gradient from the main softmax layer
  gradInput = outputNetwork[0]->backward(localWord, coefTensor);

  // gradContextFeature = gradInput
  gradContextFeature.axpy(gradInput, 1);

  // For each predicted word, forward, backward and update other softmax layers
  intTensor oneLocalCodeWord;
  floatTensor oneLocalCoef(1, 1);
  for (rBlockSize = 0; rBlockSize < subBlockSize; rBlockSize++) {
    // Select the corresponding contextFeature for each example
    selectContextFeature.select(contextFeature, 1, rBlockSize);
    selectGradContextFeature.select(gradContextFeature, 1, rBlockSize);
    oneLocalCodeWord.select(localCodeWord, 0, rBlockSize);
    oneLocalCoef(0) = coefTensor(rBlockSize);
    for (i = 2; i < maxCodeWordLength; i += 2) {
      if (oneLocalCodeWord(i) == SIGN_NOT_WORD) {
	break;
      }
      idLocalWord = oneLocalCodeWord(i + 1);
      idParent = oneLocalCodeWord(i);
      outputNetwork[idParent]->forward(selectContextFeature);
      gradInput = outputNetwork[idParent]->backward(idLocalWord,
						    oneLocalCoef);
      if (update_output_layer == 1) {
	outputNetwork[idParent]->updateParameters(learningRate);
      }
      selectGradContextFeature.axpy(gradInput, 1);
    }
  }
  // Now gradContextFeature = sum of gradients of outputNetworks
  baseNetwork->backward(gradContextFeature);
  if (update_output_layer == 1) {
    outputNetwork[0]->updateParameters(learningRate);
  }
  baseNetwork->updateParameters(learningRate, update_lkt, update_hiddens);
}

float NeuralModel::computeOneF(intTensor& context, intTensor& word,
			       floatTensor& coefTensor) {
  time_t start, end;
  float f = 0;
  intTensor localWord;
  intTensor idLocalWord(1, 1);
  int idParent;
  int i;
  // Copy codeWord of predicted words into localCodeWord
  decodeWord(word, blockSize);

  // firstTime is required only for recurrent models, see RRLinear
  firstTime(context);

  // Forward from lkt to the last hidden layer
  baseNetwork->forward(context);

  int rBlockSize;
  // Forward for the main softmax layer outputNetwork[0]
  // localWord is the indices of top classes of prediced words,
  // the second line of localCodeWord
  localWord.select(localCodeWord, 1, 1);
  outputNetwork[0]->forward(contextFeature);

  f += outputNetwork[0]->computeF(localWord, coefTensor);

  // For each predicted word, forward, backward and update other softmax layers
  intTensor oneLocalCodeWord;
  floatTensor oneLocalCoef(1, 1);
  for (rBlockSize = 0; rBlockSize < blockSize; rBlockSize++) {
    // Select the corresponding contextFeature for each example
    selectContextFeature.select(contextFeature, 1, rBlockSize);
    selectGradContextFeature.select(gradContextFeature, 1, rBlockSize);
    oneLocalCodeWord.select(localCodeWord, 0, rBlockSize);
    oneLocalCoef(0) = coefTensor(rBlockSize);
    for (i = 2; i < maxCodeWordLength; i += 2) {
      if (oneLocalCodeWord(i) == SIGN_NOT_WORD) {
	break;
      }
      idLocalWord = oneLocalCodeWord(i + 1);
      idParent = oneLocalCodeWord(i);
      outputNetwork[idParent]->forward(selectContextFeature);
      f += outputNetwork[idParent]->computeF(idLocalWord, oneLocalCoef);
    }
  }
  return f;
}

float NeuralModel::computeF(char* dataFileName) {
  DEBUG(cerr << "Default function, return 0" << endl;)
    return 0;
}

int NeuralModel::trainTest(int maxExampleNumber, float weightDecay,
			   string learningRateType, float learningRate, float learningRateDecay,
			   intTensor& gcontext, intTensor& gword, floatTensor& coefTensor,
			   int update_lkt, string update_hiddens, int update_output_layer) {
  firstTime();
  intTensor context;
  intTensor word;
  context.resize(gcontext);
  context.copy(gcontext);
  word.resize(gword);
  word.copy(gword);
  float currentLearningRate;
  int nstep;
  nstep = 0;
  int currentExampleNumber = 0;
  int subBlockSize = blockSize;
  int percent = 1;
  float aPercent = maxExampleNumber * CONSTPRINT;
  float iPercent = aPercent * percent;

  int blockNumber = maxExampleNumber / blockSize;
  int remainingNumber = maxExampleNumber - blockSize * blockNumber;
  int i;
  setWeightDecay(weightDecay);
  for (i = 0; i < blockNumber; i++) {
    //Read one line and then train
    currentExampleNumber += blockSize;
    currentLearningRate = this->takeCurrentLearningRate(learningRate,
							learningRateType, nstep, learningRateDecay);
    trainOne(context, word, coefTensor, currentLearningRate, subBlockSize,
	     update_lkt, update_hiddens, update_output_layer);
    nstep += subBlockSize;
    if (currentExampleNumber > iPercent) {
      percent++;
      iPercent = aPercent * percent;
      cout << (float) currentExampleNumber / maxExampleNumber << " ... "
	   << flush;
    }
  }
  if (remainingNumber != 0) {
    currentLearningRate = this->takeCurrentLearningRate(learningRate,
							learningRateType, nstep, learningRateDecay);
    trainOne(context, word, coefTensor, currentLearningRate,
	     remainingNumber, update_lkt, update_hiddens,
	     update_output_layer);
  }
  cout << endl;
  return 1;
}

floatTensor&
NeuralModel::forwardOne(intTensor& context, intTensor& word) {
  int localWord;
  int idParent;
  int i;
  float localProb;

  decodeWord(word);
  firstTime(context);
  baseNetwork->forward(context);
  int idWord;
  mainProb = outputNetwork[0]->forward(contextFeature);
  intTensor oneLocalCodeWord;
  for (idWord = 0; idWord < blockSize; idWord++) {
    oneLocalCodeWord.select(localCodeWord, 0, idWord);
    localWord = oneLocalCodeWord(1);
    localProb = mainProb(localWord, idWord);
    for (i = 2; i < maxCodeWordLength; i += 2) {
      if (oneLocalCodeWord(i) == SIGN_NOT_WORD) {
	break;
      }
      localWord = oneLocalCodeWord(i + 1);
      idParent = oneLocalCodeWord(i);
      selectContextFeature.select(contextFeature, 1, idWord);
      outputNetwork[idParent]->forward(selectContextFeature);
      localProb *= outputNetwork[idParent]->output(localWord);
    }
    probabilityOne(idWord) = localProb;
  }
  return probabilityOne;
}
floatTensor&
NeuralModel::computeProbability(DataSet* dataset, char* textFileName,
				string textType) {
  cout << "[Info] Read data" << endl;
  ioFile validIof;
  if (textType == "l") {
    validIof.format = TEXT;
    validIof.takeReadFile(textFileName);
    dataset->readTextNgram(&validIof);
  } else if (textType == "n") {
    validIof.format = TEXT;
    validIof.takeReadFile(textFileName);
    dataset->readText(&validIof);
  } else if (textType == "id") {
    validIof.format = BINARY;
    validIof.takeReadFile(textFileName);
    dataset->readCoBiNgram(&validIof);
  }
  if (dataset->ngramNumber > BLOCK_NGRAM_NUMBER) {
    cerr << "ERROR: Not enough memory" << endl;
    exit(1);
  }

  dataset->createTensor();
  cout << "[Info] Finish read " << dataset->ngramNumber << " ngrams" << endl;
  cout << "[Info] Compute" << endl;
  forwardProbability(dataset->dataTensor, dataset->probTensor);
  return dataset->probTensor;
}

floatTensor&
NeuralModel::computeProbability() {
  forwardProbability(dataSet->dataTensor, dataSet->probTensor);
  return dataSet->probTensor;

}

float NeuralModel::computePerplexity(DataSet* dataset, char* textFileName,
				     string textType) {
  computeProbability(dataset, textFileName, textType);
  dataset->computePerplexity();
  return dataset->perplexity;

}

float NeuralModel::computePerplexity() {
  computeProbability();
  dataSet->computePerplexity();
  return dataSet->perplexity;
}

int NeuralModel::forwardProbability(intTensor& ngramTensor,
				    floatTensor& probTensor, int normalize) {
  cerr << "Function : " << __PRETTY_FUNCTION__ << endl;
  cerr << "Default function, does not do anything" << endl;
  return 1;
}

// base method, does nothing
void NeuralModel::showStateAG() {
  cerr << "NeuralModel::showStateAG default function, does nothing" << endl;
}

int NeuralModel::sequenceTrain(char* prefixModel, int gz, char* prefixData,
			       int maxExampleNumber, char* trainingFileName, char* validationFileName,
			       string validType, string learningRateType, int minIteration,
			       int maxIteration, int update_lkt, string update_hiddens,
			       int update_output_layer, int write_model, char* hmf, float e, float cache) {

  float learningRateForRd;
  float learningRateForParas;
  float learningRateDecay;
  float weightDecay;
  int computeDevPer = 1;
  float perplexity = 0;
  float nce_score = 0;
  float preNce_score;
  //With noUNK
  //float noUnkPerplexity = 0;
  float prePerplexity;
  float hold_prePerplexity;
  //With noUNK
  //float noUnkPrePerplexity;
  char dataFileName[STRING_SIZE];
  char validDataFileName[STRING_SIZE];
  char outputModelFileName[STRING_SIZE];
  char convertStr[STRING_SIZE];
  char convertStrPre[STRING_SIZE];
  char modelFileNamePre[STRING_SIZE];
  ioFile iofC;
  int dataC;
  int modelC;
  computeDevPer = iofC.check(validationFileName, 0);
  time_t start, end;
  int iteration;
  int divide = 0;
  ioFile parasIof;
  floatTensor parasTensor(6, 1);
  parasTensor = 0;
  parasIof.format = TEXT;
  ioFile modelIof;
  int stop = 0;

  // Read parameters (learningRate, weightDecay, blockSize...) in *.par
  sprintf(convertStr, "%d", minIteration - 1);
  strcpy(outputModelFileName, prefixModel);
  strcat(outputModelFileName, convertStr);
  strcat(outputModelFileName, ".par");
  int paraC = iofC.check(outputModelFileName, 1);
  if (!paraC) {
    return 0;
  }
  parasIof.takeReadFile(outputModelFileName);
  parasTensor.read(&parasIof);
  learningRateForRd = parasTensor(0);
  learningRateForParas = parasTensor(1);
  learningRateDecay = parasTensor(2);
  weightDecay = parasTensor(3);
  changeBlockSize((int) parasTensor(4));
  cout << "[Info] Read lrforRd " << parasTensor(0) << endl;
  cout << "[Info] Read lrForParas " <<  parasTensor(1) << endl;
  cout << "[Info] Read RateDecay " <<  parasTensor(2) << endl;
  cout << "[Info] Read WeightDecay " <<  parasTensor(3) << endl;
  cout << "[Info] Read blockSize " << (int) parasTensor(4) << endl;
  // if down or down-bloc-adagrad, take the initial value of divide from the parameter file
  if (learningRateType == LEARNINGRATE_DOWN
      || learningRateType == LEARNINGRATE_DBAG) {
    divide = (int) parasTensor(5);
  }
  // Now iteration is the number of first new model
  // show learning rate and its type
  show_current_learning_rates
    
    setWeightDecay(weightDecay);
  if (name == OVN_AG || name == OVN_NCE_AG
      || name == CE_AG || name == CE_NCE_AG
      || name == CE_CE_NCE_AG || name == CE_H_NCE_AG
      || name == CE_CWE_NCE_AG || name == CWE_NCE_AG
      || name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
    setAGparams(e, cache, 0);
    setAGtype(learningRateType);    
  }  
  // Compute perplexity of dev data, for early stopping
  if (computeDevPer) {
    time(&start);
    cout << "Compute validation perplexity:" << endl;
    
    // first compute of perplexity in charging all the validation file
    computePerplexity(this->dataSet, validationFileName, validType);
    //With noUNK
    //computePerplexity(this->noUnkDataSet, validationFileName, validType);
    // prePerplexity is the perplexity of the precedent iteration: after each iteration, we comparer the actual perplexity with the precedent one and see if there is any degradation in the system.
    prePerplexity = this->dataSet->perplexity;
    //With noUNK
    //noUnkPrePerplexity = this->noUnkDataSet->perplexity;
    time(&end);
    
    cout << "With epoch " << minIteration - 1 << ", perplexity of "
	 << validationFileName << " is " << dataSet->perplexity << " ("
	 << dataSet->ngramNumber << " ngrams)" << endl;
    //With noUNK
    /*cout << "Perplexity of "
         << validationFileName << "with no unknown words is " << noUnkDataSet->perplexity << " ("
         << noUnkDataSet->ngramNumber << " ngrams)" << endl;*/
    cout << "Finish after " << difftime(end, start) << " seconds" << endl;
  }
  
  // the name of the file containing perplexities on validation set
  char outputPerplexity[STRING_SIZE];
  strcpy(outputPerplexity, prefixModel);
  strcat(outputPerplexity, "out.per");

  // the file containing perplexities on validation set
  ofstream outputPerp;
  outputPerp.open(outputPerplexity, ios_base::app);

  // name of the file containing execution time
  char outputTimeExeFileName[STRING_SIZE];
  strcpy(outputTimeExeFileName, prefixModel);
  strcat(outputTimeExeFileName, "out.Time");

  ofstream outputTimeFile;
  outputTimeFile.open(outputTimeExeFileName, ios_base::app);

  // execution time
  float timeExe = 0;

  // Open the validation data if we use NCE to monitor values
  if ((name == OVN_NCE || name == OVN_NCE_AG 
       || name == CE_NCE || name == CE_NCE_AG 
       || name == CWE_NCE || name == CWE_NCE_AG
       || name == LKT_CE_NCE
       || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
       || name == CE_CE_NCE || name == CE_CE_NCE_AG 
       || name == CE_H_NCE || name == CE_H_NCE_AG 
       || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
       || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
      && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {
    strcpy(validDataFileName, prefixData);
    strcat(validDataFileName, "valid.1");  
    dataC = iofC.check(validDataFileName, 0);
    if (!dataC) {
      strcat(dataFileName, ".gz");
      dataC = iofC.check(dataFileName, 0);
      if (!dataC) {
	cout << "Valid data file for NCE monitoring does not exist"
	     << endl;
	return 0;
      }
    }
  }

  if ((name == OVN_NCE || name == OVN_NCE_AG
       || name == CE_NCE || name == CE_NCE_AG
       || name == CWE_NCE || name == CWE_NCE_AG
       || name == LKT_CE_NCE
       || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
       || name == CE_CE_NCE || name == CE_CE_NCE_AG
       || name == CE_H_NCE || name == CE_H_NCE_AG
       || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
       || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
      && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {    
    NgramModelNCE* modelNCE = static_cast<NgramModelNCE*>(this);
    nce_score = modelNCE->monitorNCE(validDataFileName, prefixModel, 0);
    //nce_score = computeF(validDataFileName);
    if (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG) {
      preNce_score = nce_score;
      cout << "With epoch " << iteration << ", NCE score of"
	   << validationFileName << " is " << nce_score
	   << " (" << dataSet->ngramNumber << " ngrams)" << endl;
    }
  }
  
  // Now, train a model
  for (iteration = minIteration; iteration < maxIteration + 1; iteration++) {
    cout << "Iteration: " << iteration << endl;
    // show the state of AdaGrad algorithm
    if (name == OVN_AG || name == OVN_NCE_AG
	|| name == CE_AG || name == CE_NCE_AG
	|| name == CE_CE_NCE_AG || name == CE_H_NCE_AG
	|| name == CE_CWE_NCE_AG || name == CWE_NCE_AG
	|| name == LKT_CWE_NCE_AG || name == CWE_CWE_NCE_AG) {
      DEBUG(this->showStateAG();)
	if (iteration < 20) {
	  //resetAGparams();
	  setAGparams(e, cache, 0);
	} else {
	  setAGparams(e, cache, 0);
	}
    }
    sprintf(convertStr, "%d", iteration);
    strcpy(dataFileName, prefixData);
    strcat(dataFileName, convertStr);
    dataC = iofC.check(dataFileName, 0);
    if (!dataC) {
      strcat(dataFileName, ".gz");
      dataC = iofC.check(dataFileName, 0);
      if (!dataC) {
	cout << "Train data file " << convertStr << " does not exist"
	     << endl;
	return 0;
      }
    }
    strcpy(outputModelFileName, prefixModel);
    strcat(outputModelFileName, convertStr);
    if (gz) {
      strcat(outputModelFileName, ".gz");
    }
    modelC = iofC.check(outputModelFileName, 0);
    if (modelC) {
      cerr << "WARNING: Train model file " << convertStr << " exists"
	   << endl;
      return 0;
    }
    time(&start);
    // show learning rate and its type
    show_current_learning_rates
      int outTrain;
    outTrain = train(dataFileName, maxExampleNumber, iteration,
		     learningRateType, learningRateForParas, learningRateDecay,
		     update_lkt, update_hiddens, update_output_layer);
    if (outTrain == 0) {
      cerr << "ERROR: Can't finish training" << endl;
      exit(1);
    }
    time(&end);
    cout << "Finish after " << difftime(end, start) / 60 << " minutes"
	 << endl;
    // accumulate
    timeExe += difftime(end, start);
    int upDivide = 0;
    // monitor NCE values
    if ((name == OVN_NCE || name == OVN_NCE_AG
       || name == CE_NCE || name == CE_NCE_AG
       || name == CWE_NCE || name == CWE_NCE_AG
       || name == LKT_CE_NCE
       || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
       || name == CE_CE_NCE || name == CE_CE_NCE_AG
       || name == CE_H_NCE || name == CE_H_NCE_AG
       || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
	 || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
	&& (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {     
      preNce_score = nce_score;    
      NgramModelNCE* modelNCE = static_cast<NgramModelNCE*>(this);  
      nce_score = modelNCE->monitorNCE(validDataFileName, prefixModel, 0);
      //nce_score = computeF(validDataFileName);
    }
    if (computeDevPer) {
      // calculate execution time
      time(&start);
      
      cout << "Compute validation perplexity:" << endl;
      forwardProbability(dataSet->dataTensor, dataSet->probTensor);
      //With noUNK
      //forwardProbability(noUnkDataSet->dataTensor, noUnkDataSet->probTensor);
      // the perplexity of the precedent iteration
      prePerplexity = dataSet->perplexity;
      //With noUNK                                                                                                                       
      //noUnkPrePerplexity = noUnkDataSet->perplexity;

      perplexity = dataSet->computePerplexity();
      //With noUNK
      //noUnkPerplexity = noUnkDataSet->computePerplexity();
      //computePerplexity(this->dataSet, validationFileName, validType);
      perplexity = dataSet->perplexity;

      cout << "With epoch " << iteration << ", perplexity of "
	   << validationFileName << " is " << dataSet->perplexity
	   << " (" << dataSet->ngramNumber << " ngrams)" << endl;
      if ((name == OVN_NCE || name == OVN_NCE_AG
	   || name == CE_NCE || name == CE_NCE_AG
	   || name == CWE_NCE || name == CWE_NCE_AG
	   || name == LKT_CE_NCE
	   || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
	   || name == CE_CE_NCE || name == CE_CE_NCE_AG
	   || name == CE_H_NCE || name == CE_H_NCE_AG
	   || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
	   || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
	  && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {
	cout << "With epoch " << iteration << ", NCE score of"
	     << validationFileName << " is " << nce_score
	     << " (" << dataSet->ngramNumber << " ngrams)" << endl;
      }
      //With noUNK
      /*cout << "Perplexity of "
         << validationFileName << "with no unknown words is " << noUnkDataSet->perplexity << " ("
         << noUnkDataSet->ngramNumber << " ngrams)" << endl;*/
      time(&end);
      
      cout << "Finish after " << difftime(end, start) / 60 << " minutes"
	   << endl;
      
      if ((name == OVN_NCE || name == OVN_NCE_AG
	   || name == CE_NCE || name == CE_NCE_AG
	   || name == CWE_NCE || name == CWE_NCE_AG
	   || name == LKT_CE_NCE
	   || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
	   || name == CE_CE_NCE || name == CE_CE_NCE_AG
	   || name == CE_H_NCE || name == CE_H_NCE_AG
	   || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
	   || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
	  && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {
	hold_prePerplexity = prePerplexity;
	prePerplexity = preNce_score;
	perplexity = nce_score;
      }
      
      if (isnan(perplexity) || perplexity > prePerplexity) {
	if (isnan(perplexity)) {
	  if ((name == OVN_NCE || name == OVN_NCE_AG
	       || name == CE_NCE || name == CE_NCE_AG
	       || name == CWE_NCE || name == CWE_NCE_AG
	       || name == LKT_CE_NCE
	       || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
	       || name == CE_CE_NCE || name == CE_CE_NCE_AG
	       || name == CE_H_NCE || name == CE_H_NCE_AG
	       || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
	       || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
	      && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {
	    cout << "NCE Score is NaN" << endl;
	  } else {
	    cout << "Perplexity is NaN" << endl;
	  }
	} else {
	  if ((name == OVN_NCE || name == OVN_NCE_AG
	       || name == CE_NCE || name == CE_NCE_AG
	       || name == CWE_NCE || name == CWE_NCE_AG
	       || name == LKT_CE_NCE
	       || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
	       || name == CE_CE_NCE || name == CE_CE_NCE_AG
	       || name == CE_H_NCE || name == CE_H_NCE_AG
	       || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
	       || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
	      && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {	    
            cout << "WARNING: NCE Score increases" << endl;
          } else {
	    // perplexity increases
	    cout << "WARNING: Perplexity increases" << endl;
	  }
	}
	// upDivide is for Down and DBAG, takes effect later
	upDivide = 1;
	// if UNDO = 1, we undo this iteration
	if (learningRateType != LEARNINGRATE_NCE) {
	  //&& ((name != OVN_AG && name != OVN_NCE_AG) || isnan(perplexity))) {
	  if (UNDO == 1) {
	    cout << "Back to the precedent model" << endl;
	    sprintf(convertStrPre, "%d", iteration - 1);
	    strcpy(modelFileNamePre, prefixModel);
	    strcat(modelFileNamePre, convertStrPre);
	    modelC = iofC.check(modelFileNamePre, 0);
	    if (!modelC) {
	      cerr << "WARNING: Train model file " << modelFileNamePre
		   << " does not exists" << endl;
	      return 0;
	    }
	    modelIof.takeReadFile(modelFileNamePre);
	    if (name == CE || name == CWE || name == CWE_L 
		|| name == CE_NCE || name == CWE_NCE || name == CWE_NCE_L 
		|| name == CE_CE_NCE || name == CWE_CE_NCE
		|| name == LKT_CE_NCE || name == LKT_CWE_NCE
		|| name == CE_CWE_NCE || name == CWE_CWE_NCE 
		|| name == CE_H || name == CE_H_NCE
		|| name == CE_AG || name == CE_NCE_AG
		|| name == CWE_NCE_AG || name == CE_H_NCE_AG
		|| name == CE_CE_NCE_AG || name == LKT_CWE_NCE_AG
		|| name == CE_CWE_NCE_AG|| name == CWE_CWE_NCE_AG) {
	      read(&modelIof, 0, (int) parasTensor(4), hmf);
	    } else {
	      read(&modelIof, 0, (int) parasTensor(4));
	    }
	    // return perplexity to the precedent value
	    perplexity = prePerplexity;
	    dataSet->perplexity = prePerplexity;
	    if ((name == OVN_NCE || name == OVN_NCE_AG)
		&& (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {
	      nce_score = preNce_score;
	      perplexity = hold_prePerplexity;
	      dataSet->perplexity = hold_prePerplexity;
	    }
	  } else {
	    cout << "We do not back to the precedent model" << endl;
	  }
	} else {
	  if ((name == OVN_NCE || name == OVN_NCE_AG
               || name == CE_NCE || name == CE_NCE_AG
               || name == CWE_NCE || name == CWE_NCE_AG
               || name == LKT_CE_NCE
               || name == LKT_CWE_NCE || name == LKT_CWE_NCE_AG
               || name == CE_CE_NCE || name == CE_CE_NCE_AG
               || name == CE_H_NCE || name == CE_H_NCE_AG
               || name == CE_CWE_NCE || name == CE_CWE_NCE_AG
               || name == CWE_CWE_NCE || name == CWE_CWE_NCE_AG)
              && (learningRateType == LEARNINGRATE_NCE || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE_BAG)) {
	    nce_score = preNce_score;
	    perplexity = hold_prePerplexity;
	    dataSet->perplexity = hold_prePerplexity;
	  }
	}
	if (learningRateType == LEARNINGRATE_ADJUST || learningRateType == LEARNINGRATE_NCE_ADJUST || (learningRateType == LEARNINGRATE_NCE && isinf(perplexity))) {
	  // firstly, divide the learning rate by learningRateDecay
	  learningRateForParas = learningRateForParas
	    / learningRateDecay;
	}
      } else if (log(perplexity) * MUL_LOGLKLHOOD > log(prePerplexity)) {
	// perplexity does not increase, but descend not enough
	if (learningRateType == LEARNINGRATE_ADJUST || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE) {
	  // divide the learning rate by learningRateDecay to increase convergence speed
	  learningRateForParas = learningRateForParas
	    / learningRateDecay;
	}
	if (learningRateType == LEARNINGRATE_DOWN
	    || learningRateType == LEARNINGRATE_DBAG) {
	  // in the case of Down and DBAG, we see if the perplexity go down enough
	  upDivide = 1;
	}
      } else {
	if (learningRateType == LEARNINGRATE_ADJUST || learningRateType == LEARNINGRATE_NCE_ADJUST || learningRateType == LEARNINGRATE_NCE) {
	  // we multiply the learning rate with ACC_RATE = 1.1 to increase convergence speed
	  learningRateForParas = learningRateForParas * ACC_RATE;
	}
      }
      // write validation perplexity on file
      outputPerp << iteration << " " << dataSet->perplexity << endl;
    }

    if (write_model == 1 && strcmp(prefixModel, "xxx")) {
      modelIof.takeWriteFile(outputModelFileName);
      write(&modelIof, 1);
    }

    // if divide > 0, we continue to increase it
    if (divide == 0 && upDivide == 1) {
      divide = 1;
    } else if (divide >= 1 && (learningRateType == LEARNINGRATE_DOWN
			       || learningRateType == LEARNINGRATE_DBAG)) {
      divide++;
      learningRateForParas = learningRateForParas
	/ learningRateDecay;
    }
    strcat(outputModelFileName, ".par");
    parasTensor(0) = learningRateForRd;
    parasTensor(1) = learningRateForParas;
    parasTensor(2) = learningRateDecay;
    parasTensor(3) = weightDecay;
    parasTensor(4) = blockSize;
    if (learningRateType == LEARNINGRATE_DOWN
	|| learningRateType == LEARNINGRATE_DBAG) {
      parasTensor(5) = divide;
    }
    if (strcmp(prefixModel, "xxx")) {
      parasIof.takeWriteFile(outputModelFileName);
      parasTensor.write(&parasIof);
      parasIof.freeWriteFile();
    }

    if (divide >= MAX_DIVIDE && name != OVNB) {
      stop = 1;
    }
    if (stop == 1) {
      break;
    }
    outputTimeFile << iteration << " " << timeExe << endl;
  }
  outputTimeFile.close();

  outputPerp.close();

  if (!strcmp(prefixModel, "xxx") && !isnan(perplexity)
      && (minIteration != maxIteration)) {
    modelIof.takeWriteFile(outputModelFileName);
    write(&modelIof, 1);
  }
  return 1;
}

readStrip(Int, int)
readStrip(Float, float)

float NeuralModel::takeCurrentLearningRate(float learningRate,
					   string learningRateType, int nstep, float learningRateDecay) {
  //	DEBUG(
  //			cerr << "default function, adapted only to one-learning rate regimes" << endl;)
  float currentLearningRate = 0;
  if (learningRateType == LEARNINGRATE_NORMAL) {
    currentLearningRate = learningRate / (1 + nstep * learningRateDecay);
  } else if (learningRateType == LEARNINGRATE_DOWN) {
    currentLearningRate = learningRate;
  } else if (learningRateType == LEARNINGRATE_ADJUST) {
    currentLearningRate = learningRate;
  } else if (learningRateType == LEARNINGRATE_NCE) {
    currentLearningRate = learningRate;
  } else if (learningRateType == LEARNINGRATE_NCE_ADJUST) {
    currentLearningRate = learningRate;
  } else {

    DEBUG(cout << "learningRateType not compatible" << endl;)
      exit(1);

  }
  return currentLearningRate;
}

void NeuralModel::writeOutput(char* prefix) {
  char outputModelFileName[260];
  char convert[10];
  ioFile iof;
  for (int i = 0; i < outputNetworkNumber; i++) {
    strcpy(outputModelFileName, prefix);
    strcat(outputModelFileName, "OutputWeight");
    sprintf(convert, "%d", i);
    strcat(outputModelFileName, convert);
    iof.takeWriteFile(outputModelFileName);
    outputNetwork[i]->weight.write(&iof);
    strcpy(outputModelFileName, prefix);
    strcat(outputModelFileName, "OutputBias");
    strcat(outputModelFileName, convert);
    iof.takeWriteFile(outputModelFileName);
    outputNetwork[i]->bias.write(&iof);
  }
}

float NeuralModel::distance2(NeuralModel* anotherModel) {
  DEBUG(cerr << "default function, return 0" << endl;)
    return 0;
}

void NeuralModel::read(ioFile* iof, int allocation, int blockSize, char* hmf) {
}
