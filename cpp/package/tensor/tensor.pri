SOURCES += \
    ../package/tensor/floatTensor.cc \
    ../package/tensor/intTensor.cc \
    ../package/tensor/outils.cc \
    ../package/tensor/Tensor.cc

HEADERS += \
    ../package/tensor/floatTensor.H \
    ../package/tensor/intTensor.H \
    ../package/tensor/mainTensor.H \
    ../package/tensor/outils.H \
    ../package/tensor/Tensor.H
