#!/usr/bin/env python
import math
import os, sys
import numpy
import gzip
if (len(sys.argv) != 6):
	print('vocInSrcFileName vocOutSrcFileName vocInTrgFileName vocOutTrgFileName prefixOutputFileName')
else:
	voc_in_src_fn = sys.argv[1]
	voc_out_src_fn = sys.argv[2]
	voc_in_trg_fn = sys.argv[3]
	voc_out_trg_fn = sys.argv[4]
	prefixOutputFileName = sys.argv[5]
	voc_in_src = open(voc_in_src_fn, "r").readlines()
	voc_out_src = open(voc_out_src_fn, "r").readlines()
	voc_in_trg = open(voc_in_trg_fn, "r").readlines()
	voc_out_trg = open(voc_out_trg_fn, "r").readlines()
	try:
		x = voc_in_src.index("NULL\n")
	except ValueError:
		voc_in_src.append("NULL\n")
	try:
		x = voc_out_src.index("NULL\n")
	except ValueError:
		voc_out_src.append("NULL\n")
	try:
		x = voc_in_trg.index("NULL\n")
	except ValueError:
		voc_in_trg.append("NULL\n")
	try:
		x = voc_out_trg.index("NULL\n")
	except ValueError:
		voc_out_trg.append("NULL\n")



	voc = []
	srcOutVoc = []
	trgOutVoc = voc_out_trg
	for i in range(len(voc_in_trg))	:
		voc.append(voc_in_trg[i])
	for i in range(len(voc_in_src))	:
		voc.append("src." + voc_in_src[i])
	for i in range(len(voc_out_src)) :
		if(voc_out_src[i] != "</s>\n" and voc_out_src[i] != "<s>\n" and voc_out_src[i] != "<UNK>\n"):
			srcOutVoc.append("src." + voc_out_src[i])
		else:
			srcOutVoc.append(voc_out_src[i])
	open(prefixOutputFileName + "inVoc", "w").write("".join(voc))
	open(prefixOutputFileName + "srcOutVoc", "w").write("".join(srcOutVoc))
	open(prefixOutputFileName + "trgOutVoc", "w").write("".join(trgOutVoc))
