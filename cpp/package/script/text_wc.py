#!/usr/bin/env python
import os
import sys
import glob

if(len(sys.argv) != 5):
	print('dataDirName suffix wcFileName dataTrainDesFileName')
else:
	dataDirName = os.path.abspath(sys.argv[1])
	suffix = sys.argv[2]
	wcFileName = sys.argv[3]
	dataTrainDesFileName = sys.argv[4]
	maxLen = 0
	fileList = []
	lsFile = os.popen('ls ' + dataDirName).read().split()
	for i in range(len(lsFile)):
		fileList.append(dataDirName + '/' + lsFile[i])
		if(maxLen < len(fileList[-1])):
			maxLen = len(fileList[-1])
	wcFile = open(wcFileName, 'w')
	dataTrainDesFile =  open(dataTrainDesFileName, 'w')
	for mfile in fileList:
		if(mfile[- 3 - len(suffix):] == suffix + '.gz'):
			print(mfile)
			wcFile.write(mfile + ' ' * (maxLen + 2 - len(mfile)))
			dataTrainDesFile.write(mfile + ' ' * (maxLen + 2 - len(mfile)))
			line = os.popen('zcat ' + mfile + ' | wc').readlines()[0]
			iLine = map(int, line.split())
			strW = ''
			for i in range(len(iLine)):
				strW = strW + '%12d ' % iLine[i]
			wcFile.write(strW + '\n')
			dataTrainDesFile.write('%12d\n' % iLine[0])
		elif(mfile[- len(suffix):] == suffix):
			print(mfile)
			wcFile.write(mfile + ' ' * (maxLen + 2 - len(mfile)))
			dataTrainDesFile.write(mfile + ' ' * (maxLen + 2 - len(mfile)))
			line = os.popen('cat ' + mfile + ' | wc').readlines()[0]
			iLine = map(int, line.split())
			strW = ''
			for i in range(len(iLine)):
				strW = strW + '%12d ' % iLine[i]
			wcFile.write(strW + '\n')
			dataTrainDesFile.write('%12d\n' % iLine[0])

	wcFile.close()
	dataTrainDesFile.close()


