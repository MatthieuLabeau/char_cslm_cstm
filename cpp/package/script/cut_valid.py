from __future__ import division
import sys
import os
import re
from itertools import groupby

def valid_cut(path_to_unk_file, path_to_data_file, path_to_out_file):
    unk=set()
    with open(path_to_unk_file, 'r') as unk_file:
        for line in unk_file:
            unk.add(line.strip())

    with open(path_to_data_file, 'r') as data_file:
        with open(path_to_out_file,'w') as out_file:
            for line in data_file:               
                l = line.strip().split()
                l_out = []
                for w in l:
                    if w in unk:
                        l_out.append('\n')
                    else:
                        l_out.append(w)
                l_out.append('\n')
                out_file.write((' '.join([x[0] for x in groupby(l_out)])).lstrip())
                    
                    
def main(argv):
    if (len(argv) != 3):
        print("unkVocPath dataPath outPath")
    valid_cut(argv[0], argv[1], argv[2])                      
                           
if __name__ == "__main__":
    main(sys.argv[1:])
