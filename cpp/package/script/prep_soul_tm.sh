#!/bin/bash
configFile=$1
echo "Using config file ${configFile}"
. $configFile

# from the vocabularies of LM models, create mixed vocabularies of future bilingual models
mkdir -p ${dir}/voc
voc_createWordTrans.py $vocSrc $vocSrc $vocTrg $vocTrg ${dir}/voc/voc.

# prepare data
mkdir -p ${dir}/data
if [ "${validation}" == "xxx" ]; then
	# if you donot have a validation set, so we will make one
	for (( id=0 ; id<${#list_data[@]} ; id ++ ))
	do
		total=`grep "EOS" -c ${list_data[id]}`
		remain=$(( $total - $validSize ))
		echo "total $total sentences, validation $validSize sentences, remaining $remain sentences..."
		random_tuples.py $total $validSize ${dir}/data/$id.idr.$validSize ${list_data[id]} ${dir}/data/$id.tuple.$validSize
		exclu.py $total ${dir}/data/$id.idr.$validSize ${dir}/data/$id.idr.$remain
		random_tuples.py $total $remain ${dir}/data/$id.idr.$remain ${list_data[id]} ${dir}/data/$id.tuple.$remain

		if (( $id == 0 )); then
			cat ${dir}/data/$id.tuple.$validSize > ${dir}/data/tuple.forValidation
		else
			cat ${dir}/data/$id.tuple.$validSize >> ${dir}/data/tuple.forValidation
		fi
		mv -v ${dir}/data/$id.tuple.$remain ${dir}/data/$id.tuple.forTraining
	done
else
	# if you have a validation set, we just copy entirely the training file
	for (( id=0 ; id<${#list_data[@]} ; id ++ ))
	do
		cp -v ${list_data[id]} ${dir}/data/$id.tuple.forTraining
	done
fi

for (( id=0 ; id<${#list_data[@]} ; id ++ ))
do
	tuple_countWord.py ${dir}/data/$id.tuple.forTraining > ${dir}/data/$id.tuple.forTraining.info
	nbLine=`head -n 1 ${dir}/data/$id.tuple.forTraining.info | awk 'NF>1{print $NF}'`
	echo "nbLine : $nbLine"
	nbNgram=`tail -n 1 ${dir}/data/$id.tuple.forTraining.info | awk 'NF>1{print $NF}'`
	echo "nbNgram : $nbNgram"
	if (( $id == 0 )); then
		echo "${dir}/data/$id.tuple.forTraining ${nbLine} ${nbNgram} 0 ${group_data[id]}" > ${dir}/data/wc.info
	else
		echo "${dir}/data/$id.tuple.forTraining ${nbLine} ${nbNgram} 0 ${group_data[id]}" >> ${dir}/data/wc.info
	fi
done

echo "creating dataTrainDes..."
text_resamplingRate.py ${dir}/data/wc.info 1 ${max_ngram} ${dir}/data/dataTrainDes
