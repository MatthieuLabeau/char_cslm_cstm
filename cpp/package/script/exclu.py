#!/usr/bin/env python
import sys

if len(sys.argv) != 4 :
    print('totalSize chosenSet remainingSet')
    exit(1)
    
# inputs
totalSize = int(sys.argv[1])
chosen_name = sys.argv[2]
remaining_name = sys.argv[3]

# open files
chosen_file = open(chosen_name, 'r')
remaining_file = open(remaining_name, 'w')

set = range(totalSize)
for line_chosen_file in chosen_file :
    set.remove(int(line_chosen_file))
    
for i in range(len(set)) :
    remaining_file.write(str(set[i]) + '\n')
    
# close files
chosen_file.close()
remaining_file.close()