#!/bin/bash

function prepare {
	echo "Preparing the training..."
	case "$1" in
		1) key=sl
			;;
		2) key=outSl
			;;
		3) key=all
			;;
	esac
	current_resamplingDir=${resamplingDir}/$key
	mkdir -p ${current_resamplingDir}
	if [ "$1" == "2" ]; then
		in_trainDir=${current_trainDir}
	elif [ "$1" == "3" ]; then
		out_trainDir=${current_trainDir}
	fi
	current_trainDir=${trainDir}/$key
	mkdir -p ${current_trainDir}
}

function makeParas0 {
	echo 1 > ${current_trainDir}/0.par
	echo 6 1 >> ${current_trainDir}/0.par
	echo ${lr} ${lr} ${lr_dc} ${w_dc} $blockSize $divide >> ${current_trainDir}/0.par
}

function createPrototype {
	echo "Creating initial model..."
	if [ "$1" == 1 ]; then
		destVoc=${dir}/voc/triVoc.${size_sl}
	fi
	createPrototype.exe $type ${dir}/voc/voc ${destVoc} $mapIUnk $mapOUnk $n $dimensionSize $nonLinearType $hiddenLayerSizeCode $codeWordFileName $outputNetworkSizeFileName ${current_trainDir}/0
}

function sequenceTrain {
	echo "Training..."
	sequenceTrain.exe ${current_trainDir}/ ${current_resamplingDir}/train. 0 xxx $validation $validType $learningRateType $first $last ${update_lkt} ${update_hiddens} ${update_output_layer}
}

function resamplingData {
	echo "Resampling data..."
	if [ "$1" == "1" ]; then
		destVoc=${dir}/voc/triVoc.${size_sl}
		dataTrainDes=${dir}/data/dataTrainDesSl0
		mapOUnk=0
	elif [ "$1" == "2" ]; then
		destVoc=${dir}/voc/outVoc.${size_sl}
		dataTrainDes=${dir}/data/dataTrainDesOsl0
		mapOUnk=0
	elif [ "$1" == "3" ]; then
		destVoc=${dir}/voc/voc
		dataTrainDes=${dir}/data/dataTrainDesAll0
		# with three special tokens
		mapOUnk=1
	fi
	resamplingData.exe $dataTrainDes ${dir}/voc/voc ${destVoc} $n $mapIUnk $mapOUnk ${current_resamplingDir}/train. $first $last $type
}

function removeUnnecessaryFiles {
	rm -v ${current_resamplingDir}/train.*                                                                                          
	for (( id_file=0 ; id_file<$last ; id_file ++ ))                                                                          
	do                                                                                                                             
        	rm -v ${current_trainDir}/${id_file}                                                                                    
	done
}

configFile=$1
echo "Using config file ${configFile}"
. $configFile

# directory containing training binary file for each iteration
resamplingDir=${dir}/resampling
mkdir -p ${resamplingDir}

# directory containing trained models
trainDir=${dir}/model
mkdir -p ${trainDir}

# first step, shortlist
prepare 1

mapShortList=${dir}/voc/triVoc.${size_sl}.mapVoc
mapIUnk=1
mapOUnk=0
codeWordFileName=xxx
outputNetworkSizeFileName=xxx
first=1
last=$shortlistLastEpoch

# create initial model
createPrototype 1
# make 0.par
makeParas0
# resampling data
resamplingData 1
# training
sequenceTrain
# remove unnecessary files                                                                                    
removeUnnecessaryFiles

# second step, build SOUL structures

# extract continuous word embeddings
pushAllParameter.exe ${current_trainDir}/$last ${current_trainDir}/$last"_" l

# reduce dimension of word embeddings
code_pcaFeature.py ${current_trainDir}/$last"_"LookupTable T $nPCA ${current_trainDir}/$last"_"LookupTable.pca$nPCA

# build SOUL structure on all vocabulary words
# the SOUL structure is contained in two files : *codeWord which identifies the branching of each word, and *outputNetworkSize which specifies the size of each softmax output layer
code_clustering.py ${current_trainDir}/$last"_"LookupTable.pca${nPCA} ${dir}/voc/voc ${dir}/voc/triVoc.${size_sl} $group $nKmeans ${current_trainDir}/$last"_"LookupTable.pca${nPCA}.$group.

# from the SOUL structure on all vocabulary words, build a sub-structure on out-of-shortlist vocabulary words for the third step : *out.codeWord and *.out.outputNetworkSize
code_createOutCode.py ${current_trainDir}/$last"_"LookupTable.pca${nPCA}.${group}.codeWord ${current_trainDir}/$last"_"LookupTable.pca${nPCA}.${group}.outputNetworkSize ${dir}/voc/voc $mapShortList ${current_trainDir}/$last"_"LookupTable.pca${nPCA}.$group.out.

# third step, train using out-of-shortlist vocabulary words in the softmax output layer
prepare 2

growPredictionSpace.exe ${in_trainDir}/$last ${dir}/voc/outVoc.${size_sl} $mapIUnk $mapOUnk ${in_trainDir}/$last"_"LookupTable.pca${nPCA}.$group.out.codeWord ${in_trainDir}/$last"_"LookupTable.pca${nPCA}.$group.out.outputNetworkSize ${current_trainDir}/0

first=1
last=$outLastEpoch

makeParas0
# resampling data for third step
resamplingData 2
# ok, train
sequenceTrain
# remove unnecessary files
removeUnnecessaryFiles

# fourth step, all vocabulary
prepare 3

mapIUnk=1
mapOUnk=1
first=1
last=$lastEpoch

# from the output models of the two precedent training (shortlist and out-of-shortlist), combine them to from an initial model
growOutPredictionSpace.exe ${in_trainDir}/$shortlistLastEpoch ${out_trainDir}/$outLastEpoch ${current_trainDir}/0

makeParas0
resamplingData 3
sequenceTrain

# optional
removeUnnecessaryFiles
