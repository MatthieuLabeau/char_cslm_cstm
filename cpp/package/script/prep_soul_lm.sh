#!/bin/bash
configFile=$1
echo "Using config file ${configFile}"
. $configFile

# vocabularies
mkdir -p ${dir}/voc
echo "Making wfreq file..."
cat ${list_data[@]} | text2wfreq > ${dir}/voc/brutVoc.wfreq
echo "Making brutVoc..."
cat ${dir}/voc/brutVoc.wfreq | sort -k2,2 -nr | awk '{print $1;}' > ${dir}/voc/brutVoc

if [ $voc == "xxx" ]; then
	echo "Vocabulary contains the most ${size_voc} frequent words..."
	head -n ${size_voc} ${dir}/voc/brutVoc > ${dir}/voc/voc
else
	echo "Copying from ${voc} to ${dir}/voc/voc..."
	cp ${voc} ${dir}/voc/voc
fi

# test if the vocabulary contains three special tokens : <s>, </s> and <UNK>
nb=`grep "<s>" -c ${dir}/voc/voc`
if (( $nb == 0 )); then
	echo "Add <s> at the end of ${dir}/voc/voc"
	echo "<s>" >> ${dir}/voc/voc
fi
nb=`grep "</s>" -c ${dir}/voc/voc`
if (( $nb == 0 )); then
	echo "Add </s> at the end of ${dir}/voc/voc"
	echo "</s>" >> ${dir}/voc/voc
fi
nb=`grep "<UNK>" -c ${dir}/voc/voc`
if (( $nb == 0 )); then
	echo "Add <UNK> at the end of ${dir}/voc/voc"
	echo "<UNK>" >> ${dir}/voc/voc
fi

mapVoc.exe ${dir}/voc/brutVoc ${dir}/voc/voc ${dir}/voc/brutVoc.mapVoc
paste ${dir}/voc/brutVoc.mapVoc ${dir}/voc/brutVoc | grep -v "^\-1" | awk '{print $2;}' | head -n ${size_sl} > ${dir}/voc/triVoc.${size_sl}
mapVoc.exe ${dir}/voc/triVoc.${size_sl} ${dir}/voc/voc ${dir}/voc/triVoc.${size_sl}.mapVoc
echo "Creating out of shortlist vocabulary..."
voc_createOutVoc.py ${dir}/voc/voc ${dir}/voc/triVoc.${size_sl} ${dir}/voc/outVoc.${size_sl}

echo "Finish preparing vocabularies..."

echo "Start preparing training data..."
mkdir -p ${dir}/data

for (( id=0 ; id<${#list_data[@]} ; id ++ ))
do
	res=`wc ${list_data[id]} | awk '{print $4" "$1" "$2" "$3}'`
	echo "$res ${group_data[id]}" >> ${dir}/data/wc.info
done

# make dataTrainDes file which contains the resampling rates
text_resamplingRate.py ${dir}/data/wc.info 1 ${max_ngram_sl} ${dir}/data/dataTrainDesSl
text_resamplingRate.py ${dir}/data/wc.info 1 ${max_ngram_osl} ${dir}/data/dataTrainDesOsl
text_resamplingRate.py ${dir}/data/wc.info 1 ${max_ngram_all} ${dir}/data/dataTrainDesAll

echo "Finish prepare data"
