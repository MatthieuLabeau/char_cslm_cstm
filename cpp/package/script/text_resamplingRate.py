#!/usr/bin/env python
import numpy
import sys
if(len(sys.argv) != 5):
	print('wcFileName method ngramE prefixDataTrainDes')
	print('method 1: 0.75 for one, 0.25 for the others')
	print('method 2: 0.5 for one (group 0), remaining part: 0.5 for second, 0.5 for the others')
else:
	wcFileName = sys.argv[1]
	method = sys.argv[2]
	ngramE = int(sys.argv[3])
	prefixDataTrainDes = sys.argv[4]
	name = []
	nline = []
	word = []
	group = []
	maxLen = 0
	for line in open(wcFileName, 'r'):
		splitLine = line.split()
		name.append(splitLine[0]) 
		nline.append(int(splitLine[1]))
		word.append(int(splitLine[2]))
		group.append(int(splitLine[4]))	
		if(len(name[-1]) > maxLen):
			maxLen = len(name[-1])
	nline = numpy.array(nline)
	word = numpy.array(word)
	group = numpy.array(group)
	maxGroup = numpy.max(group) + 1
	ngram = nline + word
	totalNgram = numpy.sum(ngram)
	if (method == '1'): #0.75 for one, 0.25 for the others
		if(maxGroup != 1):
			mainPC = 0.75
		else:
			mainPC = 1.0
		for i in range(maxGroup):
			pc = mainPC
			ngramGroup = 0
			for j in range(len(nline)):
				if(group[j] == i):
					ngramGroup = ngramGroup + ngram[j]
			if (ngramE * pc / ngramGroup > 1):
				pc = 1.0 * ngramGroup / ngramE
			if(maxGroup != 1):
				repcr = ngramE * (1 - pc) / (totalNgram - ngramGroup)
			else:
				repcr = 0
			repcM = [repcr] * len(nline)
			for j in range(len(nline)):
				if(group[j] == i):					
					repcM[j] = ngramE * pc / ngramGroup
			outputFile = open(prefixDataTrainDes + str(i), 'w')
			for j in range(len(nline)):
				outputFile.write(name[j] + ' ' * (maxLen + 2 - len(name[j])))
				strW = '%12d  ' % nline[j]
				if(repcM[j] > 1):
					sys.stderr.write(name[j] + ": " + str(repcM[j]) + " > 1\n")
					repcM[j] = 1
				strW = strW + '%1.7f' % repcM[j]
				outputFile.write(strW + '\n')
			outputFile.close()
	if (method == '2'):  #0.5 for one (group 0), remaining part: 0.5 for second, 0.5 for the others
		p0 = 0.5
		p1 = 0.5
		ngramGroup0 = 0
		for j in range(len(nline)):
			if(group[j] == 0):
				totalNgram = totalNgram - ngram[j]
				ngramGroup0 = ngramGroup0 + ngram[j]
		if (ngramE * p0 / ngramGroup0 > 1):
			p0 = 1.0 * ngramGroup0 / ngramE
		repcM0 = ngramE * p0 / ngramGroup0	
		ngramE = ngramE * (1 - p0)
		mainPC = p1

		for i in range(1, maxGroup):
			pc = mainPC
			ngramGroup = 0
			for j in range(len(nline)):
				if(group[j] == i):
					ngramGroup = ngramGroup + ngram[j]
			if (ngramE * pc / ngramGroup > 1):
				pc = 1.0 * ngramGroup / ngramE
			repcr = ngramE * (1 - pc) / (totalNgram - ngramGroup)	
			repcM = [repcr] * len(nline)
			for j in range(len(nline)):
				if(group[j] == i):					
					repcM[j] = ngramE * pc / ngramGroup
				elif(group[j] == 0):
					repcM[j] = repcM0
			outputFile = open(prefixDataTrainDes + str(i - 1), 'w')
			for j in range(len(nline)):
				outputFile.write(name[j] + ' ' * (maxLen + 2 - len(name[j])))
				strW = '%12d  ' % nline[j]
				if(repcM[j] > 1):
					sys.stderr(name[j] + ": " + repcM[j] + " > 1\n")
					repcM[j] = 1
				strW = strW + '%1.7f' % repcM[j]
				outputFile.write(strW + '\n')
			outputFile.close()

