#!/bin/bash
configFile=$1
echo "Using config file ${configFile}"
. $configFile

# Vocabularies
# First, get voc for training data
mkdir -p ${dir}/voc
echo "Making wfreq file for training data..."
cat ${list_data[@]} | text2wfreq > ${dir}/voc/brutVoc.wfreq
cat ${dir}/voc/brutVoc.wfreq | sort -k2,2 -nr | awk '{print $1;}' > ${dir}/voc/brutVoc
cat ${dir}/voc/brutVoc.wfreq | sort -k2,2 -nr | awk '{print $2;}' > ${dir}/voc/brutFreq

if [ $voc == "xxx" ]; then
    echo "Vocabulary contains the most ${size_voc} frequent words..."
    head -n ${size_voc} ${dir}/voc/brutVoc > ${dir}/voc/voc
    #tail -n +${size_voc} ${dir}/voc/brutVoc > ${dir}/voc/unk.tmp
    head -n ${size_voc} ${dir}/voc/brutFreq > ${dir}/voc/freq
    tail -n +${size_voc} ${dir}/voc/brutFreq > ${dir}/voc/tmpUnk
    head -n ${size_voc_lk} ${dir}/voc/brutVoc > ${dir}/voc/vocLookup
else
    echo "Copying from ${voc} to ${dir}/voc/voc..."
    cp ${voc} ${dir}/voc/voc
    cp ${freq} ${dir}/voc/freq
fi

rm ${dir}/voc/brutVoc.wfreq

# Test if this vocabulary contains three special tokens : <s>, </s> and <UNK>
nb=`grep "<UNK>" -c ${dir}/voc/voc`
if (( $nb == 0 )); then
    echo "Add <UNK> at the beginning of ${dir}/voc/voc"
    echo "<UNK>" >> tmpv
    unkn=`awk '{s+=$1} END {print s}' ${dir}/voc/tmpUnk`
    echo ${unkn} >> tmpf
fi
nb=`grep "<s>" -c ${dir}/voc/voc`
if (( $nb == 0 )); then
    echo "Add <s> at the beginning of ${dir}/voc/voc"
    echo "<s>" >> tmpv
    sn=`wc -l ${dir}/voc/voc | awk '{print $1;}'`
    echo ${sn} >> tmpf
fi
nb=`grep "</s>" -c ${dir}/voc/voc`
if (( $nb == 0 )); then
    echo "Add </s> at the beginning of ${dir}/voc/voc"
    echo "</s>" >> tmpv
    sn=`wc -l ${dir}/voc/voc | awk '{print $1;}'`
    echo ${sn} >> tmpf
fi

cat ${dir}/voc/voc >> tmpv
cat ${dir}/voc/freq >> tmpf
rm ${dir}/voc/voc
rm ${dir}/voc/freq
cat tmpv >> ${dir}/voc/voc
cat tmpf >> ${dir}/voc/freq
rm tmpv
rm tmpf

nb=`grep "<UNK>" -c ${dir}/voc/vocLookup`
if (( $nb == 0 )); then
    echo "Add <UNK> at the beginning of ${dir}/voc/vocLookup"
    echo "<UNK>" >> tmpv
fi
nb=`grep "<s>" -c ${dir}/voc/vocLookup`
if (( $nb == 0 )); then
    echo "Add <s> at the beginning of ${dir}/voc/vocLookup"
    echo "<s>" >> tmpv
fi
nb=`grep "</s>" -c ${dir}/voc/vocLookup`
if (( $nb == 0 )); then
    echo "Add </s> at the beginning of ${dir}/voc/vocLookup"
    echo "</s>" >> tmpv
fi
cat ${dir}/voc/vocLookup >> tmpv
rm ${dir}/voc/vocLookup
cat tmpv >> ${dir}/voc/vocLookup
rm tmpv

#From concatenation of training and test data, get the full vocabulary 
echo "Making wfreq file for all data..."
cat ${list_data[@]} $validation | text2wfreq > ${dir}/voc/dataVoc.wfreq
cat ${dir}/voc/dataVoc.wfreq | sort -k2,2 -nr | awk '{print $1;}' > ${dir}/voc/dataVoc
grep -F -x -v -f ${dir}/voc/vocLookup ${dir}/voc/dataVoc > ${dir}/voc/diffVoc
cat ${dir}/voc/vocLookup ${dir}/voc/diffVoc > ${dir}/voc/dataVoc
rm ${dir}/voc/dataVoc.wfreq
rm ${dir}/voc/diffVoc

# Test if this vocabulary contains three special tokens : <s>, </s> and <UNK>                                                                                                                           
nb=`grep "<UNK>" -c ${dir}/voc/dataVoc`
if (( $nb == 0 )); then
    echo "Add <UNK> at the beginning of ${dir}/voc/dataVoc"
    echo "<UNK>" >> tmp
fi
nb=`grep "<s>" -c ${dir}/voc/dataVoc`
if (( $nb == 0 )); then
    echo "Add <s> at the beginning of ${dir}/voc/dataVoc"
    echo "<s>" >> tmp
fi
nb=`grep "</s>" -c ${dir}/voc/dataVoc`
if (( $nb == 0 )); then
    echo "Add </s> at the beginning of ${dir}/voc/dataVoc"
    echo "</s>" >> tmp
fi

cat ${dir}/voc/dataVoc >> tmp
rm ${dir}/voc/dataVoc
cat tmp >> ${dir}/voc/dataVoc
rm tmp

# We can then obtain the char-word hashmap
if [ $hmf == "xxx" ]; then
    echo "Creating charWord Hashmap from all data..."
    cat ${list_data[@]} $validation > ${dir}/voc/allData.tmp
    echo "Char Vocabulary contains the characters that appear more than ${threshold_charVoc} times..."
    python ${script_dir}/get_hashmap.py ${dir}/voc/allData.tmp ${dir}/voc/dataVoc ${dir}/voc/charVoc ${dir}/voc/charWordHMap $threshold_charVoc $nC $padding
    hmf=${dir}/voc/charWordHMap
    rm ${dir}/voc/allData.tmp
fi

echo "Finish preparing vocabularies..."


echo "Start preparing training data..."
mkdir -p ${dir}/data

for (( id=0 ; id<${#list_data[@]} ; id ++ ))
do
    res=`wc ${list_data[id]} | awk '{print $4" "$1" "$2" "$3}'`
    echo "$res ${group_data[id]}" >> ${dir}/data/wc.info
done

# make dataTrainDes file which contains the resampling rates
text_resamplingRate.py ${dir}/data/wc.info 1 ${max_ngram_all} ${dir}/data/dataTrainDesAll

echo "Start preparing valid data..."
res=`wc ${validation} | awk '{print $4" "$1" "$2" "$3}'`
rm ${dir}/data/wc.info
echo "$res 0" >> ${dir}/data/wc.info
#ngram_validation=`wc ${validation} | awk '{print $2;}'`                                                                                                                                                     
ngram_validation=97004
text_resamplingRate.py ${dir}/data/wc.info 1 ${ngram_validation} ${dir}/data/dataValidDesAll

#if validCut, remove UNK words from validation data
#if [ $validCut == 1 ]; then
#    python ${script_dir}/cut_valid.py ${dir}/voc/unk.tmp $validation ${dir}/data/validation_$size_voc
#fi
#rm ${dir}/voc/unk.tmp

echo "Finish prepare data"
