#!/bin/bash

function prepare {
    case "$1" in 
	1) key=all
            ;;
	2) key=nce
	    ;;
    esac
    current_resamplingDir=${resamplingDir}/${key}/$type
    mkdir -p ${current_resamplingDir}
    if [ "$1" == "1" ]; then
	out_trainDir=${current_trainDir}
	fi
    current_trainDir=${trainDir}/$key
    mkdir -p ${current_trainDir}
}

function makeParas0 {
    echo 1 > ${current_trainDir}/0.par
    echo 6 1 >> ${current_trainDir}/0.par
    echo ${lr} ${lr} ${lr_dc} ${w_dc} $blockSize $divide >> ${current_trainDir}/0.par
}

function createPrototype {
    echo "Creating initial model..."
    destVoc=${dir}/voc/voc

    if [ "${type}" == "ovn" -o "${type}" == "ovn_nce" -o "${type}" == "ovn_ag" -o "${type}" == "ovn_nce_ag" -o "${type}" == "lkt_ce_nce" -o "${type}" == "lkt_cwe_nce" -o "${type}" == "lkt_cwe_nce_ag" ]; then
    dataVoc=${dir}/voc/vocLookup
    fi
    if [ "${type}" == "ce_h_nce" -o "${type}" == "ce_h_nce_ag" ]; then
	createPrototype.exe $type $preModel ${dir}/voc/charVoc ${dataVoc} ${destVoc} ${dir}/voc/charWordHMap $charDimensionSize $nonLinearType $codeWordFileName $outputNetworkSizeFileName ${current_trainDir}/0
    else
	createPrototype.exe $type ${dir}/voc/charVoc ${dataVoc} ${destVoc} ${dir}/voc/charWordHMap $mapIUnk $mapOUnk $n $charDimensionSize $dimensionSize $nonLinearType $hiddenLayerSizeCode $codeWordFileName $outputNetworkSizeFileName ${current_trainDir}/0
    fi
}

function sequenceTrain {
    echo "Training..."
    sequenceTrain.exe ${current_trainDir}/ ${current_resamplingDir}/train. 0 xxx $validation $validType $learningRateType $first $last ${update_lkt} ${update_hiddens} ${update_output_layer} ${dir}/voc/charWordHMap $e $cache
}

function resamplingData {
    echo "Resampling data..."
    destVoc=${dir}/voc/voc
    dataTrainDes=${dir}/data/dataTrainDesAll0
    if [ "$1" != "2" ]; then
	resamplingData.exe $dataTrainDes ${dataVoc} ${destVoc} $n $mapIUnk $mapOUnk ${current_resamplingDir}/train. $first $last $type
    else
	echo "Resampling pre-data for nce..."
	resamplingData.exe $dataTrainDes ${dataVoc} ${destVoc} $n $mapIUnk $mapOUnk ${current_resamplingDir}/cllTrain. $first $last ovn
	echo "Resampling data for NCE..."
	if [ "${freq}" == "all" ]; then
	    freq=${dir}/voc/freq
	fi
	echo "freq = $freq"
	if [ "${freq}" != "xxx" ]; then
	    if [ "${unigram_pow}" != "xxx" ]; then
		python ${script_dir}/pow_freq.py $freq $unigram_pow
		freq=${freq}.pow
	    fi
	    resamplingNce.sh ${current_resamplingDir}/cllTrain. $freq $nceOutputSize $first $last ${current_resamplingDir}/train.
	else
	    resamplingNce.sh ${current_resamplingDir}/cllTrain. $nceOutputSize $first $last ${current_resamplingDir}/train.
	fi
	echo "Resampling validation data for monitoring NCE..."
        dataValidDes=${dir}/data/dataValidDesAll0
        resamplingData.exe $dataValidDes ${dataVoc} ${destVoc} $n $mapIUnk $mapOUnk ${current_resamplingDir}/cllValid. $first $first ovn
        resamplingNce.sh ${current_resamplingDir}/cllValid. $nceOutputSize $first $first ${current_resamplingDir}/train.valid.
    fi
}

function removeUnnecessaryFiles {
    rm -v ${current_resamplingDir}/*
    for (( id_file=0 ; id_file<$last ; id_file ++ ))
    do
	rm -v ${current_trainDir}/${id_file}
	done
}

configFile=$1
echo "Using config file ${configFile}"
. $configFile

#if [ $validCut == 1 ]; then
#    validation=${dir}/data/validation_$size_voc
#fi

# directory containing training binary file for each iteration
resamplingDir=${dir}/resampling
mkdir -p ${resamplingDir}

# directory containing trained models
t=1;
trainDir="${dir}/model$t"
while [ -d "$trainDir" ]; do
    t=`expr "$t" + 1`;
    trainDir="${dir}/model$t"
done
mkdir -p ${trainDir}
cp $configFile ${trainDir}/saveConfig

if [ "${type}" == "ce" -o "${type}" == "cwe"  -o "${type}" == "cwe_l" -o "${type}" == "ovn" -o "${type}" == "ovn_ag" -o "${type}" == "ce_ag" ]; then
    prepare 1
    
    dataVoc=${dir}/voc/dataVoc
    first=1
    last=$lastEpoch
    mapIUnk=1
    mapOUnk=1

    codeWordFileName=xxx
    outputNetworkSizeFileName=xxx

    createPrototype
    makeParas0
    resamplingData 1
    sequenceTrain

elif [ "${type}" == "ce_nce" -o "${type}" == "cwe_nce" -o "${type}" == "cwe_nce_l" -o "${type}" == "ovn_nce" -o "${type}" == "ce_ce_nce" -o "${type}" == "cwe_ce_nce" -o "${type}" == "lkt_ce_nce" -o "${type}" == "lkt_cwe_nce" -o "${type}" == "cwe_cwe_nce" -o "${type}" == "ce_cwe_nce" -o "${type}" == "ovn_nce_ag" -o "${type}" == "ce_nce_ag" -o "${type}" == "cwe_nce_ag" -o "${type}" == "ce_h_nce_ag" -o "${type}" == "ce_ce_nce_ag" -o "${type}" == "ce_cwe_nce_ag" -o "${type}" == "lkt_cwe_nce_ag" -o "${type}" == "cwe_cwe_nce_ag" ]; then
    prepare 2

    dataVoc=${dir}/voc/dataVoc
    first=1
    last=$lastEpoch
    mapIUnk=1
    mapOUnk=1

    codeWordFileName=xxx
    outputNetworkSizeFileName=xxx

    createPrototype
    makeParas0
    resamplingData 2
    sequenceTrain
fi

# optional
#removeUnnecessaryFiles
