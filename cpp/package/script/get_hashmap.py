import sys
import os
import re
from collections import defaultdict
from nltk.util import ngrams

def filter_vocab(vocab, threshold):
    return dict((k, v) for k, v in vocab.iteritems() if v >= threshold), sum(v for k, v in vocab.iteritems() if v < threshold)

def create_files(path_to_data_file, path_to_voc_file, path_to_char_file, path_to_map_file, threshold_c = 0, n=5, padding = 1):
    #If the charVoc file doesn't exist yet:
    if not os.path.exists(path_to_char_file):

        #Count characters
        char_count=defaultdict(int)
        with open(path_to_data_file, 'r') as data_file:
            for line in data_file:
                for c in "".join(line.split()).strip('<s>').strip('</s>'):
                    char_count[c.lower()] += 1
        c_f, _ = filter_vocab(char_count, threshold_c)

        #Create char vocab 
        c_voc = defaultdict(int)
        c_voc['UNK'] = 0
        c_voc['bow'] = 1
        c_voc['eow'] = 2

        with open(path_to_char_file,'w') as char_file:
            char_file.write( 'UNK' + '\n')
            char_file.write( 'bow' + '\n')
            char_file.write( 'eow' + '\n')
            for i, (c, k) in enumerate(sorted(c_f.items(), key=lambda x:x[1], reverse=True)):
                #char_file.write( c + '\t' + str(k) + '\n')
                char_file.write( c + '\n')
                c_voc[c] = i+3

    else:
        c_voc = defaultdict(int)
        i = 0
        with open(path_to_char_file,'r') as char_file:
            for c in char_file:
                c_voc[c.strip()] =  i
                i += 1

    #Get word voc 
    word_count=[]
    i = 0
    with open(path_to_voc_file, 'r') as voc_file:
        for w in voc_file:
            word_count.append((w.strip(), i))
            i += 1
    max_len = len(max([w for w, i in word_count], key=len)) + (n-1)
        
    #Create in parallel word file and word -> char n-grams map file
    with open(path_to_map_file,'w') as map_file:
        map_file.write(str(len(word_count)) + '\n')
        map_file.write(str(n) + '\n')
        map_file.write(str(max_len) + '\n')
        for i in range(3):
            map_file.write(str(i) + ' ' + str(1) + ' ' + ' '.join([str(i)]*n) + '\n')                      
        for w, i in word_count[3:]:
            if padding:
                c_n_grams = ngrams([str(c_voc['bow'])]*(n-1) + [str(c_voc.get(c, c_voc['UNK'])) for c in w] + [str(c_voc['eow'])]*(n-1), n)
            else:
                if (len(w) >= (n-2)):
                    c_n_grams = ngrams([str(c_voc['bow'])] + [str(c_voc.get(c, c_voc['UNK'])) for c in w] + [str(c_voc['eow'])], n)
                else:
                    c_n_grams = ngrams([str(c_voc['bow'])]*(((n-1)/2)+1) + [str(c_voc.get(c, c_voc['UNK'])) for c in w] + [str(c_voc['eow'])]*(((n-1)/2)+1), n)
            map_file.write( str(i) + ' ' + str(len(c_n_grams)) + ' ' + ' '.join([' '.join(c_n_gram) for c_n_gram in c_n_grams]) + '\n')


def main(argv):
    if (len(argv) == 7):
        create_files(argv[0], argv[1], argv[2], argv[3], int(argv[4]), int(argv[5]), int(argv[6]))
    else:
        if (len(argv) != 6):
            print("allDataPath allDataVocPath charVocPath charWordHMapPath charVocThrehsold nC")
        create_files(argv[0], argv[1], argv[2], argv[3], int(argv[4]), int(argv[5]))                      
                           
if __name__ == "__main__":
    main(sys.argv[1:])
