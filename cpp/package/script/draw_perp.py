import sys
import os
import re
import math
from collections import defaultdict
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

def draw_perp(modelPaths, legends, title, outputPath):
    perps = defaultdict(list)
    count = 0
    for modelpath in modelPaths:
        with open(modelpath, 'r') as perp_file:
            if ('per' in modelpath):
                perps[count].append(math.log(193855))
            for line in perp_file:
                if ('norm' in modelpath):
                    perps[count].append(math.log(float(line.split()[0])))
                else:
                    if ('per' in modelpath):
                        perps[count].append(math.log(float(line.split()[1])))
                    else:
                        perps[count].append(float(line.split()[0]))        
                if ('ad' in title):
                    if (not ('norm' in modelpath)):
                        if (( len( perps[count] ) > 1) and (perps[count][-1] > perps[count][-2])):
                            perps[count][-1] = perps[count][-2]
        count += 1

    color=list(plt.cm.rainbow(np.linspace(0,1,len(perps))))
    for i, modelpath in enumerate(modelPaths):
        plt.plot(range(1, len(perps[i]) + 1), perps[i], label=legends[i], color=color[i])
        #plt.text(len(perps[i]) + 0.1, perps[i][-1]**((float(i+1)/15)+1), str(perps[i][-1]), rotation=0, fontsize=10, fontweight='heavy', color=color[i]) 
    plt.grid(True)
    plt.xlim(1,50)
    plt.ylim(0,20)
    plt.ylabel('Score')
    plt.xlabel('Iterations')
    plt.title(title)
    plt.legend()
    plt.savefig(outputPath + '.png', bbox_inches='tight')


def main(argv):
    if (len(argv) == 0):
        print("modelPath list, legend list")
    length = len(argv)/2 - 1
    draw_perp(argv[:length], argv[length:-2], argv[-2], argv[-1])                      
                           
if __name__ == "__main__":
    main(sys.argv[1:])
