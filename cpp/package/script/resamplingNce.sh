#!/bin/bash

if (( $# != 6 ))&&(( $# != 5 )); then
	echo "cllTrainPrefix freqFileName<optional> nceOutputSize minEpoch maxEpoch nceTrainPrefix"
	exit
fi

# inputs
do_count=0
if (( $# == 6 )); then
	cllTrainPrefix=$1
	freqFileName=$2
	nceOutputSize=$3
	minEpoch=$4
	maxEpoch=$5
	nceTrainPrefix=$6
else if (( $# == 5 )); then
	cllTrainPrefix=$1
	nceOutputSize=$2
	minEpoch=$3
	maxEpoch=$4
	nceTrainPrefix=$5
	
	# need to count word frequencies
	do_count=1
fi fi

type=ovn
for (( id=$minEpoch ; id<=${maxEpoch} ; id ++ ))
do
	# check
	if [ ! -f ${cllTrainPrefix}${id} ]; then
		echo "Error. ${cllTrainPrefix}${id} does not exist. The program will exit."
		exit
	fi
	
	# do counting
	if [ "${do_count}" == "1" ]; then
		echo "Computing word frequencies..."
		cmd="train2freq.exe ${cllTrainPrefix}${id} $type ${nceTrainPrefix}$id.freq"
		echo $cmd
		$cmd
		freqFile=${nceTrainPrefix}$id.freq
	else if [ "${do_count}" == "0" ]; then
		freqFile=$freqFileName
	fi fi
	
	# resampling nce
	echo "Resampling NCE..."
	cmd="resamplingNce2.exe ${cllTrainPrefix}$id $freqFile ${nceTrainPrefix}$id $nceOutputSize"
	echo $cmd
	$cmd
done