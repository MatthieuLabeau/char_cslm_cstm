#!/usr/bin/env python
import sys
import os.path
import random

def takeOneHypo(file_nbest_tuple) :
    out_hypo = []
    line_file_nbest_tuple = ''
    first_word = ''
    while(first_word != 'EOS') :
        line_file_nbest_tuple = file_nbest_tuple.readline().rstrip()
        words = line_file_nbest_tuple.split()
        first_word = words[0]
        if (first_word != 'EOS') :
            out_hypo.append(line_file_nbest_tuple)
        
    return out_hypo

if len(sys.argv) != 6 :
    print('nb_lines nb_random lines_chosen orig_file output_file')
    exit(1)
    
# inputs
nb_lines = int(sys.argv[1])
nb_random = int(sys.argv[2])
lines_chosen_name = sys.argv[3]
orig_name = sys.argv[4]
output_name = sys.argv[5]

do_random = 1
if os.path.exists(lines_chosen_name) :
    do_random = 0
    
if do_random == 1 :
    lines_chosen_file = open(lines_chosen_name, 'w')
else :
    lines_chosen_file = open(lines_chosen_name, 'r')
    
orig_file = open(orig_name, 'r')
output_file = open(output_name, 'w')

rand = []
if do_random == 1 :
    print('Randomly choose lines')
    set = range(nb_lines)
    for i in range(nb_random) :
        elem = random.choice(set)
        set.remove(elem)
        rand.append(elem)
    rand.sort()
    print('finish sorting rand')
    for i in range(len(rand)) :
        lines_chosen_file.write(str(rand[i]) + '\n')
else :
    print('Random lines exists')
    for line_lines_chosen_file in lines_chosen_file :
        line_lines_chosen_file = line_lines_chosen_file.rstrip()
        rand.append(int(line_lines_chosen_file))
        
id = 0
next_id_rand = 0
while next_id_rand < nb_random :
    line_orig_file = takeOneHypo(orig_file)
    if id == rand[next_id_rand] :
        for id_line_orig_file in range(len(line_orig_file)) :
            output_file.write(line_orig_file[id_line_orig_file] + '\n')
        output_file.write('EOS' + '\n')
        next_id_rand += 1
    id += 1
    
lines_chosen_file.close()
orig_file.close()
output_file.close()
