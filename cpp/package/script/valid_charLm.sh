#!/bin/bash 

function computeScores {
    text2Perp_UnnmProb.exe ${trainDir}/${i} ${blockSize} $valid $validType ${dir}/voc/validCharWordHMap
}

#Change dir and valid to specify where and on what to do validation
list_data=(/people/labeau/pynn/github/NonlexNN/data/train.wrd)
dir=/vol/work2/labeau/Char_CSLM_CSTM/tigerExp2
list_dir=$(ls -d ${dir}/model*)
valid=/people/labeau/pynn/github/NonlexNN/data/test.wrd
nC=5
script_dir=/people/labeau/CSLM_CSTM/cpp/package/script

#Create charwordHMap that goes with current validation text
echo "Making wfreq file for all data..."
cat ${list_data[@]} $valid | /people/lehaison/share/rock/CMU-Cam_Toolkit_v2/bin/text2wfreq > ${dir}/voc/validVoc.wfreq
cat ${dir}/voc/validVoc.wfreq | sort -k2,2 -nr | awk '{print $1;}' > ${dir}/voc/validVoc
grep -F -x -v -f ${dir}/voc/voc ${dir}/voc/validVoc > ${dir}/voc/diffVoc
cat ${dir}/voc/voc ${dir}/voc/diffVoc > ${dir}/voc/validVoc
rm ${dir}/voc/validVoc.wfreq

nb=`grep "<UNK>" -c ${dir}/voc/validVoc`
if (( $nb == 0 )); then
    echo "Add <UNK> at the beginning of ${dir}/voc/validVoc"
    echo "<UNK>" >> tmp
fi
nb=`grep "<s>" -c ${dir}/voc/validVoc`
if (( $nb == 0 )); then
    echo "Add <s> at the beginning of ${dir}/voc/validVoc"
    echo "<s>" >> tmp
fi
nb=`grep "</s>" -c ${dir}/voc/validVoc`
if (( $nb == 0 )); then
    echo "Add </s> at the beginning of ${dir}/voc/validVoc"
    echo "</s>" >> tmp
fi

cat ${dir}/voc/validVoc >> tmp
rm ${dir}/voc/validVoc
cat tmp >> ${dir}/voc/validVoc
rm tmp

echo "Creating charWord Hashmap from all data..."
cat ${list_data[@]} $valid > ${dir}/voc/allData.tmp
python ${script_dir}/get_hashmap.py ${dir}/voc/allData.tmp ${dir}/voc/validVoc ${dir}/voc/charVoc ${dir}/voc/validCharWordHMap 0 $nC
rm ${dir}/voc/allData.tmp

for localdir in $list_dir
do
    configFile=${localdir}/saveConfig
    . $configFile
    trainDir=${localdir}/nce 
    rm ${trainDir}/out.*p
    echo "Validation for ${localdir}:"
    for (( i=1; i<=$lastEpoch; i++ ))
    do
	echo "Validation: model $i..."
	computeScores >> log_valid
    done
    python ${script_dir}/draw_perp.py ${trainDir}/out.perp ${trainDir}/out.noUnkPerp ${trainDir}/out.unnmPerp ${trainDir}/out.unnmNoUnkPerp 'Perplexity' 'Perplexity with no unknown words' 'Unnormalized Score' 'Unnormalized Score with no unknown words' "${type} ${n} ${nC} ${nonLinearType} ${charDimensionSize} ${hiddenLayerSizeCode}" "${localdir}"
done

