#!/bin/bash
configFile=$1
echo "Using config file ${configFile}"
. $configFile

# which output voc and ngram_type to be used?
# all 4 models :
# ngram_type=0 --> TrgSrc
# ngram_type=1 --> Trg
# ngram_type=2 --> SrcTrg
# ngram_type=3 --> Src
function preciseVocs {
	input_voc=${dir}/voc/voc.inVoc
	case "$1" in
		TrgSrc) output_voc=${dir}/voc/voc.trgOutVoc
			ngram_type=0
			;;
		Src) output_voc=${dir}/voc/voc.srcOutVoc
		     ngram_type=3
			;;
		SrcTrg) output_voc=${dir}/voc/voc.srcOutVoc
			ngram_type=2
			;;
		Trg) output_voc=${dir}/voc/voc.trgOutVoc
		     ngram_type=1
			;;
	esac
}

function makeParas0 {
	echo 1 > ${current_trainDir}/0.par
	echo 6 1 >> ${current_trainDir}/0.par
	echo ${lr} ${lr} ${lr_dc} ${w_dc} $blockSize $divide >> ${current_trainDir}/0.par
}

function sequenceTrain {
	echo "Training..."
	sequenceTrain.exe ${current_trainDir}/ ${current_resamplingDir}/train. 0 xxx $validation $validType $learningRateType $first $last ${update_lkt} ${update_hiddens} ${update_output_layer}
}

function removeUnnecessaryFiles {
	rm -v ${current_resamplingDir}/train.*                                                                                          
	for (( id_file=0 ; id_file<$last ; id_file ++ ))                                                                          
	do                                                                                                                             
        	rm -v ${current_trainDir}/${id_file}                                                                                    
	done
}

# create initial bilingual models
mkdir -p ${dir}/init

if [ "${init}" == "monopre" ]; then
	# add NULL in LM models
	echo "NULL to src lm..."
	addNULL.exe $srcLM ${dir}/init/srcLMNULL
	echo "NULL to trg lm..."
	addNULL.exe $trgLM ${dir}/init/trgLMNULL

	echo "Combining these two models..."
	disWordTupleMono2Model.exe ${dir}/init/srcLMNULL ${dir}/init/trgLMNULL $type ${dir}/init/biling.

	for (( id=0 ; id<${#set_model[@]} ; id ++ ))
	do
		mkdir -p ${dir}/init/${set_model[id]}
		ln -s ${dir}/init/biling.${set_model[id]} ${dir}/init/${set_model[id]}/0
	done
elif [ "${init}" == "random" ]; then
	for (( id=0 ; id<${#set_model[@]} ; id ++ ))
	do
		mkdir -p ${dir}/init/${set_model[id]}
		echo "Creating initial model of ${set_model[id]}..."
		preciseVocs ${set_model[id]}
		disWordTupleCreatePrototype.exe $type ${ngram_type} ${input_voc} ${output_voc} $mapIUnk $mapOUnk $n $dimensionSize $nonLinearType $hiddenLayerSizeCode xxx xxx ${dir}/init/${set_model[id]}/0
	done
fi

resamplingDir=${dir}/resampling
mkdir -p ${resamplingDir}
trainDir=${dir}/model
mkdir -p ${trainDir}
for (( id=0 ; id<${#set_model[@]} ; id ++ ))
do
	mkdir -p ${resamplingDir}/${set_model[id]}
	preciseVocs ${set_model[id]}
	first=$minEpoch
	last=$maxEpoch
	echo "Resampling data for ${set_model[id]}..."
	wordTranslationResamplingData.exe ${ngram_type} ${dir}/data/dataTrainDes0 ${input_voc} ${output_voc} $n $mapIUnk $mapOUnk ${resamplingDir}/${set_model[id]}/train. $first $last $type
	mkdir -p ${trainDir}/${set_model[id]}
	ln -s ${dir}/init/${set_model[id]}/0 ${trainDir}/${set_model[id]}
	current_trainDir=${trainDir}/${set_model[id]}
	current_resamplingDir=${resamplingDir}/${set_model[id]}
	makeParas0
	if [ "$validation" == "xxx" ]; then
		validation=${dir}/data/tuple.forValidation
	fi
	echo "Training model ${set_model[id]}..."
	sequenceTrain
	removeUnnecessaryFiles
done
