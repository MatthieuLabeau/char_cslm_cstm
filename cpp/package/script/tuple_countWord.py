#!/usr/bin/env python
import math
import os, sys
import numpy
import gzip
if (len(sys.argv) != 2):
	print('inputFileName')
else:
	inputFileName = sys.argv[1]
	srcCnt = 0
	trgCnt = 0
	sentCnt = 0
	for line in open(inputFileName, 'r'):
		words = line.split()
		if(words[0] == "EOS"):
			sentCnt = sentCnt + 1
			pre_words = preLine.split()
			if(pre_words[0] != "EOS"):
				srcCnt = srcCnt + 1
				trgCnt = trgCnt + 1
		else:
			splitLine = line.rsplit(' ||| ')
			srcCnt = srcCnt + len(splitLine[0].split())
			trgCnt = trgCnt + len(splitLine[1].split())
		preLine = line
	print("Sentence number: "  + str(sentCnt))
	print("Source word number: " + str(srcCnt - sentCnt))
	print("Target word number: " + str(trgCnt - sentCnt))
	print("ngram number for src, srcTrg models: " + str(srcCnt))
	print("ngram number for trg, trgSrc models: " + str(trgCnt))
