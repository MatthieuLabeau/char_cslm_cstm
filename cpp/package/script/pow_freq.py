from __future__ import division
import sys
import os
import math

def freq_pow(path_to_freq_file, power):
    with open(path_to_freq_file, 'r') as freq_file:
        with open(path_to_freq_file + ".pow", 'w') as out_file:
            for line in freq_file:
                out_file.write(str(int(math.ceil(int(line) ** power))) + '\n')

def main(argv):
    if (len(argv) != 2):
        print("freqPath pow")
    freq_pow(argv[0], float(argv[1]))

if __name__ == "__main__":
    main(sys.argv[1:])
