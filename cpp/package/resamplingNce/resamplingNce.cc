#include "mainTensor.H"
#include "text.H"

Strip(read, Int, int)
Strip(write, Int, int)
Strip(read, Float, float)
Strip(write, Float, float)

void genRanWord(floatTensor& noiseFreq, floatTensor& cumulFreq, float& prop,
		floatTensor& noiseProp, intTensor& genWord, floatTensor& logProp,
		outils* otl, int first) {
	int idWord;

	// vocabulary size
	int vocSize = noiseFreq.length;

	// only at the first time we resample
	if (first == 1) {
		cumulFreq.resize(noiseFreq);
		prop = 0;
		for (idWord = 0; idWord < vocSize; idWord++) {
			prop += noiseFreq(idWord);
			cumulFreq(idWord) = prop;
		}
		noiseProp.copy(noiseFreq);
		noiseProp.scal(1 / prop);
	}

	floatTensor genNum;
	genNum.resize(genWord.size[0], genWord.size[1]);
	genNum.uniform(0, prop, otl);
	for (int i = 0; i < genWord.size[0]; i++) {
		for (int j = 0; j < genWord.size[1]; j++) {
			idWord = 0;
			while (cumulFreq(idWord) < genNum(i, j)) {
				idWord++;
			}
			genWord(i, j) = idWord;
			logProp(i, j) = log(noiseProp(idWord));
		}
	}
}

void genNCE(char* origTrainFileName, char* freqFileName, char* outputFileName,
		int nceOutputSize) {
	ioFile origTrainFile, outputFile, freqFile, ioFC;

	if (ioFC.check(outputFileName, 0)) {
		cerr << "nce training file exists" << endl;
		return;
	}

	origTrainFile.takeReadFile(origTrainFileName);
	outputFile.takeWriteFile(outputFileName);
	freqFile.format = TEXT;
	freqFile.takeReadFile(freqFileName);
	// for test
	//cout << "resamplingNce::genNce here" << endl;
	float freq[1000000];
	// for test
	//cout << "resamplingNce::genNce here 1" << endl;
	int id = 0;
	string line = "";
	while (!freqFile.getEOF()) {
		freqFile.getLine(line);
		freq[id] = atof(line.c_str());
		id++;
	}
	// frequencies of words in vocabulary
	floatTensor noiseFreq, cumulFreq, noiseProp;
	// normalization constant, to be initialized once at the first bucket
	float prop;
	noiseFreq.array2Tensor(freq, id, 1);
	// for test
	//cout << "resamplingNce::genNCE here 2" << endl;

	// start reading the original data file
	int ngramNumber;
	origTrainFile.readInt(ngramNumber);
	int N;
	origTrainFile.readInt(N);

	int nbBlock = ngramNumber / NCE_RESAMPLING_NGRAM_NUMBER;
	int remainingNgramNumber = ngramNumber % NCE_RESAMPLING_NGRAM_NUMBER;

	intTensor writeTensor(NCE_RESAMPLING_NGRAM_NUMBER, N + nceOutputSize - 1);
	intTensor readTensor;
	readTensor.sub(writeTensor, 0, writeTensor.size[0] - 1, 0, N - 1);
	floatTensor logPropTensor(NCE_RESAMPLING_NGRAM_NUMBER, nceOutputSize);
	string name = OVN;
	int idBlock;
	int idNgram;
	intTensor subWriteTensor;
	floatTensor subLogPropTensor;

	// write Ngrams
	string newName = OVN_NCE;
	outputFile.writeInt(ngramNumber);
	outputFile.writeInt(N);
	// k = nceOutputSize - 1
	outputFile.writeInt(nceOutputSize - 1);

	// outil to resample
	outils* otl = new outils();
	// get seed
	otl->sgenrand(time(NULL) + getpid());

	int first = 1;
	int nbNgramFinished = 0;
	int nextShow = 1;
	float percent;
	for (idBlock = 0; idBlock < nbBlock; idBlock++) {
		// for test
		cout << nbNgramFinished << " ... " << flush;
		// read in one time
		readIntTrainFile(origTrainFile, readTensor, logPropTensor, N, name);
		subWriteTensor.sub(writeTensor, 0, writeTensor.size[0] - 1, N,
				N + nceOutputSize - 2);
		subLogPropTensor.sub(logPropTensor, 0, logPropTensor.size[0] - 1, 1,
				nceOutputSize - 1);
		// for test
		//cout << "resamplingNce::genNce here 5" << endl;
		// resampling
		genRanWord(noiseFreq, cumulFreq, prop, noiseProp, subWriteTensor,
				subLogPropTensor, otl, first);
		// for test
		//cout << "resamplingNce::genNce here 6" << endl;
		// the first resampling is accomplished, cumulFreq and noiseProp will not be changed anymore
		first = 0;
		// compute log prop of positive output word
		for (idNgram = 0; idNgram < writeTensor.size[0]; idNgram++) {
			logPropTensor(idNgram, 0) = log(
					noiseProp(writeTensor(idNgram, N - 1)));
		}
		writeIntTrainFile(outputFile, writeTensor, logPropTensor, N, newName);
# if PRINT_DEBUG
		nbNgramFinished += NCE_RESAMPLING_NGRAM_NUMBER;
		percent = (float)nbNgramFinished / ngramNumber ;
		if (percent > nextShow * CONSTPRINT) {
			cout << percent << " ... " << flush ;
		}
		while (percent > nextShow * CONSTPRINT) {
			nextShow += 1;
		}
	}
# endif
	// the last block
	if (remainingNgramNumber != 0) {
		writeTensor.resize(remainingNgramNumber, N + nceOutputSize - 1);
		readTensor.sub(writeTensor, 0, writeTensor.size[0] - 1, 0, N - 1);
		logPropTensor.resize(remainingNgramNumber, nceOutputSize);

		// read in one time
		readIntTrainFile(origTrainFile, readTensor, logPropTensor, N, name);
		subWriteTensor.sub(writeTensor, 0, writeTensor.size[0] - 1, N,
				N + nceOutputSize - 2);
		subLogPropTensor.sub(logPropTensor, 0, logPropTensor.size[0] - 1, 1,
				nceOutputSize - 1);
		// resampling
		genRanWord(noiseFreq, cumulFreq, prop, noiseProp, subWriteTensor,
				subLogPropTensor, otl, first);
		// compute log prop of positive output word
		for (idNgram = 0; idNgram < writeTensor.size[0]; idNgram++) {
			logPropTensor(idNgram, 0) = log(
					noiseProp(writeTensor(idNgram, N - 1)));
		}
		writeIntTrainFile(outputFile, writeTensor, logPropTensor, N, newName);
	}

	origTrainFile.freeReadFile();
	freqFile.freeReadFile();
	outputFile.freeWriteFile();
	delete otl;
}

int main(int argc, char *argv[]) {
	if (argc != 5) {
		cerr << "origTrainFileName freqFileName outputFileName nceOutputSize"
				<< endl;
		exit(1);
	}
	// inputs
	char* origTrainFileName = argv[1];
	char* freqFileName = argv[2];
	char* outputFileName = argv[3];
	int nceOutputSize = atoi(argv[4]);
	genNCE(origTrainFileName, freqFileName, outputFileName, nceOutputSize);
}
