QT       -= gui

TARGET = ./bin/wordTranslationResamplingData.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/wordTranslationResamplingData.cc"

include(cslm-and-cstm-c-config.pri)
