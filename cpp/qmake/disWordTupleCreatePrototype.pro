QT       -= gui

TARGET = ./bin/disWordTupleCreatePrototype.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/disWordTupleCreatePrototype.cc"

include(cslm-and-cstm-c-config.pri)
