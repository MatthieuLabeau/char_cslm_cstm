QT       -= gui

TARGET = ./bin/test_NgramModelNCE2.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/test/test_NgramModelNCE2.cc"

include(cslm-and-cstm-c-config.pri)
