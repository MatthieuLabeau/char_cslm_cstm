QT       -= gui

TARGET = ./bin/mapVoc.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/mapVoc.cc"

include(cslm-and-cstm-c-config.pri)
