QT       -= gui

TARGET = ./bin/growOutPredictionSpace.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/growOutPredictionSpace.cc"

include(cslm-and-cstm-c-config.pri)
