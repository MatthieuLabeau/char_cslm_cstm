QT       -= gui

TARGET = ./bin/ovn2ovnb.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/ovn2ovnb.cc"

include(cslm-and-cstm-c-config.pri)
