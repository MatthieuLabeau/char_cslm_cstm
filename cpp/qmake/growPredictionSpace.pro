QT       -= gui

TARGET = ./bin/growPredictionSpace.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/growPredictionSpace.cc"

include(cslm-and-cstm-c-config.pri)
