## a config file used both for library building and application building ##

# For BLAS, need to indicate include paths and lib paths
# that can be done in Qt Creator by modifying INCLUDEPATH and LIBS
# add one of the 5 following options : MKL (0), ACML_32 (5), ACML_64 (1), CBLAS (2), OPENBLAS (3), MLK_11_3 (4) (new version of MKL)
# I installed ACML in my personal computer, so if you installed other libraries, add other thing
#CONFIG += MKL
CONFIG += ACML_32
#CONFIG += CBLAS
#CONFIG += OPENBLAS
#CONFIG += MLK_11_3

ACML_32 {
    DEFINES += 'PROC=5'
    # depends on where you install ACML
    INCLUDEPATH += "/home/dokhanh/install/acml/gfortran32_mp/include"
    LIBS += -L"/home/dokhanh/install/acml/gfortran32_mp/lib/" -lacml_mp
    QMAKE_CXXFLAGS += -fPIC -O3
}

# include headers. TODO : make a general SOUL.h that can replace all the following and facilitate external uses
tensorpath = "../package/tensor/"
INCLUDEPATH += $${tensorpath}

modulepath = "../package/module/"
INCLUDEPATH += $${modulepath}

modelpath = "../package/model/"
INCLUDEPATH += $${modelpath}

iofilepath = "../package/ioFile/"
INCLUDEPATH += $${iofilepath}

configpath = "../package/config/"
INCLUDEPATH += $${configpath}

resamplingNCEpath = "../package/resamplingNCE/"
INCLUDEPATH += $${resamplingNCEpath}

textpath = "../package/text/"
INCLUDEPATH += $${textpath}
