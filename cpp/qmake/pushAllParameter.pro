QT       -= gui

TARGET = ./bin/pushAllParameter.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/pushAllParameter.cc"

include(cslm-and-cstm-c-config.pri)
