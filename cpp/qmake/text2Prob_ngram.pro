QT       -= gui

TARGET = ./bin/text2Prob_ngram.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/text2Prob_ngram.cc"

include(cslm-and-cstm-c-config.pri)
