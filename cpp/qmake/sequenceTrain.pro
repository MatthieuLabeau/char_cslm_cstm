QT       -= gui

TARGET = ./bin/sequenceTrain.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/sequenceTrain.cc"

include(cslm-and-cstm-c-config.pri)
