QT       -= gui

TARGET = ./bin/demoEmbedding.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/demoEmbedding.cc"

include(cslm-and-cstm-c-config.pri)
