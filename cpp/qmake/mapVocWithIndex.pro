QT       -= gui

TARGET = ./bin/mapVocWithIndex.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/mapVocWithIndex.cc"

include(cslm-and-cstm-c-config.pri)
