QT       -= gui

TARGET = ./bin/readFile.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/test/readFile.cc"

include(cslm-and-cstm-c-config.pri)
