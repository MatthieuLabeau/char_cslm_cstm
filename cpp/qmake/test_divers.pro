QT       -= gui

TARGET = ./bin/test_divers.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/test/test_divers.cc"

include(cslm-and-cstm-c-config.pri)
