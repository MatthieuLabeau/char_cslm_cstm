QT       -= gui

TARGET = ./bin/forwardCoefNorm.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/forwardCoefNorm.cc"

include(cslm-and-cstm-c-config.pri)
