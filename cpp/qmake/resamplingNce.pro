QT       -= gui

TARGET = ./bin/resamplingNce.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/resamplingNce/resamplingNce.cc"

include(cslm-and-cstm-c-config.pri)
