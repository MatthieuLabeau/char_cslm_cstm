QT       -= gui

TARGET = ./bin/resamplingData.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/resamplingData.cc"

include(cslm-and-cstm-c-config.pri)
