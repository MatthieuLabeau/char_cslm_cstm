QT       -= gui

TARGET = ./bin/test.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/test/test.cc"

include(cslm-and-cstm-c-config.pri)
