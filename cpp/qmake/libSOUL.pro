#-------------------------------------------------
#
# Project created by QtCreator 2015-12-21T15:02:54
#
#-------------------------------------------------

QT       -= gui

CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = lib
TARGET = ./lib/SOUL
CONFIG += staticlib
#CONFIG += shared_and_static build_all

include(cslm-and-cstm-c-config.pri)

include(../package/tensor/tensor.pri)

include(../package/module/module.pri)

include(../package/model/model.pri)

include(../package/ioFile/iofile.pri)

include(../package/config/config.pri)

include(../package/resamplingNce/resamplingNCE.pri)

include(../package/text/text.pri)
