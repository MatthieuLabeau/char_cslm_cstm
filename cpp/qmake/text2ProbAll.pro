QT       -= gui

TARGET = ./bin/text2ProbAll.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/text2ProbAll.cc"

include(cslm-and-cstm-c-config.pri)
