QT       -= gui

TARGET = ./bin/train2freq.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/train2freq.cc"

include(cslm-and-cstm-c-config.pri)
