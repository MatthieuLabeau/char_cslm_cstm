QT       -= gui

TARGET = ./bin/resamplingNce2.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/resamplingNce/resamplingNce2.cc"

include(cslm-and-cstm-c-config.pri)
