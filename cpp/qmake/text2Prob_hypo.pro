QT       -= gui

TARGET = ./bin/text2Prob_hypo.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/text2Prob_hypo.cc"

include(cslm-and-cstm-c-config.pri)
