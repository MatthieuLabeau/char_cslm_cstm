QT       -= gui

TARGET = ./bin/countWordGen.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/test/countWordGen.cc"

include(cslm-and-cstm-c-config.pri)
