#-------------------------------------------------
#
# Project created by QtCreator 2015-12-21T15:02:54
#
#-------------------------------------------------

QT       -= gui

CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = subdirs

SUBDIRS += libSOUL sequenceTrain_nbestList createPrototype resamplingData sequenceTrain ovn2ovnb text2ProbAll \
           text2Prob_hypo text2Prob_ngram mapVoc mapVocWithIndex pushAllParameter growPredictionSpace \
           growOutPredictionSpace disWordTupleMono2Model wordTranslationResamplingData addNULL addNULLNCE \
           disWordTupleCreatePrototype demoEmbedding forwardCoefNorm train2freq resamplingNce resamplingNce2 \
           test test_divers readBinaryTrainingFile readFile test_NgramModelNCE2 countWordGen

libSOUL.file = ./libSOUL.pro
sequenceTrain_nbestList.file = ./sequenceTrain_nbestList.pro
createPrototype.file = ./createPrototype.pro
resamplingData.file = ./resamplingData.pro
sequenceTrain.file = ./sequenceTrain.pro
ovn2ovnb.file = ./ovn2ovnb.pro
text2ProbAll.file = ./text2ProbAll.pro
text2Prob_hypo.file = ./text2Prob_hypo.pro
text2Prob_ngram.file = ./text2Prob_ngram.pro
mapVoc.file = ./mapVoc.pro
mapVocWithIndex.file = ./mapVocWithIndex.pro
pushAllParameter.file = ./pushAllParameter.pro
growPredictionSpace.file = ./growPredictionSpace.pro
growOutPredictionSpace.file = ./growOutPredictionSpace.pro
disWordTupleMono2Model.file = ./disWordTupleMono2Model.pro
wordTranslationResamplingData.file = ./wordTranslationResamplingData.pro
addNULL.file = ./addNULL.pro
addNULLNCE.file = ./addNULLNCE.pro
disWordTupleCreatePrototype.file = ./disWordTupleCreatePrototype.pro
demoEmbedding.file = ./demoEmbedding.pro
forwardCoefNorm.file = ./forwardCoefNorm.pro
train2freq.file = ./train2freq.pro
resamplingNce.file = ./resamplingNce.pro
resamplingNce2.file = ./resamplingNce2.pro
test.file = ./test.pro
test_divers.file = ./test_divers.pro
readBinaryTrainingFile.file = ./readBinaryTrainingFile.pro
readFile.file = ./readFile.pro
test_NgramModelNCE2.file = ./test_NgramModelNCE2.pro
countWordGen.file = ./countWordGen.pro
