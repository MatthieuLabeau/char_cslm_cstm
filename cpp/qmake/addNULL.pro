QT       -= gui

TARGET = ./bin/addNULL.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/addNULL.cc"

include(cslm-and-cstm-c-config.pri)
