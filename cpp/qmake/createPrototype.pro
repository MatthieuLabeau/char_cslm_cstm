QT       -= gui

TARGET = ./bin/createPrototype.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/createPrototype.cc"

include(cslm-and-cstm-c-config.pri)
