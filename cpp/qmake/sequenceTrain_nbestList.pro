#-------------------------------------------------
#
# Project created by QtCreator 2015-12-21T19:16:44
#
#-------------------------------------------------

QT       -= gui

TARGET = ./bin/sequenceTrain_nbestList.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/sequenceTrain_nbestList.cc"

include(cslm-and-cstm-c-config.pri)
