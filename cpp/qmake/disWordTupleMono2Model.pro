QT       -= gui

TARGET = ./bin/disWordTupleMono2Model.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/disWordTupleMono2Model.cc"

include(cslm-and-cstm-c-config.pri)
