QT       -= gui

TARGET = ./bin/addNULLNCE.exe
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L./lib -lSOUL

SOURCES += "../package/command/addNULLNCE.cc"

include(cslm-and-cstm-c-config.pri)
